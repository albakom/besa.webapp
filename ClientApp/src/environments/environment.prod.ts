import { IEnviromentSettingsModel } from './ienviroment-settings-model';

export const environment: IEnviromentSettingsModel = {
  production: true,
  apiUrl: 'https://besa-service.albakom.de',
  authUrl: 'https://besa-auth.albakom.de',
};
