export interface IEnviromentSettingsModel {
    production: boolean;
    apiUrl: string;
    authUrl: string;
}
