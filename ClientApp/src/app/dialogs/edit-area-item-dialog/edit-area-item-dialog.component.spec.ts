import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditAreaItemDialogComponent } from './edit-area-item-dialog.component';

describe('EditAreaItemDialogComponent', () => {
  let component: EditAreaItemDialogComponent;
  let fixture: ComponentFixture<EditAreaItemDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditAreaItemDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditAreaItemDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
