import { IEmptySpaceDetailModel } from './../../models/service/building/iempty-space-detail-model';
import { AreaPlanningService } from './../../services/area-planning/area-planning.service';
import { IBuildingDetailModel } from './../../models/service/building/ibuilding-detail-model';
import { IEditAreaItemDialogOptions } from './iedit-area-item-dialog-options';
import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormGroup, FormBuilder } from '@angular/forms';
import { AreaItemType } from '../../models/service/building/area-item-type.enum';
import { IGPSCoordinate } from '../../models/service/other/igpscoordinate';

@Component({
  selector: 'besa-edit-area-item-dialog',
  templateUrl: './edit-area-item-dialog.component.html',
  styleUrls: ['./edit-area-item-dialog.component.scss']
})
export class EditAreaItemDialogComponent implements OnInit {

  settings: FormGroup;
  coordinates: IGPSCoordinate[];

  constructor(
    public dialogRef: MatDialogRef<EditAreaItemDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: IEditAreaItemDialogOptions,
    private _fb: FormBuilder,
    private _service: AreaPlanningService
  ) {
    this.coordinates = new Array();
  }

  ngOnInit() {
    this.initForm();
  }

  initForm() {
    if (this.data.type === AreaItemType.building) {
      this.settings = this._fb.group({
        streetName: [this.data.buildingDetails.streetName, []],
        streetNumber: [this.data.buildingDetails.streetNumber, []],
        householdUnits: [this.data.buildingDetails.householdUnits, []],
        commercialUnits: [this.data.buildingDetails.commercialUnits, []],
        comment: [this.data.buildingDetails.comment, []]
      });
      this.coordinates = this.data.buildingDetails.coordinates;
    } else if (this.data.type === AreaItemType.emptySpace) {
      this.settings = this._fb.group({
        streetName: [this.data.emptySpaceDetails.streetName, []],
        comment: [this.data.emptySpaceDetails.comment, []]
      });
      this.coordinates = this.data.emptySpaceDetails.coordinates;
    }

  }

  addCoordinate(lat: number, long: number) {
    let result: IGPSCoordinate = {
      latitude: lat,
      longitude: long
    };
    this.coordinates.push(result);
  }

  deleteCoordinate(item: IGPSCoordinate) {
    let index = this.coordinates.findIndex(x => x.latitude === item.latitude && x.longitude === item.longitude);
    this.coordinates.splice(index,1);
  }

  cancel() {
    this.dialogRef.close(null);
  }

  finish() {
    if (this.data.type === AreaItemType.building) {
      let result: IBuildingDetailModel = {
        id: this.data.buildingDetails.id,
        streetName: this.settings.value.streetName,
          streetNumber: this.settings.value.streetNumber,
          householdUnits: this.settings.value.householdUnits,
          commercialUnits: this.settings.value.commercialUnits,
          comment: this.settings.value.comment,
          coordinates: this.coordinates,
          fileIds: this.data.buildingDetails.fileIds
      }

      this._service.editBuilding(result).subscribe((result) => {
        if (result) {
          this.dialogRef.close(true);
        } else this.dialogRef.close(false);
      });
    } 
    // else if (this.data.type === AreaItemType.emptySpace) {
    //   let result: IEmptySpaceDetailModel = {
    //     id: this.data.buildingDetails.id,
    //     streetName: this.settings.value.streetName,
    //       comment: this.settings.value.comment,
    //       coordinates: this.coordinates,
    //       fileIds: this.data.buildingDetails.fileIds
    //   }

    //   this._service.e(result).subscribe((result) => {
    //     if (result) {
    //       this.dialogRef.close(true);
    //     } else this.dialogRef.close(false);
    //   });
    // }
  }

}
