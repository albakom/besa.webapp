import { IBuildingDetailModel } from './../../models/service/building/ibuilding-detail-model';
import { AreaItemType } from "../../models/service/building/area-item-type.enum";
import { IEmptySpaceDetailModel } from '../../models/service/building/iempty-space-detail-model';

export interface IEditAreaItemDialogOptions {
    title: string;
    type: AreaItemType;
    buildingDetails?: IBuildingDetailModel;
    emptySpaceDetails?: IEmptySpaceDetailModel;
}
