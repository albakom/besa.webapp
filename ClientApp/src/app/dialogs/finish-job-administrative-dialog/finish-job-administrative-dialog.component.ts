import { fuseAnimations } from '@fuse/animations';
import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { IFinishJobAdministrativeDialogOptions } from './i-finish-job-administrative-dialog-options';
import { FormBasedPage } from '../../pages/base/form-based-page';
import { FormErrorMessage } from '../../models/helper/form-error-message';
import { FinishJobAdministrativeFormErrorMessage } from '../../models/error-messages/finish-job-administrative-form-error-message';
import { FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'besa-finish-job-administrative-dialog',
  templateUrl: './finish-job-administrative-dialog.component.html',
  animations: fuseAnimations,
  styleUrls: ['./finish-job-administrative-dialog.component.scss']
})
export class FinishJobAdministrativeDialogComponent extends FormBasedPage implements OnInit {

  protected getErrorMessages(): FormErrorMessage[] {
    return FinishJobAdministrativeFormErrorMessage;
  }

  constructor(
    public dialogRef: MatDialogRef<FinishJobAdministrativeDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: IFinishJobAdministrativeDialogOptions,
    private _fb: FormBuilder) {
    super();

    super.initForm(
      _fb.group(
        {
          jobId: [data.jobId, [Validators.required]],
          finishedAt: ['', []],
          createNextJobInPipeLine: [true, [Validators.required]],
        }
      )
    );
  }

  onNoClick(): void {
    this.dialogRef.close(null);
  }

  onYesClick(): void {
    this.dialogRef.close(this.form.value);
  }

  ngOnInit() {
  }

}
