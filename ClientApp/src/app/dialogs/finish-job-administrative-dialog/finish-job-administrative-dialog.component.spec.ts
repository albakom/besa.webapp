import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FinishJobAdministrativeDialogComponent } from './finish-job-administrative-dialog.component';

describe('FinishJobAdministrativeDialogComponent', () => {
  let component: FinishJobAdministrativeDialogComponent;
  let fixture: ComponentFixture<FinishJobAdministrativeDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FinishJobAdministrativeDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FinishJobAdministrativeDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
