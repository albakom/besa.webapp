import { finalize } from 'rxjs/operators';
import { MatSelectionList } from '@angular/material';
import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
import { ISelectUpdateTaskObjectsDialogOptions } from './iselect-update-task-objects-dialog-options';
import { UpdateTaskService } from '../../services/update-task/update-task.service';
import { IUpdateTaskObjectOverviewModel } from '../../models/service/tasks/iupdate-task-object-overview-model';
import { fuseAnimations } from '@fuse/animations';

@Component({
  selector: 'besa-select-update-task-objects-dialog',
  templateUrl: './select-update-task-objects-dialog.component.html',
  styleUrls: ['./select-update-task-objects-dialog.component.scss'],
  animations: fuseAnimations,
})
export class SelectUpdateTaskObjectsDialogComponent implements OnInit {

  //#region Properties

  public loadingInProgress: boolean;
  public toogleState: boolean = false;
  public items: IUpdateTaskObjectOverviewModel[];

  @ViewChild("objects")
  public list: MatSelectionList;

  //#endregion

  constructor(public dialogRef: MatDialogRef<SelectUpdateTaskObjectsDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: ISelectUpdateTaskObjectsDialogOptions,
    private _service: UpdateTaskService,
    private _snackBar: MatSnackBar) {

  }

  ngOnInit() {
    this.loadingInProgress = true;
    this._service.getObjectForTask(this.data.type).pipe(
      finalize(() => { this.loadingInProgress = false }))
      .subscribe((result) => {
        this.items = result;
      }, (err) => {
        this._snackBar.open('Objekte konnte nicht geladen werden', 'Fehler', { duration: 4500 });
        console.error(err);
      })
  }

  //#region Methods

  public toogleSelect(list: MatSelectionList): void {
    if (this.toogleState === false) {
      list.selectAll();
      this.toogleState = true;
    }
    else {
      list.deselectAll();
      this.toogleState = false;
    }
  }

  public confirmDialog(): void {
    if (!this.list || this.list.selectedOptions.isEmpty() === true) { return; }

    const objectIds: number[] = this.list.selectedOptions.selected.map(x => <number>x.value);
    this.dialogRef.close(objectIds);
  }

  //#endregion

}
