import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectUpdateTaskObjectsDialogComponent } from './select-update-task-objects-dialog.component';

describe('SelectUpdateTaskObjectsDialogComponent', () => {
  let component: SelectUpdateTaskObjectsDialogComponent;
  let fixture: ComponentFixture<SelectUpdateTaskObjectsDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectUpdateTaskObjectsDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectUpdateTaskObjectsDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
