import { UpdateTasks } from "../../models/service/tasks/update-tasks.enum";

export interface ISelectUpdateTaskObjectsDialogOptions {
    type: UpdateTasks
}