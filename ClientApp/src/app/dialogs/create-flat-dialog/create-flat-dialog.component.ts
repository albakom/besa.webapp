import { ConstructionStageService } from './../../services/construction-stage.service';
import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ICreateFlatDialogOptions } from './icreate-flat-dialog-options';
import { fuseAnimations } from '@fuse/animations';
import { FormBasedPage } from '../../pages/base/form-based-page';
import { OnDestroy } from '@angular/core/src/metadata/lifecycle_hooks';
import { FormBuilder, Validators } from '@angular/forms';
import { FormErrorMessage } from '../../models/helper/form-error-message';
import { FlatCreateErrorMessage } from '../../models/error-messages/flat-create-error-message';
import { IFlatCreateModel } from '../../models/service/flats/iflat-create-model';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'besa-create-flat-dialog',
  templateUrl: './create-flat-dialog.component.html',
  styleUrls: ['./create-flat-dialog.component.scss'],
  animations: fuseAnimations,
})
export class CreateFlatDialogComponent extends FormBasedPage implements OnInit, OnDestroy {

  //#region Properties

  public inProgress: boolean;
  public errorHappend: boolean;

  //#endregion


  constructor(
    _fb: FormBuilder,
    private _service: ConstructionStageService,
    public dialogRef: MatDialogRef<CreateFlatDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: ICreateFlatDialogOptions) {
    super();

    super.initForm(
      _fb.group({
        buildingId: [data.buildingId, [Validators.required]],
        number: ['', [Validators.required, Validators.maxLength(128)]],
        description: ['', [Validators.maxLength(1024)]],
        floor: ['', [Validators.maxLength(128)]],
      }));
  }


  ngOnInit() {
  }

  ngOnDestroy() {
    super.unsubcripeAll();
  }

  //#region Methods

  protected getErrorMessages(): FormErrorMessage[] {
    return FlatCreateErrorMessage;
  }

  onNoClick(): void {
    if (this.inProgress === true) { return; }

    this.dialogRef.close(null);
  }

  onYesClick(): void {
    if (this.form.valid === false) { return; }
    if (this.inProgress === true) { return; }

    const model: IFlatCreateModel = this.form.value;
    this.inProgress = true;
    this.errorHappend = false;

    this._service.creatFlat(model).pipe(finalize(() => {
      this.inProgress = false;
    })).subscribe((result) => {
      this.dialogRef.close(result);
    }, (err) => {
      this.errorHappend = true;
    })
  }

  //#endregion

}

