import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateFlatDialogComponent } from './create-flat-dialog.component';

describe('CreateFlatDialogComponent', () => {
  let component: CreateFlatDialogComponent;
  let fixture: ComponentFixture<CreateFlatDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateFlatDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateFlatDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
