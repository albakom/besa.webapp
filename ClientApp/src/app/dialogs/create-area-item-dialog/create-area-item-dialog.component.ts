import { IEmptySpaceCreateModel } from './../../models/service/building/iempty-space-create-model';
import { IBuildingCreateModel } from './../../models/service/building/ibuilding-create-model';
import { IGPSCoordinate } from './../../models/service/other/igpscoordinate';
import { debounceTime, distinctUntilChanged, switchMap, finalize } from 'rxjs/operators';
import { AreaItemType } from './../../models/service/building/area-item-type.enum';
import { ICreateAreaItemDialogOptions } from './icreate-area-item-dialog-options';
import { animation } from '@angular/animations';
import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AreaPlanningService } from '../../services/area-planning/area-planning.service';
import { MatDialogRef, MAT_DIALOG_DATA, MatExpansionPanel, MatSnackBar } from '@angular/material';
import { SubscriptionAwareComponent } from '../../helper/subscription-aware-component';
import { of } from 'rxjs';
import { UploadFileModel } from '../../models/helper/upload-file-model';
import { UploadComponent } from '../../pages/upload/upload.component';
import { duration } from 'moment';
import { IStreetCreateModel } from '../../models/service/building/istreet-create-model';
import { ICreateAreaItemModel } from '../../models/service/building/icreate-area-item-model';

@Component({
  selector: 'besa-create-area-item-dialog',
  templateUrl: './create-area-item-dialog.component.html',
  styleUrls: ['./create-area-item-dialog.component.scss'],
  animations: fuseAnimations
})
export class CreateAreaItemDialogComponent extends SubscriptionAwareComponent implements OnInit {
  settings: FormGroup;
  coordinates: IGPSCoordinate[];
  public files: UploadFileModel[];

  selectedType: AreaItemType;
  types: AreaItemType[] = [
    AreaItemType.building,
    AreaItemType.street,
    AreaItemType.emptySpace
  ];

  uploadingInProgress: boolean = false;

  @ViewChild('dataPanel')
  public dataPanel: MatExpansionPanel;

  @ViewChild('upload')
  public upload: UploadComponent;

  constructor(
    private _fb: FormBuilder,
    private _service: AreaPlanningService,
    public dialogRef: MatDialogRef<CreateAreaItemDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: ICreateAreaItemDialogOptions,
    private _snackBar: MatSnackBar
  ) {
    super();

    this.files = new Array();

    this.settings = this._fb.group({
      streetName: ['', []],
      streetNumber: ['', []],
      householdUnits: [0, []],
      commercialUnits: [0, []],
      comment: ['', []]
    });

    this.coordinates = new Array();

  }

  ngOnInit() {
  }

  addCoordinate(lat: number, long: number) {
    console.log(lat);
    console.log(long);
    let result: IGPSCoordinate = {
      latitude: lat,
      longitude: long
    };
    this.coordinates.push(result);
  }

  onFileSelect(files: FileList) {
    for (let index = 0; index < files.length; index++) {
      const file = files.item(index);

      const fileModel: UploadFileModel = {
        type: file.type.startsWith('image') === true ? 'image' : 'file',
        file: file,
        name: file.name,
      };

      if (this.files.length == 0 && this.dataPanel) {
        this.dataPanel.open();
      }

      this.files.unshift(fileModel);

      if (fileModel.type === 'image') {
        const reader = new FileReader();
        fileModel.previewLoading = true;

        reader.onload = (event: any) => {
          fileModel.imgAsString = event.target.result;
          fileModel.previewLoading = false;
        };

        reader.readAsDataURL(file);
      }

    }

    console.log(this.files);

  }

  public removeFile(file: UploadFileModel): void {
    const index = this.files.indexOf(file);
    if (index >= 0) {
      this.files.splice(index, 1);
    }
  }

  cancelCreate() {
    this.dialogRef.close(null);
  }

  createItem() {
    if (this.selectedType === AreaItemType.building || this.selectedType === AreaItemType.emptySpace) {
      this.uploadingInProgress = true;

      const uploadRef = this._snackBar.open('Dateien werden hochgeladen', 'Verarbeitung', { duration: -1 });

      this.upload.startUpload(this.files.map((val) => val.file)).pipe(finalize(() => {
        this.uploadingInProgress = false;
      })).subscribe((fileIds: number[]) => {
        //this._snackBar.open('Erstellen wird abgeschlossen', 'In Bearbeitung', {duration: -1});
        uploadRef.dismiss();

        if (this.selectedType === AreaItemType.building) {
          let building: IBuildingCreateModel = {
            streetName: this.settings.value.streetName,
            streetNumber: this.settings.value.streetNumber,
            householdUnits: this.settings.value.householdUnits,
            commercialUnits: this.settings.value.commercialUnits,
            comment: this.settings.value.comment,
            coordinates: this.coordinates,
            fileIds: fileIds
          }

          let result: ICreateAreaItemModel = {
            type: AreaItemType.building,
            building: building
          };

          this.dialogRef.close(result);
        } else if (this.selectedType === AreaItemType.emptySpace) {
          let emptySpace: IEmptySpaceCreateModel = {
            streetName: this.settings.value.streetName,
            comment: this.settings.value.comment,
            coordinates: this.coordinates,
            fileIds: fileIds
          }

          let result: ICreateAreaItemModel = {
            type: AreaItemType.emptySpace,
            emptySpace: emptySpace,
          }
          this.dialogRef.close(result);
        }
      });
    }

    else if (this.selectedType === AreaItemType.street) {
      let street: IStreetCreateModel = {
        streetName: this.settings.value.streetName
      };
      let result: ICreateAreaItemModel = {
        type: AreaItemType.street,
        street: street
      }

      this.dialogRef.close(result);
    }
  }

}
