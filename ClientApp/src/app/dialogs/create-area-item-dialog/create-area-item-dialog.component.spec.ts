import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateAreaItemDialogComponent } from './create-area-item-dialog.component';

describe('CreateAreaItemDialogComponent', () => {
  let component: CreateAreaItemDialogComponent;
  let fixture: ComponentFixture<CreateAreaItemDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateAreaItemDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateAreaItemDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
