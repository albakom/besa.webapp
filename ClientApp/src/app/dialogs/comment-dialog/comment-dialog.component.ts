import { CommentDialogOptions } from './comment-dialog-options';
import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { fuseAnimations } from '@fuse/animations';

@Component({
  selector: 'besa-comment-dialog',
  templateUrl: './comment-dialog.component.html',
  styleUrls: ['./comment-dialog.component.scss'],
  animations: fuseAnimations
})
export class CommentDialogComponent implements OnInit {

  comment: string;

  constructor(
    public dialogRef: MatDialogRef<CommentDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: CommentDialogOptions,
  ) { }

  ngOnInit() {
  }

  cancel() {
    this.dialogRef.close(null);
  }


  finish() {
    this.dialogRef.close(this.comment);
  }

}
