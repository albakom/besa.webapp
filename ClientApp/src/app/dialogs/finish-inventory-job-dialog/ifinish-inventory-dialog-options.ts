export class IFinishInventoryDialogOptions {
    jobId: number;
    comment: string;
}
