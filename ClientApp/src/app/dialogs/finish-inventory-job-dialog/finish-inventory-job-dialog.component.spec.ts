import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FinishInventoryJobDialogComponent } from './finish-inventory-job-dialog.component';

describe('FinishInventoryJobDialogComponent', () => {
  let component: FinishInventoryJobDialogComponent;
  let fixture: ComponentFixture<FinishInventoryJobDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FinishInventoryJobDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FinishInventoryJobDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
