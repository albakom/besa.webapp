import { fuseAnimations } from '@fuse/animations';
import { JobService } from './../../services/job.service';
import { Component, OnInit, OnDestroy, Inject } from '@angular/core';
import { FormBasedPage } from '../../pages/base/form-based-page';
import { FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { IFinishInventoryDialogOptions } from './ifinish-inventory-dialog-options';
import { FormErrorMessage } from '../../models/helper/form-error-message';
import { FinishInventoryJobErrorMessage } from '../../models/error-messages/finish-inventory-job-error-message';
import { UploadFileModel } from '../../models/helper/upload-file-model';
import { IInventoryFinishedJobModel } from '../../models/service/jobs/inventory/iinventory-finished-job-model';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'besa-finish-inventory-job-dialog',
  templateUrl: './finish-inventory-job-dialog.component.html',
  styleUrls: ['./finish-inventory-job-dialog.component.scss'],
  animations: fuseAnimations
})
export class FinishInventoryJobDialogComponent extends FormBasedPage implements OnInit, OnDestroy {

  inProgress: boolean;

  constructor(
    private _fb: FormBuilder,
    private _service: JobService,
    public dialogRef: MatDialogRef<FinishInventoryJobDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: IFinishInventoryDialogOptions
  ) { 
    super();

    super.initForm(
      _fb.group({
        jobId: [data.jobId, [Validators.required]],
        comment: ['', [Validators.maxLength(1024)]],
        problemHappend: _fb.group({
          value: [false, []],
          description: ['', Validators.maxLength(1024)],
        }),
      })
    );
  }

  ngOnInit() {
  }

  ngOnDestroy() {
    super.unsubcripeAll();
  }

  protected getErrorMessages(): FormErrorMessage[] {
    return FinishInventoryJobErrorMessage;
  }

  finishJob() {
    const model: IInventoryFinishedJobModel = this.form.value;

    this._service.finishInventoryJob(model).pipe(finalize(() => {

    })).subscribe((result) => {
      this.dialogRef.close(result);
    }, (err) => {

    })
  }

  cancelFinishJob() {
    if (this.inProgress === true) { return; }

    this.dialogRef.close(null);
  }

}
