import { ICustomerNotInterestedDialogOptions } from './icustomer-not-interested-dialog-options';
import { OnDestroy, Inject } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { FormBasedPage } from '../../pages/base/form-based-page';
import { FormErrorMessage } from '../../models/helper/form-error-message';
import { FormBuilder } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { IContactAfterFinishedUnsuccessfullyModel } from '../../models/service/contacts-after-sale-job/icontact-after-finished-unsuccessfully-model';
import { JobService } from '../../services/job.service';
import { fuseAnimations } from '@fuse/animations';

@Component({
  selector: 'besa-customer-not-interested-dialog',
  templateUrl: './customer-not-interested-dialog.component.html',
  styleUrls: ['./customer-not-interested-dialog.component.scss'],
  animations: fuseAnimations
})
export class CustomerNotInterestedDialogComponent extends FormBasedPage implements OnInit, OnDestroy {

  constructor(
    private _fb: FormBuilder,
    public dialogRef: MatDialogRef<CustomerNotInterestedDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: ICustomerNotInterestedDialogOptions,
    private _service: JobService
  ) {
    super();

    super.initForm(
      _fb.group({
        comment: ['', []],
      })
    );
   }

  ngOnInit() {
  }

  ngOnDestroy(): void {
    super.unsubcripeAll();
  }

  protected getErrorMessages(): FormErrorMessage[] {
    return new Array<FormErrorMessage>();
  }

  cancelDeleteCustomer() {
    this.dialogRef.close(null);
  }

  deleteCustomer() {
    const contact: IContactAfterFinishedUnsuccessfullyModel = {
      jobId: this.data.jobId,
      comment: this.form.value.comment
    }
    
    this._service.finishContactAfterFinished(contact).subscribe((result) => {
      this.dialogRef.close(result);
    })
  }

}
