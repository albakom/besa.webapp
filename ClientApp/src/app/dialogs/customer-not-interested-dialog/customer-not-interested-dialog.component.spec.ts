import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomerNotInterestedDialogComponent } from './customer-not-interested-dialog.component';

describe('CustomerNotInterestedDialogComponent', () => {
  let component: CustomerNotInterestedDialogComponent;
  let fixture: ComponentFixture<CustomerNotInterestedDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomerNotInterestedDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomerNotInterestedDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
