import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WriteCSVFileDialogComponent } from './write-csvfile-dialog.component';

describe('WriteCSVFileDialogComponent', () => {
  let component: WriteCSVFileDialogComponent;
  let fixture: ComponentFixture<WriteCSVFileDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WriteCSVFileDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WriteCSVFileDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
