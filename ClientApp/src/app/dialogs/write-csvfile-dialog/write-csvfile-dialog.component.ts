import { fuseAnimations } from './../../../@fuse/animations/index';
import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { IWriteCSVFileDialogOptions } from './iwrite-csvfile-dialog-options';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';

@Component({
  selector: 'besa-write-csvfile-dialog',
  templateUrl: './write-csvfile-dialog.component.html',
  styleUrls: ['./write-csvfile-dialog.component.scss'],
  animations: fuseAnimations
})
export class WriteCSVFileDialogComponent implements OnInit {

  file: SafeUrl;
  location: string = '';

  constructor(public dialogRef: MatDialogRef<WriteCSVFileDialogComponent>,
     @Inject(MAT_DIALOG_DATA) public data: IWriteCSVFileDialogOptions,
      private _sanitizer: DomSanitizer) { }

  ngOnInit() {
    this.location = 'test2432424.csv';
  }

  onGenerateCSVFile() {
    const dangerousFile = 'data:application/csv;charset=utf-8,%22Column%20One%22%2C%22Column%20Two%22%2C%22Column%20Three%22%0D%0A%22Row%201%20Col%201%22%2C%22Row%201%20Col%202%22%2C%22Row%201%20Col%203%20%22%0D%0A%22Row%202%20Col%201%22%2C%22Row%202%20Col%202%22%2C%22Row%202%20Col%203%22%0D%0A%22Row%203%20Col%201%22%2C%22Row%203%20Col%202%22%2C%22Row%203%20Col%203%22';
    this.file = this._sanitizer.bypassSecurityTrustUrl(dangerousFile);
    console.log(this.file);
  }

}
