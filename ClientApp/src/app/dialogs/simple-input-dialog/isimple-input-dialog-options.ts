export interface ISimpleInputDialogOptions {
    title: string;
    inputValue?: string;
    yesLabel?: string;
    noLabel?: string;
}
