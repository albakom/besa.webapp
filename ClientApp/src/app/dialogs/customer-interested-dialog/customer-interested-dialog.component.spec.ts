import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomerInterestedDialogComponent } from './customer-interested-dialog.component';

describe('CustomerInterestedDialogComponent', () => {
  let component: CustomerInterestedDialogComponent;
  let fixture: ComponentFixture<CustomerInterestedDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomerInterestedDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomerInterestedDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
