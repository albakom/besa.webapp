import { IRequestCustomerConnectionByContactModel } from './../../models/service/contacts-after-sale-job/irequest-customer-connection-by-contact-model';
import { Component, OnInit, OnDestroy, Inject } from '@angular/core';
import { FormBasedPage } from '../../pages/base/form-based-page';
import { FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ICustomerInterestedDialogOptions } from 'app/dialogs/customer-interested-dialog/icustomer-interested-dialog-options';
import { JobService } from '../../services/job.service';
import { FormErrorMessage } from '../../models/helper/form-error-message';
import { CreateCustomerFormMessage } from '../../models/error-messages/create-customer-form-message';
import { fuseAnimations } from '@fuse/animations';

@Component({
  selector: 'besa-customer-interested-dialog',
  templateUrl: './customer-interested-dialog.component.html',
  styleUrls: ['./customer-interested-dialog.component.scss'],
  animations: fuseAnimations
})
export class CustomerInterestedDialogComponent extends FormBasedPage implements OnInit, OnDestroy {

  constructor(
    private _fb: FormBuilder,
    public dialogRef: MatDialogRef<CustomerInterestedDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: ICustomerInterestedDialogOptions,
    private _service: JobService
  ) {
    super();

    super.initForm(
      this._fb.group(
        {
          ductAmount: [null, []],
          subscriberEndpointLength: [null, []],
          connectionAppointment: [null, [Validators.required]],
          subscriberEndpointNearConnectionPoint: this._fb.group({
            value: [true, []],
            description: [null, []],
          }),
          activePointNearSubscriberEndpoint: this._fb.group({
            value: [false, []],
            description: [null, []],
          }),
          powerForActiveEquipment: this._fb.group({
            value: [false, []],
            description: [null, []],
          }),
          activationAsSoonAsPossible: [false, []]
        }
    ));
  }

  ngOnInit() {
  }

  ngOnDestroy() {
    super.unsubcripeAll();
  }

  protected getErrorMessages(): FormErrorMessage[] {
    return CreateCustomerFormMessage;
  }

  public checkIfFormIsValid(): boolean {
    if (!this.form.value.connectionAppointment) return false;
    else true;
  }

  public submitCustomer() {
    const result: IRequestCustomerConnectionByContactModel = {
      jobId: this.data.jobId,
      subscriberEndpointNearConnectionPoint: this.form.value.subscriberEndpointNearConnectionPoint,
      ductAmount: this.form.value.ductAmount,
      subscriberEndpointLength: this.form.value.subscriberEndpointLength,
      activePointNearSubscriberEndpoint: this.form.value.activePointNearSubscriberEndpoint,
      powerForActiveEquipment: this.form.value.powerForActiveEquipment,
      connectionAppointment: this.form.value.connectionAppointment,
      activationAsSoonAsPossible: this.form.value.activationAsSoonAsPossible
    }

    this._service.createRequestFromContactCustomerJob(result).subscribe((result) => {
      this.dialogRef.close(result);
    })
  }

  public cancelCreateCustomer() {
    this.dialogRef.close(null);
  }

}
