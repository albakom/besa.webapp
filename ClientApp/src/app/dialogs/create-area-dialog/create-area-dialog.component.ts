import { IGPSCoordinate } from './../../models/service/other/igpscoordinate';
import { ICreateAreaDialogOptions } from './icreate-area-dialog-options';
import { Component, OnInit, Inject } from '@angular/core';
import { SubscriptionAwareComponent } from '../../helper/subscription-aware-component';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { AreaPlanningService } from '../../services/area-planning/area-planning.service';
import { IBuildingAreaCreateModel } from '../../models/service/building/ibuilding-area-create-model';

@Component({
  selector: 'besa-create-area-dialog',
  templateUrl: './create-area-dialog.component.html',
  styleUrls: ['./create-area-dialog.component.scss']
})
export class CreateAreaDialogComponent extends SubscriptionAwareComponent implements OnInit {

  name: string;

  constructor(
    public dialogRef: MatDialogRef<CreateAreaDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: ICreateAreaDialogOptions,
    private _service: AreaPlanningService
  ) {
    super();

   }

  ngOnInit() {
  }

  cancel() {
    this.dialogRef.close(null);
  }

  finish() {
    this.dialogRef.close(this.name);
  }

}
