import { FilesService } from './../../services/files/files.service';
import { fuseAnimations } from '@fuse/animations';
import { Component, OnInit, OnDestroy, Inject, ViewChild } from '@angular/core';
import { SubscriptionAwareComponent } from '../../helper/subscription-aware-component';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog, MatSnackBar } from '@angular/material';
import { IUploadFileDialogOptions } from './iupload-file-dialog-options';
import { UploadFileModel } from '../../models/helper/upload-file-model';
import { IShowPictureDialogOptions } from '../show-picture-dialog/ishow-picture-dialog-options';
import { ShowPictureDialogComponent } from '../show-picture-dialog/show-picture-dialog.component';
import { finalize } from 'rxjs/operators';
import { UploadComponent } from '../../pages/upload/upload.component';

@Component({
  selector: 'besa-upload-file-dialog',
  templateUrl: './upload-file-dialog.component.html',
  styleUrls: ['./upload-file-dialog.component.scss'],
  animations: fuseAnimations,
})
export class UploadFileDialogComponent extends SubscriptionAwareComponent implements OnInit, OnDestroy {

  //#region Properties

  public files: UploadFileModel[];
  public uploadingInProgress: boolean = false;

  @ViewChild('upload')
  public upload: UploadComponent;

  //#endregion

  constructor(
    private _service: FilesService,
    private _dialog: MatDialog,
    private _snackBar: MatSnackBar,
    public dialogRef: MatDialogRef<UploadFileDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: IUploadFileDialogOptions) {
    super();

    this.files = new Array();
  }


  ngOnInit() {
  }

  ngOnDestroy() {
    super.unsubcripeAll();
  }

  //#region Methods

  public onFileSelect(files: FileList) {

    for (let index = 0; index < files.length; index++) {
      const file = files.item(index);

      const fileModel: UploadFileModel = {
        type: file.type.startsWith('image') === true ? 'image' : 'file',
        file: file,
        name: file.name,
      };

      this.files.unshift(fileModel);

      if (fileModel.type === 'image') {
        const reader = new FileReader();
        fileModel.previewLoading = true;

        reader.onload = (event: any) => {
          fileModel.imgAsString = event.target.result;
          fileModel.previewLoading = false;
        };

        reader.readAsDataURL(file);
      }
    }
  }

  public removeFile(file: UploadFileModel): void {
    const index = this.files.indexOf(file);
    if (index >= 0) {
      this.files.splice(index, 1);
    }
  }

  public showPreview(file: UploadFileModel): void {
    if (!file.imgAsString) { return; }

    const options: IShowPictureDialogOptions = {
      imgAsString: file.imgAsString
    };
    const ref = this._dialog.open<ShowPictureDialogComponent>(ShowPictureDialogComponent, { data: options });
  }

  onNoClick(): void {
    if (this.uploadingInProgress === true) { return; }
    this.dialogRef.close(null);
  }

  onYesClick(): void {
    if (this.files.length == 0) { return; }
    if (this.uploadingInProgress === true) { return; }

    this.uploadingInProgress = true;
    this._snackBar.open('Dateien werden hochgeladen', 'Verarbeitung', { duration: -1 });

    this.upload.startUpload(this.files.map((val) => val.file)).pipe(finalize(() => {
      this._snackBar.dismiss();
      this.uploadingInProgress = false;
    })).subscribe((fileIds) => {
      this.dialogRef.close(fileIds);
    }, (err) => {
      this._snackBar.open('Dateien konnten nicht hochgeladen werden', 'Fehler', { duration: 4500 });
    });
  }

  //#endregion

}
