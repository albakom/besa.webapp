export interface IShowPictureDialogOptions {
    imgAsString: string;
}