import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { IShowPictureDialogOptions } from './ishow-picture-dialog-options';

@Component({
  selector: 'besa-show-picture-dialog',
  templateUrl: './show-picture-dialog.component.html',
  styleUrls: ['./show-picture-dialog.component.scss']
})
export class ShowPictureDialogComponent implements OnInit {

  //#region Properties

  public imgSrc: string;

  //#endregion

  constructor(public dialogRef: MatDialogRef<ShowPictureDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: IShowPictureDialogOptions) {

    this.imgSrc = data.imgAsString;
  }

  ngOnInit() {
  }

}
