import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TakePicturesDialogComponent } from './take-pictures-dialog.component';

describe('TakePicturesDialogComponent', () => {
  let component: TakePicturesDialogComponent;
  let fixture: ComponentFixture<TakePicturesDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TakePicturesDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TakePicturesDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
