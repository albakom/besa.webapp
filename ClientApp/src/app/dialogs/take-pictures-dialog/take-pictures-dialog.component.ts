import { Component, OnInit, ViewChild, ElementRef, AfterViewInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ITakePictureDialogOptions } from './itake-picture-dialog-options';
import { OnDestroy } from '@angular/core/src/metadata/lifecycle_hooks';

@Component({
  selector: 'besa-take-pictures-dialog',
  templateUrl: './take-pictures-dialog.component.html',
  styleUrls: ['./take-pictures-dialog.component.scss']
})
export class TakePicturesDialogComponent implements OnInit, AfterViewInit, OnDestroy {

  //#region Fields

  private _videoHeight: number;
  private _videoWidth: number;
  private _mediaStream: MediaStream;

  //#endregion

  //#region Properties

  public notAllowed: boolean;
  public noCameraFound: boolean;
  public cameraNotReadable: boolean;
  public unkownErrorHapped: boolean;

  public imgDatas: string[];

  @ViewChild('video')
  public videoView: ElementRef;

  @ViewChild('pictureConverter')
  public canvasView: ElementRef;

  //#endregion 

  constructor(public dialogRef: MatDialogRef<TakePicturesDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: ITakePictureDialogOptions) {

    this.imgDatas = new Array();
  }

  ngOnInit() {

  }

  ngAfterViewInit() {
   // this.startCapture();
  }

  ngOnDestroy() {
    //this.stopCapture();
  }

  //#region Methods

  // private startCapture(): void {
  //   const video: HTMLVideoElement = this.videoView.nativeElement;
  //   this.notAllowed = this.noCameraFound = this.cameraNotReadable = this.unkownErrorHapped = false;

  //   navigator.mediaDevices.getUserMedia({ video: true, audio: false })
  //     .then((stream) => {
  //       this._mediaStream = stream;
  //       video.srcObject = stream;
  //       video.play();
  //     })
  //     .catch((err: DOMException) => {
  //       switch (err.name) {
  //         case 'NotAllowedError':
  //           this.notAllowed = true;
  //           break;
  //         case 'NotFoundError':
  //           this.noCameraFound = true;
  //           break;
  //         case 'NotReadableError':
  //           this.cameraNotReadable = true;
  //           break;
  //         default:
  //           this.unkownErrorHapped = true;
  //           break;
  //       }

  //       console.log("error while opening camera device" + err);
  //     });
  // }

  // private stopCapture() {
  //   const video: HTMLVideoElement = this.videoView.nativeElement;
  //   video.srcObject.getVideoTracks().forEach(track => track.stop());

  //   if (this._mediaStream) {
  //     this._mediaStream.stop();
  //   }
  // }

  // public takePicture(): void {
  //   const canvas: HTMLCanvasElement = this.canvasView.nativeElement;
  //   const video: HTMLVideoElement = this.videoView.nativeElement;

  //   if (video.videoHeight > 0 && video.videoWidth > 0) {
  //     const context = canvas.getContext('2d');
  //     canvas.width = video.videoWidth;
  //     canvas.height = video.videoHeight;
  //     context.drawImage(video, 0, 0, video.videoWidth, video.videoHeight);

  //     const data = canvas.toDataURL('image/jpeg');

  //     this.imgDatas.unshift(data);
  //   }
  // }

  //#endregion

}
