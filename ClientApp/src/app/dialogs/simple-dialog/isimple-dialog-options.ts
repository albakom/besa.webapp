export interface ISimpleDialogOptions {
    title: string;
    question: string;
    yesLabel?: string;
    noLabel?: string;
}
