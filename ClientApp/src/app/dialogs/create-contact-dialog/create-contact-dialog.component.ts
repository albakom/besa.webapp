import { IContactInfo } from './../../models/service/icontact-info';
import { Component, OnInit, OnDestroy, Inject } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';
import { FormBasedPage } from '../../pages/base/form-based-page';
import { FormBuilder, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { ICreateContactDialogOptions } from './icreate-contact-dialog-options';
import { finalize } from 'rxjs/operators';
import { FormErrorMessage } from '../../models/helper/form-error-message';
import { ContactCreateErrorMessage } from '../../models/error-messages/contact-create-error-message';
import { CustomerService } from '../../services/customer.service';
import { PersonTypes } from '../../models/service/person-types.enum';
import { customEmailValidator } from '../../customer-form-validators/allow-empty-mail';

@Component({
  selector: 'besa-create-contact-dialog',
  templateUrl: './create-contact-dialog.component.html',
  styleUrls: ['./create-contact-dialog.component.scss'],
  animations: fuseAnimations,
})
export class CreateContactDialogComponent extends FormBasedPage implements OnInit, OnDestroy {

  //#region Properties

  public inProgress: boolean;
  public errorHappend: boolean;
  public genderTypes: PersonTypes[];

  //#endregion

  constructor(
    _fb: FormBuilder,
    private _service: CustomerService,
    public dialogRef: MatDialogRef<CreateContactDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: ICreateContactDialogOptions) {
    super();

    this.genderTypes = [PersonTypes.Male, PersonTypes.Female];

    super.initForm(
      _fb.group({
        type: ['', [Validators.required]],
        companyName: ['', [Validators.maxLength(255)]],
        surname: ['', [Validators.maxLength(64)]],
        lastname: ['', [Validators.required, Validators.maxLength(64)]],
        emailAddress: ['', [customEmailValidator, Validators.maxLength(255)]],
        phone: ['', [Validators.required, Validators.maxLength(32)]],
        address: _fb.group({
          street: ['', [Validators.required, Validators.maxLength(255)]],
          streetNumber: ['', [Validators.required, Validators.maxLength(8)]],
          city: ['', [Validators.required, Validators.maxLength(64)]],
          postalCode: ['', [Validators.required, Validators.maxLength(5), Validators.minLength(4)]],
        }),
      }));

    if (this.data.addressToCopy) {
      this.form.patchValue({
        address: this.data.addressToCopy
      });
    }
  }

  ngOnInit() {
  }

  ngOnDestroy() {
    super.unsubcripeAll();
  }

  //#region Methods

  protected getErrorMessages(): FormErrorMessage[] {
    return ContactCreateErrorMessage;
  }

  onNoClick(): void {
    if (this.inProgress === true) { return; }

    this.dialogRef.close(null);
  }

  onYesClick(): void {
    if (this.form.valid === false) { return; }
    if (this.inProgress === true) { return; }

    const model: IContactInfo = this.form.value;
    this.inProgress = true;
    this.errorHappend = false;

    this._service.createContact(model).pipe(finalize(() => {
      this.inProgress = false;
    })).subscribe((result) => {
      model.id = result;
      this.dialogRef.close(model);
    }, (err) => {
      this.errorHappend = true;
    })
  }

  //#endregion

}

