import { IAddressModel } from './../../models/service/iaddress-model';
export interface ICreateContactDialogOptions {
    addressToCopy?: IAddressModel;
}