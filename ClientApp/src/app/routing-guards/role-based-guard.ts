import { BesaRoles } from './../models/service/besa-roles.enum';
import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { LoginService } from '../services/login.service';
import { map } from 'rxjs/operators';

export abstract class RoleBasedGuard implements CanActivate {

    //#region Fields

    private _needRole: BesaRoles;

    //#endregion

    constructor(
        private _loginService: LoginService,
        private _router: Router,
        role: BesaRoles) {
        this._needRole = role;

    }

    private checkResult(role: BesaRoles): boolean {
        const result = (role & this._needRole) == this._needRole;

        if(result === false) {
            this._router.navigate(['/403']);
        }

        return result;
    }

    canActivate(
        next: ActivatedRouteSnapshot,
        state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

        if (this._loginService.currentUserInfo.value) {
            return this.checkResult(this._loginService.currentUserInfo.value.roles);
        } else {
            return this._loginService.checkRoles().pipe(map((result) => this.checkResult(this._loginService.currentUserInfo.value.roles)));
        }
    }
}
