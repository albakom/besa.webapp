import { TestBed, async, inject } from '@angular/core/testing';

import { CanNavigateToProjectManangerAreaGuard } from './can-navigate-to-project-mananger-area.guard';

describe('CanNavigateToProjectManangerAreaGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CanNavigateToProjectManangerAreaGuard]
    });
  });

  it('should ...', inject([CanNavigateToProjectManangerAreaGuard], (guard: CanNavigateToProjectManangerAreaGuard) => {
    expect(guard).toBeTruthy();
  }));
});
