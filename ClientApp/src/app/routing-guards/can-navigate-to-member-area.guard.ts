import { MemberStatus } from './../models/service/member-status.enum';
import { OAuthService } from 'angular-oauth2-oidc';
import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { LoginService } from '../services/login.service';
import { WebServiceClientService } from '../services/web-service-client.service';
import { map } from 'rxjs/operators';

@Injectable()
export class CanNavigateToMemberAreaGuard implements CanActivate {

  constructor(
    private _webClient: WebServiceClientService,
    private _loginService: LoginService,
    private _oAuthService: OAuthService,
    private _router: Router) {
  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

    const preResult = this._oAuthService.hasValidAccessToken();
    if (preResult === true) {

      return this._webClient.checkIfUserIsAlreadyMember().pipe(map((state) => {
        let canAccess = false;
        if (state === MemberStatus.IsMember) {
          canAccess = true;
        } else if(state === MemberStatus.NoMember) {
          this._router.navigate(['/requstAccess']);
        } else if(state === MemberStatus.RequestedOutstanding) {
          this._router.navigate(['requstAccessOutStanding'])
        }

        return canAccess;
      }));
      
    } else {
      this._loginService.redirectUri = state.url;
      this._router.navigate(['/login']);
      return false;
    }
  }
}
