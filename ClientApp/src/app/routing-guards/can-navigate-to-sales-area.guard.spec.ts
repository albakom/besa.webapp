import { TestBed, async, inject } from '@angular/core/testing';

import { CanNavigateToSalesAreaGuard } from './can-navigate-to-sales-area.guard';

describe('CanNavigateToSalesAreaGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CanNavigateToSalesAreaGuard]
    });
  });

  it('should ...', inject([CanNavigateToSalesAreaGuard], (guard: CanNavigateToSalesAreaGuard) => {
    expect(guard).toBeTruthy();
  }));
});
