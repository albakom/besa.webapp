import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { RoleBasedGuard } from './role-based-guard';
import { BesaRoles } from '../models/service/besa-roles.enum';
import { LoginService } from '../services/login.service';

@Injectable()
export class CanNavigateToInventoryAreaGuard extends RoleBasedGuard {

  constructor(
    router: Router,
    loginService: LoginService) {
    super(loginService, router, BesaRoles.Inventory)
  }

}
