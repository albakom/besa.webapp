import { TestBed, async, inject } from '@angular/core/testing';

import { CanNavigateToInventoryAreaGuard } from './can-navigate-to-inventory-area.guard';

describe('CanNavigateToInventoryAreaGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CanNavigateToInventoryAreaGuard]
    });
  });

  it('should ...', inject([CanNavigateToInventoryAreaGuard], (guard: CanNavigateToInventoryAreaGuard) => {
    expect(guard).toBeTruthy();
  }));
});
