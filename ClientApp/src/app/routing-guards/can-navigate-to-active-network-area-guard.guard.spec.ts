import { TestBed, async, inject } from '@angular/core/testing';

import { CanNavigateToActiveNetworkAreaGuardGuard } from './can-navigate-to-active-network-area-guard.guard';

describe('CanNavigateToActiveNetworkAreaGuardGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CanNavigateToActiveNetworkAreaGuardGuard]
    });
  });

  it('should ...', inject([CanNavigateToActiveNetworkAreaGuardGuard], (guard: CanNavigateToActiveNetworkAreaGuardGuard) => {
    expect(guard).toBeTruthy();
  }));
});
