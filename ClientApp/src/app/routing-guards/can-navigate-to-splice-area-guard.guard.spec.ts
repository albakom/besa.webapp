import { TestBed, async, inject } from '@angular/core/testing';

import { CanNavigateToSpliceAreaGuardGuard } from './can-navigate-to-splice-area-guard.guard';

describe('CanNavigateToSpliceAreaGuardGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CanNavigateToSpliceAreaGuardGuard]
    });
  });

  it('should ...', inject([CanNavigateToSpliceAreaGuardGuard], (guard: CanNavigateToSpliceAreaGuardGuard) => {
    expect(guard).toBeTruthy();
  }));
});
