import { TestBed, async, inject } from '@angular/core/testing';

import { CanNavigateToMemberAreaGuard } from './can-navigate-to-member-area.guard';

describe('CanNavigateToMemberAreaGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CanNavigateToMemberAreaGuard]
    });
  });

  it('should ...', inject([CanNavigateToMemberAreaGuard], (guard: CanNavigateToMemberAreaGuard) => {
    expect(guard).toBeTruthy();
  }));
});
