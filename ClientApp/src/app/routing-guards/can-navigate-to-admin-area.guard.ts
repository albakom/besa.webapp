import { Router } from '@angular/router';
import { BesaRoles } from './../models/service/besa-roles.enum';
import { Injectable } from '@angular/core';
import { RoleBasedGuard } from './role-based-guard';
import { LoginService } from '../services/login.service';

@Injectable()
export class CanNavigateToAdminAreaGuard extends RoleBasedGuard {

  constructor(
    router: Router,
    loginService: LoginService) {
    super(loginService, router, BesaRoles.Admin)

  }

}
