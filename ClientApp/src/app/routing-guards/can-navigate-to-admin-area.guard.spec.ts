import { TestBed, async, inject } from '@angular/core/testing';

import { CanNavigateToAdminAreaGuard } from './can-navigate-to-admin-area.guard';

describe('CanNavigateToAdminAreaGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CanNavigateToAdminAreaGuard]
    });
  });

  it('should ...', inject([CanNavigateToAdminAreaGuard], (guard: CanNavigateToAdminAreaGuard) => {
    expect(guard).toBeTruthy();
  }));
});
