import { AreaPlanningEmptySpaceDetailPageComponent } from './pages/area-planning-empty-space-detail-page/area-planning-empty-space-detail-page.component';
import { AreaPlanningBuildingDetailPageComponent } from './pages/area-planning-building-detail-page/area-planning-building-detail-page.component';
import { UpdatePlanningDataDetailsPageComponent } from './pages/update-planning-data-details-page/update-planning-data-details-page.component';
import { ProtocolPageComponent } from './pages/protocol-page/protocol-page.component';
import { FinishPrepareBranchableJobPageComponent } from './pages/finish-prepare-branchable-job-page/finish-prepare-branchable-job-page.component';
import { FinishActivationJobPageComponent } from './pages/finish-activation-job-page/finish-activation-job-page.component';
import { InventoryOverviewPageComponent } from './pages/inventory-overview-page/inventory-overview-page.component';
import { SpliceBranchableJobDetailPageComponent } from './pages/splice-branchable-job-detail-page/splice-branchable-job-detail-page.component';
import { SpliceBranchableJobOverviewPageComponent } from './pages/splice-branchable-job-overview-page/splice-branchable-job-overview-page.component';
import { ConnectBuildingProcedureDetailPageComponent } from './pages/connect-building-procedure-detail-page/connect-building-procedure-detail-page.component';
import { UserDetailPageComponent } from './pages/user-detail-page/user-detail-page.component';
import { ChangeLogPageComponent } from './pages/change-log-page/change-log-page.component';
import { JobsToAcknowledgePageComponent } from './pages/jobs-to-acknowledge-page/jobs-to-acknowledge-page.component';
import { FinishBranchOffJobPageComponent } from './pages/finish-branch-off-job-page/finish-branch-off-job-page.component';
import { CreateCustomerPageComponent } from './pages/create-customer-page/create-customer-page.component';
import { ArticleEditPageComponent } from './pages/article-edit-page/article-edit-page.component';
import { ArticleOverviewPageComponent } from './pages/article-overview-page/article-overview-page.component';
import { CollectionJobDetailsPageComponent } from './pages/collection-job-details-page/collection-job-details-page.component';
import { CanNavigateToProjectManangerAreaGuard } from './routing-guards/can-navigate-to-project-mananger-area.guard';
import { LogoutPageComponent } from './pages/logout-page/logout-page.component';
import { AnotherPageComponent } from './pages/another-page/another-page.component';
import { NotFoundComponent } from './pages/not-found/not-found.component';
import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomePageComponent } from './pages/home-page/home-page.component';
import { DashboardPageComponent } from './pages/dashboard-page/dashboard-page.component';
import { ErrorPageComponent } from './pages/error-page/error-page.component';
import { CanNavigateToMemberAreaGuard } from './routing-guards/can-navigate-to-member-area.guard';
import { LoginPageComponent } from './pages/login-page/login-page.component';
import { UserPageComponent } from './pages/user-page/user-page.component';
import { CanNavigateToAdminAreaGuard } from './routing-guards/can-navigate-to-admin-area.guard';
import { AccessDeniedComponent } from './pages/access-denied/access-denied.component';
import { RequestAcessPageComponent } from './pages/request-acess-page/request-acess-page.component';
import { RequestAcessOutstandingPageComponent } from './pages/request-acess-outstanding-page/request-acess-outstanding-page.component';
import { UserAccessRequestOverviewPageComponent } from './pages/user-access-request-overview-page/user-access-request-overview-page.component';
import { CompanyOverviewPageComponent } from './pages/company-overview-page/company-overview-page.component';
import { ConstructionStageOverviewPageComponent } from './pages/construction-stage-overview-page/construction-stage-overview-page.component';
import { CreateConstructionStageComponent } from './pages/create-construction-stage/create-construction-stage.component';
import { GrantUserAccessPageComponent } from './pages/grant-user-access-page/grant-user-access-page.component';
import { CompanyEditPageComponent } from './pages/company-edit-page/company-edit-page.component';
import { CompanyCreatePageComponent } from './pages/company-create-page/company-create-page.component';
import { CreateNewTaskPageComponent } from './pages/create-new-task-page/create-new-task-page.component';
import { CilvilWorkOverviewPageComponent } from './pages/cilvil-work-overview-page/cilvil-work-overview-page.component';
import { CilvilWorkDetailPageComponent } from './pages/cilvil-work-detail-page/cilvil-work-detail-page.component';
import { BindTaskOverviewPageComponent } from './pages/bind-task-overview-page/bind-task-overview-page.component';
import { BranchOffJobDetailsPageComponent } from './pages/branch-off-job-details-page/branch-off-job-details-page.component';
import { UserOverviewPageComponent } from './pages/user-overview-page/user-overview-page.component';
import { UserEditPageComponent } from './pages/user-edit-page/user-edit-page.component';
import { ArticleCreatePageComponent } from './pages/article-create-page/article-create-page.component';
import { CanNavigateToSalesAreaGuard } from './routing-guards/can-navigate-to-sales-area.guard';
import { UploadComponent } from './pages/upload/upload.component';
import { FilesComponent } from './pages/files/files.component';
import { FileDetailComponent } from './pages/file-detail/file-detail.component';
import { CustomerRequestOverviewPageComponent } from './pages/customer-request-overview-page/customer-request-overview-page.component';
import { ConnectBuildingJobDetailsPageComponent } from './pages/connect-building-job-details-page/connect-building-job-details-page.component';
import { ContactOverviewPageComponent } from './pages/contact-overview-page/contact-overview-page.component';
import { CustomerRequestDetailsPageComponent } from './pages/customer-request-details-page/customer-request-details-page.component';
import { DocumentationPageComponent } from './pages/documentation-page/documentation-page.component';
import { FinishConnectBuildingJobPageComponent } from './pages/finish-connect-building-job-page/finish-connect-building-job-page.component';
import { InjectJobOverviewPageComponent } from './pages/inject-job-overview-page/inject-job-overview-page.component';
import { InjectConstructionStageDetailPageComponent } from './pages/inject-construction-stage-detail-page/inject-construction-stage-detail-page.component';
import { InjectJobDetailsPageComponent } from './pages/inject-job-details-page/inject-job-details-page.component';
import { ContactEditPageComponent } from './pages/contact-edit-page/contact-edit-page.component';
import { ContactDetailPageComponent } from './pages/contact-detail-page/contact-detail-page.component';
import { FinishInjectJobPageComponent } from './pages/finish-inject-job-page/finish-inject-job-page.component';
import { ImportContactsPageComponent } from './pages/import-contacts-page/import-contacts-page.component';
import { CompanyDetailPageComponent } from './pages/company-detail-page/company-detail-page.component';
import { ImportProceduresPageComponent } from './pages/import-procedures-page/import-procedures-page.component';
import { ProcedureOverviewPageComponent } from './pages/procedure-overview-page/procedure-overview-page.component';
import { ConnectCustomerProcedureDetailPageComponent } from './pages/connect-customer-procedure-detail-page/connect-customer-procedure-detail-page.component';
import { CanNavigateToInventoryAreaGuard } from './routing-guards/can-navigate-to-inventory-area.guard';
import { InventoryDetailPageComponent } from './pages/inventory-detail-page/inventory-detail-page.component';
import { CanNavigateToSpliceAreaGuardGuard } from './routing-guards/can-navigate-to-splice-area-guard.guard';
import { SpliceBranchableConstructionStageDetailPageComponent } from './pages/splice-branchable-construction-stage-detail-page/splice-branchable-construction-stage-detail-page.component';
import { PrepareBranchableForSpliceJobOverviewPageComponent } from './pages/prepare-branchable-for-splice-job-overview-page/prepare-branchable-for-splice-job-overview-page.component';
import { PrepareBranchableForSpliceJobDetailPageComponent } from './pages/prepare-branchable-for-splice-job-detail-page/prepare-branchable-for-splice-job-detail-page.component';
import { PrepareBranchableConstructionStageDetailPageComponent } from './pages/prepare-branchable-construction-stage-detail-page/prepare-branchable-construction-stage-detail-page.component';
import { SpliceOdfjobOverviewPageComponent } from './pages/splice-odfjob-overview-page/splice-odfjob-overview-page.component';
import { SpliceOdfjobDetailPageComponent } from './pages/splice-odfjob-detail-page/splice-odfjob-detail-page.component';
import { ActivationJobOverviewPageComponent } from './pages/activation-job-overview-page/activation-job-overview-page.component';
import { CanNavigateToActiveNetworkAreaGuard } from './routing-guards/can-navigate-to-active-network-area-guard.guard';
import { ActivationJobDetailsPageComponent } from './pages/activation-job-details-page/activation-job-details-page.component';
import { FinishSpliceBranchableJobPageComponent } from './pages/finish-splice-branchable-job-page/finish-splice-branchable-job-page.component';
import { FinishSpliceOdfjobPageComponent } from './pages/finish-splice-odfjob-page/finish-splice-odfjob-page.component';
import { UpdatePlanningDataComponent } from './pages/update-planning-data/update-planning-data.component';
import { ConvertJobPageComponent } from './pages/convert-job-page/convert-job-page.component';
import { ConstructionStageUpdatePageComponent } from './pages/construction-stage-update-page/construction-stage-update-page.component';
import { TaskOverviewPageComponent } from './pages/task-overview-page/task-overview-page.component';
import { MailComponent } from './pages/messages-page/mail.component';
import { ContactAfterSalesJobDetailPageComponent } from './pages/contact-after-sales-job-detail-page/contact-after-sales-job-detail-page.component';
import { ContactAfterSalesJobOverviewPageComponent } from './pages/contact-after-sales-job-overview-page/contact-after-sales-job-overview-page.component';
import { AreaPlanningPageComponent } from './pages/area-planning-page/area-planning-page.component';

const routes: Routes =
    [
        { path: '', pathMatch: 'full', redirectTo: '/home' },
        { path: 'home', component: HomePageComponent, },
        { path: 'logoutSuccess', pathMatch: 'full', redirectTo: '/logout' },
        { path: 'logout', component: LogoutPageComponent, },
        { path: 'login', component: LoginPageComponent, },
        { path: 'loginSuccess', pathMatch: 'full', redirectTo: '/login' },
        { path: 'requstAccessOutStanding', component: RequestAcessOutstandingPageComponent, },
        { path: 'requstAccess', component: RequestAcessPageComponent, },
        {
            path: 'member', canActivate: [CanNavigateToMemberAreaGuard],
            children:
                [
                    { path: '', pathMatch: 'full', redirectTo: '/member/dashboard' },
                    { path: 'dashboard', component: DashboardPageComponent },
                    { path: 'documentation', component: DocumentationPageComponent },
                    { path: 'changelog', component: ChangeLogPageComponent },
                    { path: 'anotherBlub', component: AnotherPageComponent },
                    { path: 'user-request-overview', component: UserAccessRequestOverviewPageComponent, canActivate: [CanNavigateToAdminAreaGuard] },
                    { path: 'company-overview', component: CompanyOverviewPageComponent, canActivate: [CanNavigateToAdminAreaGuard] },
                    { path: 'company-edit/:id', component: CompanyEditPageComponent, canActivate: [CanNavigateToAdminAreaGuard] },
                    { path: 'company-create', component: CompanyCreatePageComponent, canActivate: [CanNavigateToAdminAreaGuard] },
                    { path: 'company-details/:id', component: CompanyDetailPageComponent, canActivate: [CanNavigateToAdminAreaGuard] },
                    { path: 'users-overview', component: UserOverviewPageComponent, canActivate: [CanNavigateToAdminAreaGuard] },
                    { path: 'users-edit/:id', component: UserEditPageComponent, canActivate: [CanNavigateToAdminAreaGuard] },
                    { path: 'users-details/:id', component: UserDetailPageComponent, canActivate: [CanNavigateToAdminAreaGuard] },
                    { path: 'article-overview', component: ArticleOverviewPageComponent, canActivate: [CanNavigateToAdminAreaGuard] },
                    { path: 'article-edit/:id', component: ArticleEditPageComponent, canActivate: [CanNavigateToAdminAreaGuard] },
                    { path: 'article-create', component: ArticleCreatePageComponent, canActivate: [CanNavigateToAdminAreaGuard] },
                    { path: 'upload', component: UploadComponent, canActivate: [CanNavigateToAdminAreaGuard] },
                    { path: 'grant-user-acess', component: GrantUserAccessPageComponent, canActivate: [CanNavigateToAdminAreaGuard] },
                    { path: 'construction-stage-overview', component: ConstructionStageOverviewPageComponent, canActivate: [CanNavigateToProjectManangerAreaGuard] },
                    { path: 'construction-stage-update/:id', component: ConstructionStageUpdatePageComponent, canActivate: [CanNavigateToProjectManangerAreaGuard] },
                    { path: 'new-construciton-stage', component: CreateConstructionStageComponent, canActivate: [CanNavigateToProjectManangerAreaGuard] },
                    { path: 'create-new-task', component: CreateNewTaskPageComponent, canActivate: [CanNavigateToProjectManangerAreaGuard] },
                    { path: 'civil-work-overview', component: CilvilWorkOverviewPageComponent },
                    { path: 'civil-work-construction-stage-detail/:id', component: CilvilWorkDetailPageComponent },
                    { path: 'inject-job-overview', component: InjectJobOverviewPageComponent },
                    { path: 'inject-job-construction-stage-detail/:id', component: InjectConstructionStageDetailPageComponent },
                    { path: 'bind-tasks', component: BindTaskOverviewPageComponent, canActivate: [CanNavigateToProjectManangerAreaGuard] },
                    { path: 'task-overview', component: TaskOverviewPageComponent, canActivate: [CanNavigateToProjectManangerAreaGuard] },
                    { path: 'jobs-to-acknowledge', component: JobsToAcknowledgePageComponent, canActivate: [CanNavigateToProjectManangerAreaGuard] },
                    { path: 'customer-request-overview', component: CustomerRequestOverviewPageComponent, canActivate: [CanNavigateToProjectManangerAreaGuard] },
                    { path: 'customer-request-details/:id', component: CustomerRequestDetailsPageComponent, canActivate: [CanNavigateToProjectManangerAreaGuard] },
                    { path: 'branch-off-job/:id', component: BranchOffJobDetailsPageComponent },
                    { path: 'connection-building-job/:id', component: ConnectBuildingJobDetailsPageComponent },
                    { path: 'inject-job/:id', component: InjectJobDetailsPageComponent },
                    { path: 'collection-job/:id', component: CollectionJobDetailsPageComponent },
                    { path: 'create-customer', component: CreateCustomerPageComponent, canActivate: [CanNavigateToSalesAreaGuard] },
                    { path: 'finish-branchoff-job/:id', component: FinishBranchOffJobPageComponent },
                    { path: 'finish-connect-building-job/:id', component: FinishConnectBuildingJobPageComponent },
                    { path: 'finish-inejct-job/:id', component: FinishInjectJobPageComponent },
                    { path: 'finish-activation-job/:id', component: FinishActivationJobPageComponent },
                    { path: 'finish-splice-branchable-job/:id', component: FinishSpliceBranchableJobPageComponent },
                    { path: 'finish-prepare-branchable-job/:id', component: FinishPrepareBranchableJobPageComponent },
                    { path: 'finish-splice-odf-job/:id', component: FinishSpliceOdfjobPageComponent },
                    { path: 'files', component: FilesComponent, canActivate: [CanNavigateToProjectManangerAreaGuard] },
                    { path: 'file-detail/:id', component: FileDetailComponent, canActivate: [CanNavigateToProjectManangerAreaGuard] },
                    { path: 'contact-overview', component: ContactOverviewPageComponent, canActivate: [CanNavigateToAdminAreaGuard] },
                    { path: 'contact-edit/:id', component: ContactEditPageComponent, canActivate: [CanNavigateToAdminAreaGuard] },
                    { path: 'contact-details/:id', component: ContactDetailPageComponent, canActivate: [CanNavigateToAdminAreaGuard] },
                    { path: 'contact-import', component: ImportContactsPageComponent, canActivate: [CanNavigateToSalesAreaGuard] },
                    { path: 'procedure-import', component: ImportProceduresPageComponent, canActivate: [CanNavigateToSalesAreaGuard] },
                    { path: 'procedure-overview', component: ProcedureOverviewPageComponent, canActivate: [CanNavigateToSalesAreaGuard] },
                    { path: 'connect-building-procedure-details/:id', component: ConnectBuildingProcedureDetailPageComponent, canActivate: [CanNavigateToSalesAreaGuard] },
                    { path: 'connect-customer-procedure-details/:id', component: ConnectCustomerProcedureDetailPageComponent, canActivate: [CanNavigateToSalesAreaGuard] },
                    { path: 'inventory-overview', component: InventoryOverviewPageComponent, canActivate: [CanNavigateToInventoryAreaGuard] },
                    { path: 'inventory-detail/:id', component: InventoryDetailPageComponent, canActivate: [CanNavigateToInventoryAreaGuard] },
                    { path: 'splice-customer-overview', component: SpliceBranchableJobOverviewPageComponent, canActivate: [CanNavigateToSpliceAreaGuardGuard] },
                    { path: 'splice-branchable-job-construction-stage-detail/:id', component: SpliceBranchableConstructionStageDetailPageComponent, canActivate: [CanNavigateToSpliceAreaGuardGuard] },
                    { path: 'splice-branchable-job/:id', component: SpliceBranchableJobDetailPageComponent, canActivate: [CanNavigateToSpliceAreaGuardGuard] },
                    { path: 'prepare-branchable-overview', component: PrepareBranchableForSpliceJobOverviewPageComponent, canActivate: [CanNavigateToSpliceAreaGuardGuard] },
                    { path: 'prepare-branchable-job-construction-stage-detail/:id', component: PrepareBranchableConstructionStageDetailPageComponent, canActivate: [CanNavigateToSpliceAreaGuardGuard] },
                    { path: 'prepare-branchable-job/:id', component: PrepareBranchableForSpliceJobDetailPageComponent, canActivate: [CanNavigateToSpliceAreaGuardGuard] },
                    { path: 'splice-odf-overview', component: SpliceOdfjobOverviewPageComponent, canActivate: [CanNavigateToSpliceAreaGuardGuard] },
                    { path: 'splice-odf-details/:id', component: SpliceOdfjobDetailPageComponent, canActivate: [CanNavigateToSpliceAreaGuardGuard] },
                    { path: 'activation-job-overview', component: ActivationJobOverviewPageComponent, canActivate: [CanNavigateToActiveNetworkAreaGuard] },
                    { path: 'activation-job-details/:id', component: ActivationJobDetailsPageComponent, canActivate: [CanNavigateToActiveNetworkAreaGuard] },
                    { path: 'update-planning-data', component: UpdatePlanningDataComponent, canActivate: [CanNavigateToProjectManangerAreaGuard] },
                    { path: 'update-planning-data-details/:id', component: UpdatePlanningDataDetailsPageComponent, canActivate: [CanNavigateToProjectManangerAreaGuard] },
                    { path: 'convert-jobs', component: ConvertJobPageComponent, canActivate: [CanNavigateToProjectManangerAreaGuard] },
                    { path: 'protocol', component: ProtocolPageComponent, canActivate: [CanNavigateToProjectManangerAreaGuard] },
                    { path: 'messages', component: MailComponent },
                    { path: 'contact-after-sales-job-overview', component: ContactAfterSalesJobOverviewPageComponent, canActivate: [CanNavigateToSalesAreaGuard, CanNavigateToProjectManangerAreaGuard]},
                    { path: 'contact-after-sales-job-details/:id', component: ContactAfterSalesJobDetailPageComponent, canActivate: [CanNavigateToSalesAreaGuard, CanNavigateToProjectManangerAreaGuard]},
                    { path: 'area-planning', component: AreaPlanningPageComponent, canActivate: [CanNavigateToProjectManangerAreaGuard]},
                    { path: 'area-planning-building-detail/:id', component: AreaPlanningBuildingDetailPageComponent, canActivate: [CanNavigateToProjectManangerAreaGuard] },
                    { path: 'area-planning-emptyspace-detail/:id', component: AreaPlanningEmptySpaceDetailPageComponent, canActivate: [CanNavigateToProjectManangerAreaGuard]}
                ]
        },
        { path: 'error', component: ErrorPageComponent },
        { path: '404', component: NotFoundComponent },
        { path: '403', component: AccessDeniedComponent },
        { path: '**', pathMatch: 'full', redirectTo: '/404' },
    ];

@NgModule({
    imports: [RouterModule.forRoot(routes, {
        //  initialNavigation: true
    })],
    exports: [RouterModule]
})
export class AppRoutingModule { }
