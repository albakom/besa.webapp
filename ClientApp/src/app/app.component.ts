import { MemberStatus } from './models/service/member-status.enum';
import { Component, Optional, Inject, OnInit, OnDestroy } from '@angular/core';
import { OAuthService, JwksValidationHandler, OAuthModuleConfig } from 'angular-oauth2-oidc';
import { authConfig } from './helper/auth-config';

import 'hammerjs';
import { adminNavItems, airInjectorNavItems, projectManagerNavItems, civilWorkItems, salesItems, inventoryItems, spliceItems, activeNetworkItems, generellMenuItems } from './navigation/fuse-navigation-model';
import { LoginService } from './services/login.service';
import { Router } from '@angular/router/';
import { WebServiceClientService } from './services/web-service-client.service';
import { BesaRoles } from './models/service/besa-roles.enum';

import { FuseConfigService } from '@fuse/services/config.service';
import { FuseNavigationService } from '@fuse/components/navigation/navigation.service';
import { FuseSidebarService } from '@fuse/components/sidebar/sidebar.service';
import { FuseSplashScreenService } from '@fuse/services/splash-screen.service';

import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { FuseNavigation, FuseConfig } from '@fuse/types';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {

  //#region Fields

  private _unsubscribeAll: Subject<any>;
  private _discoveryDocumentLoaded = false;
  private _roleToNavItems: { [role: number]: FuseNavigation[] };

  //#endregion

  //#region Properties

  public navigation: FuseNavigation[];
  public fuseConfig: FuseConfig;

  //#endregion

  constructor(
    private _fuseConfigService: FuseConfigService,
    private _fuseNavigationService: FuseNavigationService,
    private _fuseSidebarService: FuseSidebarService,
    private _fuseSplashScreenService: FuseSplashScreenService,
    private _router: Router,
    private _loginSerivce: LoginService,
    private oauthService: OAuthService,
    private _webClient: WebServiceClientService,
    @Inject('IS_IN_PRODUCTION') private _isProduction: boolean,
    @Inject('API_URL') _apiUrl: string,
    @Inject('AUTH_URL') private _authUrl: string,
    @Optional() private _moduleConfig: OAuthModuleConfig) {

    // Get default navigation
    this.navigation = generellMenuItems;

    // Register the navigation to the service
    this._fuseNavigationService.register('main', this.navigation);

    // Set the main navigation as our current navigation
    this._fuseNavigationService.setCurrentNavigation('main');

    // Set the private defaults
    this._unsubscribeAll = new Subject();

    this.configureOAuthService();

    if (_moduleConfig && _moduleConfig.resourceServer) {
      if (!_moduleConfig.resourceServer.allowedUrls) {
        _moduleConfig.resourceServer.allowedUrls = new Array();
      }

      _moduleConfig.resourceServer.allowedUrls.push(_apiUrl);
    }

    this._roleToNavItems = {};
    this._roleToNavItems[BesaRoles.Admin] = adminNavItems;
    this._roleToNavItems[BesaRoles.AirInjector] = airInjectorNavItems;
    this._roleToNavItems[BesaRoles.ProjectManager] = projectManagerNavItems;
    this._roleToNavItems[BesaRoles.CivilWorker] = civilWorkItems;
    this._roleToNavItems[BesaRoles.Sales] = salesItems;
    this._roleToNavItems[BesaRoles.Inventory] = inventoryItems;
    this._roleToNavItems[BesaRoles.Splice] = spliceItems;
    this._roleToNavItems[BesaRoles.ActiveNetwork] = activeNetworkItems;

    this._loginSerivce.currentUserInfo.subscribe((result) => {
      if (result) {
        this.buildMenu(result.roles);
        this.getIdentityInfo();
      }
    })
  }

  ngOnInit(): void {
    // Subscribe to config changes
    this._fuseConfigService.config
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe((config) => {
        this.fuseConfig = config;
      });
  }

  ngOnDestroy(): void {
    // Unsubscribe from all subscriptions
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }

  //#region Methods



  private configureOAuthService() {
    authConfig.issuer = this._authUrl;

    if (this._isProduction === true) {
      if (authConfig.redirectUri.startsWith('https') == false) {
        authConfig.redirectUri = 'https' + authConfig.redirectUri.substr(4);
        authConfig.postLogoutRedirectUri = 'https' + authConfig.postLogoutRedirectUri.substr(4);
      }
    }

    this.oauthService.configure(authConfig);
    // this.oauthService.tokenValidationHandler = new JwksValidationHandler();

    this.oauthService.tryLogin().then(() => {
      if (this.oauthService.hasValidAccessToken() === true) {
        this._webClient.checkIfUserIsAlreadyMember().subscribe((result) => {
          if (result == MemberStatus.IsMember) {
            this._webClient.getUserInfo().subscribe((userInfo) => {
              this._loginSerivce.updateUserInfo(userInfo);
            })
          }
        });
      }
    });
  }

  private buildMenu(roles: BesaRoles): void {
    const mainNavItem: FuseNavigation[] = this._fuseNavigationService.getNavigation('main');

    const possibleRoles = [
      BesaRoles.Admin,
      BesaRoles.AirInjector,
      BesaRoles.CivilWorker,
      BesaRoles.ProjectManager,
      BesaRoles.Sales,
      BesaRoles.Splice,
      BesaRoles.ActiveNetwork,
      BesaRoles.Inventory,
    ];

    if (roles === 0) { return; }
    for (let i = 0; i < possibleRoles.length; i++) {
      const role = possibleRoles[i];
      if ((roles & role) === role) {
        const navItems = this._roleToNavItems[role];
        if (navItems) {
          mainNavItem.push(...navItems);
        }
      }
    }
  }

  private loadIdentiyInfo() {
    this._loginSerivce.getIdentityInformationen(this.oauthService.getAccessToken(), this.oauthService.userinfoEndpoint).subscribe(
      (identityResult) => {
      });
  }

  private getIdentityInfo() {
    if (this.oauthService.hasValidAccessToken() === false) { return; }

    if (this._discoveryDocumentLoaded === false) {
      this._discoveryDocumentLoaded = true;
      this.oauthService.loadDiscoveryDocument().then((any) => {
        this.loadIdentiyInfo();
      });
    } else {
      this.loadIdentiyInfo();
    }
  }

  //#endregion
  public toggleSidebarOpen(key): void {
    this._fuseSidebarService.getSidebar(key).toggleOpen();
  }
}



