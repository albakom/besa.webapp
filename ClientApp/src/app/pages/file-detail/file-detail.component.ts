import { IFileEditModel } from './../../models/service/files/ifile-edit-model';
import { OnDestroy } from '@angular/core/src/metadata/lifecycle_hooks';
import { FileAccessObjects } from './../../models/service/files/file-access-objects.enum';
import { IFileAccessOverviewModel } from './../../models/service/files/ifile-access-overview-model';
import { Component, OnInit } from '@angular/core';
import { IFileDetailModel } from '../../models/service/files/ifile-detail-model';
import { FormBasedPage } from '../base/form-based-page';
import { FormErrorMessage } from '../../models/helper/form-error-message';
import { fuseAnimations } from '@fuse/animations';
import { FileDetailFormErrorMessage } from '../../models/error-messages/file-detail-form-error-message';
import { FilesService } from '../../services/files/files.service';
import { ActivatedRoute, Router } from '@angular/router';
import { finalize, debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { MatSnackBar, MatAutocompleteSelectedEvent } from '@angular/material';
import { FormBuilder, Validators } from '@angular/forms';
import { Subject } from 'rxjs';
import { IFileObjectOverviewModel } from '../../models/service/files/ifile-object-overview-model';

@Component({
  selector: 'besa-file-detail',
  templateUrl: './file-detail.component.html',
  styleUrls: ['./file-detail.component.scss'],
  animations: fuseAnimations
})
export class FileDetailComponent extends FormBasedPage implements OnInit, OnDestroy {

  public loadingDataInProgress: boolean = false;

  public changeInProgress: boolean = false;

  public file: IFileDetailModel;
  private _id: number;
  private _searchTerm: Subject<string>;
  public searchAccess: IFileObjectOverviewModel[];
  public selectOptions: FileAccessObjects[] = [
    FileAccessObjects.Building,
    FileAccessObjects.BranchOffJob,
    FileAccessObjects.HouseConnectionJob,
  ];

  constructor(
    private _service: FilesService,
    private _activeRoute: ActivatedRoute,
    private _snackBar: MatSnackBar,
    private _fb: FormBuilder,
    private _router: Router,
  ) {
    super();

    this._searchTerm = new Subject<string>();
    this.searchAccess = new Array();

    this.initializeFormValues();

    super.addSubscription(this._searchTerm.pipe(
      debounceTime(150),
      distinctUntilChanged(),
      switchMap((item) => {
        return this._service.searchAccess(this.form.value.newAccessEntry.searchAccessType, item, 20);
      })
    ).subscribe((items) => {
      this.searchAccess = items;
    }));

    super.addSubscription(this.form.get('newAccessEntry').valueChanges.subscribe((result: any) => {
      this._searchTerm.next(result.searchTermValue);
    }));
  }

  ngOnInit() {
    this.loadData();
  }

  ngOnDestroy() {
    super.unsubcripeAll();
  }

  public initializeFormValues() {
    super.initForm(
      this._fb.group({
        name: ['', [Validators.required, Validators.maxLength(64)]],
        newAccessEntry: this._fb.group(
          {
            searchTermValue: ['', []],
            searchAccessType: [FileAccessObjects.Building, [Validators.required]]
          }
        ),
      })
    )
  }

  public loadData() {
    this.loadingDataInProgress = true;
    this._id = +this._activeRoute.snapshot.paramMap.get('id');
    this._service.getFileDetail(this._id).pipe(finalize(() => {
      this.loadingDataInProgress = false;

    })).subscribe((result) => {
      this.form.patchValue({
        name: result.name
      });

      this.file = result;
      this.loadingDataInProgress = false;
    }, (err) => {
      this._snackBar.open('Die ausstehende Anfrage konnte nicht geladen werden', 'Fehler', { duration: 4500 });
    });
  }

  public editFile() {
    if (this.changeInProgress == true) { return; }
    if (this.form.valid === false) {
      this._snackBar.open('Fehler beim Speichern durch ungültige Werte', 'Fehler', { duration: 4500 });
    }

    this.changeInProgress = true;

    let file: IFileEditModel = {
      id: this.file.id,
      name: this.form.value.name,
      accessList: this.file.accessControl
    }

    this._service.editFile(file).pipe(finalize(() => {
      this.changeInProgress = false;
    })).subscribe((result) => {
      this.loadData();
    }, (err) => {
      this._snackBar.open('Die ausstehende Änderung konnte nicht umgesetzt werden', 'Fehler', { duration: 4500 });
    });
  }

  public cancelEdit() {
    this._router.navigate(['/member/files']);
  }

  public removeAccess(access: IFileAccessOverviewModel) {
    this.file.accessControl.splice(this.file.accessControl.indexOf(access), 1);
  }

  protected getErrorMessages(): FormErrorMessage[] {
    return FileDetailFormErrorMessage;
  }

  public accessSelected(eventInfo: MatAutocompleteSelectedEvent) {
    const access: IFileAccessOverviewModel = {
      objectName: eventInfo.option.value.name,
      objectType: this.form.value.newAccessEntry.searchAccessType,
      objectId: eventInfo.option.value.id
    };

    const existing: IFileAccessOverviewModel = this.file.accessControl
      .find((value, ind, arr) => value.objectId == access.objectId && value.objectType == access.objectType);

    if (!existing) {
      this.file.accessControl.push(access);
    }
  }

  public displayAccessResult(item?: IFileObjectOverviewModel): string {
    return item ? item.name : '';
  }

}
