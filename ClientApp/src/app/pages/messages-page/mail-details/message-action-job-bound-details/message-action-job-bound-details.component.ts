import { Component, OnInit, Input } from '@angular/core';
import { MessageModel } from '../../../../models/service/messages/message-model';
import { IMessageActionJobBoundDetailsModel } from '../../../../models/service/messages/details/imessage-action-job-bound-details-model';
import { Router } from '@angular/router';
import { JobService } from '../../../../services/job.service';
import { ISimpleJobOverview } from '../../../../models/service/jobs/isimple-job-overview';

@Component({
  selector: 'besa-message-action-job-bound-details',
  templateUrl: './message-action-job-bound-details.component.html',
  styleUrls: ['./message-action-job-bound-details.component.scss']
})
export class MessageActionJobBoundDetailsComponent implements OnInit {

  @Input()
  public message: MessageModel;

  @Input()
  public details: IMessageActionJobBoundDetailsModel;

  constructor(
    private _router: Router,
    private _jobService: JobService) { }


  ngOnInit() {
  }

    //#region Viewhelpers

    public goToJobDetails(job: ISimpleJobOverview) {
      if (!job) { return; }
  
      const url = this._jobService.getUrlByJob(job);
      this._router.navigate([url, job.id]);
    }
  
    //#endregion

}
