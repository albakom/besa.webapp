import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MessageActionJobBoundDetailsComponent } from './message-action-job-bound-details.component';

describe('MessageActionJobBoundDetailsComponent', () => {
  let component: MessageActionJobBoundDetailsComponent;
  let fixture: ComponentFixture<MessageActionJobBoundDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MessageActionJobBoundDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MessageActionJobBoundDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
