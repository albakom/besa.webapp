import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MessageActionJobAcknowledgedDetailsComponent } from './message-action-job-acknowledged-details.component';

describe('MessageActionJobAcknowledgedDetailsComponent', () => {
  let component: MessageActionJobAcknowledgedDetailsComponent;
  let fixture: ComponentFixture<MessageActionJobAcknowledgedDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MessageActionJobAcknowledgedDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MessageActionJobAcknowledgedDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
