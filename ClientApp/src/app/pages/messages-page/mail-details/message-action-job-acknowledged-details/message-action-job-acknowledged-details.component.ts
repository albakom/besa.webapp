import { Component, OnInit, Input } from '@angular/core';
import { MessageModel } from '../../../../models/service/messages/message-model';
import { IMessageActionJobAcknowledgedDetailsModel } from '../../../../models/service/messages/details/imessage-action-job-acknowledged-details-model';
import { ISimpleJobOverview } from '../../../../models/service/jobs/isimple-job-overview';
import { JobService } from '../../../../services/job.service';

@Component({
  selector: 'besa-message-action-job-acknowledged-details',
  templateUrl: './message-action-job-acknowledged-details.component.html',
  styleUrls: ['./message-action-job-acknowledged-details.component.scss']
})
export class MessageActionJobAcknowledgedDetailsComponent implements OnInit {

  @Input()
  public message: MessageModel;

  @Input()
  public details: IMessageActionJobAcknowledgedDetailsModel;

  constructor(private _jobService: JobService) { }

  ngOnInit() {
  }

  //#region view helper

  public getNavigationPathForJob(job: ISimpleJobOverview): string{
    const url = this._jobService.getUrlByJob(job);
    return url;
  }

  //#endregion

}
