import { JobTypes } from './../../../models/service/jobs/job-types.enum';
import { JobMessageModel } from './../../../models/service/messages/job-message-model';
import { takeUntil } from 'rxjs/internal/operators';
import { MessageModel } from './../../../models/service/messages/message-model';
import { MessageService } from './../../../services/messages/message.service';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subject } from 'rxjs';

import { fuseAnimations } from '@fuse/animations';

import { MessageActions } from '../../../models/service/messages/message-actions.enum';
import { ActionType } from '../../../models/service/messages/action-type.enum';
import { JobStates } from '../../../models/service/jobs/job-states.enum';

@Component({
    selector: 'mail-details',
    templateUrl: './mail-details.component.html',
    styleUrls: ['./mail-details.component.scss'],
    animations: fuseAnimations
})
export class MailDetailsComponent implements OnInit, OnDestroy {
    message: MessageModel;
    messageText: string;

    private _unsubscribeAll: Subject<any>;

    constructor(
        private _messageService: MessageService
    ) {
        this._unsubscribeAll = new Subject();
    }

    ngOnInit(): void {
        this._messageService.onCurrentMessageChanged.pipe(takeUntil(this._unsubscribeAll)).subscribe(currentMessage => {
            this.message = currentMessage;
        });
    }

    ngOnDestroy(): void {
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }
}
