import { JobService } from './../../../../services/job.service';
import { Component, OnInit, Input } from '@angular/core';
import { MessageModel } from '../../../../models/service/messages/message-model';
import { IMessageActionJobFinishedDetailsModel } from '../../../../models/service/messages/details/imessage-action-job-finished-details-model';
import { ISimpleJobOverview } from '../../../../models/service/jobs/isimple-job-overview';

@Component({
  selector: 'besa-message-action-job-finished-details',
  templateUrl: './message-action-job-finished-details.component.html',
  styleUrls: ['./message-action-job-finished-details.component.scss']
})
export class MessageActionJobFinishedDetailsComponent implements OnInit {

  @Input()
  public message: MessageModel;

  @Input()
  public details: IMessageActionJobFinishedDetailsModel;

  constructor(private _jobService: JobService) { }

  ngOnInit() {
  }

  //#region view helper

  public getNavigationPathForJob(job: ISimpleJobOverview): string {
    const url = this._jobService.getUrlByJob(job);
    return url;
  }

  //#endregion

}
