import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MessageActionJobFinishedDetailsComponent } from './message-action-job-finished-details.component';

describe('MessageActionJobFinishedDetailsComponent', () => {
  let component: MessageActionJobFinishedDetailsComponent;
  let fixture: ComponentFixture<MessageActionJobFinishedDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MessageActionJobFinishedDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MessageActionJobFinishedDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
