import { Router } from '@angular/router';
import { Component, OnInit, Input } from '@angular/core';
import { MessageModel } from '../../../../models/service/messages/message-model';
import { IMessageActionCollectionJobDetailsModel } from '../../../../models/service/messages/details/imessage-action-collection-job-details-model';
import { ISimpleJobOverview } from '../../../../models/service/jobs/isimple-job-overview';
import { JobService } from '../../../../services/job.service';

@Component({
  selector: 'besa-message-action-collection-job-details',
  templateUrl: './message-action-collection-job-details.component.html',
  styleUrls: ['./message-action-collection-job-details.component.scss']
})
export class MessageActionCollectionJobDetailsComponent implements OnInit {

  @Input()
  public message: MessageModel;

  @Input()
  public details: IMessageActionCollectionJobDetailsModel;

  constructor(
    private _router: Router,
    private _jobService: JobService) { }

  ngOnInit() {
  }

  //#region Viewhelpers

  public goToJobDetails(job: ISimpleJobOverview) {
    if (!job) { return; }

    const url = this._jobService.getUrlByJob(job);
    this._router.navigate([url, job.id]);
  }

  //#endregion

}
