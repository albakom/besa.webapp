import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MessageActionCollectionJobDetailsComponent } from './message-action-collection-job-details.component';

describe('MessageActionCollectionJobDetailsComponent', () => {
  let component: MessageActionCollectionJobDetailsComponent;
  let fixture: ComponentFixture<MessageActionCollectionJobDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MessageActionCollectionJobDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MessageActionCollectionJobDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
