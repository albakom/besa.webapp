import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MessageActionJobDiscardDetailsComponent } from './message-action-job-discard-details.component';

describe('MessageActionJobDiscardDetailsComponent', () => {
  let component: MessageActionJobDiscardDetailsComponent;
  let fixture: ComponentFixture<MessageActionJobDiscardDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MessageActionJobDiscardDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MessageActionJobDiscardDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
