import { JobService } from './../../../../services/job.service';
import { Component, OnInit, Input } from '@angular/core';
import { IMessageActionJobDiscardDetailsModel } from '../../../../models/service/messages/details/imessage-action-job-discard-details-model';
import { MessageModel } from '../../../../models/service/messages/message-model';
import { ISimpleJobOverview } from '../../../../models/service/jobs/isimple-job-overview';

@Component({
  selector: 'besa-message-action-job-discard-details',
  templateUrl: './message-action-job-discard-details.component.html',
  styleUrls: ['./message-action-job-discard-details.component.scss']
})
export class MessageActionJobDiscardDetailsComponent implements OnInit {

  @Input()
  public message: MessageModel;

  @Input()
  public details: IMessageActionJobDiscardDetailsModel;

  constructor(private _jobService: JobService) { }

  ngOnInit() {
  }

    //#region view helper

    public getNavigationPathForJob(job: ISimpleJobOverview): string{
      const url = this._jobService.getUrlByJob(job);
      return url;
    }
  
    //#endregion

}
