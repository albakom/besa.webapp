import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MessageActionJobFinishJobAdministrativeDetailComponent } from './message-action-job-finish-job-administrative-detail.component';

describe('MessageActionJobFinishJobAdministrativeDetailComponent', () => {
  let component: MessageActionJobFinishJobAdministrativeDetailComponent;
  let fixture: ComponentFixture<MessageActionJobFinishJobAdministrativeDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MessageActionJobFinishJobAdministrativeDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MessageActionJobFinishJobAdministrativeDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
