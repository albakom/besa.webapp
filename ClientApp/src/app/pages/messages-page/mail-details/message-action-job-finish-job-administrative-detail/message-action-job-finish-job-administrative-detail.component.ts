import { JobService } from './../../../../services/job.service';
import { Component, OnInit, Input } from '@angular/core';
import { IMessageActionJobFinishJobAdministrativeDetailModel } from '../../../../models/service/messages/details/imessage-action-job-finish-job-administrative-detail-model';
import { MessageModel } from '../../../../models/service/messages/message-model';
import { ISimpleJobOverview } from '../../../../models/service/jobs/isimple-job-overview';

@Component({
  selector: 'besa-message-action-job-finish-job-administrative-detail',
  templateUrl: './message-action-job-finish-job-administrative-detail.component.html',
  styleUrls: ['./message-action-job-finish-job-administrative-detail.component.scss']
})
export class MessageActionJobFinishJobAdministrativeDetailComponent implements OnInit {

  @Input()
  public message: MessageModel;

  @Input()
  public details: IMessageActionJobFinishJobAdministrativeDetailModel;

  constructor(private _jobService: JobService) { }

  ngOnInit() {
  }


  //#region view helper

  public getNavigationPathForJob(job: ISimpleJobOverview): string {
    const url = this._jobService.getUrlByJob(job);
    return url;
  }

  //#endregion

}
