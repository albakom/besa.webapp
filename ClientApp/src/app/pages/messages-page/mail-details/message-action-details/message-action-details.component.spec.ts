import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MessageActionDetailsComponent } from './message-action-details.component';

describe('MessageActionDetailsComponent', () => {
  let component: MessageActionDetailsComponent;
  let fixture: ComponentFixture<MessageActionDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MessageActionDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MessageActionDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
