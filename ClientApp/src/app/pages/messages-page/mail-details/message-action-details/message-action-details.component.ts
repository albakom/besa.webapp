import { Component, OnInit, Input } from '@angular/core';
import { MessageModel } from '../../../../models/service/messages/message-model';
import { IMessageActionDetailsModel } from '../../../../models/service/messages/details/imessage-action-details-model';

@Component({
  selector: 'besa-message-action-details',
  templateUrl: './message-action-details.component.html',
  styleUrls: ['./message-action-details.component.scss']
})
export class MessageActionDetailsComponent implements OnInit {

  @Input()
  public message: MessageModel;

  @Input()
  public details: IMessageActionDetailsModel;

  constructor() { }

  ngOnInit() {
  }

}
