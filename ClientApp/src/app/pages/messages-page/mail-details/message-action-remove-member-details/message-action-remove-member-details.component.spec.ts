import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MessageActionRemoveMemberDetailsComponent } from './message-action-remove-member-details.component';

describe('MessageActionRemoveMemberDetailsComponent', () => {
  let component: MessageActionRemoveMemberDetailsComponent;
  let fixture: ComponentFixture<MessageActionRemoveMemberDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MessageActionRemoveMemberDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MessageActionRemoveMemberDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
