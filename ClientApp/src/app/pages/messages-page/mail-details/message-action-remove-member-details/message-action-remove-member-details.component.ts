import { Component, OnInit, Input } from '@angular/core';
import { MessageModel } from '../../../../models/service/messages/message-model';
import { IMessageActionRemoveMemberDetailsModel } from '../../../../models/service/messages/details/imessage-action-remove-member-details-model';

@Component({
  selector: 'besa-message-action-remove-member-details',
  templateUrl: './message-action-remove-member-details.component.html',
  styleUrls: ['./message-action-remove-member-details.component.scss']
})
export class MessageActionRemoveMemberDetailsComponent implements OnInit {

  @Input()
  public message: MessageModel;

  @Input()
  public details: IMessageActionRemoveMemberDetailsModel;

  constructor() { }

  ngOnInit() {
  }

}
