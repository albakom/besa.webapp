import { Component, OnInit, Input } from '@angular/core';
import { IMessageActionEmployesBoundDetailsModel } from '../../../../models/service/messages/details/imessage-action-employes-bound-details-model';
import { MessageModel } from '../../../../models/service/messages/message-model';

@Component({
  selector: 'besa-message-action-employes-bound-details',
  templateUrl: './message-action-employes-bound-details.component.html',
  styleUrls: ['./message-action-employes-bound-details.component.scss']
})
export class MessageActionEmployesBoundDetailsComponent implements OnInit {

  @Input()
  public message: MessageModel;

  @Input()
  public details: IMessageActionEmployesBoundDetailsModel;

  constructor() { }

  ngOnInit() {
  }

  //#region Viewhelper

  public getNavigationPathForCompany(): string {
    return '/member/company-details/'
  }

  public getNavigationPathForUser(): string {
    return '/member/users-details/'
  }

  //#endregion

}
