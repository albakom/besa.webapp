import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MessageActionEmployesBoundDetailsComponent } from './message-action-employes-bound-details.component';

describe('MessageActionEmployesBoundDetailsComponent', () => {
  let component: MessageActionEmployesBoundDetailsComponent;
  let fixture: ComponentFixture<MessageActionEmployesBoundDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MessageActionEmployesBoundDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MessageActionEmployesBoundDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
