import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MessageActionUpdateFromAdapterDetailsComponent } from './message-action-update-from-adapter-details.component';

describe('MessageActionUpdateFromAdapterDetailsComponent', () => {
  let component: MessageActionUpdateFromAdapterDetailsComponent;
  let fixture: ComponentFixture<MessageActionUpdateFromAdapterDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MessageActionUpdateFromAdapterDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MessageActionUpdateFromAdapterDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
