import { Component, OnInit, Input } from '@angular/core';
import { IMessageActionUpdateFromAdapterDetailsModel } from '../../../../models/service/messages/details/imessage-action-update-from-adapter-details-model';
import { MessageModel } from '../../../../models/service/messages/message-model';

@Component({
  selector: 'besa-message-action-update-from-adapter-details',
  templateUrl: './message-action-update-from-adapter-details.component.html',
  styleUrls: ['./message-action-update-from-adapter-details.component.scss']
})
export class MessageActionUpdateFromAdapterDetailsComponent implements OnInit {

  @Input()
  public message: MessageModel;

  @Input()
  public details: IMessageActionUpdateFromAdapterDetailsModel;

  constructor() { }

  ngOnInit() {
  }

}
