import { Component, OnInit, Input } from '@angular/core';
import { IMessageActionProcedureFinishedDetailsModel } from '../../../../models/service/messages/details/imessage-action-procedure-finished-details-model';
import { MessageModel } from '../../../../models/service/messages/message-model';
import { ProcedureService } from '../../../../services/procedure.service';
import { IProcedureOverviewModel } from '../../../../models/service/procedure/iprocedure-overview-model';

@Component({
  selector: 'besa-message-action-procedure-finished-details',
  templateUrl: './message-action-procedure-finished-details.component.html',
  styleUrls: ['./message-action-procedure-finished-details.component.scss']
})
export class MessageActionProcedureFinishedDetailsComponent implements OnInit {

  @Input()
  public message: MessageModel;

  @Input()
  public details: IMessageActionProcedureFinishedDetailsModel;

  constructor(
    private _procedureService: ProcedureService
  ) { }

  ngOnInit() {
  }

  //#region view helper

  public getNavigationPathForProcedure(procedure: IProcedureOverviewModel): string {
    const path = this._procedureService.getPathToProcedure(procedure);
    return path;
  }

  //#endregion

}
