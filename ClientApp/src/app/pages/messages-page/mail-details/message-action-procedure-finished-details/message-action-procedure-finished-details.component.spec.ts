import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MessageActionProcedureFinishedDetailsComponent } from './message-action-procedure-finished-details.component';

describe('MessageActionProcedureFinishedDetailsComponent', () => {
  let component: MessageActionProcedureFinishedDetailsComponent;
  let fixture: ComponentFixture<MessageActionProcedureFinishedDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MessageActionProcedureFinishedDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MessageActionProcedureFinishedDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
