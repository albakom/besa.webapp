import { MatSnackBar } from '@angular/material';
import { finalize } from 'rxjs/operators';
import { SubscriptionAwareComponent } from './../../../helper/subscription-aware-component';
import { LoginService } from './../../../services/login.service';
import { MessageFilterModel } from './../../../models/service/messages/message-filter-model';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Subject } from 'rxjs';
import { takeUntil, debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';

import { fuseAnimations } from '@fuse/animations';

import { MessageService } from '../../../services/messages/message.service';
import { MessageModel } from '../../../models/service/messages/message-model';
import { MessageActions } from '../../../models/service/messages/message-actions.enum';
import { ActionType } from '../../../models/service/messages/action-type.enum';
import { MarkedAsRead } from '../../../models/service/messages/marked-as-read.enum';
import { RefreshManagerService } from '../../../services/refresh-manager.service';

@Component({
    selector: 'mail-list',
    templateUrl: './mail-list.component.html',
    styleUrls: ['./mail-list.component.scss'],
    animations: fuseAnimations
})
export class MailListComponent extends SubscriptionAwareComponent implements OnInit, OnDestroy {

    //#region Properties

    public messages: MessageModel[];
    public currentMessage: MessageModel;
    public loadingMessageInProgress: boolean;

    //#endregion

    //#region constructor

    constructor(
        private _messageService: MessageService,
        private _loginService: LoginService,
        private _snackBar: MatSnackBar,
        refreshManager: RefreshManagerService
    ) {
        super();

        this.messages = new Array();

        super.addSubscription(refreshManager.refreshRequested.subscribe(() => {
            this.loadData()
        }));
    }

    ngOnInit(): void {

        this.loadData();


        super.addSubscription(this._messageService.onCurrentMessageChanged
            .subscribe(currentMessage => {
                if (!currentMessage) {
                    this.currentMessage = null;
                }
                else {
                    this.currentMessage = currentMessage;
                }
            }));

        super.addSubscription(this._messageService.onFilterChanged.pipe(
            debounceTime(450),
            switchMap((item) => {
                this.loadingMessageInProgress = true;
                return this._messageService.getMessages(item).pipe(
                    finalize(() => { this.loadingMessageInProgress = false; })
                );
            })
        ).subscribe((items) => {
            this.messages = items;
        }, (err) => {
            this._snackBar.open('Nachrichten konnte nicht geladen werden', 'Fehler', { duration: 4500 });
        }));
    }

    ngOnDestroy(): void {
        super.unsubcripeAll();
    }

    //#endregion

    //#region Methods

    public loadData() {
        this.loadingMessageInProgress = true;
        this._messageService.getDefaultMessages().pipe(
            finalize(() => { this.loadingMessageInProgress = false; })
        ).subscribe((result) => {
            if (result) {
                this.messages = result;
            }
        }, (err) => {
            this._snackBar.open('Nachrichten konnte nicht geladen werden', 'Fehler', { duration: 4500 });
        });
    }

    public markMessageAsRead(msg: MessageModel): void {
        this._messageService.setCurrentMessage(msg);
        if (msg.markedAsRead == true) { return; }

        this._messageService.markMessageAsRead(msg.id).subscribe((result) => {
            if (result == true) {
                msg.markedAsRead = true;

                if (this._loginService.currentUserInfo.value) {
                    const msgIndex = this._loginService.currentUserInfo.value.unreadMessage.findIndex(x => x.id == msg.id);
                    if (msgIndex >= 0) {
                        this._loginService.currentUserInfo.value.unreadMessage.splice(msgIndex, 1);
                    }
                }
            }
        });
    }

    //#endregion
}
