import { Component, HostBinding, Input, OnDestroy, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { MessageModel } from '../../../../models/service/messages/message-model';
import { MessageActions } from '../../../../models/service/messages/message-actions.enum';
import { MessageRelatedObjectTypes } from '../../../../models/service/tasks/message-related-object-types.enum';

@Component({
    selector: 'mail-list-item',
    templateUrl: './mail-list-item.component.html',
    styleUrls: ['./mail-list-item.component.scss']
})
export class MailListItemComponent implements OnInit, OnDestroy {

    //#region Properties

    @Input() message: MessageModel;

    //#endregion


    constructor(
    ) {
    }

    ngOnInit(): void {
    }

    ngOnDestroy(): void {
    }

    public getMessageSubject(msg: MessageModel): string {
        let result = '';

        let showName = true;
        switch (msg.relatedObjectType) {
            case MessageRelatedObjectTypes.ActivationJob:
                result += 'Aktivierungsauftrag '
                break;
            case MessageRelatedObjectTypes.Article:
                result += 'Ware '
                break;
            case MessageRelatedObjectTypes.Branchable:
                result += 'Verzweiger '
                break;
            case MessageRelatedObjectTypes.BranchOffJob:
                result += 'Vorstreckeraufgabe '
                break;
            case MessageRelatedObjectTypes.BuildingConnenctionProcedure:
                result += 'Hausanschluss-Vorgang '
                break;
            case MessageRelatedObjectTypes.Buildings:
                result += 'Gebäude '
                break;
            case MessageRelatedObjectTypes.CollectionJob:
                result += 'Sammelauftrag '
                break;
            case MessageRelatedObjectTypes.Company:
                result += 'Firma '
                break;
            case MessageRelatedObjectTypes.ConnectBuildingJob:
                result += 'Hausanschlussaufgabe '
                break;
            case MessageRelatedObjectTypes.ConstructionStage:
                result += 'Bauabschnitt '
                break;
            case MessageRelatedObjectTypes.ContactAfterFinishedJob:
                result += 'Nachverfolgungsauftrag '
                break;
            case MessageRelatedObjectTypes.ContactInfo:
                result += 'Kontaktinformationen '
                break;
            case MessageRelatedObjectTypes.CustomerConnenctionProcedure:
                result += 'Kundenanschluss-Vorgang '
                break;
            case MessageRelatedObjectTypes.CustomerRequest:
                result += 'Anfrage '
                break;
            case MessageRelatedObjectTypes.Fiber:
                result += 'Faser '
                showName = false;
                break;
            case MessageRelatedObjectTypes.FiberCable:
                result += 'Glasfaserkabel '
                showName = false;
                break;
            case MessageRelatedObjectTypes.FiberCableType:
                result += 'Glasfaserkabeltyp '
                showName = false;
                break;
            case MessageRelatedObjectTypes.FiberConnection:
                result += 'Patchverbindung '
                showName = false;
                break;
            case MessageRelatedObjectTypes.File:
                result += 'Datei '
                break;
            case MessageRelatedObjectTypes.FileAccessEntry:
                result += 'Dateizugriffseintrag '
                showName = false;
                break;
            case MessageRelatedObjectTypes.Flat:
                result += 'Wohnung '
                break;
            case MessageRelatedObjectTypes.InjectJob:
                result += 'Einblausaufgabe '
                break;
            case MessageRelatedObjectTypes.InventoryJob:
                result += 'Kommisierungsaufgabe '
                break;
            case MessageRelatedObjectTypes.JobGenerell:
                result += 'Aufgabe '
                break;
            case MessageRelatedObjectTypes.MainCables:
                result += 'Hauptkabel '
                showName = false;
                break;
            case MessageRelatedObjectTypes.PrepareBranchableJob:
                result += 'Verzweiger-Vorbereiten-Aufgabe '
                break;
            case MessageRelatedObjectTypes.PoP:
                result += 'PoP '
                break;
            case MessageRelatedObjectTypes.Splice:
                result += 'Spleiß '
                showName = false;
                break;
            case MessageRelatedObjectTypes.SpliceInBranchableJob:
                result += 'Spleiß-im-Verzweiger-Aufgabe '
                break;
            case MessageRelatedObjectTypes.SpliceMainCableInPoPJob:
                result += 'HK-Spleiß-Aufgabe '
                break;
            case MessageRelatedObjectTypes.User:
                result += 'Benutzer '
                break;
            case MessageRelatedObjectTypes.UserRequests:
                result += 'Benutzeranfrage '
                break;
        }

        if (showName === true && msg.actionDetails) {
            result += '"' + msg.actionDetails.name + '" ';
        }

        switch (msg.action) {
            case MessageActions.Update:
                result += 'wurde bearbeitet';
                break;
            case MessageActions.AddEmployeeToCompany:
                result += 'hat einen neuen Mitarbeiter';
                break;
            case MessageActions.AddFlatsToBuildingWithOneUnit:
                result += 'hat eine "Standardwohnung" bekommen';
                break;
            case MessageActions.AddFlatToBuilding:
                result += 'hat eine Wohnung bekommen';
                break;
            case MessageActions.CollectionJobBoundToCompany:
                result += 'wurde zugewiesen';
                break;
            case MessageActions.Create:
                result += 'wurde erstellt';
                break;
            case MessageActions.CreateBranchOffJobs:
                result += 'wurden erstellt';
                break;
            case MessageActions.CreateViaImport:
                result += 'wurde durch Import erstellt';
                break;
            case MessageActions.Delete:
                result += 'wurde gelöscht';
                break;
            case MessageActions.FinishedProcedures:
                result += 'wurde abgeschlossen';
                break;
            case MessageActions.InformJobFinisherAboutAcknlowedged:
            case MessageActions.JobAcknowledged:
                result += 'wurde angenommen';
                break;
            case MessageActions.InformJobFinisherAboutDiscard:
            case MessageActions.JobDiscared:
                result += 'wurde abgelehnt';
                break;
            case MessageActions.JobFinished:
                result += 'wurde abgeschlossen';
                break;
            case MessageActions.JobFinishJobAdministrative:
                result += 'wurde administrativ abgeschlossen';
                break;
            case MessageActions.RemoveEmployeeToCompany:
                result += 'hat einen Mitarbeiter entfernt';
                break;
            case MessageActions.RemoveMemberState:
                result += 'ist nicht länger ein Mitglied';
                break;
            case MessageActions.UpdateFromAdapter:
            case MessageActions.UpdateAllCableTypesFromAdapter:
            case MessageActions.UpdateBuildingCableInfoFromAdapter:
            case MessageActions.UpdateBuildingNameFromAdapter:
            case MessageActions.UpdateBuildigsFromAdapter:
            case MessageActions.UpdatePopInformation:
            case MessageActions.UpdateMainCables:
            case MessageActions.UpdateSplices:
            case MessageActions.UpdateMainCableForBranchable:
            case MessageActions.UpdatePatchConnections:
            case MessageActions.UpdateCablesFromAdapter:
                result += 'wurde aus der Planungsdatenbank aktualisiert';
                break;
        }

        return result;
    }
}
