import { ActionTypeCheckboxModel } from 'app/models/service/messages/action-type-checkbox-model';
import { MessageActionCheckboxModel } from './../../../../models/service/messages/message-action-checkbox-model';
import { ActionType } from './../../../../models/service/messages/action-type.enum';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { MatDialog, MatCheckboxChange } from '@angular/material';
import { Subject, BehaviorSubject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { fuseAnimations } from '@fuse/animations';
import { MessageService } from '../../../../services/messages/message.service';
import { MessageActions } from '../../../../models/service/messages/message-actions.enum';
import { MarkedAsRead } from '../../../../models/service/messages/marked-as-read.enum';
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import * as moment from 'moment';
import { ISelectedable } from '../../../../models/helper/iselectedable';
import { MessageRelatedObjectTypes } from 'app/models/service/tasks/message-related-object-types.enum';

@Component({
    selector: 'mail-main-sidebar',
    templateUrl: './main-sidebar.component.html',
    styleUrls: ['./main-sidebar.component.scss'],
    animations: fuseAnimations
})
export class MailMainSidebarComponent implements OnInit, OnDestroy {

    //#region Fields

    //#endregion    

    //#region Properties

    public filterActionTypes: ISelectedable<MessageActions>[];
    public filterMessageActions: ISelectedable<MessageRelatedObjectTypes>[];

    public filterMarkedAsReadSubject: BehaviorSubject<MarkedAsRead>;
    public set filterMarkedAsRead(value: MarkedAsRead) {
        this.filterMarkedAsReadSubject.next(value);
    }
    public get filterMarkedAsRead(): MarkedAsRead {
        return this.filterMarkedAsReadSubject.getValue();
    }
    public filterMarkedAsReadValues: MarkedAsRead[];

    public startDateSubject: BehaviorSubject<Date>;
    public set startDate(value: Date) {
        this.startDateSubject.next(value);
    }
    public get startDate(): Date {
        return this.startDateSubject.getValue();
    }

    public endDateSubject: BehaviorSubject<Date>;
    public set endDate(value: Date) {
        this.endDateSubject.next(value);
    }
    public get endDate(): Date {
        return this.endDateSubject.getValue();
    }

    //#endregion   

    //#region  constructor

    constructor(
        private _messageService: MessageService
    ) {
    }

    ngOnInit(): void {

        this.filterMarkedAsReadSubject = new BehaviorSubject<MarkedAsRead>(MarkedAsRead.All);
        this.startDateSubject = new BehaviorSubject<Date>(moment(Date.now()).subtract(90, 'day').toDate());
        this.endDateSubject = new BehaviorSubject<Date>(new Date());

        this.filterActionTypes = this._messageService.getPossibleMessageActions().map((x) => {
            const value: ISelectedable<MessageActions> = {
                value: x,
                isSelected: false,
            }

            return value;
        })

        this.filterMessageActions = this._messageService.getPossibleMessageRelatedTypes().map(x => {
            const value: ISelectedable<MessageRelatedObjectTypes> = {
                value: x,
                isSelected: false,
            }

            return value;
        })

        this.filterMarkedAsReadValues = [
            MarkedAsRead.All,
            MarkedAsRead.OnlyRead,
            MarkedAsRead.OnlyUnread
        ]

        this.filterMarkedAsReadSubject.subscribe(item => {
            this._messageService.changeMarkedAsReadStateToFilter(item);
        });

        this.startDateSubject.subscribe(item => {
            this._messageService.changeStartDateOfFilter(item);
        });

        this.endDateSubject.subscribe(item => {
            this._messageService.changeEndDateOfFilter(item);
        });
    }

    ngOnDestroy(): void {
        this.filterMarkedAsReadSubject.unsubscribe();
        this.startDateSubject.unsubscribe();
        this.endDateSubject.unsubscribe();
    }

    //#endregion

    //#region view helper 

    updateFilterMessages(item: ISelectedable<MessageActions>, args: MatCheckboxChange): void {
        item.isSelected = args.checked;
        this._messageService.changeMessageActionTypeOfFilter(item);
    }

    updateFilterActions(item: ISelectedable<MessageRelatedObjectTypes>, args: MatCheckboxChange): void {
        item.isSelected = args.checked;
        this._messageService.changeMessageRelatedObjectTypeOfFilter(item);
    }

    //#endregion
}
