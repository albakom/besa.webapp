import { fuseAnimations } from './../../../@fuse/animations/index';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, takeUntil } from 'rxjs/operators';

import { FuseSidebarService } from '@fuse/components/sidebar/sidebar.service';


import { MessageService } from '../../services/messages/message.service';
import { SubscriptionAwareComponent } from '../../helper/subscription-aware-component';

@Component({
    selector: 'mail',
    templateUrl: './mail.component.html',
    styleUrls: ['./mail.component.scss'],
    animations: fuseAnimations
})
export class MailComponent extends SubscriptionAwareComponent implements OnInit, OnDestroy {

    //#region Fields

    //#endregion

    //#region  Properties

    public searchInput: FormControl = new FormControl('');

    //#endregion

    //#region ctor

    constructor(
        private _fuseSidebarService: FuseSidebarService,
        private _messageService: MessageService
    ) {
        super();
    }

    public ngOnInit(): void {
        super.addSubscription(this.searchInput.valueChanges
            .subscribe(searchText => {
                this._messageService.setQueryValueForFilter(searchText);
            }));
    }

    public ngOnDestroy(): void {
        super.unsubcripeAll();
    }

    //#endregion

    //#region view helper

    toggleSidebar(name): void {
        this._fuseSidebarService.getSidebar(name).toggleOpen();
    }

    //#endregion
}
