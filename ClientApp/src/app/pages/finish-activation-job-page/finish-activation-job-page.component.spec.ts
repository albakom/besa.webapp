import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FinishActivationJobPageComponent } from './finish-activation-job-page.component';

describe('FinishActivationJobPageComponent', () => {
  let component: FinishActivationJobPageComponent;
  let fixture: ComponentFixture<FinishActivationJobPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FinishActivationJobPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FinishActivationJobPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
