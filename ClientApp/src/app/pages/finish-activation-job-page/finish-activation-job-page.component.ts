import { Component, OnInit } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';
import { FinishJobPageBase } from '../base/finish-job-page-base';
import { OnDestroy } from '@angular/core/src/metadata/lifecycle_hooks';
import { MatDialog, MatSnackBar } from '@angular/material';
import { FormBuilder, Validators } from '@angular/forms';
import { JobService } from '../../services/job.service';
import { Router, ActivatedRoute } from '@angular/router';
import { UploadService } from '../../services/upload/upload.service';
import { Observable } from 'rxjs';
import { FormErrorMessage } from '../../models/helper/form-error-message';
import { IFinishActivationJobModel } from '../../models/service/jobs/activation/ifinish-activation-job-model';
import { FinishActivationJobErrorMessages } from '../../models/error-messages/finish-activation-job-error-messages';

@Component({
  selector: 'besa-finish-activation-job-page',
  templateUrl: './finish-activation-job-page.component.html',
  styleUrls: ['./finish-activation-job-page.component.scss'],
  animations: fuseAnimations
})
export class FinishActivationJobPageComponent extends FinishJobPageBase<IFinishActivationJobModel> implements OnInit, OnDestroy {

  constructor(
    dialog: MatDialog,
    fb: FormBuilder,
    snackbar: MatSnackBar,
    jobService: JobService,
    router: Router,
    uploadService: UploadService,
    activeRoute: ActivatedRoute,
  ) {
    super(dialog, fb, snackbar, jobService, router, uploadService, activeRoute);
   }

  ngOnInit() {
    this.filesNeeded = true;

    super.initFinishForm({
      
    });
  }

  ngOnDestroy() {
    super.unsubcripeAll();
  }

  protected checkIfModelIsValid(model: IFinishActivationJobModel): boolean {
    return true;
  }

  protected getfinishJobInvoker(model: IFinishActivationJobModel): Observable<number> {
    return this._jobService.finishActivationJob(model);
  }
  
  protected getJobUrl(): string {
    return '/member/activation-job-details';
  }

  protected getErrorMessages(): FormErrorMessage[] {
    return FinishActivationJobErrorMessages;
  }

}
