import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomerRequestDetailsPageComponent } from './customer-request-details-page.component';

describe('CustomerRequestDetailsPageComponent', () => {
  let component: CustomerRequestDetailsPageComponent;
  let fixture: ComponentFixture<CustomerRequestDetailsPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomerRequestDetailsPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomerRequestDetailsPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
