import { RefreshManagerService } from '../../services/refresh-manager.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { SubscriptionAwareComponent } from '../../helper/subscription-aware-component';
import { ICustomerRequestDetails } from '../../models/service/jobs/icustomer-request-details';
import { ActivatedRoute, Router } from '@angular/router';
import { JobService } from '../../services/job.service';
import { finalize } from 'rxjs/operators';
import { MatSnackBar } from '@angular/material';
import { fuseAnimations } from '@fuse/animations';
import { JobTypes } from '../../models/service/jobs/job-types.enum';
import { JobStates } from '../../models/service/jobs/job-states.enum';

@Component({
  selector: 'besa-customer-request-details-page',
  templateUrl: './customer-request-details-page.component.html',
  styleUrls: ['./customer-request-details-page.component.scss'],
  animations: fuseAnimations,
})
export class CustomerRequestDetailsPageComponent extends SubscriptionAwareComponent implements OnInit, OnDestroy {

  //#region Properties

  public loadingDetailsInProgress: boolean;
  public details: ICustomerRequestDetails;

  public createJobInProgress: boolean;
  public deleteInProgress: boolean;

  //#endregion

  constructor(refreshManager: RefreshManagerService,
    private _activeRoute: ActivatedRoute,
    private _snackBar: MatSnackBar,
    private _router: Router,
    private _service: JobService,
  ) {
    super();

    super.addSubscription(refreshManager.refreshRequested.subscribe((result) => {
      this.loadDetails(+_activeRoute.snapshot.params['id']);
    }));

    super.addSubscription(_activeRoute.params.subscribe((params) => {
      this.loadDetails(+params['id']);
    }))
  }

  ngOnInit() {
  }

  ngOnDestroy() {
    super.unsubcripeAll();
  }

  //#region Methods

  public loadDetails(id: number): void {
    if (isNaN(id) === true || this.loadingDetailsInProgress == true) { return; }

    this.loadingDetailsInProgress = true;

    this._service.loadRequestDetails(id).pipe(finalize(() => {
      this.loadingDetailsInProgress = false;
    })).subscribe((result) => {
      this.details = result;
    }, (err) => {
      this._snackBar.open('Details zur Anfrage konnten nicht geladen werden', 'Fehler', { duration: 4500 });
    });
  }

  public createConnectBuildingJob(): void {
    const id = +this._activeRoute.snapshot.params['id'];
    if (isNaN(id) === true || this.createJobInProgress == true) { return; }

    this.createJobInProgress = true;
    this._snackBar.open('Hausanschluss Auftrag wird erstellt', 'In Bearbeitung', { duration: -1 });

    this._service.createBuildingConnenctionJob(id).pipe(finalize(() => {
      this.createJobInProgress = false;
    })).subscribe((result) => {
      this._snackBar.open('Auftrag wurde erfolgreich erstellt', 'Erfolg', { duration: 4500 });
      const url = this._service.getUrlByJobType(JobTypes.HouseConnenction);
      this._router.navigate([url, result]);
    }, (err) => {
      this._snackBar.open('Auftrag konnte nicht erstellt werden', 'Fehler', { duration: 4500 });
    });
  }

  public createJob(): void {
    const id = +this._activeRoute.snapshot.params['id'];
    if (isNaN(id) === true || this.createJobInProgress == true) { return; }

    this.createJobInProgress = true;
    this._snackBar.open('Auftrag wird erstellt', 'In Bearbeitung', { duration: -1 });

    this._service.createJobByRequestId(id).pipe(finalize(() => {
      this.createJobInProgress = false;
    })).subscribe((result) => {
      this._snackBar.open('Auftrag wurde erfolgreich erstellt', 'Erfolg', { duration: 4500 });
      const url = this._service.getUrlByJobType(result.jobType)
      this._router.navigate([url, result.id]);
    }, (err) => {
      this._snackBar.open('Auftrag konnte nicht erstellt werden', 'Fehler', { duration: 4500 });
    });
  }

  public closeRequest(): void {
    const id = +this._activeRoute.snapshot.params['id'];
    if (isNaN(id) === true || this.deleteInProgress == true) { return; }

    this.deleteInProgress = true;
    this._snackBar.open('Auftrag wird anbelehnt', 'In Bearbeitung', { duration: -1 });

    this._service.deleteRequestById(id).pipe(finalize(() => {
      this.deleteInProgress = false;
    })).subscribe((result) => {
      this._snackBar.open('Auftrag wurde abgelehnt erstellt', 'Erfolg', { duration: 4500 });
      this._router.navigate(['/member/customer-request-overview']);
    }, (err) => {
      this._snackBar.open('Auftrag konnte nicht abgelehnt werden', 'Fehler', { duration: 4500 });
    });
  }

  public createConnectBuildingJobPossible(): boolean {
    return this.details && this.details.isInProgress === false &&
      (!this.details.mostRecentJob ||
        (this.details.mostRecentJob.jobType === JobTypes.BranchOff));
  }

  public createInjectJobPossible(): boolean {
    return this.details && this.details.isInProgress === false && this.details.mostRecentJob && this.details.onlyHouseConnection == false &&
      this.details.mostRecentJob.state === JobStates.Acknowledged && this.details.mostRecentJob.jobType === JobTypes.HouseConnenction;
  }

  public createSpliceJobPossible(): boolean {
    return this.details && this.details.isInProgress === false && this.details.mostRecentJob && this.details.onlyHouseConnection == false &&
      this.details.mostRecentJob.state === JobStates.Acknowledged && this.details.mostRecentJob.jobType === JobTypes.Inject;
  }

  public createActivationJobPossible(): boolean {
    return this.details && this.details.isInProgress === false && this.details.mostRecentJob && this.details.onlyHouseConnection == false &&
      this.details.mostRecentJob.state === JobStates.Acknowledged && this.details.mostRecentJob.jobType === JobTypes.SpliceInBranchable;
  }

  public getJobLink(): string {
    if (this.details && this.details.mostRecentJob) {
      return this._service.getUrlByJobType(this.details.mostRecentJob.jobType);
    }
  }

  //#endregion
}
