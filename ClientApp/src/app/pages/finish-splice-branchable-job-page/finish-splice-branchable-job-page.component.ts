import { Component, OnInit, OnDestroy } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';
import { FinishJobPageBase } from '../base/finish-job-page-base';
import { MatDialog, MatSnackBar } from '@angular/material';
import { FormBuilder, Validators } from '@angular/forms';
import { JobService } from '../../services/job.service';
import { Router, ActivatedRoute } from '@angular/router';
import { UploadService } from '../../services/upload/upload.service';
import { Observable } from 'rxjs';
import { FormErrorMessage } from '../../models/helper/form-error-message';
import { IFinishSpliceBranchableJobModel } from '../../models/service/jobs/splice/ifinish-splice-branchable-job-model';
import { FinishSpliceBranchableJobErrorMessage } from '../../models/error-messages/finish-splice-branchable-job-error-message';

@Component({
  selector: 'besa-finish-splice-branchable-job-page',
  templateUrl: './finish-splice-branchable-job-page.component.html',
  styleUrls: ['./finish-splice-branchable-job-page.component.scss'],
  animations: fuseAnimations
})
export class FinishSpliceBranchableJobPageComponent extends FinishJobPageBase<IFinishSpliceBranchableJobModel> implements OnInit, OnDestroy {

  constructor(
    dialog: MatDialog,
    fb: FormBuilder,
    snackbar: MatSnackBar,
    jobService: JobService,
    router: Router,
    uploadService: UploadService,
    activeRoute: ActivatedRoute,
  ) {
    super(dialog, fb, snackbar, jobService, router, uploadService, activeRoute);
  }

  ngOnInit() {
    this.filesNeeded = true;

    super.initFinishForm({
      cableMetric: [0.0, [Validators.required, Validators.min(0)]],
    });
  }

  ngOnDestroy() {
    super.unsubcripeAll();
  }

  //#region Methods

  protected checkIfModelIsValid(model: IFinishSpliceBranchableJobModel): boolean {
    return true;
  }

  protected getfinishJobInvoker(model: IFinishSpliceBranchableJobModel): Observable<number> {
    return this._jobService.finishSpliceBranchableJob(model);
  }

  protected getJobUrl(): string {
    return '/member/splice-branchable-job';
  }

  protected getErrorMessages(): FormErrorMessage[] {
    return FinishSpliceBranchableJobErrorMessage;
  }

  //#endregion

}
