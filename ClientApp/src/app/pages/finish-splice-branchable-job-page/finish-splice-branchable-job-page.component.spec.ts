import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FinishSpliceBranchableJobPageComponent } from './finish-splice-branchable-job-page.component';

describe('FinishSpliceBranchableJobPageComponent', () => {
  let component: FinishSpliceBranchableJobPageComponent;
  let fixture: ComponentFixture<FinishSpliceBranchableJobPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FinishSpliceBranchableJobPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FinishSpliceBranchableJobPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
