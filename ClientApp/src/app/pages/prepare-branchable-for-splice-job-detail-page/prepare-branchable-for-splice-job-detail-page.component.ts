import { Component, OnInit, OnDestroy } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';
import { JobDetailsPageBase } from '../base/job-details-page-base';
import { IFinishPrepareBranchableForSpliceJobDetailModel } from '../../models/service/jobs/splice/ifinish-prepare-branchable-for-splice-job-detail-model';
import { IPrepareBranchableForSpliceJobDetailModel } from '../../models/service/jobs/splice/iprepare-branchable-for-splice-job-detail-model';
import { RefreshManagerService } from '../../services/refresh-manager.service';
import { ActivatedRoute, Router } from '@angular/router';
import { MatSnackBar, MatDialog, MatTableDataSource } from '@angular/material';
import { LoginService } from '../../services/login.service';
import { UriService } from '../../services/uri.service';
import { JobService } from '../../services/job.service';
import { Observable } from 'rxjs';
import { ISpliceEntryModel } from '../../models/service/jobs/splice/isplice-entry-model';

@Component({
  selector: 'besa-prepare-branchable-for-splice-job-detail-page',
  templateUrl: './prepare-branchable-for-splice-job-detail-page.component.html',
  animations: fuseAnimations,
  styleUrls: ['./prepare-branchable-for-splice-job-detail-page.component.scss']
})
export class PrepareBranchableForSpliceJobDetailPageComponent extends
  JobDetailsPageBase<IFinishPrepareBranchableForSpliceJobDetailModel, IPrepareBranchableForSpliceJobDetailModel>
  implements OnInit, OnDestroy {



  public spliceDataSource: MatTableDataSource<ISpliceEntryModel>;

  constructor(refreshManager: RefreshManagerService,
    activeRoute: ActivatedRoute,
    snackBar: MatSnackBar,
    dialog: MatDialog,
    loginService: LoginService,
    router: Router,
    uriService: UriService,
    service: JobService,
  ) {
    super(refreshManager, activeRoute, snackBar, dialog, loginService, router, uriService, service);
  }

  ngOnInit() {
    super.loadDetailsInternal();
  }

  ngOnDestroy() {
    super.unsubcripeAll();
  }

  //#region Methods

  protected afterDetailsLoaded(details: IPrepareBranchableForSpliceJobDetailModel): void {
    if (details && details.splices) {
      this.spliceDataSource = new MatTableDataSource<ISpliceEntryModel>(details.splices);
    }
  }

  protected getDetails(id: number): Observable<IPrepareBranchableForSpliceJobDetailModel> {
    return this._service.loadPrepareBranchableJobDetails(id);
  }

  protected getFinishUrl(): string {
    return '/member/finish-prepare-branchable-job';
  }

  //#endregion



}