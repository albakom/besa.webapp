import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrepareBranchableForSpliceJobDetailPageComponent } from './prepare-branchable-for-splice-job-detail-page.component';

describe('PrepareBranchableForSpliceJobDetailPageComponent', () => {
  let component: PrepareBranchableForSpliceJobDetailPageComponent;
  let fixture: ComponentFixture<PrepareBranchableForSpliceJobDetailPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrepareBranchableForSpliceJobDetailPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrepareBranchableForSpliceJobDetailPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
