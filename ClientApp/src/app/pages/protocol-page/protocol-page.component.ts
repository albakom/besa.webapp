import { fuseAnimations } from './../../../@fuse/animations/index';
import { RefreshManagerService } from './../../services/refresh-manager.service';
import { FormBuilder } from '@angular/forms';
import { MessageActions } from './../../models/service/messages/message-actions.enum';
import { ProtocolFilterModel } from './../../models/service/messages/protocol-filter-model';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { MessageService } from '../../services/messages/message.service';
import { ProtocolModel } from '../../models/service/messages/protocol-model';
import { MatSnackBar } from '@angular/material';
import { finalize, debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { FormBasedPage } from '../base/form-based-page';
import { FormErrorMessage } from '../../models/helper/form-error-message';
import { ProtocolFilterFormErrorMessage } from '../../models/error-messages/protocol-filter-form-error-message';
import { MessageRelatedObjectTypes } from '../../models/service/tasks/message-related-object-types.enum';

@Component({
  selector: 'besa-protocol-page',
  templateUrl: './protocol-page.component.html',
  styleUrls: ['./protocol-page.component.scss'],
  animations: fuseAnimations,
})
export class ProtocolPageComponent extends FormBasedPage implements OnInit, OnDestroy {

  //#region  Fields

  private _itemPerRequest = 100;

  //#endregion

  //#region   Properties

  public loadingDataInProgress: boolean;
  public entries: ProtocolModel[];
  public filter: ProtocolFilterModel;
  public possibleActions: MessageActions[];
  public possibleTypes: MessageRelatedObjectTypes[];

  public loadingMoreDataInProgress: boolean;

  //#endregion

  //#region Constructor

  constructor(
    private _service: MessageService,
    private _snackBar: MatSnackBar,
    _refreshManager: RefreshManagerService,
    private _fb: FormBuilder
  ) {
    super();

    this.filter = {
      startTime: null,
      endTime: null,
      actions: new Array(),
      types: new Array(),
      amount: this._itemPerRequest,
      start: 0
    };

    this.entries = new Array();

    this.possibleActions = this._service.getPossibleMessageActions();
    this.possibleTypes = this._service.getPossibleMessageRelatedTypes();

    console.log(this.possibleActions);

    super.initForm(this._fb.group({
      startTime: [null, []],
      endTime: [null, []],
      actions: [new Array(), []],
      types: [new Array(), []],
      amount: [this._itemPerRequest, []],
    }));

    this.form.valueChanges.pipe(
      debounceTime(350),
      distinctUntilChanged(),
      switchMap((value: ProtocolFilterModel) => {
        this.filter = value;
        this.filter.start = this.entries.length;
        this.loadingDataInProgress = true;
        return this._service.getProtocol(value).pipe(
          finalize(() => {
            this.loadingDataInProgress = false;
          })
        );
      }),
    ).subscribe((items: ProtocolModel[]) => {
      this.entries = items;
    }, (err) => {
      this._snackBar.open('Fehler beim Laden der Prokolleinträge', 'Fehler', { duration: 4500 });
    });

    _refreshManager.refreshRequested.subscribe((input) => this.loadData());
  }

  ngOnInit() {
    this.loadData()
  }

  ngOnDestroy() {
    super.unsubcripeAll();
  }

  //#endregion

  loadData() {
    this.loadingDataInProgress = true;

    this._service.getProtocol(this.filter).pipe(finalize(() => {
      this.loadingDataInProgress = false;
    })).subscribe((result) => {
      this.entries = result;
    }, (err) => {
      this._snackBar.open('Fehler beim Laden der Prokolleinträge', 'Fehler', { duration: 4500 });
    })
  }

  protected getErrorMessages(): FormErrorMessage[] {
    return ProtocolFilterFormErrorMessage;
  }

  public resetFilter() {
    this.form.patchValue({
      startTime: null,
      endTime: null,
      actions: new Array(),
      types: new Array(),
      amount: this._itemPerRequest,
    });
  }

  public filterItems() {
    this.loadingDataInProgress = true;

    this.filter = this.form.value;
    this.loadData();
  }

  public onScroll() {
    if (this.loadingMoreDataInProgress === true) { return; }

    this.filter.start = this.entries.length;
    this.loadingMoreDataInProgress = true;

    this._service.getProtocol(this.filter).pipe(finalize(() => {
      this.loadingMoreDataInProgress = false;
    })).subscribe((result) => {
      this.entries.push(...result);
    }, (err) => {
      this._snackBar.open('Protokolle wurden nicht gelanden', 'Fehler', { duration: 4500 });
    });
  }

}
