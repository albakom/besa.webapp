import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProtocolPageComponent } from './protocol-page.component';

describe('ProtocolPageComponent', () => {
  let component: ProtocolPageComponent;
  let fixture: ComponentFixture<ProtocolPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProtocolPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProtocolPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
