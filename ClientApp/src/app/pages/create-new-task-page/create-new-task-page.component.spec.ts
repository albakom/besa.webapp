import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateNewTaskPageComponent } from './create-new-task-page.component';

describe('CreateNewTaskPageComponent', () => {
  let component: CreateNewTaskPageComponent;
  let fixture: ComponentFixture<CreateNewTaskPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateNewTaskPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateNewTaskPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
