import { RefreshManagerService } from './../../services/refresh-manager.service';
import { SubscriptionAwareComponent } from './../../helper/subscription-aware-component';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { ConstructionStageService } from '../../services/construction-stage.service';
import { finalize } from 'rxjs/operators';
import { MatSnackBar } from '@angular/material';
import { fuseAnimations } from '@fuse/animations';
import { IConstructionStageForProjectManagementOverviewModel } from '../../models/service/construction-stage/iconstruction-stage-for-project-management-overview-model';

@Component({
  selector: 'besa-create-new-task-page',
  templateUrl: './create-new-task-page.component.html',
  styleUrls: ['./create-new-task-page.component.scss'],
  animations: fuseAnimations,
})
export class CreateNewTaskPageComponent extends SubscriptionAwareComponent implements OnInit, OnDestroy {

  //#region Properties

  public loadingConstructionStages: boolean;
  public constructionStages: IConstructionStageForProjectManagementOverviewModel[];

  public createTaskInProgress: boolean;


  //#endregion

  constructor(private _refreshManager: RefreshManagerService,
    private _snachBar: MatSnackBar,
    private _constructionStageService: ConstructionStageService) {
    super();

    super.addSubscription(_refreshManager.refreshRequested.subscribe((result) => {
      this.loadConstructionStages();
    }));
  }

  ngOnInit() {
    this.loadConstructionStages();
  }

  ngOnDestroy() {
    super.unsubcripeAll();
  }

  //#region Methods

  public loadConstructionStages() {
    this.loadingConstructionStages = true;

    this._constructionStageService.loadOverviewForProjectManagement().pipe(finalize(() => {
      this.loadingConstructionStages = false;
    })).subscribe((result) => {
      this.constructionStages = result;
    }, (err) => {
      this._snachBar.open('Bauabschnitte konnte nicht geladen werden', 'Fehler', { duration: 4500 });
    });
  }

  public createBranchOffJobs(overview: IConstructionStageForProjectManagementOverviewModel): void {

    if (this.createTaskInProgress === true) { return; }

    this.createTaskInProgress = true;
    this._snachBar.open('Aufgaben werden erstellt', 'In Bearbeitung', { duration: -1 });

    this._constructionStageService.createBranchOffJobs(overview.id).pipe(finalize(() => {
      this.createTaskInProgress = false;
    })).subscribe((result) => {
      this._snachBar.dismiss();
      if (result === true) {
        this._snachBar.open('Aufgaben erfolgreich erstellt', 'Erfolg', { duration: 4500 });
      } else {
        this._snachBar.open('Vorstreckersaufgabe konnte nicht erstellt werden', 'Fehler', { duration: 4500 });
      }
    }, (err) => {
      this._snachBar.dismiss();
      this._snachBar.open('Vorstreckersaufgabe konnte nicht erstellt werden', 'Fehler', { duration: 4500 });
    });
  }

  //#endregion

}
