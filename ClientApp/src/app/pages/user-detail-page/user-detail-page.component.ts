import { LoginService } from './../../services/login.service';
import { BesaRoles } from './../../models/service/besa-roles.enum';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from './../../services/user/user.service';
import { SubscriptionAwareComponent } from './../../helper/subscription-aware-component';
import { Component, OnInit } from '@angular/core';
import { IUserEditModel } from '../../models/service/user/iuser-edit-model';
import { MatSnackBar, MatDialog } from '@angular/material';
import { finalize } from 'rxjs/operators';
import { ISimpleDialogOptions } from '../../dialogs/simple-dialog/isimple-dialog-options';
import { SimpleDialogComponent } from '../../dialogs/simple-dialog/simple-dialog.component';
import { fuseAnimations } from '@fuse/animations';

@Component({
  selector: 'besa-user-detail-page',
  templateUrl: './user-detail-page.component.html',
  styleUrls: ['./user-detail-page.component.scss'],
  animations: fuseAnimations
})
export class UserDetailPageComponent extends SubscriptionAwareComponent implements OnInit {

  private _id: number;
  public loadingDataInProgress: boolean;
  public user: IUserEditModel;
  public roles: BesaRoles[];
  private _possibleRoles: BesaRoles[];

  constructor(
    private _service: UserService,
    private _router: Router,
    private _activeRoute: ActivatedRoute,
    private _snackBar: MatSnackBar,
    private _dialog: MatDialog,
    private _loginService: LoginService
  ) {
    super();

    this._possibleRoles = this._loginService.possibleRoles;
    this.roles = new Array();
   }

  ngOnInit() {
    this.loadData();
  }

  public loadData() {
    this._id = +this._activeRoute.snapshot.paramMap.get('id');
    this.loadingDataInProgress = true;
    this._service.getUser(this._id).pipe(finalize(() => {
      this.loadingDataInProgress = false;
    })).subscribe((result) => {
      this.user = result;
      this._possibleRoles.forEach(role => {
        if ((this.user.role & role) === role) {
          this.roles.push(role);
        }
      });
    }, (err) => {
      this._snackBar.open('Die ausstehende Anfrage konnte nicht bearbeitet werden', 'Fehler', { duration: 4500 });
    });
  }

  public navigateBack() {
    this._router.navigate(['/member/users-overview']);
  }

  public editUser() {
    this._router.navigate(['member/users-edit', this._id]);
  }

  public deleteUser() {
    const dialogOptions: ISimpleDialogOptions = {
      title: 'Löschen bestätigen',
      question: 'Möchten Sie wirklich löschen?'
    };

    const dialogRef = this._dialog.open(SimpleDialogComponent, {
      data: dialogOptions
    });

    dialogRef.afterClosed().subscribe((result: boolean) => {
      if (result === true) {
        this.loadingDataInProgress = true;
        this._snackBar.open('Der Benutzer wird gelöscht', 'In Bearbeitung', { duration: -1 });
        this._service.deleteUser(this._id).pipe(finalize(() => {
          this.loadingDataInProgress = false;
          this._router.navigate(['/member/users-overview']);
        })).subscribe(
          (result) => {
            if (result)
              this._snackBar.open('Der Benutzer wurde gelöscht.', 'Erfolg', { duration: 4500 });
          }, (err) => {
            this._snackBar.open('Der Benutzer konnte nicht gelöscht werden', 'Fehler', { duration: 4500 });
          }
          );
      }
    })
  }

}
