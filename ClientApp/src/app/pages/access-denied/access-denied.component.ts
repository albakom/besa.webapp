import { Component, OnInit } from '@angular/core';
import { NonMemberPageBase } from '../base/non-member-page-base';
import { FuseConfigService } from '@fuse/services/config.service';

@Component({
  selector: 'besa-access-denied',
  templateUrl: './access-denied.component.html',
  styleUrls: ['./access-denied.component.scss']
})
export class AccessDeniedComponent extends NonMemberPageBase implements OnInit {

  constructor(
    fuseConfigService: FuseConfigService,
  ) {
    super(fuseConfigService);
    super.setDefaultLayout()
  }

  ngOnInit() {
  }

}
