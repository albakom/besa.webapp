import { Component, OnInit } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';

@Component({
  selector: 'besa-convert-job-page',
  templateUrl: './convert-job-page.component.html',
  animations: fuseAnimations,
  styleUrls: ['./convert-job-page.component.scss']
})
export class ConvertJobPageComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
