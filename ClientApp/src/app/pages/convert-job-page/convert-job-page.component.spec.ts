import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConvertJobPageComponent } from './convert-job-page.component';

describe('ConvertJobPageComponent', () => {
  let component: ConvertJobPageComponent;
  let fixture: ComponentFixture<ConvertJobPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConvertJobPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConvertJobPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
