import { JobService } from './../../services/job.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { SubscriptionAwareComponent } from '../../helper/subscription-aware-component';
import { fuseAnimations } from '@fuse/animations';
import { RefreshManagerService } from '../../services/refresh-manager.service';
import { ActivatedRoute, Router } from '@angular/router';
import { MatSnackBar } from '@angular/material';
import { finalize } from 'rxjs/operators';
import { ISimpleJobOverview } from '../../models/service/jobs/isimple-job-overview';
import { JobTypes } from '../../models/service/jobs/job-types.enum';
import { ICollectionJobDetailsModel } from '../../models/service/jobs/icollection-job-details-model';
import { JobStates } from '../../models/service/jobs/job-states.enum';

@Component({
  selector: 'besa-collection-job-details-page',
  templateUrl: './collection-job-details-page.component.html',
  styleUrls: ['./collection-job-details-page.component.scss'],
  animations: fuseAnimations,
})
export class CollectionJobDetailsPageComponent extends SubscriptionAwareComponent implements OnInit, OnDestroy {

  //#region Properties

  public loadingDetailsInProgress: boolean;
  public details: ICollectionJobDetailsModel;

  //#endregion

  constructor(refreshManager: RefreshManagerService,
    activeRoute: ActivatedRoute,
    private _router: Router,
    private _snackBar: MatSnackBar,
    private _service: JobService,
  ) {
    super();

    super.addSubscription(refreshManager.refreshRequested.subscribe((result) => {
      this.loadDetails(+activeRoute.snapshot.params['id']);
    }));

    super.addSubscription(activeRoute.params.subscribe((params) => {
      this.loadDetails(+params['id']);
    }))
  }

  ngOnInit() {
  }

  ngOnDestroy() {
    super.unsubcripeAll();
  }

  //#region Methods

  public loadDetails(id: number): void {
    if (isNaN(id) === true || this.loadingDetailsInProgress == true) { return; }

    this.loadingDetailsInProgress = true;

    this._service.loadCollectionJobDetails(id).pipe(finalize(() => {
      this.loadingDetailsInProgress = false;
    })).subscribe((result) => {
      this.details = result;
    }, (err) => {
      this._snackBar.open('Details zur Sammelaufgabe konnten nicht geladen werden', 'Fehler', { duration: 4500 });
    });
  }

  public navigateToJob(job: ISimpleJobOverview): void {
    if (!job) { return; }

    const url = this._service.getUrlByJob(job);
    this._router.navigate([url, job.id]);
  }

  public getJobType(): JobTypes {
    if (this.details && this.details.jobs.length > 0) {
      return this.details.jobs[0].jobType;
    } else { return null; }
  }

  public getFinishedJobs(): number {
    if (!this.details.jobs || this.details.jobs.length == 0) { return 0; }
    else {
      let result = 0;
      this.details.jobs.forEach(job => {
        if (job.state === JobStates.Acknowledged || job.state === JobStates.Finished) {
          result += 1;
        }
      });
      return result;
    }
  }

  //#endregion

}
