import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CollectionJobDetailsPageComponent } from './collection-job-details-page.component';

describe('CollectionJobDetailsPageComponent', () => {
  let component: CollectionJobDetailsPageComponent;
  let fixture: ComponentFixture<CollectionJobDetailsPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CollectionJobDetailsPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CollectionJobDetailsPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
