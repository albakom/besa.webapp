import { Component, OnInit } from '@angular/core';
import { NonMemberPageBase } from '../base/non-member-page-base';
import { FuseConfigService } from '@fuse/services/config.service';

@Component({
  selector: 'besa-not-found',
  templateUrl: './not-found.component.html',
  styleUrls: ['./not-found.component.scss']
})
export class NotFoundComponent extends NonMemberPageBase implements OnInit {

  constructor(
    fuseConfig: FuseConfigService,
  ) {
    super(fuseConfig);
    super.setDefaultLayout();
  }

  ngOnInit() {
  }
}

