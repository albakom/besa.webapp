import { Component, OnInit, OnDestroy } from '@angular/core';
import { IInejectJobOverviewModel } from '../../models/service/jobs/inject/iinject-job-overview-model';
import { ConstrutionStageJobDetailBasePage } from '../base/constrution-stage-job-detail-base-page';
import { MatSnackBar } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { RefreshManagerService } from '../../services/refresh-manager.service';
import { ConstructionStageService } from '../../services/construction-stage.service';
import { fuseAnimations } from '@fuse/animations';
import { Observable } from 'rxjs';
import { IConstructionStageDetailModel } from '../../models/service/construction-stage/iconstruction-stage-detail-for-model';
import { IJobOverviewModel } from '../../models/service/jobs/ijobs-overview-model';

@Component({
  selector: 'besa-inject-construction-stage-detail-page',
  templateUrl: './inject-construction-stage-detail-page.component.html',
  styleUrls: ['./inject-construction-stage-detail-page.component.scss'],
  animations: fuseAnimations,
})
export class InjectConstructionStageDetailPageComponent extends ConstrutionStageJobDetailBasePage<IInejectJobOverviewModel> implements OnInit, OnDestroy {


  //#region Constructor

  constructor(
    refreshManager: RefreshManagerService,
    activeRoute: ActivatedRoute,
    router: Router,
    snackBar: MatSnackBar,
    private _service: ConstructionStageService,
  ) {
    super(refreshManager, activeRoute, router, snackBar);

    this.initDone = true;
  }

  ngOnInit() {
    super.loadDetailsInternal();
  }

  ngOnDestroy() {
    super.unsubcripeAll();
  }

  //#endregion

  //#region Methods

  protected loadDetailsFromService(id: number, onlyUnboundend: boolean): Observable<IConstructionStageDetailModel<IJobOverviewModel>> {
    return this._service.loadDetailsForInjectJobs(id, onlyUnboundend);
  }

  protected getNavigationPathForDetails(job: IInejectJobOverviewModel): string {
    return '/member/inject-job';
  }

  //#endregion

}
