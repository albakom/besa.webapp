import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InjectConstructionStageDetailPageComponent } from './inject-construction-stage-detail-page.component';

describe('InjectConstructionStageDetailPageComponent', () => {
  let component: InjectConstructionStageDetailPageComponent;
  let fixture: ComponentFixture<InjectConstructionStageDetailPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InjectConstructionStageDetailPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InjectConstructionStageDetailPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
