import { Router } from '@angular/router/';
import { finalize } from 'rxjs/operators';
import { MatSnackBar } from '@angular/material';
import { ActivatedRoute } from '@angular/router';
import { RefreshManagerService } from './../../services/refresh-manager.service';
import { SubscriptionAwareComponent } from './../../helper/subscription-aware-component';
import { OnDestroy } from '@angular/core/src/metadata/lifecycle_hooks';
import { Component, OnInit } from '@angular/core';
import { ConstructionStageService } from '../../services/construction-stage.service';
import { fuseAnimations } from '@fuse/animations';
import { ISimpleBuildingConnenctionJobOverview } from '../../models/service/jobs/overview/isimple-building-connenction-job-overview';
import { BuildingConnenctionTypes } from '../../models/service/jobs/building-connenction-types.enum';
import { IConstructionStageDetailModel } from '../../models/service/construction-stage/iconstruction-stage-detail-for-model';

@Component({
  selector: 'besa-cilvil-work-detail-page',
  templateUrl: './cilvil-work-detail-page.component.html',
  styleUrls: ['./cilvil-work-detail-page.component.scss'],
  animations: fuseAnimations,
})
export class CilvilWorkDetailPageComponent extends SubscriptionAwareComponent implements OnInit, OnDestroy {

  //#region Properties

  public loadingDetailsInProgress: boolean;
  public details: IConstructionStageDetailModel<ISimpleBuildingConnenctionJobOverview>;

  //#endregion

  constructor(refreshManager: RefreshManagerService,
    activeRoute: ActivatedRoute,
    private _router: Router,
    private _snackBar: MatSnackBar,
    private _service: ConstructionStageService,
  ) {
    super();

    super.addSubscription(refreshManager.refreshRequested.subscribe((result) => {
      this.loadDetails(+activeRoute.snapshot.params['id']);
    }));

    super.addSubscription(activeRoute.params.subscribe((params) => {
      this.loadDetails(+params['id']);
    }))
  }

  ngOnInit() {
  }

  ngOnDestroy() {
    super.unsubcripeAll();
  }

  //#region Methods

  public loadDetails(id: number): void {
    if (isNaN(id) === true || this.loadingDetailsInProgress == true) { return; }

    this.loadingDetailsInProgress = true;

    this._service.loadDetailsForCivilWork(id, false).pipe(finalize(() => {
      this.loadingDetailsInProgress = false;
    })).subscribe((result) => {
      this.details = result;
    }, (err) => {
      this._snackBar.open('Details zum Bauabschnitt konnten nicht geladen werden', 'Fehler', { duration: 4500 });
    });
  }

  public navigateToJob(job: ISimpleBuildingConnenctionJobOverview): void {
    if (!job) { return; }

    let url = '/member/branch-off-job';
    if (job.connectionJobType === BuildingConnenctionTypes.BuildingConnection) {
      url = '/member/connection-building-job';
    }

    this._router.navigate([url, job.jobId]);
  }

  //#endregion

}
