import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CilvilWorkDetailPageComponent } from './cilvil-work-detail-page.component';

describe('CilvilWorkDetailPageComponent', () => {
  let component: CilvilWorkDetailPageComponent;
  let fixture: ComponentFixture<CilvilWorkDetailPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CilvilWorkDetailPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CilvilWorkDetailPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
