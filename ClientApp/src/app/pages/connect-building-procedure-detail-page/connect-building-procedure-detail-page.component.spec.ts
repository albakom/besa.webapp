import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConnectBuildingProcedureDetailPageComponent } from './connect-building-procedure-detail-page.component';

describe('ConnectBuildingProcedureDetailPageComponent', () => {
  let component: ConnectBuildingProcedureDetailPageComponent;
  let fixture: ComponentFixture<ConnectBuildingProcedureDetailPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConnectBuildingProcedureDetailPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConnectBuildingProcedureDetailPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
