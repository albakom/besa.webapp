import { ProcedureService } from './../../services/procedure.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { SubscriptionAwareComponent } from '../../helper/subscription-aware-component';
import { fuseAnimations } from '@fuse/animations';
import { IBuildingConnectionProcedureDetailModel } from '../../models/service/procedure/ibuilding-connection-procedure-detail-model';
import { ActivatedRoute, Router } from '@angular/router';
import { MatSnackBar } from '@angular/material';
import { RefreshManagerService } from '../../services/refresh-manager.service';
import { finalize } from 'rxjs/operators';
import { JobService } from '../../services/job.service';
import { IProcedureTimelineElementDetailModel } from '../../models/service/procedure/iprocedure-timeline-element-detail-model';

@Component({
  selector: 'besa-connect-building-procedure-detail-page',
  templateUrl: './connect-building-procedure-detail-page.component.html',
  animations: fuseAnimations,
  styleUrls: ['./connect-building-procedure-detail-page.component.scss']
})
export class ConnectBuildingProcedureDetailPageComponent extends SubscriptionAwareComponent implements OnInit, OnDestroy {

  private _id: number;
  public loadingDataInProgress: boolean;
  public data: IBuildingConnectionProcedureDetailModel;

  constructor(
    private _router: Router,
    private _activeRoute: ActivatedRoute,
    private _service: ProcedureService,
    private _snackBar: MatSnackBar,
    private _jobService: JobService,
    private _refreshManager: RefreshManagerService,
  ) {
    super();

    super.addSubscription(_refreshManager.refreshRequested.subscribe((result) => { this.loadData() }));
  }

  ngOnInit() {
    this.loadData();
  }

  ngOnDestroy() {
    super.unsubcripeAll();
  }

  public loadData() {
    this._id = +this._activeRoute.snapshot.paramMap.get('id');
    this.loadingDataInProgress = true;
    this._service.getConnectBuildingProcedureDetails(this._id).pipe(finalize(() => {
      this.loadingDataInProgress = false;
    })).subscribe((result) => {
      this.data = result;
    }, (err) => {
      this._snackBar.open('Der Vorgang konnte nicht geladen werden', 'Fehler', { duration: 4500 });
    });
  }

  public navigateBack() {
    this._router.navigate(['/member/procedure-overview']);
  }
  
  public getJobLink(element: IProcedureTimelineElementDetailModel): string {
    if (element.relatedJobId && element.relatedJobType) {
      return this._jobService.getUrlByJobType(element.relatedJobType);
    } else {
      return '';
    }
  }
}

