import { LoginService } from './../../services/login.service';
import { BesaRoles } from './../../models/service/besa-roles.enum';
import { Component, OnInit } from '@angular/core';
import { RequestAccessService } from '../../services/request-access.service';
import { ActivatedRoute, Router } from '@angular/router';
import { MatSnackBar, MatCheckboxChange } from '@angular/material';
import { FormBasedPage } from '../base/form-based-page';
import { Validators, FormBuilder } from '@angular/forms';
import { GrantUserAccessFormMessage } from '../../models/error-messages/grant-user-access-form-message';
import { FormErrorMessage } from '../../models/helper/form-error-message';
import { ICompanieOverviewModel } from '../../models/service/icompanie-overview-model';
import { finalize } from 'rxjs/operators';
import { IUserAccessRequestOverviewModel } from '../../models/service/user/iuser-access-request-overview-model';
import { IGrantUserAccessModel } from '../../models/service/user/igrant-user-access-model';

@Component({
  selector: 'besa-grant-user-access-page',
  templateUrl: './grant-user-access-page.component.html',
  styleUrls: ['./grant-user-access-page.component.scss']
})
export class GrantUserAccessPageComponent extends FormBasedPage implements OnInit {

  //#region Properties

  public loadingCompaniesInProgress: boolean;
  public companies: ICompanieOverviewModel[];
  public request: IUserAccessRequestOverviewModel;
  public isRequestinInProgress: boolean;
  public possibleRoles: BesaRoles[];

  //#endregion

  //#region Constructor

  constructor(
    private _service: RequestAccessService,
    private _router: Router,
    private _loginService: LoginService,
    private _route: ActivatedRoute,
    private _snackBar: MatSnackBar,
    private _fb: FormBuilder,
  ) {
    super()

    this.possibleRoles = _loginService.possibleRoles;
  }

  //#endregion

  ngOnInit() {
    if (!this._service.requestModel) {
      this._snackBar.open('Es wurde keine Anfrage ausgewählt', 'Fehler', { duration: 4500 });
      this.navigateToOverview();
    } else {
      this.initializeFormValues();

      this.loadingCompaniesInProgress = true;
      this._service.loadCompanies().pipe(finalize(() => {
        this.loadingCompaniesInProgress = false;
      })).subscribe((result) => {
        this.companies = result;
      }, (err) => {
        this._snackBar.open('Beim Laden der Firmen ist ein Fehler aufgetreten ', 'Fehler', { duration: 4500 });
      });
    }
  }

  //#region Methods

  private navigateToOverview(): void {
    this._router.navigate(['../user-request-overview'], { relativeTo: this._route });
  }

  private initializeFormValues() {
    const value = this._service.requestModel;
    super.initForm(
      this._fb.group({
        authRequestId: [value.id, [Validators.required]],
        surname: [value.surname, [Validators.required, Validators.maxLength(64)]],
        lastname: [value.lastname, [Validators.required, Validators.maxLength(64)]],
        eMailAddress: [value.eMailAddress, [Validators.required, Validators.maxLength(255), Validators.email]],
        phone: [value.phone, [Validators.required, Validators.maxLength(32)]],
        companyId: [(value.companyInfo ? value.companyInfo.id : null), []],
        role: [0, [Validators.required, Validators.min(1)]]
      })
    );

    this.request = value;
  }

  protected getErrorMessages(): FormErrorMessage[] {
    return GrantUserAccessFormMessage;
  }

  public roleChanged(event: MatCheckboxChange, value: BesaRoles): void {
    const control = this.form.get('role');
    let currentBesaValue = +control.value;
    if (event.checked == true) {
      currentBesaValue |= value;
    } else {
      currentBesaValue ^= value;
    }

    control.setValue(currentBesaValue);
  }

  public grantAccess(): void {
    if (this.form.invalid === true) { return; }
    if (this.isRequestinInProgress === true) { return; }

    this.isRequestinInProgress = true;

    this._snackBar.open('Der Zugang wird gewährt...', 'In Bearbeitung', { duration: -1 });

    const model: IGrantUserAccessModel = this.form.value;

    this._service.grantAccess(model).subscribe(
      (result) => {
        this._snackBar.dismiss();
        this._snackBar.open('Der Zugang wurde gewährt', 'Fertig', { duration: 4500 });
        this.navigateToOverview();
      }, (err) => {
        this._snackBar.dismiss();
        this._snackBar.open('Der Zugang konnt nicht gewährt werden', 'Fehler', { duration: 4500 });
      }
    );
  }

  //#endregion

}
