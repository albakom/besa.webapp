import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GrantUserAccessPageComponent } from './grant-user-access-page.component';

describe('GrantUserAccessPageComponent', () => {
  let component: GrantUserAccessPageComponent;
  let fixture: ComponentFixture<GrantUserAccessPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GrantUserAccessPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GrantUserAccessPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
