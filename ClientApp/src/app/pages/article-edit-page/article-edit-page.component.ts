import { IArticleEditModel } from './../../models/service/article/iarticle-edit-model';
import {FormErrorMessage} from '../../models/helper/form-error-message';
import { Component, OnInit } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';
import { FormBasedPage } from '../base/form-based-page';
import { IArticleOverviewModel } from '../../models/service/article/iarticle-overview-model';
import { ArticleService } from '../../services/article/article.service';
import { Router, ActivatedRoute } from '@angular/router';
import { MatSnackBar, MatDialog } from '@angular/material';
import { FormBuilder, Validators } from '@angular/forms';
import { ArticleEditErrorMessage } from '../../models/error-messages/article-edit-error-message';
import { ISimpleDialogOptions } from '../../dialogs/simple-dialog/isimple-dialog-options';
import { SimpleDialogComponent } from '../../dialogs/simple-dialog/simple-dialog.component';

@Component({
  selector: 'besa-article-edit-page',
  templateUrl: './article-edit-page.component.html',
  styleUrls: ['./article-edit-page.component.scss'],
  animations: fuseAnimations
})
export class ArticleEditPageComponent extends FormBasedPage implements OnInit {
  
  private _id: number;
  public article: IArticleEditModel;
  public loadingDataInProgress: boolean;

  constructor(
    private _router: Router,
    private _activeRoute: ActivatedRoute,
    private _service: ArticleService,
    private _snackBar: MatSnackBar,
    private _fb: FormBuilder,
    private _dialog: MatDialog
  ) {
    super();
   }

  ngOnInit() {
    this.initializeFormValues();
    this.loadData();
  }

  initializeFormValues(): void {
    super.initForm(
      this._fb.group({
        name: ['', Validators.required],
        productSoldByMeter: [false, Validators.required]
      })
    );
  }

  protected getErrorMessages(): FormErrorMessage[] {
    return ArticleEditErrorMessage;
  }

  loadData(): void {
    this._id = +this._activeRoute.snapshot.paramMap.get('id');
    this.loadingDataInProgress = true;
    this._service.getArticleDetails(this._id).subscribe((result) => {
      this.article = result;
      if(this.article) {
        this.form.setValue({
          name: this.article.name,
          productSoldByMeter: this.article.productSoldByMeter
        });
      }
      this.loadingDataInProgress = false;
    })
  }

  editArticle(): void {
    const dialogOptions: ISimpleDialogOptions = {
      title: 'Speichern bestätigen',
      question: 'Möchten Sie wirklich speichern?'
    };

    const dialogRef = this._dialog.open(SimpleDialogComponent, {
      data: dialogOptions
    });

    dialogRef.afterClosed().subscribe((result: boolean) => {
      if (result === true) {
        const formValue: IArticleOverviewModel = this.form.value;

    const editedArticle: IArticleEditModel = {
      name: formValue.name,
      productSoldByMeter: formValue.productSoldByMeter,
      id: this.article.id
    };

    this._service.editArticle(editedArticle).pipe().subscribe(
      (result) => {
        this._snackBar.open('Die Änderung wurde verarbeitet', 'Erfolg', { duration: 4500 });
        this._router.navigate(['/member/article-overview']);
      },
      (err) => this._snackBar.open('Die ausstehende Änderung konnte nicht verarbeitet werden', 'Fehler', { duration: 4500 })
    );
      }
    });
  }

  cancelEdit(): void {
    this._router.navigate(['/member/article-overview']);
  }

}
