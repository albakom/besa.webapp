import { Router } from '@angular/router';
import { OnDestroy } from '@angular/core/src/metadata/lifecycle_hooks';
import { Component, OnInit } from '@angular/core';
import { SubscriptionAwareComponent } from '../../helper/subscription-aware-component';
import { fuseAnimations } from '@fuse/animations';
import { RefreshManagerService } from '../../services/refresh-manager.service';
import { JobService } from '../../services/job.service';
import { MatSnackBar } from '@angular/material';
import { finalize } from 'rxjs/operators';
import { IActivationJobOverviewModel } from '../../models/service/jobs/activation/iactivation-job-overview-model';

@Component({
  selector: 'besa-activation-job-overview-page',
  templateUrl: './activation-job-overview-page.component.html',
  animations: fuseAnimations,
  styleUrls: ['./activation-job-overview-page.component.scss']
})
export class ActivationJobOverviewPageComponent extends SubscriptionAwareComponent implements OnInit, OnDestroy {

  //#region  Properties

  public loadingDataInProgress: boolean;
  public items: IActivationJobOverviewModel[];

  //#endregion

  constructor(refreshManager: RefreshManagerService,
    private _jobService: JobService,
    private _snackBar: MatSnackBar,
    private _router: Router,
  ) {
    super();

    super.addSubscription(refreshManager.refreshRequested.subscribe((result) => { this.loadItems(); }))
  }

  ngOnInit() {
    this.loadItems();
  }

  ngOnDestroy() {
    super.unsubcripeAll();
  }

  //#region Methods

  public loadItems(): void {
    this.loadingDataInProgress = true;
    this._jobService.loadActivationJobsOverview().pipe(finalize(() => {
      this.loadingDataInProgress = false;
    })).subscribe((result) => {
      this.items = result;
    }, (err) => {
      this._snackBar.open('Fehler beim Laden der Aufträge', 'Fehler', { duration: 4500 });
    });
  }

  public navigateToDetails(item: IActivationJobOverviewModel): void {
    const url = '/member/activation-job-details';
    this._router.navigate([url, item.jobId]);
  }

  //#endregion
    
}
