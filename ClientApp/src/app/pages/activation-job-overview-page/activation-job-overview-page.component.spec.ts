import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActivationJobOverviewPageComponent } from './activation-job-overview-page.component';

describe('ActivationJobOverviewPageComponent', () => {
  let component: ActivationJobOverviewPageComponent;
  let fixture: ComponentFixture<ActivationJobOverviewPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActivationJobOverviewPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActivationJobOverviewPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
