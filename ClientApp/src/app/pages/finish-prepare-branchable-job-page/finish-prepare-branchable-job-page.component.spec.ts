import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FinishPrepareBranchableJobPageComponent } from './finish-prepare-branchable-job-page.component';

describe('FinishPrepareBranchableJobPageComponent', () => {
  let component: FinishPrepareBranchableJobPageComponent;
  let fixture: ComponentFixture<FinishPrepareBranchableJobPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FinishPrepareBranchableJobPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FinishPrepareBranchableJobPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
