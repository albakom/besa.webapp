import { Component, OnInit, OnDestroy } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';
import { FinishJobPageBase } from '../base/finish-job-page-base';
import { MatDialog, MatSnackBar } from '@angular/material';
import { FormBuilder } from '@angular/forms';
import { JobService } from '../../services/job.service';
import { Router, ActivatedRoute } from '@angular/router';
import { UploadService } from '../../services/upload/upload.service';
import { Observable } from 'rxjs';
import { FormErrorMessage } from '../../models/helper/form-error-message';
import { IFinishPrepareBranchableJobModel } from '../../models/service/jobs/splice/ifinish-prepare-branchable-job-model';
import { FinishPrepareBranchableJobErrorMessage } from '../../models/error-messages/finish-prepare-branchable-job-error-message';

@Component({
  selector: 'besa-finish-prepare-branchable-job-page',
  templateUrl: './finish-prepare-branchable-job-page.component.html',
  styleUrls: ['./finish-prepare-branchable-job-page.component.scss'],
  animations: fuseAnimations,
})
export class FinishPrepareBranchableJobPageComponent extends FinishJobPageBase<IFinishPrepareBranchableJobModel> implements OnInit, OnDestroy {

  constructor(
    dialog: MatDialog,
    fb: FormBuilder,
    snackbar: MatSnackBar,
    jobService: JobService,
    router: Router,
    uploadService: UploadService,
    activeRoute: ActivatedRoute,
  ) {
    super(dialog, fb, snackbar, jobService, router, uploadService, activeRoute);
   }

   ngOnInit() {
    this.filesNeeded = true;

    super.initFinishForm({
      
    });
  }

  ngOnDestroy() {
    super.unsubcripeAll();
  }

  //#region Methods

  protected checkIfModelIsValid(model: IFinishPrepareBranchableJobModel): boolean {
    return true;
  }

  protected getfinishJobInvoker(model: IFinishPrepareBranchableJobModel): Observable<number> {
    return this._jobService.finishPrepareBranchableJob(model);
  }
  
  protected getJobUrl(): string {
    return '/member/prepare-branchable-job';
  }

  protected getErrorMessages(): FormErrorMessage[] {
    return FinishPrepareBranchableJobErrorMessage;
  }

  //#endregion

}
