import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FinishInjectJobPageComponent } from './finish-inject-job-page.component';

describe('FinishInjectJobPageComponent', () => {
  let component: FinishInjectJobPageComponent;
  let fixture: ComponentFixture<FinishInjectJobPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FinishInjectJobPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FinishInjectJobPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
