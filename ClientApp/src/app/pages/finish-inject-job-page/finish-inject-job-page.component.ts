import {FormErrorMessage} from '../../models/helper/form-error-message';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';
import { FinishJobPageBase } from '../base/finish-job-page-base';
import { IFinishInjectJobModel } from '../../models/service/jobs/inject/ifinish-inject-job-model';
import { MatDialog, MatSnackBar } from '@angular/material';
import { FormBuilder, Validators } from '@angular/forms';
import { JobService } from '../../services/job.service';
import { Router, ActivatedRoute } from '@angular/router';
import { UploadService } from '../../services/upload/upload.service';
import { Observable } from 'rxjs';
import { FinishInjectJobErrorMessage } from '../../models/error-messages/finish-inject-job-error-message';

@Component({
  selector: 'besa-finish-inject-job-page',
  templateUrl: './finish-inject-job-page.component.html',
  styleUrls: ['./finish-inject-job-page.component.scss'],
  animations: fuseAnimations,
})
export class FinishInjectJobPageComponent extends FinishJobPageBase<IFinishInjectJobModel> implements OnInit, OnDestroy {

  constructor(dialog: MatDialog,
    fb: FormBuilder,
    snackbar: MatSnackBar,
    jobService: JobService,
    router: Router,
    uploadService: UploadService,
    activeRoute: ActivatedRoute,
  ) {
    super(dialog, fb, snackbar, jobService, router, uploadService, activeRoute);
  }

  ngOnInit() {
    this.filesNeeded = true;

    super.initFinishForm({
      length: [0.0, [Validators.required, Validators.min(1), Validators.max(4500)]],
    });
  }

  ngOnDestroy() {
    super.unsubcripeAll();
  }

  //#region Methods

  protected checkIfModelIsValid(model: IFinishInjectJobModel): boolean {
    return true;
  }

  protected getfinishJobInvoker(model: IFinishInjectJobModel): Observable<number> {
    return this._jobService.finishInjectJob(model);
  }
  
  protected getJobUrl(): string {
    return '/member/inject-job';
  }

  protected getErrorMessages(): FormErrorMessage[] {
    return FinishInjectJobErrorMessage;
  }

  //#endregion

}
