import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ArticleOverviewPageComponent } from './article-overview-page.component';

describe('ArticleOverviewPageComponent', () => {
  let component: ArticleOverviewPageComponent;
  let fixture: ComponentFixture<ArticleOverviewPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ArticleOverviewPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArticleOverviewPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
