import { ISimpleDialogOptions } from '../../dialogs/simple-dialog/isimple-dialog-options';
import { ArticleService } from './../../services/article/article.service';
import { IArticleOverviewModel } from './../../models/service/article/iarticle-overview-model';
import { fuseAnimations } from '@fuse/animations';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { SubscriptionAwareComponent } from '../../helper/subscription-aware-component';
import { MatSnackBar, MatDialog } from '@angular/material';
import { RefreshManagerService } from '../../services/refresh-manager.service';
import { Router, ActivatedRoute } from '@angular/router';
import { finalize } from 'rxjs/operators';
import { SimpleDialogComponent } from '../../dialogs/simple-dialog/simple-dialog.component';

@Component({
  selector: 'besa-article-overview-page',
  templateUrl: './article-overview-page.component.html',
  styleUrls: ['./article-overview-page.component.scss'],
  animations: fuseAnimations
})
export class ArticleOverviewPageComponent extends SubscriptionAwareComponent implements OnInit, OnDestroy {

  public loadingDataInProgress: boolean;
  public articles: IArticleOverviewModel[];

  constructor(
    private _service: ArticleService,
    private _snackBar: MatSnackBar,
    private _refreshManager: RefreshManagerService,
    private _router: Router,
    private _activeRoute: ActivatedRoute,
    private _dialog: MatDialog
  ) {
    super();

    super.addSubscription(_refreshManager.refreshRequested.subscribe((result) => {
      this.loadData();
    }));
  }

  ngOnInit() {
    this.loadData();
  }

  ngOnDestroy() {
    super.unsubcripeAll();
  }

  protected loadData(): void {
    this.loadingDataInProgress = true;

    this._service.loadArticle().pipe(finalize(() => {
      this.loadingDataInProgress = false;
    })).subscribe((result) => {
      this.articles = result;
    }, (err) => {
      this._snackBar.open('Die ausstehenden Anfragen konnten nicht geladen werden', 'Fehler', { duration: 4500 });
    })
  }

  public editArticle(article: IArticleOverviewModel): void {
    this._router.navigate(['member/article-edit', article.id]);
  }

  public deleteArticle(article: IArticleOverviewModel): void {
    if (article.isInUse) {
      const dialogOptions: ISimpleDialogOptions = {
        title: 'Löschen bestätigen',
        question: 'Möchten Sie wirklich löschen obwohl dieser verwendet wird?'
      };

      const dialogRef = this._dialog.open(SimpleDialogComponent, {
        data: dialogOptions
      });

      dialogRef.afterClosed().subscribe((result: boolean) => {
        if (result === true) {
          this.loadingDataInProgress = true;

          this._service.deleteArticle(article.id, article.isInUse).pipe(finalize(() => {
            this.loadingDataInProgress = false;
            this.loadData();
          })).subscribe(
            (result) => {
              if (result)
                this._snackBar.open('Die Ware wurde gelöscht.', 'Erfolg', { duration: 4500 });
              else
                this._snackBar.open('Die Ware konnte nicht gelöscht werden', 'Fehler', { duration: 4500 });
            }
          );
        }
      });
    } else {
      const dialogOptions: ISimpleDialogOptions = {
        title: 'Löschen bestätigen',
        question: 'Möchten Sie wirklich löschen?'
      };

      const dialogRef = this._dialog.open(SimpleDialogComponent, {
        data: dialogOptions
      });

      dialogRef.afterClosed().subscribe((result: boolean) => {
        if (result === true) {
          this.loadingDataInProgress = true;

          this._service.deleteArticle(article.id).pipe(finalize(() => {
            this.loadingDataInProgress = false;
            this.loadData();
          })).subscribe(
            (result) => {
              if (result)
                this._snackBar.open('Die Ware wurde gelöscht.', 'Erfolg', { duration: 4500 });
              else
                this._snackBar.open('Die Ware konnte nicht gelöscht werden', 'Fehler', { duration: 4500 });
            }
          );
        }
      });
    }
  }

  createNewItem(): void {
    this._router.navigate(['member/article-create']);
  }

}
