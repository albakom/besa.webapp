import { IConstructionStageUpdateModel } from './../../models/service/construction-stage/iconstruction-stage-update-model';
import { element } from 'protractor';
import { fuseAnimations } from '@fuse/animations';
import { Component, OnInit, OnDestroy } from "@angular/core";
import { FormBasedPage } from "../base/form-based-page";
import { Subject } from "rxjs";
import { ISimpleCabinetOverviewModel } from "../../models/service/construction-stage/isimple-cabinet-overview-model";
import { ISimpleBuildingOverviewModel } from "../../models/service/construction-stage/isimple-building-overview-model";
import { Router, ActivatedRoute } from "@angular/router";
import { ConstructionStageService } from "../../services/construction-stage.service";
import {Validators, FormBuilder} from '@angular/forms';
import { MatSnackBar, MatAutocompleteSelectedEvent } from "@angular/material";
import { debounceTime, distinctUntilChanged, switchMap, finalize } from "rxjs/operators";
import { FormErrorMessage } from "../../models/helper/form-error-message";
import { CreateConstructionStageFormMessages } from "../../models/error-messages/create-construction-stage-form-message";


@Component({
  selector: 'besa-construction-stage-update-page',
  templateUrl: './construction-stage-update-page.component.html',
  styleUrls: ['./construction-stage-update-page.component.scss'],
  animations: fuseAnimations
})
export class ConstructionStageUpdatePageComponent extends FormBasedPage implements OnInit, OnDestroy {
  
  public loadingDataInProgress: boolean;
  public stage: IConstructionStageUpdateModel;

  private _id: number;

  private _searchTerm: Subject<string>;
  public searchCabinets: ISimpleCabinetOverviewModel[];

  public selectedCabinets: ISimpleCabinetOverviewModel[];
  public buildings: ISimpleBuildingOverviewModel[];
  private _buildingBranchableDict: { [branchableId: number]: ISimpleBuildingOverviewModel[]};

  public createInProgress: boolean;

  public originalBranchables: number[];
  public addedBranchables: number[];
  public removedBranchables: number[];

  constructor(
    private _router: Router,
    private _activeRoute: ActivatedRoute,
    private _service: ConstructionStageService,
    private _fb: FormBuilder,
    private _snackBar: MatSnackBar
  ) {
    super();

    this._searchTerm = new Subject<string>();
    this.searchCabinets = new Array();
    this.selectedCabinets = new Array();
    this.buildings = new Array();
    this._buildingBranchableDict = {};
    this.addedBranchables = new Array();
    this.removedBranchables = new Array();
    this.originalBranchables = new Array();
    
    if (this.form)  {
      this.form.reset();
    }

    this.initializeFormValues();

    super.addSubscription(this._searchTerm.pipe(
      debounceTime(150),
      distinctUntilChanged(),
      switchMap((item) => {
        return this._service.searchCabinents(item);
      })
    ).subscribe((items) => {
      this.searchCabinets = items;
    }))

    super.addSubscription(this.form.get('searchTermValue').valueChanges.subscribe((value: string) => {
      this._searchTerm.next(value);
    }))
   }

  ngOnInit() {
    this.loadData();
  }

  ngOnDestroy() {
    super.unsubcripeAll();
  }

  initializeFormValues(): void {
    super.initForm(
      this._fb.group({
        name: ['', [Validators.required, Validators.maxLength(64)]],
        createBranchOffJobs: [true, [Validators.required]],
        searchTermValue: ['', []]
      })
    );
  }

  loadData(): void {
    this._id = +this._activeRoute.snapshot.paramMap.get('id');
    this.loadingDataInProgress = true;
    this._service.loadDetailsForEdit(this._id).pipe(finalize(() => {
      this.form.setValue({
        name: this.stage.name,
        createBranchOffJobs: this.stage.createBranchOffJobs,
        searchTermValue: '',
      });
      this.loadingDataInProgress = false;
    })).subscribe((result) => {
      const stage: IConstructionStageUpdateModel = {
        id: result.id,
        name: result.name,
        createBranchOffJobs: true,
        addedBranchables: new Array(),
        removedBranchables: new Array()
      };
      this.stage = stage;

      this.selectedCabinets = result.branchables;
      this._buildingBranchableDict = result.buildings;
      this.selectedCabinets.forEach(element => {
        const items = this._buildingBranchableDict[element.id];
        this.buildings.push(...items);
        this.originalBranchables.push(element.id);
      });
    })
  }

  public cabinentSelected(eventInfo: MatAutocompleteSelectedEvent): void {
    const cabinet: ISimpleCabinetOverviewModel = eventInfo.option.value;

    if (this.selectedCabinets.indexOf(cabinet) >= 0) {return;}

    if (!(this.originalBranchables.indexOf(cabinet.id) >= 0) && !(this.addedBranchables.indexOf(cabinet.id) >= 0)) {
      this.addedBranchables.push(cabinet.id);
      if (this.removedBranchables.indexOf(cabinet.id) >= 0) {
        this.removedBranchables.splice(this.removedBranchables.indexOf(cabinet.id),1);
      }
    }
    this.searchCabinets.push(cabinet);
    this._service.loadBuldingsByBrancableId(cabinet.id).subscribe((result) => {
      this.buildings.push(...result);
      this._buildingBranchableDict[cabinet.id] = result;
    }, (err) => {
      this._snackBar.open('Gebäude konnten nicht geladen werden', 'Fehler', {duration: 4500});
    })
  }

  public removeCabinet(cabinet: ISimpleCabinetOverviewModel): void {
    const index = this.selectedCabinets.indexOf(cabinet);
    if (index < 0) {return;}

    this.selectedCabinets.splice(index, 1);

    if ((this.originalBranchables.indexOf(cabinet.id) >= 0) && !(this.removedBranchables.indexOf(cabinet.id) >= 0)) {
      this.removedBranchables.push(cabinet.id);
      if (this.addedBranchables.indexOf(cabinet.id) >= 0) {
        this.addedBranchables.splice(this.addedBranchables.indexOf(cabinet.id),1);
      }
    }

    const buildings: ISimpleBuildingOverviewModel[] = this._buildingBranchableDict[cabinet.id];
    if (this.buildings) {
      const buildingStartIndex = this.buildings.indexOf(this.buildings[0]);
      if (buildingStartIndex >= 0) {
        this.buildings.splice(buildingStartIndex, buildings.length);
      }

      delete this._buildingBranchableDict[cabinet.id];
    }
  }

  protected getErrorMessages(): FormErrorMessage[] {
    return CreateConstructionStageFormMessages;
  }

  public displayCabinentResult(item?: ISimpleCabinetOverviewModel): string {
    return item ? item.name : '';
  }

  public editConstructionStage() {
    if (this.form.valid == false) { return; }
    if (this.createInProgress === true) {return;}
    if (this.selectedCabinets.length == 0) {return;}

    const editModel: IConstructionStageUpdateModel = {
      id: this.stage.id,
      name: this.form.value.name,
      createBranchOffJobs: this.form.value.createBranchOffJobs,
      addedBranchables: this.addedBranchables,
      removedBranchables: this.removedBranchables
    };

    this._snackBar.open('Baugebiet wird aktualisiert', 'In Bearbeitung', { duration: -1});
    
    this._service.updateConstructionStage(editModel).pipe(finalize(() => {
      this.createInProgress = false;
    })).subscribe((result) => {
      this._snackBar.dismiss();
      this._snackBar.open('Baugebiet wurde erfolgreich aktualisiert', 'Erfolg', {duration: 4500});
      this._router.navigate(['member/construction-stage-overview']);
    }, (err) => {
      this._snackBar.dismiss();
      this._snackBar.open('Fehler beim aktualisieren des Baugebiets', 'Fehler', {duration: 4500});
    });

    this.createInProgress = true;
  }

}
