import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConstructionStageUpdatePageComponent } from './construction-stage-update-page.component';

describe('ConstructionStageUpdatePageComponent', () => {
  let component: ConstructionStageUpdatePageComponent;
  let fixture: ComponentFixture<ConstructionStageUpdatePageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConstructionStageUpdatePageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConstructionStageUpdatePageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
