import { Component, OnInit, AfterContentChecked } from '@angular/core';
import { NonMemberPageBase } from '../base/non-member-page-base';
import { FuseConfigService } from '@fuse/services/config.service';

@Component({
  selector: 'besa-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss']
})
export class HomePageComponent extends NonMemberPageBase implements OnInit {

  constructor(
    fuseConfig: FuseConfigService,
  ) {
    super(fuseConfig);
    super.setDefaultLayout();
  }

  ngOnInit() {
  }

}
