import { SimpleDialogComponent } from '../../dialogs/simple-dialog/simple-dialog.component';
import { ISimpleDialogOptions } from '../../dialogs/simple-dialog/isimple-dialog-options';
import { element } from 'protractor';
import { CompanyEditFormErrorMessage } from './../../models/error-messages/company-edit-form-error-message';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { finalize } from 'rxjs/operators';
import { Component, OnInit } from '@angular/core';
import { SubscriptionAwareComponent } from '../../helper/subscription-aware-component';
import { Router, ActivatedRoute } from '@angular/router';
import { CompanyService } from '../../services/companies/company.service';
import { ICompanyDetailModel } from '../../models/service/companies/icompany-detail-model';
import { MatSnackBar, MatDialog } from '@angular/material';
import { fuseAnimations } from '@fuse/animations';
import { ICompanyEditModel } from '../../models/service/companies/icompany-edit-model';
import { IFormCompanyEditModel } from '../../models/forms/iform-company-edit-model';
import { FormBasedPage } from '../base/form-based-page';
import { FormErrorMessage } from '../../models/helper/form-error-message';
import { ISimpleEmployeeModel } from '../../models/service/companies/isimple-employee-model';

@Component({
  selector: 'besa-company-edit-page',
  templateUrl: './company-edit-page.component.html',
  styleUrls: ['./company-edit-page.component.scss'],
  animations: fuseAnimations
})
export class CompanyEditPageComponent extends FormBasedPage implements OnInit {

  private _id: number;
  public company: ICompanyDetailModel;
  public loadingDataInProgress: boolean;
  public employees: ISimpleEmployeeModel[] = new Array();
  public companyUnchangedEmployees: ISimpleEmployeeModel[] = new Array();
  public addedEmployees: ISimpleEmployeeModel[] = new Array();
  public removedEmployees: ISimpleEmployeeModel[] = new Array();

  constructor(
    private _router: Router,
    private _activeRoute: ActivatedRoute,
    private _service: CompanyService,
    private _snackBar: MatSnackBar,
    private _fb: FormBuilder,
    private _dialog: MatDialog
  ) {
    super();
  }

  ngOnInit() {
    this.initializeFormValues();
    this.loadData();
  }

  initializeFormValues(): void {
    super.initForm(
      this._fb.group({
        name: ['', [Validators.required, Validators.maxLength(64)]],
        phone: ['', [Validators.required, Validators.maxLength(32)]],
        street: ['', [Validators.required, Validators.maxLength(64)]],
        streetNumber: ['', [Validators.required, Validators.maxLength(8)]],
        city: ['', [Validators.required, Validators.maxLength(64)]],
        postalCode: ['', [Validators.required, Validators.maxLength(16)]]
      })
    );
  }

  protected getErrorMessages(): FormErrorMessage[] {
    return CompanyEditFormErrorMessage;
  }

  loadData(): void {
    this._id = +this._activeRoute.snapshot.paramMap.get('id');
    this.loadingDataInProgress = true;
    this._service.getCompanyDetail(this._id).pipe(finalize(() => {
      this._service.getAllEmployees().pipe(finalize(() => {
        this.form.setValue({
          name: this.company.name,
          phone: this.company.phone,
          street: this.company.address.street,
          streetNumber: this.company.address.streetNumber,
          city: this.company.address.city,
          postalCode: this.company.address.postalCode
        });
        this.loadingDataInProgress = false;
      })).subscribe(
        (result) => {
          result.forEach(element => {
            if (!this.company.employees.find(x => x.id == element.id)) {
              this.employees.push(element);
            }
          });
        },
        (err) => this._snackBar.open('Die ausstehende Anfrage konnte nicht geladen werden', 'Fehler', { duration: 4500 })
      )
    }
    )).subscribe(
      (result) => {
        result.employees.forEach(element => {
          this.companyUnchangedEmployees.push(element);
        });
        this.company = result;
      },
      (err) => this._snackBar.open('Die ausstehende Anfrage konnte nicht geladen werden', 'Fehler', { duration: 4500 })
    );
  }

  editCompany(): void {
    const dialogOptions: ISimpleDialogOptions = {
      title: 'Speichern bestätigen',
      question: 'Möchten Sie wirklich speichern?'
    };

    const dialogRef = this._dialog.open(SimpleDialogComponent, {
      data: dialogOptions
    });

    dialogRef.afterClosed().subscribe((result: boolean) => {
      if (result === true) {
        const formValue: IFormCompanyEditModel = this.form.value;
    var added: number[] = new Array();
    this.addedEmployees.forEach(element => {
      added.push(element.id);
    });
    var removed: number[] = new Array();
    this.removedEmployees.forEach(element => {
      removed.push(element.id);
    });

    const editedCompany: ICompanyEditModel = {
      id: this.company.id,
      name: formValue.name,
      phone: formValue.phone,
      address:
        {
          city: formValue.city,
          street: formValue.street,
          streetNumber: formValue.streetNumber,
          postalCode: formValue.postalCode
        },
      addedEmployees: added,
      removedEmployees: removed
    };

    this._service.editCompany(editedCompany).pipe().subscribe(
      (result) => {
        this._snackBar.open('Die Änderung wurde verarbeitet', 'Erfolg', { duration: 4500 });
        this._router.navigate(['/member/company-overview']);
      },
      (err) => this._snackBar.open('Die ausstehende Änderung konnte nicht verarbeitet werden', 'Fehler', { duration: 4500 })
    );
      }
    });
    
  }

  cancelEdit(): void {
    // const dialogOptions: ISimpleDialogOptions = {
    //   title: 'Abbrechen bestätigen',
    //   question: 'Möchten Sie wirklich abbrechen und ungespeicherte Änderungen verwerfen?'
    // };

    // const dialogRef = this._dialog.open(SimpleDialogComponent, {
    //   data: dialogOptions
    // });

    // dialogRef.afterClosed().subscribe((result: boolean) => {
    //   if (result === true) {
    //     this.loadingDataInProgress = true;

        this._router.navigate(['/member/company-overview']);
    //   }
    // });
  }

  addEmployee(employee: ISimpleEmployeeModel): void {
    if (!this.addedEmployees.find(x => x.id == employee.id) && !this.companyUnchangedEmployees.find(x => x.id == employee.id)) {
      this.addedEmployees.push(employee);
    }
    if (!this.company.employees.find(x => x.id == employee.id)) {
      this.company.employees.push(employee);
    }
    if (this.removedEmployees.find(x => x.id == employee.id)) {
      const index: number = this.removedEmployees.findIndex(x => x.id == employee.id);
      this.removedEmployees.splice(index, 1);
    }
    if (this.employees.find(x => x.id == employee.id)) {
      const index: number = this.employees.findIndex(x => x.id == employee.id);
      this.employees.splice(index, 1);
    }
  }

  removeEmployee(employee: ISimpleEmployeeModel): void {
    if (!this.removedEmployees.find(x => x.id == employee.id) && this.companyUnchangedEmployees.find(x => x.id == employee.id)) {
      this.removedEmployees.push(employee);
    }
    if (this.company.employees.find(x => x.id == employee.id)) {
      const index: number = this.company.employees.findIndex(x => x.id == employee.id);
      this.company.employees.splice(index, 1);
    }
    if (this.addedEmployees.find(x => x.id == employee.id)) {
      const index: number = this.addedEmployees.findIndex(x => x.id == employee.id);
      this.addedEmployees.splice(index, 1);
    }
    if (!this.employees.find(x => x.id == employee.id)) {
      this.employees.push(employee);
    }
  }
}
