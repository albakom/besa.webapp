import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompanyEditPageComponent } from './company-edit-page.component';

describe('CompanyEditPageComponent', () => {
  let component: CompanyEditPageComponent;
  let fixture: ComponentFixture<CompanyEditPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompanyEditPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompanyEditPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
