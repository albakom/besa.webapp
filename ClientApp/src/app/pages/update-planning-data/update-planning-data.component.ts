import { UpdateTasksHubService } from './../../services/hubs/update-tasks-hub.service';
import { Router } from '@angular/router/';
import { UpdateTasks } from './../../models/service/tasks/update-tasks.enum';
import { IUpdateTaskOverviewTabModel } from './../../models/helper/iupdate-task-overview-tab-model';
import { IUpdateTaskOverviewModel } from './../../models/service/tasks/iupdate-task-overview-model';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';
import { SubscriptionAwareComponent } from '../../helper/subscription-aware-component';
import { ConstructionStageService } from '../../services/construction-stage.service';
import { Observable } from 'rxjs';
import { MatSnackBar, MatDialog } from '@angular/material';
import { UpdateTaskService } from '../../services/update-task/update-task.service';
import { finalize } from 'rxjs/operators';
import { IStartUpdateTaskModel } from '../../models/service/tasks/istart-update-task-model';
import { IUpdateTaskHeartbeatChangedModel } from '../../models/service/tasks/iupdate-task-heartbeat-changed-model';
import { IUpdateTasFinishedStateChangedModel } from '../../models/service/tasks/iupdate-tas-finished-state-changed-model';
import { SelectUpdateTaskObjectsDialogComponent } from '../../dialogs/select-update-task-objects-dialog/select-update-task-objects-dialog.component';
import { ISelectUpdateTaskObjectsDialogOptions } from '../../dialogs/select-update-task-objects-dialog/iselect-update-task-objects-dialog-options';

@Component({
  selector: 'besa-update-planning-data',
  templateUrl: './update-planning-data.component.html',
  styleUrls: ['./update-planning-data.component.scss'],
  animations: fuseAnimations
})
export class UpdatePlanningDataComponent extends SubscriptionAwareComponent implements OnInit, OnDestroy {

  loadingDataInProgress: boolean;
  private _updateTasks: IUpdateTaskOverviewModel[];

  tabs: IUpdateTaskOverviewTabModel[];

  constructor(
    private _snackBar: MatSnackBar,
    private _dialog: MatDialog,
    private _service: UpdateTaskService,
    private _router: Router,
    private _hubService: UpdateTasksHubService
  ) {
    super();

    this._updateTasks = new Array();
    this.initTabs();
  }

  ngOnInit() {
    this.loadData();
    this._hubService.connenct().subscribe(() => {

    })

    this._hubService._updateTaskAdded.subscribe((result: IUpdateTaskOverviewModel) => {
      this.tabs.find(x => x.tab == result.taskType).elements.push(result);
      console.log(result);
    });

    this._hubService._updateTaskCanceled.subscribe((result: number) => {
      this.tabs.forEach(element => {
        element.elements.forEach(item => {
          if (item.id === result) {
            item.wasCanceled = true;
          }
        });
      });
    });

    this._hubService._updateTaskHeartbeatChanged.subscribe((result: IUpdateTaskHeartbeatChangedModel) => {
      this.tabs.forEach(element => {
        element.elements.forEach(item => {
          if (item.id === result.taskId) {
            item.lastHearbeat = result.value;
            console.log(result);
          }
        });
      });
    });

    this._hubService._updateTaskFinishedStateChanged.subscribe((result: IUpdateTasFinishedStateChangedModel) => {
      this.tabs.forEach(element => {
        element.elements.forEach(item => {
          if (item.id === result.id) {
            item.endend = result.finishedAt;
            console.log(result);
          }
        });
      });
    });

    this._hubService._taskLockDetected.subscribe((id) => {
      this._snackBar.open('TaskLock entdeckt', 'Achtung', { duration: 4500 });
    })
  }

  ngOnDestroy() {
    super.unsubcripeAll();
    this._hubService.disconnect();
  }

  initTabs() {
    this.tabs = new Array();
    this.tabs.push({
      tab: UpdateTasks.Cables,
      elements: new Array<IUpdateTaskOverviewModel>()
    });
    this.tabs.push({
      tab: UpdateTasks.CableTypes,
      elements: new Array<IUpdateTaskOverviewModel>()
    });
    this.tabs.push({
      tab: UpdateTasks.MainCableForBranchable,
      elements: new Array<IUpdateTaskOverviewModel>()
    });
    this.tabs.push({
      tab: UpdateTasks.MainCableForPops,
      elements: new Array<IUpdateTaskOverviewModel>()
    });
    this.tabs.push({
      tab: UpdateTasks.PatchConnections,
      elements: new Array<IUpdateTaskOverviewModel>()
    });
    this.tabs.push({
      tab: UpdateTasks.PoPs,
      elements: new Array<IUpdateTaskOverviewModel>()
    });
    this.tabs.push({
      tab: UpdateTasks.Splices,
      elements: new Array<IUpdateTaskOverviewModel>()
    });
    this.tabs.push({
      tab: UpdateTasks.UpdateBuildings,
      elements: new Array<IUpdateTaskOverviewModel>()
    });
  }

  loadData() {
    this.loadingDataInProgress = true;
    this.initTabs();

    this._service.getOverview().pipe(finalize(() => {
      this.loadingDataInProgress = false;
    })).subscribe((result) => {
      if (result) {
        this._updateTasks = result;
        this._updateTasks.forEach(element => {
          this.tabs.find(x => x.tab == element.taskType).elements.push(element);
        });
        console.log(this.tabs);
      }
    }, (err) => {
      this._snackBar.open('Fehler beim Laden der Optionen', 'Fehler', { duration: 4500 });
    });
  }

  openDetails(item: IUpdateTaskOverviewModel) {
    this._router.navigate(['member/update-planning-data-details', item.id]);
  }

  cancelTask(item: IUpdateTaskOverviewModel) {
    this.loadingDataInProgress = true;
    this._service.cancelTask(item.id).pipe(finalize(() => {
      this.loadingDataInProgress = false;
    })).subscribe((result) => {
      if (result === true) {
        this._snackBar.open('Task wurde erfolgreich abgebrochen', 'Erfolg', { duration: 4500 });
        this.loadData();
      } else {
        this._snackBar.open('Task konnte nicht erfolgreich abgebrochen werden', 'Fehler', { duration: 4500 });
      }
    }, (err) => {
      this._snackBar.open('Task konnte nicht erfolgreich abgebrochen werden', 'Fehler', { duration: 4500 });
    })
  }

  private startTaskInternel(ids: number[], type: UpdateTasks): void {
    const startTask: IStartUpdateTaskModel = {
      objectIds: ids,
      taskType: type
    };

    this._service.startTask(startTask).subscribe((result) => {
      if (result > 0) {
        this._snackBar.open('Der Task wurde erfolgreich gestartet', 'Erfolg', { duration: 4500 });
      } else {
        this._snackBar.open('Der Task konnte nicht erfolgreich gestartet werden', 'Fehler', { duration: 4500 });
      }
    }, (err) => {
      this._snackBar.open('Der Task konnte nicht erfolgreich gestartet werden', 'Fehler', { duration: 4500 });
    });
  }

  startTask(item: IUpdateTaskOverviewTabModel) {

    let showDialog = true;
    switch (item.tab) {
      case UpdateTasks.CableTypes:
      case UpdateTasks.PoPs:
      case UpdateTasks.UpdateBuildings:
        showDialog = false;
        break;
    }

    if (showDialog === false) {
      this.startTaskInternel(null, item.tab);
    }
    else {
      const dialogOptions: ISelectUpdateTaskObjectsDialogOptions = { type: item.tab };

      const dialogRef = this._dialog.open<SelectUpdateTaskObjectsDialogComponent>(SelectUpdateTaskObjectsDialogComponent, { data: dialogOptions });
      dialogRef.afterClosed().subscribe((dialogResult: number[]) => {
        if (dialogResult && dialogResult.length > 0) {
          this.startTaskInternel(dialogResult, item.tab);
        }
      })
    }
  }

  isStartTaskDisabled(tab: UpdateTasks): boolean {
    let isDisabled: boolean = false;
    this.tabs.find(x => x.tab == tab).elements.forEach(element => {
      if (element.started && !element.endend) {
        isDisabled = true;
      }
    });

    return isDisabled;
  }


}
