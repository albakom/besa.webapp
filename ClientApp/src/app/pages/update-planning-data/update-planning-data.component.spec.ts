import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdatePlanningDataComponent } from './update-planning-data.component';

describe('UpdatePlanningDataComponent', () => {
  let component: UpdatePlanningDataComponent;
  let fixture: ComponentFixture<UpdatePlanningDataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdatePlanningDataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdatePlanningDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
