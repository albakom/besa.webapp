import { FinishJobAdministrativeDialogComponent } from './../../dialogs/finish-job-administrative-dialog/finish-job-administrative-dialog.component';
import { SubscriptionAwareComponent } from '../../helper/subscription-aware-component';
import { RefreshManagerService } from '../../services/refresh-manager.service';
import { MatSnackBar, MatDialog } from '@angular/material';
import { LoginService } from '../../services/login.service';
import { ActivatedRoute, Router } from '@angular/router';
import { finalize } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { IJobDetailModel } from '../../models/service/jobs/job-detail-model';
import { IFinishJobDetailModel } from '../../models/service/jobs/ifinish-job-detail-model';
import { JobStates } from '../../models/service/jobs/job-states.enum';
import { BesaRoles } from '../../models/service/besa-roles.enum';
import { IFileOverviewModel } from '../../models/service/files/ifile-overview-model';
import { FileTypes } from '../../models/service/files/file-types.enum';
import { IShowPictureDialogOptions } from '../../dialogs/show-picture-dialog/ishow-picture-dialog-options';
import { ShowPictureDialogComponent } from '../../dialogs/show-picture-dialog/show-picture-dialog.component';
import { JobService } from '../../services/job.service';
import { UriService } from '../../services/uri.service';
import { IAcknowledgeJobModel } from '../../models/service/jobs/iacknowledge-job-model';
import { IFinishJobAdministrativeDialogOptions } from '../../dialogs/finish-job-administrative-dialog/i-finish-job-administrative-dialog-options';
import { IFinishJobAdministrativeModel } from '../../models/service/jobs/ifinish-job-administrative-model';
import { duration } from 'moment';

export abstract class JobDetailsPageBase<
    TFinished extends IFinishJobDetailModel,
    TDetails extends IJobDetailModel<TFinished>
    > extends SubscriptionAwareComponent {
    public loadingDetailsInProgress: boolean;
    public details: TDetails;
    public showFinishInfo: boolean;
    public finishJobAdministrativeVisible: boolean;
    public actionInProgress: boolean;

    //#region Constructor
    constructor(refreshManager: RefreshManagerService,
        protected _activeRoute: ActivatedRoute,
        protected _snackBar: MatSnackBar,
        protected _dialog: MatDialog,
        protected _loginService: LoginService,
        protected _router: Router,
        protected _uriService: UriService,
        protected _service: JobService
    ) {
        super();

        this.showFinishInfo = false;

        super.addSubscription(refreshManager.refreshRequested.subscribe((result) => {
            this.loadDetails(+_activeRoute.snapshot.params['id']);
        }));

        super.addSubscription(_activeRoute.params.subscribe((params) => {
            this.loadDetails(+params['id']);
        }));

        super.addSubscription(_loginService.currentUserInfo.subscribe((info) => {
            if (this.details) {
                this.checkVisibilityOfCustomActions(this.details);
            }
        }));
    }
    //#endregion

    //#region Methods

    protected abstract getDetails(id: number): Observable<TDetails>;

    protected loadDetailsInternal(): void {
        return this.loadDetails(+this._activeRoute.snapshot.params['id']);
    }

    public openFinishJobAdministrativeDialog(): void {
        const options: IFinishJobAdministrativeDialogOptions = {
            jobId: +this._activeRoute.snapshot.params['id']
        };

        const ref = this._dialog.open<FinishJobAdministrativeDialogComponent>(FinishJobAdministrativeDialogComponent, { data: options });

        ref.afterClosed().subscribe((result: IFinishJobAdministrativeModel) => {
            if (result) {
                this._snackBar.open('Auftrag wird abgeschlossen', 'In Bearbeitung', { duration: -1 });

                this._service.finishJobAdministrative(result).subscribe((result2) => {
                    this._snackBar.open('Auftrag wurde erfolgreich abgeschlossen', 'Erfolg', { duration: 4500 });
                    this.loadDetails(options.jobId);
                }, (err) => {
                    this._snackBar.open('Auftrag konnte nicht abgeschlossen werden', 'Fehler', { duration: 4500 });
                });
            }
        });
    }

    private checkVisibilityOfCustomActions(result: IJobDetailModel<TFinished>) {
        this.finishJobAdministrativeVisible = result.state == JobStates.Open && this._loginService.hasRole(BesaRoles.ProjectManager) === true;
        this.showFinishInfo =
            (result.state === JobStates.Finished || result.state === JobStates.Acknowledged) &&
            (this._loginService.hasRole(BesaRoles.ProjectManager) === true || this._loginService.userHasManagementCapabilities() == true)
    }

    public loadDetails(id: number): void {
        if (isNaN(id) === true || this.loadingDetailsInProgress == true) { return; }

        this.loadingDetailsInProgress = true;

        this.getDetails(id).pipe(finalize(() => {
            this.loadingDetailsInProgress = false;
        })).subscribe((result) => {
            this.checkVisibilityOfCustomActions(result);
            this.details = result;

            this.afterDetailsLoaded(this.details);

        }, (err) => {
            this._snackBar.open('Details zum Auftrag konnten nicht geladen werden', 'Fehler', { duration: 4500 });
        });
    }

    protected afterDetailsLoaded(details: TDetails): void {

    }

    public showPreview(file: IFileOverviewModel): void {
        if (file.type != FileTypes.Image) { return; }

        const options: IShowPictureDialogOptions = {
            imgAsString: this._uriService.getFileUrl(file.id)
        };

        const ref = this._dialog.open<ShowPictureDialogComponent>(ShowPictureDialogComponent, { data: options });
    }

    public getFileUrl(file: IFileOverviewModel): string {
        return this._uriService.getFileUrl(file.id);
    }

    protected abstract getFinishUrl(): string;

    public navigateToFinishJob(): void {
        const url = this.getFinishUrl();
        this._router.navigate([url, +this._activeRoute.snapshot.params['id']])
    }

    public discardJob(): void {
        if (this.actionInProgress === true) { return; }

        this._snackBar.open('Auftrag wird abgelehnt', 'In Bearbeitung', { duration: 4500 });

        this._service.discardJob(+this._activeRoute.snapshot.params['id']).pipe(finalize(() => {
            this.actionInProgress = false;
        })).subscribe((result) => {
            if (result === true) {
                this._snackBar.open('Auftrag wurde erfolgreich abgelehnt', 'Erfolg', { duration: 4500 });
                this.loadDetails(+this._activeRoute.snapshot.params['id']);
            } else {
                this._snackBar.open('Auftrag konnte nicht abgelehnt werden', 'Fehler', { duration: 4500 });
            }
        }, (err) => {
            this._snackBar.open('Auftrag konnte nicht abgelehnt werden', 'Fehler', { duration: 4500 });
        })
    }

    public acknowledgeJob(): void {
        if (this.actionInProgress === true) { return; }

        const jobId = +this._activeRoute.snapshot.params['id'];
        const model: IAcknowledgeJobModel = {
            jobId: jobId,
        };

        this._snackBar.open('Auftrag wird bestätigt', 'In Bearbeitung', { duration: 4500 });

        this._service.acknowledgeJob(model).pipe(finalize(() => {
            this.actionInProgress = false;
        })).subscribe((result) => {
            if (result === true) {
                this._snackBar.open('Auftrag wurde erfolgreich bestätigt', 'Erfolg', { duration: 4500 });
                this.loadDetails(model.jobId);
            } else {
                this._snackBar.open('Auftrag konnte nicht bestätigt werden', 'Fehler', { duration: 4500 });

            }
        }, (err) => {
            this._snackBar.open('Auftrag konnte nicht bestätigt werden', 'Fehler', { duration: 4500 });
        })
    }

    //#endregion
}