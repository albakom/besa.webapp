import { SubscriptionAwareComponent } from './../../helper/subscription-aware-component';
import { FormGroup } from "@angular/forms";
import { FormErrorMessage } from '../../models/helper/form-error-message';

export abstract class FormBasedPage extends SubscriptionAwareComponent {

    //#region Properties

    public formValidationErrors: { [key: string]: string } = {};
    public form: FormGroup;

    //#endregion

    //#region Constructor
    constructor() {
        super();
    }

    //#endregion

    //#region Methods

    protected initForm(fg: FormGroup) {
        this.form = fg;

        super.addSubscription(this.form.statusChanges.subscribe(() => {
            this.updateErrorMessags();
        }));
    }

    protected abstract getErrorMessages(): FormErrorMessage[];

    private updateErrorMessags(): void {
        const messages: FormErrorMessage[] = this.getErrorMessages();
        if (!messages) { return; }

        this.formValidationErrors = {};
        let errorFound = false;
        for (const message of messages) {
            const control = this.form.get(message.forControl);
            if (control && control.dirty && control.invalid && control.errors[message.forValidator] &&
                !this.formValidationErrors[message.forControl]) {
                this.formValidationErrors[message.forControl] = message.errorMessage;
                errorFound = true;
            }
        }
    }

    public hasFormError(property: string): boolean {
        if (this.formValidationErrors[property]) {
            return true;
        } else {
            return false;
        }
    }

    public getFormError(property: string): string {
        return this.formValidationErrors[property];
    }

    //#endregion
}
