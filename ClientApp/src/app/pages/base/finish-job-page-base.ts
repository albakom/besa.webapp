import { Observable } from 'rxjs';
import { FormBasedPage } from "./form-based-page";
import { UploadFileModel } from "../../models/helper/upload-file-model";
import { ITakePictureDialogOptions } from "../../dialogs/take-pictures-dialog/itake-picture-dialog-options";
import { TakePicturesDialogComponent } from "../../dialogs/take-pictures-dialog/take-pictures-dialog.component";
import { ViewChild } from "@angular/core";
import { MatExpansionPanel, MatDialog, MatSnackBar } from "@angular/material";
import { UploadComponent } from "../upload/upload.component";
import { JobService } from "../../services/job.service";
import { Router, ActivatedRoute } from "@angular/router";
import { UploadService } from "../../services/upload/upload.service";
import { IShowPictureDialogOptions } from "../../dialogs/show-picture-dialog/ishow-picture-dialog-options";
import { ShowPictureDialogComponent } from "../../dialogs/show-picture-dialog/show-picture-dialog.component";
import { finalize } from "rxjs/operators";
import { IFinishJobModel } from "../../models/service/jobs/ifinish-job-model";
import { Validators, FormBuilder } from '@angular/forms';

export abstract class FinishJobPageBase<TFinishModel extends IFinishJobModel> extends FormBasedPage {

    //#region Properties

    public filesNeeded = false;

    public files: UploadFileModel[];
    public isFormValid: boolean;
    public sendingInProgress: boolean;
    public uploadingInProgress: boolean = false;

    @ViewChild('dataPanel')
    public dataPanel: MatExpansionPanel;

    @ViewChild('upload')
    public upload: UploadComponent;

    //#endregion

    //#region Constructor

    constructor(
        protected _dialog: MatDialog,
        protected _fb: FormBuilder,
        protected _snackbar: MatSnackBar,
        protected _jobService: JobService,
        protected _router: Router,
        protected _uploadService: UploadService,
        protected _activeRoute: ActivatedRoute,
    ) {
        super();
        this.files = new Array();

        super.addSubscription(_activeRoute.params.subscribe((params) => {
            if (this.form) {
                this.form.patchValue({ 'jobId': +params['id'] })
            }
        }));

        this.isFormValid = false;
    }

    //#endregion

    //#region Methods

    protected initFinishForm(specializedForm: any): void {

        const formValues: any = {
            jobId: [+this._activeRoute.snapshot.params['id'], [Validators.required]],
            comment: ['', [Validators.maxLength(1024)]],
            problemHappend: this._fb.group(
                {
                    value: [false, []],
                    description: ['', [Validators.maxLength(1024)]],
                }
            ),
        };

        for (const attr in specializedForm) {
            formValues[attr] = specializedForm[attr];
        }

        super.initForm(this._fb.group(formValues));

        super.addSubscription(this.form.statusChanges.subscribe((something) => {
            this.isFormValid = this.checkIfFormIsValid(false);
        }))
    }

    protected abstract checkIfModelIsValid(model: TFinishModel): boolean;

    public checkIfFormIsValid(setValue: boolean): boolean {
        let isValid = true;
        if (this.form.invalid === true) {
            isValid = false;
        } else {
            0
            const value: TFinishModel = this.form.value;
            if (
                (value.problemHappend.value === true && !value.problemHappend.description) ||
                (this.filesNeeded == true && this.files.length == 0)
            ) {
                isValid = false;
            }

            if (isValid === true) {
                isValid = this.checkIfModelIsValid(value);
            }
        }

        if (setValue === true) {
            this.isFormValid = isValid;
        }

        return isValid;
    }


    public openTakePictureDialog(): void {
        const options: ITakePictureDialogOptions = {};
        const ref = this._dialog.open<TakePicturesDialogComponent>(TakePicturesDialogComponent, { data: options });

        ref.afterClosed().subscribe((result) => {

        });
    }

    public onFileSelect(files: FileList) {

        for (let index = 0; index < files.length; index++) {
            const file = files.item(index);

            const fileModel: UploadFileModel = {
                type: file.type.startsWith('image') === true ? 'image' : 'file',
                file: file,
                name: file.name,
            };

            if (this.files.length == 0 && this.dataPanel) {
                this.dataPanel.open();
            }

            this.files.unshift(fileModel);

            if (fileModel.type === 'image') {
                const reader = new FileReader();
                fileModel.previewLoading = true;

                reader.onload = (event: any) => {
                    fileModel.imgAsString = event.target.result;
                    fileModel.previewLoading = false;
                };

                reader.readAsDataURL(file);
            }
        }

        this.checkIfFormIsValid(true);
    }

    public removeFile(file: UploadFileModel): void {
        const index = this.files.indexOf(file);
        if (index >= 0) {
            this.files.splice(index, 1);
        }

        this.checkIfFormIsValid(true);
    }

    public showPreview(file: UploadFileModel): void {
        if (!file.imgAsString) { return; }

        const options: IShowPictureDialogOptions = {
            imgAsString: file.imgAsString
        };
        const ref = this._dialog.open<ShowPictureDialogComponent>(ShowPictureDialogComponent, { data: options });
    }

    protected abstract getfinishJobInvoker(model: TFinishModel): Observable<number>;
    protected abstract getJobUrl(): string;

    private finishJobInternal(fileIds: number[]) {
        this._snackbar.open('Auftrag wird abgeschlossen', 'In Bearbeitung', { duration: -1 });

        const formValue: TFinishModel = this.form.value;
        if (fileIds) {
            formValue.fileIds = fileIds;
        }

        this.getfinishJobInvoker(formValue).pipe(finalize(() => {
            this.sendingInProgress = false;
            this.uploadingInProgress = false;
        })).subscribe((result) => {
            this._snackbar.open('Auftrag wurde  abgeschlossen', 'Erfolg', { duration: 4500 });
            this._router.navigate([this.getJobUrl(), formValue.jobId]);
        }, (err) => {
            this._snackbar.open('Auftrag wurde nicht abgeschlossen', 'Fehler', { duration: 4500 });
        });
    }

    public finishJob(): void {
        if (this.sendingInProgress === true) { return; }
        if (this.form.invalid === true) { return; }

        if (this.dataPanel) {
            this.dataPanel.open();
        }

        this.sendingInProgress = true;
        if (this.files.length > 0) {
            this.uploadingInProgress = true;

            const uploadRef = this._snackbar.open('Dateien werden hochgeladen', 'Verarbeitung', { duration: -1 });

            this.upload.startUpload(this.files.map((val) => val.file)).pipe(finalize(() => {
                uploadRef.dismiss();
                this.uploadingInProgress = false;
            })).subscribe((fileIds) => {
                this.finishJobInternal(fileIds);
            }, (err) => {
                this._snackbar.open('Dateien konnten nicht hochgeladen werden', 'Fehler', { duration: 4500 });
                this.sendingInProgress = false;
            });
        } else {
            this.uploadingInProgress = false;
            this.finishJobInternal(null);
        }

    }

    //#endregion

}