import { Observable } from 'rxjs';
import { SubscriptionAwareComponent } from "../../helper/subscription-aware-component";
import { IJobOverviewModel } from '../../models/service/jobs/ijobs-overview-model';
import { IConstructionStageDetailModel } from "../../models/service/construction-stage/iconstruction-stage-detail-for-model";
import { RefreshManagerService } from "../../services/refresh-manager.service";
import { ActivatedRoute, Router } from "@angular/router";
import { MatSnackBar } from "@angular/material";
import { finalize } from 'rxjs/operators';

export abstract class ConstrutionStageJobDetailBasePage<TJob extends IJobOverviewModel> extends SubscriptionAwareComponent {

    //#region Properties

    public loadingDetailsInProgress: boolean;
    public details: IConstructionStageDetailModel<IJobOverviewModel>;

    protected initDone: boolean = false;

    //#endregion

    constructor(refreshManager: RefreshManagerService,
        private _activeRoute: ActivatedRoute,
        private _router: Router,
        private _snackBar: MatSnackBar
    ) {
        super();

        super.addSubscription(refreshManager.refreshRequested.subscribe((result) => {
            this.loadDetails(+_activeRoute.snapshot.params['id']);
        }));

        super.addSubscription(_activeRoute.params.subscribe((params) => {
            if(this.initDone === true)
            {
                this.loadDetails(+params['id']);
            }
        }))
    }

    ngOnInit() {
    }

    ngOnDestroy() {
        super.unsubcripeAll();
    }

    //#region Methods

    protected abstract loadDetailsFromService(id: number, onlyUnboundend: boolean): Observable<IConstructionStageDetailModel<IJobOverviewModel>>;

    protected loadDetailsInternal() {
        this.loadDetails(+this._activeRoute.snapshot.params['id']);
    }

    public loadDetails(id: number): void {
        if (isNaN(id) === true || this.loadingDetailsInProgress == true) { return; }

        this.loadingDetailsInProgress = true;

        this.loadDetailsFromService(id, false).pipe(finalize(() => {
            this.loadingDetailsInProgress = false;
        })).subscribe((result) => {
            this.details = result;
        }, (err) => {
            this._snackBar.open('Details zum Bauabschnitt konnten nicht geladen werden', 'Fehler', { duration: 4500 });
        });
    }

    protected abstract getNavigationPathForDetails(job: TJob): string;

    public navigateToJob(job: TJob): void {
        this._router.navigate([this.getNavigationPathForDetails(job), job.jobId]);
    }
}
