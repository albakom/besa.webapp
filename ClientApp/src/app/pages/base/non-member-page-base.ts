import { SubscriptionAwareComponent } from '../../helper/subscription-aware-component';
import { FuseConfigService } from '../../../@fuse/services/config.service';
import { FuseConfig } from '@fuse/types';

export abstract class NonMemberPageBase extends SubscriptionAwareComponent {

    constructor(private _fuseConfig: FuseConfigService) {
        super();
    }

    protected setDefaultLayout(): void {
        const settings: FuseConfig = {
            layout: {
                width: 'fullwidth',
                navbar:
                {
                    hidden: true,
                    folded: true,

                },
                toolbar: {
                    hidden: true,
                },
                footer: {
                    hidden: true,
                }
            },
            customScrollbars: false,
        };

        this._fuseConfig.config = settings;
    }
}
