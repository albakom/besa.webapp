import { Observable } from 'rxjs';
import { MatSnackBar } from '@angular/material';
import { SubscriptionAwareComponent } from './../../helper/subscription-aware-component';
import { Component } from '@angular/core';
import { RefreshManagerService } from '../../services/refresh-manager.service';
import { finalize } from 'rxjs/operators';
import { Router } from '@angular/router';
import { IConstructionStageOverviewForJobsModel } from '../../models/service/construction-stage/iconstruction-stage-overview-for-jobs-model';

export abstract class JobOverviewPageBase extends SubscriptionAwareComponent {

    //#region Properties

    public items: IConstructionStageOverviewForJobsModel[];
    public itemsAreLoading: boolean;

    //#endregion

    constructor(refreshManger: RefreshManagerService,
        private _snackBar: MatSnackBar,
        private _router: Router) {
        super();

        super.addSubscription(refreshManger.refreshRequested.subscribe((result) => {
            this.loadConstructionStages();
        }));
    }

    ngOnInit() {
        this.loadConstructionStages();
    }

    ngOnDestroy() {
        super.unsubcripeAll();
    }

    //#region Methods

    protected abstract loadOverview(onlybounded: boolean): Observable<IConstructionStageOverviewForJobsModel[]>;

    public loadConstructionStages(): void {
        this.itemsAreLoading = true;

        this.loadOverview(false).pipe(finalize(() => {
            this.itemsAreLoading = false;
        })).subscribe((result) => {
            this.items = result;
        }, (err) => {
            this._snackBar.open('Bauabschnitte konnte nicht geladen werden', 'Fehler', { duration: 4500 });
        });
    }

    protected abstract getNavigationPathForDetails(): string;

    public navigateToDetails(item: IConstructionStageOverviewForJobsModel): void {
        this._router.navigate([this.getNavigationPathForDetails(), item.id]);
    }

    //#endregion

}
