import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InventoryOverviewPageComponent } from './inventory-overview-page.component';

describe('InventoryOverviewPageComponent', () => {
  let component: InventoryOverviewPageComponent;
  let fixture: ComponentFixture<InventoryOverviewPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InventoryOverviewPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InventoryOverviewPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
