import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserOverviewPageComponent } from './user-overview-page.component';

describe('UserOverviewPageComponent', () => {
  let component: UserOverviewPageComponent;
  let fixture: ComponentFixture<UserOverviewPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserOverviewPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserOverviewPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
