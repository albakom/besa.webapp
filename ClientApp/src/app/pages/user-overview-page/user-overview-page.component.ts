import {ISimpleDialogOptions} from '../../dialogs/simple-dialog/isimple-dialog-options';
import {SubscriptionAwareComponent} from '../../helper/subscription-aware-component';
import { IUserOverviewModel } from './../../models/service/user/iuser-overview-model';
import {ActivatedRoute, Router} from '@angular/router';
import {RefreshManagerService} from '../../services/refresh-manager.service';
import {FormErrorMessage} from '../../models/helper/form-error-message';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBasedPage } from '../base/form-based-page';
import { UserService } from '../../services/user/user.service';
import { MatSnackBar, MatDialog } from '@angular/material';
import { finalize } from 'rxjs/operators';
import { SimpleDialogComponent } from '../../dialogs/simple-dialog/simple-dialog.component';
import { fuseAnimations } from '@fuse/animations';

@Component({
  selector: 'besa-user-overview-page',
  templateUrl: './user-overview-page.component.html',
  styleUrls: ['./user-overview-page.component.scss'],
  animations: fuseAnimations
})
export class UserOverviewPageComponent extends SubscriptionAwareComponent implements OnInit, OnDestroy {
  
  public loadingDataInProgress: boolean;
  public users: IUserOverviewModel[];


  constructor(
    private _service: UserService,
    private _snackBar: MatSnackBar,
    private _refreshManager: RefreshManagerService,
    private _router: Router,
    private _activeRoute: ActivatedRoute,
    private _dialog: MatDialog
  ) {
    super();

    super.addSubscription(_refreshManager.refreshRequested.subscribe((result) => {
      this.loadData();
    }));
   }

  ngOnInit() {
    this.loadData();
  }

  ngOnDestroy() {
    super.unsubcripeAll();
  }

  protected loadData(): void {
    this.loadingDataInProgress = true;

    this._service.loadUsers().pipe(finalize(() => this.loadingDataInProgress = false)).subscribe(
      (result) => {
        this.users = result;
      }, (err) => {
        this._snackBar.open('Die ausstehenden Anfragen konnte nicht geladen werden', 'Fehler', { duration: 4500 });
      }
    )
  }

  public addUser(): void {
    //this._router.navigate(['member/company-create']);
  }

  public editUser(user: IUserOverviewModel) {
    this._router.navigate(['/member/users-edit', user.id]);
  }

  public openDetails(user: IUserOverviewModel) {
    this._router.navigate(['/member/users-details', user.id]);
  }

  public deleteUser(user: IUserOverviewModel) {
    const dialogOptions: ISimpleDialogOptions = {
      title: 'Löschen bestätigen',
      question: 'Möchten Sie wirklich löschen?'
    };

    const dialogRef = this._dialog.open(SimpleDialogComponent, {
      data: dialogOptions
    });

    dialogRef.afterClosed().subscribe((result: boolean) => {
      if (result === true) {
        this.loadingDataInProgress = true;

        this._service.deleteUser(user.id).pipe(finalize(() => {
          this.loadingDataInProgress = false;
          this.loadData();
        })).subscribe(
          (result) => {
            if (result)
              this._snackBar.open('Der Benutzer wurde gelöscht.', 'Erfolg', { duration: 4500 });
            else
              this._snackBar.open('Der Benutzer konnte nicht gelöscht werden', 'Fehler', { duration: 4500 });
          }
          );
      }
    })


  }

}
