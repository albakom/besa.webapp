import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FinishBranchOffJobPageComponent } from './finish-branch-off-job-page.component';

describe('FinishBranchOffJobPageComponent', () => {
  let component: FinishBranchOffJobPageComponent;
  let fixture: ComponentFixture<FinishBranchOffJobPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FinishBranchOffJobPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FinishBranchOffJobPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
