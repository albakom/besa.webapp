import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, Validators } from '@angular/forms';
import { MatDialog, MatExpansionPanel, MatSnackBar } from '@angular/material';
import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { TakePicturesDialogComponent } from '../../dialogs/take-pictures-dialog/take-pictures-dialog.component';
import { ITakePictureDialogOptions } from '../../dialogs/take-pictures-dialog/itake-picture-dialog-options';
import { FormBasedPage } from '../base/form-based-page';
import { FinishBranchOffJobErrorMessage } from '../../models/error-messages/finish-branchoff-job-error-message';
import { FormErrorMessage } from '../../models/helper/form-error-message';
import { fuseAnimations } from '@fuse/animations';
import { UploadFileModel } from '../../models/helper/upload-file-model';
import { ShowPictureDialogComponent } from '../../dialogs/show-picture-dialog/show-picture-dialog.component';
import { IShowPictureDialogOptions } from '../../dialogs/show-picture-dialog/ishow-picture-dialog-options';
import { IFinishBranchOffJobModel } from '../../models/service/jobs/ifinish-branch-off-job-model';
import { UploadService } from '../../services/upload/upload.service';
import { UploadComponent } from '../upload/upload.component';
import { finalize } from 'rxjs/operators';
import { JobService } from '../../services/job.service';

@Component({
  selector: 'besa-finish-branch-off-job-page',
  templateUrl: './finish-branch-off-job-page.component.html',
  styleUrls: ['./finish-branch-off-job-page.component.scss'],
  animations: fuseAnimations,
})
export class FinishBranchOffJobPageComponent extends FormBasedPage implements OnInit, OnDestroy {


  //#region Properties

  public files: UploadFileModel[];
  public isFormValid: boolean;
  public sendingInProgress: boolean;

  public uploadingInProgress: boolean = false;

  @ViewChild('dataPanel')
  public dataPanel: MatExpansionPanel;

  @ViewChild('upload')
  public upload: UploadComponent;

  //#endregion

  constructor(
    private _dialog: MatDialog,
    private _fb: FormBuilder,
    private _snackbar: MatSnackBar,
    private _jobService: JobService,
    private _router: Router,
    private _uploadService: UploadService,
    private _activeRoute: ActivatedRoute,
  ) {
    super();
    this.files = new Array();

    super.initForm(
      this._fb.group({
        jobId: [+_activeRoute.snapshot.params['id'], [Validators.required]],
        comment: ['', [Validators.maxLength(1024)]],
        problemHappend: _fb.group(
          {
            value: [false, []],
            description: ['', [Validators.maxLength(1024)]],
          }
        ),
        ductChanged: _fb.group(
          {
            value: [false, []],
            description: ['', [Validators.maxLength(1024)]],
          }
        ),
      }));

    super.addSubscription(_activeRoute.params.subscribe((params) => {
      this.form.patchValue({ 'jobId': +params['id'] })
    }));

    super.addSubscription(this.form.statusChanges.subscribe((something) => {
      this.isFormValid = this.checkIfFormIsValid(false);
    }))

    this.isFormValid = false;
  }

  ngOnInit() {
  }

  ngOnDestroy() {
    super.unsubcripeAll();
  }

  //#region Methods

  protected getErrorMessages(): FormErrorMessage[] {
    return FinishBranchOffJobErrorMessage;
  }

  public checkIfFormIsValid(setValue: boolean): boolean {
    let isValid = true;
    if (this.form.invalid === true) {
      isValid = false;
    } else {
      const value: IFinishBranchOffJobModel = this.form.value;
      if ((value.ductChanged.value === true && !value.ductChanged.description) ||
        (value.problemHappend.value === true && !value.problemHappend.description) ||
        this.files.length == 0) {
        isValid = false;
      }
    }

    if (setValue === true) {
      this.isFormValid = isValid;
    }

    return isValid;
  }

  public openTakePictureDialog(): void {
    const options: ITakePictureDialogOptions = {};
    const ref = this._dialog.open<TakePicturesDialogComponent>(TakePicturesDialogComponent, { data: options });

    ref.afterClosed().subscribe((result) => {

    });
  }

  public onFileSelect(files: FileList) {
    for (let index = 0; index < files.length; index++) {
      const file = files.item(index);

      const fileModel: UploadFileModel = {
        type: file.type.startsWith('image') === true ? 'image' : 'file',
        file: file,
        name: file.name,
      };

      if (this.files.length == 0 && this.dataPanel) {
        this.dataPanel.open();
      }

      this.files.unshift(fileModel);

      if (fileModel.type === 'image') {
        const reader = new FileReader();
        fileModel.previewLoading = true;

        reader.onload = (event: any) => {
          fileModel.imgAsString = event.target.result;
          fileModel.previewLoading = false;
        };

        reader.readAsDataURL(file);
      }

    }

    this.checkIfFormIsValid(true);
  }

  public removeFile(file: UploadFileModel): void {
    const index = this.files.indexOf(file);
    if (index >= 0) {
      this.files.splice(index, 1);
    }
  }

  public showPreview(file: UploadFileModel): void {
    if (!file.imgAsString) { return; }

    const options: IShowPictureDialogOptions = {
      imgAsString: file.imgAsString
    };
    const ref = this._dialog.open<ShowPictureDialogComponent>(ShowPictureDialogComponent, { data: options });
  }

  public finishJob(): void {
    if (this.sendingInProgress === true) { return; }
    if (this.form.invalid === true) { return; }

    if (this.dataPanel) {
      this.dataPanel.open();
    }

    this.sendingInProgress = true;
    this.uploadingInProgress = true;

    const uploadRef = this._snackbar.open('Dateien werden hochgeladen', 'Verarbeitung', { duration: -1 });

    this.upload.startUpload(this.files.map((val) => val.file)).pipe(finalize(() => {
      uploadRef.dismiss();
      this.uploadingInProgress = false;
    })).subscribe((fileIds) => {
      this._snackbar.open('Auftrag wird abgeschlossen', 'In Bearbeitung', { duration: -1 });

      const formValue: IFinishBranchOffJobModel = this.form.value;
      formValue.fileIds = fileIds;

      this._jobService.finishBranchoffJob(formValue).pipe(finalize(() => {
        this.sendingInProgress = false;
        this.uploadingInProgress = false;
      })).subscribe((result) => {
        this._snackbar.open('Auftrag wurde  abgeschlossen', 'Erfolg', { duration: 4500 });
        this._router.navigate(['member/branch-off-job/', formValue.jobId]);
      }, (err) => {
        this._snackbar.open('Auftrag wurde nicht abgeschlossen', 'Fehler', { duration: 4500 });
      });

    }, (err) => {
      this._snackbar.open('Dateien konnten nicht hochgeladen werden', 'Fehler', { duration: 4500 });
      this.sendingInProgress = false;
    });

  }
  //#endregion

}
