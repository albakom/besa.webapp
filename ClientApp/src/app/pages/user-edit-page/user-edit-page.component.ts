import { LoginService } from './../../services/login.service';
import { SimpleDialogComponent } from '../../dialogs/simple-dialog/simple-dialog.component';
import { ISimpleDialogOptions } from '../../dialogs/simple-dialog/isimple-dialog-options';
import { finalize } from 'rxjs/operators';
import { FormErrorMessage } from '../../models/helper/form-error-message';
import { IUserEditModel } from './../../models/service/user/iuser-edit-model';
import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';
import { UserService } from '../../services/user/user.service';
import { MatSnackBar, MatDialog, MatCheckboxChange } from '@angular/material';
import { FormBuilder, Validators } from '@angular/forms';
import { FormBasedPage } from '../base/form-based-page';
import { IUserOverviewModel } from '../../models/service/user/iuser-overview-model';
import { IFormUserEditModel } from '../../models/forms/iform-user-edit-model';
import { UserEditFormErrorMessage } from '../../models/error-messages/user-edit-form-error-message';
import { BesaRoles } from '../../models/service/besa-roles.enum';

@Component({
  selector: 'besa-user-edit-page',
  templateUrl: './user-edit-page.component.html',
  styleUrls: ['./user-edit-page.component.scss'],
  animations: fuseAnimations
})
export class UserEditPageComponent extends FormBasedPage implements OnInit {

  //#region Fields

  private _id: number;

  //#endregion

  //#region Properties

  public loadingDataInProgress: boolean;
  public user: IUserEditModel;

  //#endregion

  public possibleRoles: BesaRoles[];

  constructor(
    private _router: Router,
    private _activeRoute: ActivatedRoute,
    private _loginService: LoginService,
    private _service: UserService,
    private _snackBar: MatSnackBar,
    private _fb: FormBuilder,
    private _dialog: MatDialog
  ) {
    super();

    this.possibleRoles = _loginService.possibleRoles;

  }

  ngOnInit() {
    this.initializeFormValues();
    this.loadData();
  }

  initializeFormValues(): void {
    super.initForm(
      this._fb.group({
        surname: ['', [Validators.required, Validators.maxLength(64)]],
        lastname: ['', [Validators.required, Validators.maxLength(64)]],
        phone: ['', [Validators.required, Validators.maxLength(32)]],
        eMailAddress: ['', [Validators.required, Validators.maxLength(255)]],
        role: [0, [Validators.required, Validators.min(1)]],
        isManagement: [false, [Validators.required]],
      })
    );
  }

  loadData(): void {
    this._id = +this._activeRoute.snapshot.paramMap.get('id');
    this.loadingDataInProgress = true;
    var user: IUserEditModel;
    this._service.getUser(this._id).pipe(finalize(() => {
      this.form.setValue({
        surname: user.surname,
        lastname: user.lastname,
        phone: user.phone,
        eMailAddress: user.eMailAddress,
        role: user.role,
        isManagement: user.isManagement,
      });
      this.loadingDataInProgress = false;
    })).subscribe((result) => {
      user = result;
      this.user = result;
    });
  }

  public hasRole(role: BesaRoles): boolean {
    return (this.form.value.role & role) === role;
  }

  public roleChanged(event: MatCheckboxChange, value: BesaRoles): void {
    const control = this.form.get('role');
    let currentBesaValue = +control.value;
    if (event.checked == true) {
      currentBesaValue |= value;
    } else {
      currentBesaValue ^= value;
    }

    control.setValue(currentBesaValue);
  }

  editUser(): void {
    const dialogOptions: ISimpleDialogOptions = {
      title: 'Speichern bestätigen',
      question: 'Möchten Sie wirklich speichern?'
    };

    const dialogRef = this._dialog.open(SimpleDialogComponent, {
      data: dialogOptions
    });

    dialogRef.afterClosed().subscribe((result: boolean) => {
      if (result === true) {
        const formValue: IFormUserEditModel = this.form.value;
        const editedUser: IUserEditModel = {
          id: this.user.id,
          surname: formValue.surname,
          lastname: formValue.lastname,
          phone: formValue.phone,
          eMailAddress: formValue.eMailAddress,
          role: formValue.role,
          companyId: this.user.companyId,
          isManagement: formValue.isManagement,
        };

        this._service.editUser(editedUser).pipe().subscribe(
          (result) => {
            this._snackBar.open('Die Änderung wurde verarbeitet', 'Erfolg', { duration: 4500 });
            this._router.navigate(['/member/users-overview']);
            this._loginService.checkRoles().subscribe((result) => { });
          },
          (err) => this._snackBar.open('Die ausstehende Änderung konnte nicht verarbeitet werden', 'Fehler', { duration: 4500 })
        );
      }
    });

  }

  cancelEdit(): void {
    // const dialogOptions: ISimpleDialogOptions = {
    //   title: 'Abbrechen bestätigen',
    //   question: 'Möchten Sie wirklich abbrechen und ungespeicherte Änderungen verwerfen?'
    // };

    // const dialogRef = this._dialog.open(SimpleDialogComponent, {
    //   data: dialogOptions
    // });

    // dialogRef.afterClosed().subscribe((result: boolean) => {
    //   if (result === true) {
    //     this.loadingDataInProgress = true;

    this._router.navigate(['/member/users-overview']);
    //   }
    // });
  }

  protected getErrorMessages(): FormErrorMessage[] {
    return UserEditFormErrorMessage;
  }

}
