import { FormBuilder, Validators } from '@angular/forms';
import { ConstructionStageService } from './../../services/construction-stage.service';
import { Component, OnInit } from '@angular/core';
import { ISimpleCabinetOverviewModel } from '../../models/service/construction-stage/isimple-cabinet-overview-model';
import { ISimpleBuildingOverviewModel } from '../../models/service/construction-stage/isimple-building-overview-model';
import { Subject } from 'rxjs';
import { OnDestroy } from '@angular/core/src/metadata/lifecycle_hooks';
import { debounceTime, distinctUntilChanged, switchMap, finalize } from 'rxjs/operators';
import { MatAutocompleteSelectedEvent, MatSelectionListChange, MatSnackBar } from '@angular/material';
import { FormBasedPage } from '../base/form-based-page';
import { CreateConstructionStageFormMessages } from '../../models/error-messages/create-construction-stage-form-message';
import { FormErrorMessage } from '../../models/helper/form-error-message';
import { fuseAnimations } from '@fuse/animations';
import { IConstructionStageCreateModel } from '../../models/service/construction-stage/iconstruction-stage-create-model';

@Component({
  selector: 'besa-create-construction-stage',
  templateUrl: './create-construction-stage.component.html',
  styleUrls: ['./create-construction-stage.component.scss'],
  animations: fuseAnimations,
})
export class CreateConstructionStageComponent extends FormBasedPage implements OnInit, OnDestroy {


  //#region Fields

  private _searchTerm: Subject<string>;
  private _buildingBranchableDict: { [branchableId: number]: ISimpleBuildingOverviewModel[] };

  //#endregion

  //#region Properties

  public cabinents: ISimpleCabinetOverviewModel[];
  public buildings: ISimpleBuildingOverviewModel[];


  public searchCabinets: ISimpleCabinetOverviewModel[];
  public createInProgress: boolean;


  //#endregion

  constructor(
    private _fb: FormBuilder,
    private _snackBar: MatSnackBar,
    private _service: ConstructionStageService) {
    super();

    this._searchTerm = new Subject<string>();

    this.prepare();

    super.addSubscription(this._searchTerm.pipe(
      debounceTime(150),
      distinctUntilChanged(),
      switchMap((item) => {
        return this._service.searchCabinents(item);
      })
    ).subscribe((items) => {
      this.searchCabinets = items;
    }));

    super.initForm(
      this._fb.group({
        name: ['', [Validators.required, Validators.maxLength(64)]],
        createJobs: [true, [Validators.required]],
        saerchTermValue: ['', []],
      }));

    super.addSubscription(this.form.get('saerchTermValue').valueChanges.subscribe((value: string) => {
      this._searchTerm.next(value);
    }));
  }

  ngOnInit() {
  }

  ngOnDestroy() {
    super.unsubcripeAll();
  }

  //#region Methods

  private prepare(): void {
    this.buildings = new Array();
    this.cabinents = new Array();
    this._buildingBranchableDict = {};
    this.searchCabinets = new Array();
    if (this.form) {
      this.form.reset();
    }
  }

  public cabinentSelected(eventInfo: MatAutocompleteSelectedEvent): void {
    const cabinet: ISimpleCabinetOverviewModel = eventInfo.option.value;

    if (this.cabinents.indexOf(cabinet) >= 0) { return; }

    this.cabinents.push(cabinet);
    this._service.loadBuldingsByBrancableId(cabinet.id).subscribe((result) => {
      this.buildings.push(...result);
      this._buildingBranchableDict[cabinet.id] = result;
    }, (err) => {
      this._snackBar.open("Gebäude konnen nicht geladen werden", "Fehler", { duration: 3500 });
    });
  }

  public removeCabinet(cabinet: ISimpleCabinetOverviewModel): void {
    const index = this.cabinents.indexOf(cabinet);
    if (index < 0) { return; }

    this.cabinents.slice(index, 1);

    const buldings: ISimpleBuildingOverviewModel[] = this._buildingBranchableDict[cabinet.id];
    if (this.buildings) {
      const buildingStartIndex = this.buildings.indexOf(this.buildings[0]);
      if (buildingStartIndex >= 0) {
        this.buildings.splice(buildingStartIndex, buldings.length);
      }

      delete this._buildingBranchableDict[cabinet.id];
    }
  }

  protected getErrorMessages(): FormErrorMessage[] {
    return CreateConstructionStageFormMessages;
  }

  public displayCabinentResult(item?: ISimpleCabinetOverviewModel): string {
    return item ? item.name : '';
  }

  public createConstructionStage(): void {
    if (this.form.valid == false) { return; }
    if (this.createInProgress === true) { return; }
    if (this.cabinents.length == 0) { return };

    const creationModel: IConstructionStageCreateModel = this.form.value;
    creationModel.cabinets = this.cabinents.map((value) => value.id);

    this._snackBar.open('Baugebiet wird erstellt', 'In Bearbeitung', { duration: -1 });

    this._service.createConstructionStage(creationModel).pipe(finalize(() => {
      this.createInProgress = false;
    })).subscribe((result) => {
      this._snackBar.dismiss();
      this._snackBar.open('Gebiet wurde erfolgreich erstellt', 'Erfolg', { duration: 3500 });
      this.prepare();

    }, (err) => {
      this._snackBar.dismiss();
      this._snackBar.open('Fehler beim erstellen des Gebietes', 'Fehler', { duration: 3500 });
    });


    this.createInProgress = true;
  }


  //#endregion
}
