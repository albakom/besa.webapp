import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateConstructionStageComponent } from './create-construction-stage.component';

describe('CreateConstructionStageComponent', () => {
  let component: CreateConstructionStageComponent;
  let fixture: ComponentFixture<CreateConstructionStageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateConstructionStageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateConstructionStageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
