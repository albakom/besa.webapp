import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SpliceOdfjobDetailPageComponent } from './splice-odfjob-detail-page.component';

describe('SpliceOdfjobDetailPageComponent', () => {
  let component: SpliceOdfjobDetailPageComponent;
  let fixture: ComponentFixture<SpliceOdfjobDetailPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SpliceOdfjobDetailPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpliceOdfjobDetailPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
