import { ISpliceODFJobDetailModel } from './../../models/service/jobs/splice/isplice-odf-job-detail-model';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';
import { JobDetailsPageBase } from '../base/job-details-page-base';
import { IFinishSpliceOdfjobDetailModel } from '../../models/service/jobs/splice/ifinish-splice-odfjob-detail-model';
import { RefreshManagerService } from '../../services/refresh-manager.service';
import { ActivatedRoute, Router } from '@angular/router';
import { MatSnackBar, MatDialog } from '@angular/material';
import { LoginService } from '../../services/login.service';
import { UriService } from '../../services/uri.service';
import { JobService } from '../../services/job.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'besa-splice-odfjob-detail-page',
  templateUrl: './splice-odfjob-detail-page.component.html',
  animations: fuseAnimations,
  styleUrls: ['./splice-odfjob-detail-page.component.scss']
})
export class SpliceOdfjobDetailPageComponent extends
  JobDetailsPageBase<IFinishSpliceOdfjobDetailModel, ISpliceODFJobDetailModel>
  implements OnInit, OnDestroy {

  constructor(refreshManager: RefreshManagerService,
    activeRoute: ActivatedRoute,
    snackBar: MatSnackBar,
    dialog: MatDialog,
    loginService: LoginService,
    router: Router,
    uriService: UriService,
    service: JobService,
  ) {
    super(refreshManager, activeRoute, snackBar, dialog, loginService, router, uriService, service);
  }

  ngOnInit() {
    super.loadDetailsInternal();
  }

  ngOnDestroy() {
    super.unsubcripeAll();
  }

  //#region Methods

  protected getDetails(id: number): Observable<ISpliceODFJobDetailModel> {
    return this._service.loadSpliceODFJobDetails(id);
  }

  protected getFinishUrl(): string {
    return '/member/finish-splice-odf-job';
  }

  //#endregion
}
