import { fuseAnimations } from '@fuse/animations';
import { MatSnackBar } from '@angular/material';
import { ConstructionStageService } from './../../services/construction-stage.service';
import { SubscriptionAwareComponent } from './../../helper/subscription-aware-component';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { RefreshManagerService } from '../../services/refresh-manager.service';
import { finalize } from 'rxjs/operators';
import { Router } from '@angular/router';
import { IConstructionStageOverviewForJobsModel } from '../../models/service/construction-stage/iconstruction-stage-overview-for-jobs-model';

@Component({
  selector: 'besa-cilvil-work-overview-page',
  templateUrl: './cilvil-work-overview-page.component.html',
  styleUrls: ['./cilvil-work-overview-page.component.scss'],
  animations: fuseAnimations,
})
export class CilvilWorkOverviewPageComponent extends SubscriptionAwareComponent implements OnInit, OnDestroy {

  //#region Properties

  public items: IConstructionStageOverviewForJobsModel[];
  public itemsAreLoading: boolean;

  //#endregion

  constructor(refreshManger: RefreshManagerService,
    private _snackBar: MatSnackBar,
    private _router: Router,
    private _service: ConstructionStageService) {
    super();

    super.addSubscription(refreshManger.refreshRequested.subscribe((result) => {
      this.loadConstructionStages();
    }));
  }

  ngOnInit() {
    this.loadConstructionStages();
  }

  ngOnDestroy() {
    super.unsubcripeAll();
  }

  //#region Methods

  public loadConstructionStages(): void {
    this.itemsAreLoading = true;

    this._service.loadOverviewForCivilWork(false).pipe(finalize(() => {
      this.itemsAreLoading = false;
    })).subscribe((result) => {
      this.items = result;
    }, (err) => {
      this._snackBar.open('Bauabschnitte konnte nicht geladen werden', 'Fehler', { duration: 4500 });
    });
  }

  public navigateToDetails(item: IConstructionStageOverviewForJobsModel): void {
    this._router.navigate(['/member/civil-work-construction-stage-detail', item.id]);
  }

  //#endregion

}
