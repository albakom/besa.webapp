import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CilvilWorkOverviewPageComponent } from './cilvil-work-overview-page.component';

describe('CilvilWorkOverviewPageComponent', () => {
  let component: CilvilWorkOverviewPageComponent;
  let fixture: ComponentFixture<CilvilWorkOverviewPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CilvilWorkOverviewPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CilvilWorkOverviewPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
