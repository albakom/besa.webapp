import { ISortDirections } from './../../models/service/jobs/isort-directions';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBasedPage } from '../base/form-based-page';
import { RefreshManagerService } from '../../services/refresh-manager.service';
import { MatSnackBar, MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { JobService } from '../../services/job.service';
import { IJobCollectionSortProperties } from '../../models/service/jobs/ijob-collection-sort-properties';
import { debounceTime, distinctUntilChanged, switchMap, map, finalize } from 'rxjs/operators';
import { of, Observable } from 'rxjs';
import { IJobCollectionOverviewModel } from '../../models/service/jobs/overview/ijob-collection-overview-model';
import { FormErrorMessage } from '../../models/helper/form-error-message';
import { IJobCollectionFilterModel } from '../../models/service/jobs/ijob-collection-filter-model';
import { fuseAnimations } from '@fuse/animations';
import { ISimpleDialogOptions } from '../../dialogs/simple-dialog/isimple-dialog-options';
import { SimpleDialogComponent } from '../../dialogs/simple-dialog/simple-dialog.component';

@Component({
  selector: 'besa-task-overview-page',
  templateUrl: './task-overview-page.component.html',
  styleUrls: ['./task-overview-page.component.scss'],
  animations: fuseAnimations,
})
export class TaskOverviewPageComponent extends FormBasedPage implements OnInit, OnDestroy {


  //#region scroll-based properties

  private _itemPerRequest: number;
  private _canScroll: boolean = false;
  public loadingMoreDataInProgress: boolean;

  //#endregion

  public loadingDataInProgress: boolean = false;
  public filter: IJobCollectionFilterModel;
  public boundedJobs: IJobCollectionOverviewModel[];
  public autoUpdateFilter: boolean;

  public filterForm: FormGroup;
  public sortingDirections: ISortDirections[];
  public sortingProperties: IJobCollectionSortProperties[];

  public deleteInProgress: boolean;

  constructor(
    private refreshManager: RefreshManagerService,
    private _snackBar: MatSnackBar,
    private _router: Router,
    private _fb: FormBuilder,
    private _dialog: MatDialog,
    private _service: JobService,
  ) {
    super();

    this.autoUpdateFilter = true;
    this._itemPerRequest = 30;

    this.sortingDirections = [ISortDirections.Ascending, ISortDirections.Descending];
    this.sortingProperties = [IJobCollectionSortProperties.Name, IJobCollectionSortProperties.CompanyName, IJobCollectionSortProperties.DoingPercentage, IJobCollectionSortProperties.FinishedTill];

    this.filter = { start: 0, amount: this._itemPerRequest, sortProperty: IJobCollectionSortProperties.FinishedTill, sortDirection: ISortDirections.Ascending };

    super.addSubscription(refreshManager.refreshRequested.subscribe((result) => {
      this.loadDataWithSubscripe();
    }));

    super.initForm(this._fb.group
      (
      {
        start: [0, [Validators.required]],
        amount: [this._itemPerRequest, [Validators.required]],
        sortProperty: [IJobCollectionSortProperties.FinishedTill, []],
        query: [null, []],
        sortDirection: [ISortDirections.Ascending, []],
      }
      ));

    super.addSubscription(this.form.valueChanges.pipe(
      debounceTime(150),
      distinctUntilChanged(),
      switchMap((item: any) => {
        if (this.autoUpdateFilter == true && item) {
          this.filter = this.form.value;
          return this.loadData();
        }
        else {
          return of(new Array<IJobCollectionOverviewModel>());
        }
      })
    ).subscribe((result) => {
    }, (err) => {
      this._snackBar.open("Aufträge wurden nicht gelanden", "Fehler", { duration: 4500 });
    }));
  }

  ngOnInit() {
    this.loadDataWithSubscripe();
  }

  ngOnDestroy() {
    super.unsubcripeAll();
  }

  //#region Methods  

  protected getErrorMessages(): FormErrorMessage[] {
    return new Array();
  }

  private loadDataWithSubscripe() {
    this.loadData().subscribe((result) => {
    }, (err) => {
      this._snackBar.open("Aufträge wurden nicht gelanden", "Fehler", { duration: 4500 });
    });
  }

  loadData(): Observable<IJobCollectionOverviewModel[]> {
    this.loadingDataInProgress = true;
    const obs = this._service.getCollectionJobOverview(this.filter).pipe(
      map((result) => {
        this.boundedJobs = result;
        this._canScroll = this._itemPerRequest == result.length;
        return result;
      }),
      finalize(() => {
        this.loadingDataInProgress = false;
      }));

    return obs;
  }

  onScroll() {
    if (this._canScroll === false) { return; }
    if (this.loadingMoreDataInProgress === true) { return; }

    this.filter.start = this.boundedJobs.length;

    this.loadingMoreDataInProgress = true;
    this._service.getCollectionJobOverview(this.filter).pipe(finalize(() => {
      this.loadingMoreDataInProgress = false;
    })).subscribe((result) => {
      this.boundedJobs.push(...result);
      this._canScroll = this._itemPerRequest == result.length;
    }, (err) => {
      this._snackBar.open("Aufträge wurden nicht gelanden", "Fehler", { duration: 4500 });
    });
  }

  public createNewCollectionJob(): void {
    this._router.navigate(['/member/bind-tasks']);
  }

  public deleteJob(jobCollection: IJobCollectionOverviewModel) {
    if (this.deleteInProgress === true) { return; }
    const dialogOptions: ISimpleDialogOptions = {
      title: 'Löschen bestätigen',
      question: `Möchten Sie die Auftragszuweisung ${jobCollection.name} wirklich löschen?`
    };

    const dialogRef = this._dialog.open(SimpleDialogComponent, {
      data: dialogOptions
    });

    dialogRef.afterClosed().subscribe((result: boolean) => {
      if (result === true) {

        this._snackBar.open('Die Auftragszuweisung wird gelöscht', 'In Bearbeitung', { duration: -1 });
        this.deleteInProgress = true;

        this._service.deleteCollectionJob(jobCollection.collectionJobId).pipe(finalize(() => {
          this.deleteInProgress = false;
        })).subscribe(
          (result) => {
            if (result)
              this._snackBar.open('Zuweisung wurde gelöscht.', 'Erfolg', { duration: 4500 });
              const index = this.boundedJobs.indexOf(jobCollection);
            if (index >= 0) {
              this.boundedJobs.splice(index, 1);
            }
            else
              this._snackBar.open('Zuweisung konnte nicht gelöscht werden', 'Fehler', { duration: 4500 });
          }
        );
      }
    });
  }

  public filterItems(): void {
    this.loadDataWithSubscripe();
  }

  public resetFilter(): void {
    this.form.setValue(
      {
        start: 0,
        amount: this._itemPerRequest,
        query: null,
        sortProperty: IJobCollectionSortProperties.FinishedTill,
        sortDirection: ISortDirections.Ascending,
      }
    );
  }

  //#endregion

}