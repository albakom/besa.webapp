import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TaskOverviewPageComponent } from './task-overview-page.component';

describe('TaskOverviewPageComponent', () => {
  let component: TaskOverviewPageComponent;
  let fixture: ComponentFixture<TaskOverviewPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TaskOverviewPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaskOverviewPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
