import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FinishConnectBuildingJobPageComponent } from './finish-connect-building-job-page.component';

describe('FinishConnectBuildingJobPageComponent', () => {
  let component: FinishConnectBuildingJobPageComponent;
  let fixture: ComponentFixture<FinishConnectBuildingJobPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FinishConnectBuildingJobPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FinishConnectBuildingJobPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
