import { OnDestroy } from '@angular/core/src/metadata/lifecycle_hooks';
import { Component, OnInit } from '@angular/core';
import { FinishJobPageBase } from '../base/finish-job-page-base';
import { IFinishConnectBuildingModel } from '../../models/service/jobs/connect-building/ifinish-connect-building-model';
import { MatDialog, MatSnackBar } from '@angular/material';
import { FormBuilder, Validators } from '@angular/forms';
import { JobService } from '../../services/job.service';
import { Router, ActivatedRoute } from '@angular/router';
import { UploadService } from '../../services/upload/upload.service';
import { Observable } from 'rxjs';
import { FinishBranchOffJobErrorMessage } from '../../models/error-messages/finish-branchoff-job-error-message';
import { FormErrorMessage } from '../../models/helper/form-error-message';
import { fuseAnimations } from '@fuse/animations';

@Component({
  selector: 'besa-finish-connect-building-job-page',
  templateUrl: './finish-connect-building-job-page.component.html',
  styleUrls: ['./finish-connect-building-job-page.component.scss'],
  animations: fuseAnimations
})
export class FinishConnectBuildingJobPageComponent extends FinishJobPageBase<IFinishConnectBuildingModel> implements OnInit, OnDestroy {

  constructor(dialog: MatDialog,
    fb: FormBuilder,
    snackbar: MatSnackBar,
    jobService: JobService,
    router: Router,
    uploadService: UploadService,
    activeRoute: ActivatedRoute,
  ) {
    super(dialog, fb, snackbar, jobService, router, uploadService, activeRoute);
  }

  ngOnInit() {
    this.filesNeeded = true;

    super.initFinishForm({
      ductChanged: this._fb.group(
        {
          value: [false, []],
          description: ['', [Validators.maxLength(1024)]],
        }
      )
    });
  }

  ngOnDestroy() {
    super.unsubcripeAll();
  }

  //#region Methods

  protected checkIfModelIsValid(model: IFinishConnectBuildingModel): boolean {
    if (model.ductChanged.value == true && !model.ductChanged.description) {
      return false;
    }
    else {
      return true;
    }
  }

  protected getfinishJobInvoker(model: IFinishConnectBuildingModel): Observable<number> {
    return this._jobService.finishConnectBuildingJob(model);
  }
  protected getJobUrl(): string {
    return '/member/connection-building-job';
  }
  protected getErrorMessages(): FormErrorMessage[] {
    return FinishBranchOffJobErrorMessage;
  }

  //#endregion

}
