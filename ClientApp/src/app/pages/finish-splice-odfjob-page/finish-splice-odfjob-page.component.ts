import { Component, OnInit, OnDestroy } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';
import { FinishJobPageBase } from '../base/finish-job-page-base';
import { MatDialog, MatSnackBar } from '@angular/material';
import { FormBuilder, Validators } from '@angular/forms';
import { JobService } from '../../services/job.service';
import { Router, ActivatedRoute } from '@angular/router';
import { UploadService } from '../../services/upload/upload.service';
import { Observable } from 'rxjs';
import { FormErrorMessage } from '../../models/helper/form-error-message';
import { IFinishSpliceODFJobModel } from '../../models/service/jobs/splice/ifinish-splice-odfjob-model';
import { FinishSpliceODFJobErrorMessage } from '../../models/error-messages/finish-splice-odfjob-error-message';

@Component({
  selector: 'besa-finish-splice-odfjob-page',
  templateUrl: './finish-splice-odfjob-page.component.html',
  styleUrls: ['./finish-splice-odfjob-page.component.scss'],
  animations: fuseAnimations,
})
export class FinishSpliceOdfjobPageComponent extends FinishJobPageBase<IFinishSpliceODFJobModel> implements OnInit, OnDestroy {

  constructor(
    dialog: MatDialog,
    fb: FormBuilder,
    snackbar: MatSnackBar,
    jobService: JobService,
    router: Router,
    uploadService: UploadService,
    activeRoute: ActivatedRoute,
  ) {
    super(dialog, fb, snackbar, jobService, router, uploadService, activeRoute);
   }

  ngOnInit() {
    this.filesNeeded = true;

    super.initFinishForm({
      cableMetric: [0.0, [Validators.required, Validators.min(0)]],
    });
  }

  ngOnDestroy() {
    super.unsubcripeAll();
  }

   //#region Methods

   protected checkIfModelIsValid(model: IFinishSpliceODFJobModel): boolean {
    return true;
  }

  protected getfinishJobInvoker(model: IFinishSpliceODFJobModel): Observable<number> {
    return this._jobService.finishSpliceODFJob(model);
  }
  
  protected getJobUrl(): string {
    return '/member/splice-odf-details';
  }

  protected getErrorMessages(): FormErrorMessage[] {
    return FinishSpliceODFJobErrorMessage;
  }

  //#endregion

}
