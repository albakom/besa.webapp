import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FinishSpliceOdfjobPageComponent } from './finish-splice-odfjob-page.component';

describe('FinishSpliceOdfjobPageComponent', () => {
  let component: FinishSpliceOdfjobPageComponent;
  let fixture: ComponentFixture<FinishSpliceOdfjobPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FinishSpliceOdfjobPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FinishSpliceOdfjobPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
