import { SubscriptionAwareComponent } from './../../helper/subscription-aware-component';
import { fuseAnimations } from '@fuse/animations';
import { Component, OnInit } from '@angular/core';
import { IContactInfo } from '../../models/service/icontact-info';
import { Router, ActivatedRoute } from '@angular/router';
import { CustomerService } from '../../services/customer.service';
import { MatSnackBar, MatDialog } from '@angular/material';
import { finalize } from 'rxjs/operators';
import { ISimpleDialogOptions } from '../../dialogs/simple-dialog/isimple-dialog-options';
import { SimpleDialogComponent } from '../../dialogs/simple-dialog/simple-dialog.component';

@Component({
  selector: 'besa-contact-detail-page',
  templateUrl: './contact-detail-page.component.html',
  styleUrls: ['./contact-detail-page.component.scss'],
  animations: fuseAnimations
})
export class ContactDetailPageComponent extends SubscriptionAwareComponent implements OnInit {

  private _id: number;
  public loadingDataInProgress: boolean;
  public contact: IContactInfo;

  constructor(
    private _router: Router,
    private _activeRoute: ActivatedRoute,
    private _service: CustomerService,
    private _snackBar: MatSnackBar,
    private _dialog: MatDialog
  ) {
    super();
   }

  ngOnInit() {
    this.loadData();
  }

  public loadData() {
    this._id = +this._activeRoute.snapshot.paramMap.get('id');
    this.loadingDataInProgress = true;
    this._service.getContact(this._id).pipe(finalize(() => {
      this.loadingDataInProgress = false;
    })).subscribe((result) => {
      this.contact = result;
    }, (err) => {
      this._snackBar.open('Die ausstehende Anfrage konnte nicht bearbeitet werden', 'Fehler', { duration: 4500 });
    });
  }

  public navigateBack() {
    this._router.navigate(['/member/contact-overview']);
  }

  public editContact() {
    this._router.navigate(['/member/contact-edit', this._id]);
  }

  public deleteContact() {
    // checkt ob Kontakt in Verwendung ist
    this._service.checkIfContactIsInUse(this._id).pipe().subscribe((result: Boolean) => {
      // wenn nein löschen
      if (!result) {
        const dialogOptions: ISimpleDialogOptions = {
          title: 'Löschen bestätigen',
          question: `Möchten Sie wirklich den Kontakt ${this.contact.surname} ${this.contact.lastname} löschen?`,
        };
    
        const dialogRef = this._dialog.open(SimpleDialogComponent, {
          data: dialogOptions
        });
    
        dialogRef.afterClosed().subscribe((result: boolean) => {
          if (result == true) {
            this.loadingDataInProgress = true;
            this._snackBar.open('Der Kontakt wird gelöscht', 'In Bearbeitung', { duration: -1 });
    
            this._service.deleteContact(this._id).pipe(finalize(() => {
              this.loadingDataInProgress = false;
              this._router.navigate(['/member/contact-overview']);
            })).subscribe((result) => {
              if (result) {
                this._snackBar.open('Der Kontakt wurde gelöscht', 'Erfolg', { duration: 4500 });
              } else {
                this._snackBar.open('Der Kontakt konnte nicht gelöscht werden', 'Fehler', { duration: 4500 });
              }
            }, (err) => {
              this._snackBar.open('Der Kontakt konnte nicht gelöscht werden', 'Fehler', { duration: 4500 });
            });
          } else {
            this.loadingDataInProgress = false;
          }
        })
      } else {
        // Wenn ja explizit nachfragen
        this._snackBar.open('Kontakt konnte nicht gelöscht werden, da dieser in Verwendung ist', 'Fehler', { duration: 4500 });
      }
    }, (err) => {
      // was da tun?
    });
    
  }

}
