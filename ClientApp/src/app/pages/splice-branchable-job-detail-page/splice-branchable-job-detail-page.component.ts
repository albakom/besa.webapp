import { ISpliceEntryModel } from './../../models/service/jobs/splice/isplice-entry-model';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';
import { JobDetailsPageBase } from '../base/job-details-page-base';
import { RefreshManagerService } from '../../services/refresh-manager.service';
import { ActivatedRoute, Router } from '@angular/router';
import { MatSnackBar, MatDialog, MatTableDataSource, MatSlideToggleChange } from '@angular/material';
import { LoginService } from '../../services/login.service';
import { UriService } from '../../services/uri.service';
import { JobService } from '../../services/job.service';
import { Observable } from 'rxjs';
import { ISpliceBranchableJobDetailModel } from '../../models/service/jobs/splice/isplice-branchable-job-detail-model';
import { IFinishSpliceBranchableJobDetailModel } from '../../models/service/jobs/splice/ifinish-splice-branchable-job-detail-model';

@Component({
  selector: 'besa-splice-branchable-job-detail-page',
  templateUrl: './splice-branchable-job-detail-page.component.html',
  styleUrls: ['./splice-branchable-job-detail-page.component.scss'],
  animations: fuseAnimations,
})
export class SpliceBranchableJobDetailPageComponent extends
JobDetailsPageBase<IFinishSpliceBranchableJobDetailModel, ISpliceBranchableJobDetailModel>
implements OnInit, OnDestroy {

  private _normelDisplayColumns: string[];
  private _reverseDisplayColumns: string[];

  public tableViewDesc: boolean = false;
  public displayColumns: string[];
  public tableData: MatTableDataSource<ISpliceEntryModel>;

constructor(refreshManager: RefreshManagerService,
  activeRoute: ActivatedRoute,
  snackBar: MatSnackBar,
  dialog: MatDialog,
  loginService: LoginService,
  router: Router,
  uriService: UriService,
  service: JobService,
) {
  super(refreshManager, activeRoute, snackBar, dialog, loginService, router, uriService, service);

  this._normelDisplayColumns = ['firstFiberName','firstFiberBundle','firstFiberNumber','firstFiberColor','secondFiberName','secondFiberBundle','secondFiberNumber','secondFiberColor','tray','number'];
  this._reverseDisplayColumns = ['secondFiberName','secondFiberBundle','secondFiberNumber','secondFiberColor','firstFiberName','firstFiberBundle','firstFiberNumber','firstFiberColor','tray','number'];
  this.displayColumns = this._normelDisplayColumns;
}

ngOnInit() {
  super.loadDetailsInternal();
}

ngOnDestroy() {
  super.unsubcripeAll();
}

//#region Methods

protected getDetails(id: number): Observable<ISpliceBranchableJobDetailModel> {
  return this._service.loadSpliceJobDetails(id);
}

protected afterDetailsLoaded(details: ISpliceBranchableJobDetailModel): void {
  this.tableData = new MatTableDataSource<ISpliceEntryModel>(details.entries);
}

protected getFinishUrl(): string {
  return '/member/finish-splice-branchable-job';
}

public tableViewChanged(event: MatSlideToggleChange): void {
  if (event.checked === true) {
    this.displayColumns = this._reverseDisplayColumns;
  } else {
    this.displayColumns = this._normelDisplayColumns;
  }
}

//#endregion



}
