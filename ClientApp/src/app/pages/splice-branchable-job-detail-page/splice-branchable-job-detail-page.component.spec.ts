import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SpliceBranchableJobDetailPageComponent } from './splice-branchable-job-detail-page.component';

describe('SpliceBranchableJobDetailPageComponent', () => {
  let component: SpliceBranchableJobDetailPageComponent;
  let fixture: ComponentFixture<SpliceBranchableJobDetailPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SpliceBranchableJobDetailPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpliceBranchableJobDetailPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
