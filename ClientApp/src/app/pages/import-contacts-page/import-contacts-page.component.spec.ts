import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ImportContactsPageComponent } from './import-contacts-page.component';

describe('ImportContactsPageComponent', () => {
  let component: ImportContactsPageComponent;
  let fixture: ComponentFixture<ImportContactsPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ImportContactsPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ImportContactsPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
