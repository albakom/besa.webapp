import { Router } from '@angular/router';
import { finalize } from 'rxjs/operators';
import { CustomerService } from './../../services/customer.service';
import { Component, OnInit } from '@angular/core';
import { StepperSelectionEvent } from '@angular/cdk/stepper';
import { UploadFileModel } from '../../models/helper/upload-file-model';
import { fuseAnimations } from '@fuse/animations';
import { CSVLineModel } from '../../models/helper/csv-line-model';
import { ContactImportProperties } from '../../models/helper/contact-import-properties.enum';
import { MatSelectChange, MatCheckboxChange, MatTableDataSource, MatSnackBar } from '@angular/material';
import { FormBasedPage } from '../base/form-based-page';
import { OnDestroy } from '@angular/core/src/metadata/lifecycle_hooks';
import { FormErrorMessage } from '../../models/helper/form-error-message';
import { FormBuilder, Validators } from '@angular/forms';
import { EncodingTypes } from '../../models/service/helper/encoding-types.enum';
import { ICSVImportContactModel } from '../../models/service/helper/csvimport-contact-model';
import { ImportCSVContactsFormMessage } from '../../models/error-messages/import-csv-contacts-form-message';

@Component({
  selector: 'besa-import-contacts-page',
  templateUrl: './import-contacts-page.component.html',
  styleUrls: ['./import-contacts-page.component.scss'],
  animations: fuseAnimations,
})
export class ImportContactsPageComponent extends FormBasedPage implements OnInit, OnDestroy {

  //#region Properties

  public file: UploadFileModel;
  public maxColumnAmount: number;
  public lines: CSVLineModel[];
  public displayedColumns: string[];
  public possibleValues: ContactImportProperties[];
  public firstLine: CSVLineModel;

  public lineDataSource: MatTableDataSource<CSVLineModel>;

  public propertyMapper: { [index: number]: ContactImportProperties };

  public missingProperties: ContactImportProperties[];

  public importInProgress: boolean;

  //#endregion

  constructor(
    private snackBar: MatSnackBar,
    fb: FormBuilder,
    private _router: Router,
    private _service: CustomerService) {
    super();

    this.file = null;

    this.possibleValues = [
      ContactImportProperties.Surname,
      ContactImportProperties.Lastname,
      ContactImportProperties.CompanyName,
      ContactImportProperties.Street,
      ContactImportProperties.StreetNumer,
      ContactImportProperties.Email,
      ContactImportProperties.Phone,
      ContactImportProperties.Type,
      ContactImportProperties.City,
      ContactImportProperties.ZipCode,
    ];

    this.propertyMapper = {};
    this.missingProperties = new Array();

    super.initForm(fb.group({
      seperationChar: [';', [Validators.required, Validators.maxLength(1), Validators.minLength(1)]],
      encoding: [EncodingTypes.UTF8, [Validators.required]],
      defaultPostalCode: ['17166', [Validators.maxLength(5), Validators.minLength(4)]],
      defaultCity: ['Teterow', [Validators.maxLength(64)]],
      uniqueSurnameAndLastname: [true, [Validators.required]],
    }));
  }

  ngOnInit() {
    super.addSubscription(this.form.valueChanges.subscribe((value) => {
      this.checkIfMappingIsDone();
    }));
  }

  ngOnDestroy() {
    super.unsubcripeAll();
  }

  //#region Methods

  protected getErrorMessages(): FormErrorMessage[] {
    return ImportCSVContactsFormMessage;
  }

  public onFileSelect(files: FileList) {

    for (let index = 0; index < files.length; index++) {
      const file = files.item(index);

      const fileModel: UploadFileModel = {
        type: file.name.endsWith('.csv') === true ? 'csv' : 'file',
        file: file,
        name: file.name,
      };

      if (fileModel.type === 'csv') {
        const reader = new FileReader();
        fileModel.previewLoading = true;
        this.file = fileModel;

        reader.onload = (event: any) => {
          fileModel.content = event.target.result;
          fileModel.previewLoading = false;
        };

        reader.readAsText(file);
      }
    }
  }

  public stepChanged(event: StepperSelectionEvent): void {
    if (event.selectedIndex == 1) {
      this.loadFile();
    }
  }

  private loadFile(): void {
    const lines: string[] = this.file.content.split('\r\n');
    this.lines = new Array();
    let max: number = 0;
    for (let index = 0; index < lines.length; index++) {
      const line = lines[index];
      const parts = line.split(';');
      // const parts = line.split(';').filter((value, index, arr) => {
      //   return value.match('^[0-9a-zA-Z]+$');
      // });

      if (parts.length > max) {
        max = parts.length;
      }
      this.lines.push(new CSVLineModel(parts));

      if (index === 0) {
        this.firstLine = this.lines[0];
      }
    }

    this.displayedColumns = new Array();
    for (let index = 0; index < max; index++) {
      this.displayedColumns.push(index.toString());
    }

    this.maxColumnAmount = max;
    this.lineDataSource = new MatTableDataSource(this.lines);
    this.checkIfMappingIsDone();
  }

  public getColumnHeader(index: number): string {
    return index.toString();
  }

  public getColumnName(index: number): string {
    return index.toString();
  }

  public propertySelected(event: MatSelectChange, index: number): void {
    const realValue: ContactImportProperties = event.value;

    const oldValue = this.propertyMapper[index];
    if (oldValue) {
      this.possibleValues.push(oldValue);
    }

    this.propertyMapper[index] = event.value;

    const propertyIndex = this.possibleValues.indexOf(realValue);
    if (propertyIndex >= 0) {
      this.possibleValues.splice(propertyIndex, 1);
    }

    this.checkIfMappingIsDone();
  }

  public removeIndex(index: number): void {
    const value: ContactImportProperties = this.propertyMapper[index];
    if (!value) { return; }

    delete this.propertyMapper[index];
    this.possibleValues.push(value);
    this.checkIfMappingIsDone();
  }

  public changeFirstLine(event: MatCheckboxChange): void {
    const value: boolean = event.checked;
    const firstLineInArray = this.firstLine == this.lines[0];
    if (value === true) {
      if (firstLineInArray === true) {
        this.lines.shift();
      }
    } else {
      if (firstLineInArray == false) {
        this.lines.unshift(this.firstLine);
      }
    }

    this.lineDataSource._updateChangeSubscription();
  }

  public checkIfMappingIsDone(): boolean {
    let result = true;
    this.missingProperties = new Array();

    const neededMappings = [
      ContactImportProperties.Lastname,
      ContactImportProperties.Street,
      ContactImportProperties.StreetNumer,
      ContactImportProperties.Phone,
    ];

    const formValue: ICSVImportContactModel = this.form.value;
    if (!formValue.defaultCity) {
      neededMappings.push(ContactImportProperties.City);
    }

    if (!formValue.defaultPostalCode) {
      neededMappings.push(ContactImportProperties.ZipCode);
    }

    for (let index = 0; index < neededMappings.length; index++) {
      const element = neededMappings[index];

      const existingIndex = this.possibleValues.indexOf(element);
      if (existingIndex >= 0) {
        result = false;
        this.missingProperties.push(element);
      }
    }

    return result;
  }

  public startImport(): void {
    if (this.importInProgress === true) { return; }
    if (this.form.invalid === true || this.checkIfMappingIsDone() == false) { return; }

    this.importInProgress = true;

    const formValue: ICSVImportContactModel = this.form.value;
    formValue.mappings = {};

    for (var key in this.propertyMapper) {
      if (this.propertyMapper.hasOwnProperty(key)) {
        const realKey = +key;
        const value: number = this.propertyMapper[realKey];
        formValue.mappings[value] = realKey;
        if (value) {
          formValue.mappings[value] = realKey;
        }
      }
    }

    formValue.lines = this.lines.map(x => x.parts);

    this._service.importContacts(formValue).pipe(finalize(() => {
      this.importInProgress = false;
    })).subscribe((result) => {
      this.snackBar.open('Import erfolgreich', 'Erfolg', { duration: 4500 });
      this._router.navigate(['member/contact-overview']);
    }, (err) => {
      this.snackBar.open('Import konnte nicht durchgeführt werden', 'Fehler', { duration: 4500 });
    });
  }

  //#endregion


}
