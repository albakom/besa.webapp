import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SpliceOdfjobOverviewPageComponent } from './splice-odfjob-overview-page.component';

describe('SpliceOdfjobOverviewPageComponent', () => {
  let component: SpliceOdfjobOverviewPageComponent;
  let fixture: ComponentFixture<SpliceOdfjobOverviewPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SpliceOdfjobOverviewPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpliceOdfjobOverviewPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
