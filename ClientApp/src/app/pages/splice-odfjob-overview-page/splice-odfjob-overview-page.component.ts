import { MatSnackBar } from '@angular/material';
import { RefreshManagerService } from './../../services/refresh-manager.service';
import { Component, OnInit } from '@angular/core';
import { SubscriptionAwareComponent } from '../../helper/subscription-aware-component';
import { OnDestroy } from '@angular/core/src/metadata/lifecycle_hooks';
import { ISpliceODFJobOverviewModel } from '../../models/service/jobs/splice/isplice-odf-job-overview-model';
import { JobService } from '../../services/job.service';
import { Router } from '@angular/router';
import { finalize } from 'rxjs/operators';
import { fuseAnimations } from '@fuse/animations';
@Component({
  selector: 'besa-splice-odfjob-overview-page',
  templateUrl: './splice-odfjob-overview-page.component.html',
  animations: fuseAnimations,
  styleUrls: ['./splice-odfjob-overview-page.component.scss']
})
export class SpliceOdfjobOverviewPageComponent extends SubscriptionAwareComponent implements OnInit, OnDestroy {

  //#region  Properties

  public loadingDataInProgress: boolean;
  public items: ISpliceODFJobOverviewModel[];

  //#endregion

  constructor(refreshManager: RefreshManagerService,
    private _jobService: JobService,
    private _snackBar: MatSnackBar,
    private _router: Router,
  ) {
    super();

    super.addSubscription(refreshManager.refreshRequested.subscribe((result) => { this.loadItems(); }))
  }

  ngOnInit() {
    this.loadItems();
  }

  ngOnDestroy() {
    super.unsubcripeAll();
  }

  //#region Methods

  public loadItems(): void {
    this.loadingDataInProgress = true;
    this._jobService.loadSpliceODFJobsOverview().pipe(finalize(() => {
      this.loadingDataInProgress = false;
    })).subscribe((result) => {
      this.items = result;
    }, (err) => {
      this._snackBar.open('Fehler beim Laden der Aufträge', 'Fehler', { duration: 4500 });
    });

  }

  public navigateToDetails(item: ISpliceODFJobOverviewModel): void {
    const url = '/member/splice-odf-details';

    this._router.navigate([url, item.jobId]);
  }

  //#endregion

}
