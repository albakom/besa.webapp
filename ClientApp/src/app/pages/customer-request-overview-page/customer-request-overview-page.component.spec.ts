import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomerRequestOverviewPageComponent } from './customer-request-overview-page.component';

describe('CustomerRequestOverviewPageComponent', () => {
  let component: CustomerRequestOverviewPageComponent;
  let fixture: ComponentFixture<CustomerRequestOverviewPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomerRequestOverviewPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomerRequestOverviewPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
