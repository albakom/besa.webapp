import { Component, OnInit, OnDestroy } from '@angular/core';
import { SubscriptionAwareComponent } from '../../helper/subscription-aware-component';
import { JobService } from '../../services/job.service';
import { MatSnackBar, MatDialog } from '@angular/material';
import { RefreshManagerService } from '../../services/refresh-manager.service';
import { Router } from '@angular/router';
import { finalize } from 'rxjs/operators';
import { IRequestOverviewModel } from '../../models/service/jobs/irequest-overview-model';
import { ISimpleDialogOptions } from '../../dialogs/simple-dialog/isimple-dialog-options';
import { SimpleDialogComponent } from '../../dialogs/simple-dialog/simple-dialog.component';
import { fuseAnimations } from '@fuse/animations';

@Component({
  selector: 'besa-customer-request-overview-page',
  templateUrl: './customer-request-overview-page.component.html',
  styleUrls: ['./customer-request-overview-page.component.scss'],
  animations: fuseAnimations,
})
export class CustomerRequestOverviewPageComponent extends SubscriptionAwareComponent implements OnInit, OnDestroy {

  public loadingDataInProgress: boolean;
  public items: IRequestOverviewModel[];

  // public formValidationErrors: { [key: string]: string } = {};
  // public registerForm: FormGroup;

  constructor(
    private _service: JobService,
    private _snackBar: MatSnackBar,
    private _refreshManager: RefreshManagerService,
    private _router: Router,
    private _dialog: MatDialog
  ) {
    super();

    super.addSubscription(_refreshManager.refreshRequested.subscribe((result) => {
      this.loadData();
    }));
  }

  ngOnInit() {
    this.loadData();
  }

  ngOnDestroy() {
    super.unsubcripeAll();
  }

  protected loadData(): void {
    this.loadingDataInProgress = true;

    this._service.loadOpenRequest().pipe(finalize(() => this.loadingDataInProgress = false)).subscribe(
      (result) => {
        this.items = result;
      }, (err) => {
        this._snackBar.open('Die ausstehenden Anfragen konnte nicht geladen werden', 'Fehler', { duration: 4500 });
      }
    )

    this.loadingDataInProgress = false;
  }

  public gotoDetails(item: IRequestOverviewModel): void {
    this._router.navigate(['/member/customer-request-details', item.id]);
  }

  public deleteItem(item: IRequestOverviewModel) {
    const dialogOptions: ISimpleDialogOptions = {
      title: 'Löschen bestätigen',
      question: 'Möchten Sie wirklich löschen?'
    };

    const dialogRef = this._dialog.open(SimpleDialogComponent, {
      data: dialogOptions
    });

    dialogRef.afterClosed().subscribe((result: boolean) => {
      if (result === true) {
        this.loadingDataInProgress = true;

        this._service.deleteRequest(item).pipe(finalize(() => {
          this.loadingDataInProgress = false;
          this.loadData();
        })).subscribe(
          (result) => {
            if (result)
              this._snackBar.open('Die Anfragen wurde gelöscht.', 'Erfolg', { duration: 4500 });
            else
              this._snackBar.open('Die Anfrage konnte nicht gelöscht werden', 'Fehler', { duration: 4500 });
          }
          , (err) => {
            this._snackBar.open('Die Anfrage konnte nicht gelöscht werden', 'Fehler', { duration: 4500 });
          });
      }
    })

  }

}