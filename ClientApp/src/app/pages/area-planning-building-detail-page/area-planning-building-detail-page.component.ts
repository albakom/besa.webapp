import { IEditAreaItemDialogOptions } from './../../dialogs/edit-area-item-dialog/iedit-area-item-dialog-options';
import { MatSnackBar, MatDialog } from '@angular/material';
import { AreaPlanningService } from './../../services/area-planning/area-planning.service';
import { IBuildingDetailModel } from './../../models/service/building/ibuilding-detail-model';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { finalize } from 'rxjs/operators';
import { CommentDialogOptions } from '../../dialogs/comment-dialog/comment-dialog-options';
import { CommentDialogComponent } from '../../dialogs/comment-dialog/comment-dialog.component';
import { IBuildingDeleteModel } from '../../models/service/building/ibuilding-delete-model';
import { fuseAnimations } from '@fuse/animations';
import { AreaItemType } from '../../models/service/building/area-item-type.enum';
import { EditAreaItemDialogComponent } from '../../dialogs/edit-area-item-dialog/edit-area-item-dialog.component';
import { Location } from '@angular/common';

@Component({
  selector: 'besa-area-planning-building-detail-page',
  templateUrl: './area-planning-building-detail-page.component.html',
  styleUrls: ['./area-planning-building-detail-page.component.scss'],
  animations: fuseAnimations
})
export class AreaPlanningBuildingDetailPageComponent implements OnInit {

  private _id: number;
  building: IBuildingDetailModel;
  loadingDataInProgress: boolean;

  constructor(
    private _activeRoute: ActivatedRoute,
    private _service: AreaPlanningService,
    private _snackBar: MatSnackBar,
    private _router: Router,
    private _dialog: MatDialog,
    private _location: Location
  ) { }

  ngOnInit() {
    this.loadData();
  }

  loadData() {
    this._id = +this._activeRoute.snapshot.paramMap.get('id');

    this.loadingDataInProgress = true;
    this._service.getBuildingDetails(this._id).pipe(finalize(() => {
      this.loadingDataInProgress = false;
    })).subscribe((result) => {
      if (result) {
        this.building = result;
      } else this._snackBar.open('Die Daten konnten nicht geladen werden', 'Fehler', { duration: 4500 });
    }, (err) => {
      this._snackBar.open('Die Daten konnten nicht geladen werden', 'Fehler', { duration: 4500 });
    });
  }

  navigateBack() {
    this._location.back();
  }

  editBuilding() {
    const options: IEditAreaItemDialogOptions = {
      title: 'Gebäude bearbeiten',
      type: AreaItemType.building,
      buildingDetails: this.building
    };

    const ref = this._dialog.open<EditAreaItemDialogComponent>(EditAreaItemDialogComponent, {data: options});

    ref.afterClosed().subscribe((result) => {
      if (result) {
        this._snackBar.open('Gebäude wurde erfolgreich aktualisiert', 'Erfolg', { duration: 4500 });
      }
    });
  }

  deleteBuilding() {
    const options: CommentDialogOptions = {
      title: 'Warum soll dieses Gebäude gelöscht werden?'
    };

    const ref = this._dialog.open<CommentDialogComponent>(CommentDialogComponent, {data: options});

    ref.afterClosed().subscribe((result) => {
      console.log(result);
      if (result && result != null && result != "") {
        let deleteModel: IBuildingDeleteModel = {
          id: this._id,
          comment: result
        };

        this._service.deleteBuilding(deleteModel).subscribe((result) => {
          if (result) {
            this._router.navigate(['member/area-planning']);
          } 
        })
      }
      else this._snackBar.open('Gebäude löschen wurde abgebrochen', 'Abbruch', { duration: 4500 });
    })
  }

}
