import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AreaPlanningBuildingDetailPageComponent } from './area-planning-building-detail-page.component';

describe('AreaPlanningBuildingDetailPageComponent', () => {
  let component: AreaPlanningBuildingDetailPageComponent;
  let fixture: ComponentFixture<AreaPlanningBuildingDetailPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AreaPlanningBuildingDetailPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AreaPlanningBuildingDetailPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
