import { finalize } from 'rxjs/operators';
import { IAddressModel } from './../../models/service/iaddress-model';
import { ISimpleDialogOptions } from './../../dialogs/simple-dialog/isimple-dialog-options';
import { PersonTypes } from './../../models/service/person-types.enum';
import { IContactInfo } from './../../models/service/icontact-info';
import {FormBasedPage} from '../base/form-based-page';
import { fuseAnimations } from '@fuse/animations';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormErrorMessage } from '../../models/helper/form-error-message';
import { Router, ActivatedRoute } from '@angular/router';
import { CustomerService } from '../../services/customer.service';
import { MatSnackBar, MatDialog } from '@angular/material';
import { FormBuilder, Validators } from '@angular/forms';
import { ContactEditErrorMessage } from '../../models/error-messages/contact-edit-error-message';
import { RefreshManagerService } from '../../services/refresh-manager.service';
import { SimpleDialogComponent } from '../../dialogs/simple-dialog/simple-dialog.component';

@Component({
  selector: 'besa-contact-edit-page',
  templateUrl: './contact-edit-page.component.html',
  styleUrls: ['./contact-edit-page.component.scss'],
  animations: fuseAnimations
})
export class ContactEditPageComponent extends FormBasedPage implements OnInit, OnDestroy {

  private _id: number;
  public contact: IContactInfo;
  public loadingDataInProgress: boolean;
  public genderSelectOptions: PersonTypes[] = [
    PersonTypes.Male,
    PersonTypes.Female,
  ];
  
  constructor(
    private _router: Router,
    private refreshManager: RefreshManagerService,
    private _activeRoute: ActivatedRoute,
    private _service: CustomerService,
    private _snackBar: MatSnackBar,
    private _fb: FormBuilder,
    private _dialog: MatDialog
  ) {
    super();

    this.initializeFormValues();

    super.addSubscription(refreshManager.refreshRequested.subscribe((result) => {
      this.loadData();
    }));
   }

  ngOnInit() {
    this.loadData();
  }

  ngOnDestroy() {
    super.unsubcripeAll();
  }

  public initializeFormValues() {
    super.initForm(
      this._fb.group({
        surname: ['', [Validators.maxLength(64)]],
        lastname: ['', [Validators.required, Validators.maxLength(64)]],
        type: [PersonTypes.Male, [Validators.required]],
        companyName: ['', [Validators.maxLength(64)]],
        emailAddress: ['', [Validators.email, Validators.required, Validators.maxLength(64)]],
        phone: ['', [Validators.required, Validators.maxLength(64)]],
        street: ['', [Validators.required, Validators.maxLength(64)]],
        streetNumber: ['', [Validators.required, Validators.maxLength(64)]],
        city: ['', [Validators.required, Validators.maxLength(64)]],
        postalCode: ['', [Validators.required, Validators.maxLength(5), Validators.minLength(5)]],
      })
    );
  }

  protected getErrorMessages(): FormErrorMessage[] {
    return ContactEditErrorMessage;
  }

  public loadData() {
    this._id = +this._activeRoute.snapshot.paramMap.get('id');
    this.loadingDataInProgress = true;
    this._service.getContact(this._id).pipe(finalize(() => {
      this.form.setValue({
        type: this.contact.type,
        surname: this.contact.surname,
        companyName: this.contact.companyName,
        lastname: this.contact.lastname,
        emailAddress: this.contact.emailAddress,
        phone: this.contact.phone,
        street: this.contact.address.street,
        streetNumber: this.contact.address.streetNumber,
        city: this.contact.address.city,
        postalCode: this.contact.address.postalCode
      });
      this.loadingDataInProgress = false;
    })).subscribe((result) => {
      this.contact = result;
    }, (err) => {
      this._snackBar.open('Die ausstehende Anfrage konnte nicht geladen werden', 'Fehler', { duration: 4500 });
    });
  }

  public editContact() {
    const dialogOptions: ISimpleDialogOptions = {
      title: 'Speichern bestätigen',
      question: 'Möchten Sie wirklich speichern?'
    };

    const dialogRef = this._dialog.open(SimpleDialogComponent, {
      data: dialogOptions
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        let editedContact: IContactInfo = {
          id: this.contact.id,
          surname: this.form.value.surname,
          lastname: this.form.value.lastname,
          companyName: this.form.value.companyName,
          emailAddress: this.form.value.emailAddress,
          phone: this.form.value.phone,
          type: this.form.value.type,
          address: {
            city: this.form.value.city,
            street: this.form.value.street,
            streetNumber: this.form.value.streetNumber,
            postalCode: this.form.value.postalCode,
          },
        };
        this.loadingDataInProgress = true;
        this._service.editContact(editedContact).pipe(finalize(() => {
          this.loadingDataInProgress = false;
        })).subscribe((result) => {
          this._snackBar.open('Die Änderungen wurden verarbeitet', 'Erfolg', { duration: 4500 });
          this._router.navigate(['/member/contact-overview']);
        }, (err) => {
          this._snackBar.open('Die ausstehende Änderung konnte nicht verarbeitet werden', 'Fehler', { duration: 4500 });
        })
      }
    }, (err) => {
    });
  }

  public cancelEdit() {
    this._router.navigate(['/member/contact-overview']);
  }


}
