import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AreaPlanningEmptySpaceDetailPageComponent } from './area-planning-empty-space-detail-page.component';

describe('AreaPlanningEmptySpaceDetailPageComponent', () => {
  let component: AreaPlanningEmptySpaceDetailPageComponent;
  let fixture: ComponentFixture<AreaPlanningEmptySpaceDetailPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AreaPlanningEmptySpaceDetailPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AreaPlanningEmptySpaceDetailPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
