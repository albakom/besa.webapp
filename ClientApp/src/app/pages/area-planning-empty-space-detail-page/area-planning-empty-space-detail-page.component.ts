import { fuseAnimations } from '@fuse/animations';
import { IEmptySpaceDetailModel } from './../../models/service/building/iempty-space-detail-model';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AreaPlanningService } from '../../services/area-planning/area-planning.service';
import { MatSnackBar, MatDialog } from '@angular/material';
import { finalize } from 'rxjs/operators';
import { Location } from '@angular/common';

@Component({
  selector: 'besa-area-planning-empty-space-detail-page',
  templateUrl: './area-planning-empty-space-detail-page.component.html',
  styleUrls: ['./area-planning-empty-space-detail-page.component.scss'],
  animations: fuseAnimations
})
export class AreaPlanningEmptySpaceDetailPageComponent implements OnInit {
  private _id: number;
  emptySpace: IEmptySpaceDetailModel;
  loadingDataInProgress: boolean;

  constructor(
    private _activeRoute: ActivatedRoute,
    private _service: AreaPlanningService,
    private _snackBar: MatSnackBar,
    private _router: Router,
    private _location: Location
  ) { }

  ngOnInit() {
    this.loadData();
  }

  loadData() {
    this._id = +this._activeRoute.snapshot.paramMap.get('id');

    this.loadingDataInProgress = true;
    this._service.getEmptySpaceDetails(this._id).pipe(finalize(() => {
      this.loadingDataInProgress = false;
    })).subscribe((result) => {
      if (result) {
        this.emptySpace = result;
      } 
    }, (err) => {
      this._snackBar.open('Die Daten konnten nicht geladen werden', 'Fehler', { duration: 4500 });
    });
  }

  navigateBack() {
    this._location.back();
  }

  editEmptySpace() {

  }

  deleteEmptySpace() {
    this._service.deleteEmptySpace(this._id).subscribe((result) => {
      if (result) {
        this._snackBar.open('Die freie Fläche wurde erfolgreich gelöscht', 'Erfolg', { duration: 4500 });
        this._router.navigate(['member/area-planning']);
      } else {
        this._snackBar.open('Die freie Fläche konnte nicht gelöscht werden', 'Fehler', { duration: 4500 });
      }
    }, (err) => {
      this._snackBar.open('Die freie Fläche konnte nicht gelöscht werden', 'Fehler', { duration: 4500 });
    })
  }
}
