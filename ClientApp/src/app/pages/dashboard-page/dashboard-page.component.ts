import { MessageModel } from './../../models/service/messages/message-model';
import { LoginService } from './../../services/login.service';
import { Router } from '@angular/router';
import { fuseAnimations } from '@fuse/animations';
import { MatSnackBar } from '@angular/material';
import { RefreshManagerService } from './../../services/refresh-manager.service';
import { finalize } from 'rxjs/operators';
import { SubscriptionAwareComponent } from './../../helper/subscription-aware-component';
import { DashboardService } from './../../services/dashboard.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { IPersonalJobOverviewModel } from '../../models/service/jobs/overview/ipersonal-job-overview-model';

@Component({
  selector: 'besa-dashboard-page',
  templateUrl: './dashboard-page.component.html',
  styleUrls: ['./dashboard-page.component.scss'],
  animations: fuseAnimations,
})
export class DashboardPageComponent extends SubscriptionAwareComponent implements OnInit, OnDestroy {

  //#region Properties

  public loadingNextJobsInProgress: boolean;
  public nextJobs: IPersonalJobOverviewModel[];

  public unreadMessages: MessageModel[];

  //#endregion

  constructor(
    refreshManager: RefreshManagerService,
    private _router: Router,
    private _snackBar: MatSnackBar,
    private _service: DashboardService,
    private _loginService: LoginService) {
    super();

    this.unreadMessages = new Array();

    super.addSubscription(refreshManager.refreshRequested.subscribe((result) => {
      this.loadNextJobs();
      this.loadMessages();
    }))
  }

  ngOnInit() {
    this.loadNextJobs();
    this.loadMessages();
  }

  ngOnDestroy() {
    super.unsubcripeAll();
  }

  //#region  Methods

  private loadNextJobs(): void {
    if (this.loadingNextJobsInProgress === true) { return; }

    this.loadingNextJobsInProgress = true;

    this._service.getNextJobs().pipe(finalize(() => {
      this.loadingNextJobsInProgress = false;
    })).subscribe((result) => {
      this.nextJobs = result;
    }, (err) => {
      this._snackBar.open('Die Aufträge konnte nicht geladen werden', 'Fehler', { duration: 4500 });
    });
  }

  private loadMessages() {
    this._loginService.currentUserInfo.subscribe((result) => {
      if (result && result.unreadMessage) {
        this.unreadMessages = result.unreadMessage;
      }
    })
  }

  public navigateToJob(job: IPersonalJobOverviewModel): void {
    if (!job) { return; }

    this._router.navigate(['/member/collection-job', job.collectionJobId]);
  }

  //#endregion

}
