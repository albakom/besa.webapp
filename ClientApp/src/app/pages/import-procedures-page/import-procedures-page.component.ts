import { MatDialog } from '@angular/material';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBasedPage } from '../base/form-based-page';
import { CSVLineModel } from '../../models/helper/csv-line-model';
import { ProcedureImportProperties } from '../../models/helper/procedure-import-properties.enum';
import { MatTableDataSource, MatSnackBar, MatSelectChange, MatCheckboxChange } from '@angular/material';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ImportService } from '../../services/import.service';
import { FormErrorMessage } from '../../models/helper/form-error-message';
import { ImportCSVProcedureFormMessage } from '../../models/error-messages/import-csv-procedure-form-message';
import { UploadFileModel } from '../../models/helper/upload-file-model';
import { StepperSelectionEvent } from '@angular/cdk/stepper';
import { ICSVImportProcedureModel } from '../../models/service/import/icsvimport-procedure-model';
import { finalize } from 'rxjs/operators';
import { ICSVImportProcedureResultModel } from '../../models/service/import/icsvimport-procedure-result-model';
import { fuseAnimations } from '@fuse/animations';
import { ImportProcedureErrorTypes } from '../../models/service/import/import-procedure-error-types.enum';
import { WriteCSVFileDialogComponent } from '../../dialogs/write-csvfile-dialog/write-csvfile-dialog.component';
import { IWriteCSVFileDialogOptions } from '../../dialogs/write-csvfile-dialog/iwrite-csvfile-dialog-options';

@Component({
  selector: 'besa-import-procedures-page',
  templateUrl: './import-procedures-page.component.html',
  animations: fuseAnimations,
  styleUrls: ['./import-procedures-page.component.scss']
})
export class ImportProceduresPageComponent extends FormBasedPage implements OnInit, OnDestroy {

  //#region Properties

  public maxColumnAmount: number;
  public lines: CSVLineModel[];
  public displayedColumns: string[];
  public possibleValues: ProcedureImportProperties[];
  public firstLine: CSVLineModel;
  public importResult: ICSVImportProcedureResultModel;

  public file: UploadFileModel;
  public lineDataSource: MatTableDataSource<CSVLineModel>;
  public propertyMapper: { [index: number]: ProcedureImportProperties };
  public missingProperties: ProcedureImportProperties[];
  public importInProgress: boolean;
  errorIndexs: number[];

  //#endregion

  constructor(
    private snackBar: MatSnackBar,
    fb: FormBuilder,
    private _router: Router,
    private _service: ImportService,
    private _dialog: MatDialog) {
    super();

    this.possibleValues = [
      ProcedureImportProperties.ContactSurname,
      ProcedureImportProperties.ContactLastname,
      ProcedureImportProperties.Street,
      ProcedureImportProperties.StreetNumber,
      ProcedureImportProperties.Aggrement,
      ProcedureImportProperties.Contract,
      ProcedureImportProperties.ActiveCustomer,
      ProcedureImportProperties.Appointment,
      ProcedureImportProperties.ConnectBuildingFinished,
      ProcedureImportProperties.InjectFinished,
      ProcedureImportProperties.SpliceFinished,
      ProcedureImportProperties.ActivationFinished,
      ProcedureImportProperties.InformSalesAfterFinished,
      ProcedureImportProperties.ContactPhone,
      ProcedureImportProperties.ContactId,
    ];

    this.propertyMapper = {};
    this.missingProperties = new Array();
    this.errorIndexs = new Array();

    super.initForm(fb.group({
      skipError: [true, [Validators.required]],
      rolebackIfErrorHappend: [true, [Validators.required]],
      asSoonAsPossibleString: ['ASAP'],
    }));

    this.propertySelected(new MatSelectChange(null, ProcedureImportProperties.ContactLastname), 0);
    this.propertySelected(new MatSelectChange(null, ProcedureImportProperties.ContactSurname), 1);
    this.propertySelected(new MatSelectChange(null, ProcedureImportProperties.Street), 2);
    this.propertySelected(new MatSelectChange(null, ProcedureImportProperties.StreetNumber), 3);
    this.propertySelected(new MatSelectChange(null, ProcedureImportProperties.ActiveCustomer), 6);
  }

  ngOnInit() {
    super.addSubscription(this.form.valueChanges.subscribe((value) => {
      this.checkIfMappingIsDone();
    }));
  }

  ngOnDestroy() {
    super.unsubcripeAll();
  }

  //#region Methods

  protected getErrorMessages(): FormErrorMessage[] {
    return ImportCSVProcedureFormMessage;
  }

  public onFileSelect(files: FileList) {

    for (let index = 0; index < files.length; index++) {
      const file = files.item(index);

      const fileModel: UploadFileModel = {
        type: file.name.endsWith('.csv') === true ? 'csv' : 'file',
        file: file,
        name: file.name,
      };

      if (fileModel.type === 'csv') {
        const reader = new FileReader();
        fileModel.previewLoading = true;
        this.file = fileModel;

        reader.onload = (event: any) => {
          fileModel.content = event.target.result;
          fileModel.previewLoading = false;
        };

        reader.readAsText(file);
      }
    }
  }

  public stepChanged(event: StepperSelectionEvent): void {
    if (event.selectedIndex == 1) {
      this.loadFile();
    }
  }

  private loadFile(): void {
    console.log(this.file.content);
    const lines: string[] = this.file.content.split('\r\n');
    this.lines = new Array();
    let max: number = 0;
    for (let index = 0; index < lines.length; index++) {
      const line = lines[index];
      const parts = line.split(';');
      // const parts = line.split(';').filter((value, index, arr) => {
      //   return value.match('^[0-9a-zA-Z]+$');
      // });

      if (parts.length > max) {
        max = parts.length;
      }
      this.lines.push(new CSVLineModel(parts));

      if (index === 0) {
        this.firstLine = this.lines[0];
      }
    }

    this.displayedColumns = new Array();
    for (let index = 0; index < max; index++) {
      this.displayedColumns.push(index.toString());
    }

    this.maxColumnAmount = max;
    this.lineDataSource = new MatTableDataSource(this.lines);
    this.checkIfMappingIsDone();
  }

  public getColumnHeader(index: number): string {
    return index.toString();
  }

  public getColumnName(index: number): string {
    return index.toString();
  }

  public propertySelected(event: MatSelectChange, index: number): void {
    const realValue: ProcedureImportProperties = event.value;

    const oldValue = this.propertyMapper[index];
    if (oldValue) {
      this.possibleValues.push(oldValue);
    }

    this.propertyMapper[index] = event.value;

    const propertyIndex = this.possibleValues.indexOf(realValue);
    if (propertyIndex >= 0) {
      this.possibleValues.splice(propertyIndex, 1);
    }

    this.checkIfMappingIsDone();
  }

  public removeIndex(index: number): void {
    const value: ProcedureImportProperties = this.propertyMapper[index];
    if (!value) { return; }

    delete this.propertyMapper[index];
    this.possibleValues.push(value);
    this.checkIfMappingIsDone();
  }

  public changeFirstLine(event: MatCheckboxChange): void {
    const value: boolean = event.checked;
    const firstLineInArray = this.firstLine == this.lines[0];
    if (value === true) {
      if (firstLineInArray === true) {
        this.lines.shift();
      }
    } else {
      if (firstLineInArray == false) {
        this.lines.unshift(this.firstLine);
      }
    }

    this.lineDataSource._updateChangeSubscription();
  }

  public checkIfMappingIsDone(): boolean {
    let result = true;
    this.missingProperties = new Array();

    const neededMappings = [
      ProcedureImportProperties.Street,
      ProcedureImportProperties.StreetNumber,
      ProcedureImportProperties.ContactLastname,
      ProcedureImportProperties.ContactSurname,
    ];

    for (let index = 0; index < neededMappings.length; index++) {
      const element = neededMappings[index];

      const existingIndex = this.possibleValues.indexOf(element);
      if (existingIndex >= 0) {
        result = false;
        this.missingProperties.push(element);
      }
    }

    return result;
  }

  private importLines(formValue: ICSVImportProcedureModel, lineStartIndex: number, lineAmount: number): void {
    formValue.lines = this.lines.map(x => x.parts);
    formValue.lines.splice(0, lineStartIndex);
    formValue.lines.splice(lineAmount);

    if (formValue.lines.length == 0) {
      this.snackBar.open('Import erfolgreich', 'Erfolg', { duration: 4500 });
      this.importInProgress = false;
    }
    else {
      this._service.importProcedures(formValue).pipe(finalize(() => {
      })).subscribe((result) => {

        this.importLines(formValue, lineStartIndex + lineAmount, lineAmount);

        this.importResult.jobIds.push(...result.jobIds);
        this.importResult.procedureIds.push(...result.procedureIds);

        result.errors.forEach(error => {
          error.itemIndex += lineStartIndex;
          this.importResult.errors.push(error);
          this.errorIndexs.push(error.itemIndex);
        });

      }, (err) => {
        this.snackBar.open('Import konnte nicht durchgeführt werden', 'Fehler', { duration: 4500 });
        this.importInProgress = false;
      });
    }
  }

  public startImport(): void {
    if (this.importInProgress === true) { return; }
    if (this.form.invalid === true || this.checkIfMappingIsDone() == false) { return; }

    this.importResult = null;
    this.importInProgress = true;

    const formValue: ICSVImportProcedureModel = this.form.value;
    formValue.mappings = {};

    for (var key in this.propertyMapper) {
      if (this.propertyMapper.hasOwnProperty(key)) {
        const realKey = +key;
        const value: number = this.propertyMapper[realKey];
        if (value) {
          formValue.mappings[value] = realKey;
        }
      }
    }

    this.importResult = {
      errors: new Array(),
      jobIds: new Array(),
      procedureIds: new Array(),
    };

    this.errorIndexs = new Array();
    this.importLines(formValue, 0, 20);
  }

  checkItem(index: number): boolean {
    if (this.errorIndexs.indexOf(index) >= 0) {
      return true;
    } else return false;
  }

  getTooltip(index: number): string {
    let item = this.importResult.errors.find(x => x.itemIndex === index);
    let tooltip = '';
    if (item) {
      switch (item.type) {
        case ImportProcedureErrorTypes.AppointmentNotReadable:
          tooltip = 'Termin nicht lesbar';
          break;
        case ImportProcedureErrorTypes.BuildingNotFound:
          tooltip = 'Gebäude nicht gefunden';
          break;
        case ImportProcedureErrorTypes.ContactNotFound:
          tooltip = 'Kontakt nicht gefunden';
          break;
        case ImportProcedureErrorTypes.ContactNotReadable:
          tooltip = 'Kontakt nicht lesbar';
          break;
        case ImportProcedureErrorTypes.StreetNotReadable:
          tooltip = 'Straße nicht lesbar';
          break;
        case ImportProcedureErrorTypes.StreetNumberNotReadable:
          tooltip = 'Hausnummer nicht lesbar';
          break;
        default:
          tooltip = '';
          break;
      }
    }

    return tooltip;
  }

  openDialog(): void {
    const dialogRef = this._dialog.open(WriteCSVFileDialogComponent, {
      data: new IWriteCSVFileDialogOptions()
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

  //#endregion


}
