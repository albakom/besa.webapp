import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ImportProceduresPageComponent } from './import-procedures-page.component';

describe('ImportProceduresPageComponent', () => {
  let component: ImportProceduresPageComponent;
  let fixture: ComponentFixture<ImportProceduresPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ImportProceduresPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ImportProceduresPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
