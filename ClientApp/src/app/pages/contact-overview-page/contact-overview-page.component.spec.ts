import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContactOverviewPageComponent } from './contact-overview-page.component';

describe('ContactOverviewPageComponent', () => {
  let component: ContactOverviewPageComponent;
  let fixture: ComponentFixture<ContactOverviewPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContactOverviewPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContactOverviewPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
