import { ISimpleDialogOptions } from '../../dialogs/simple-dialog/isimple-dialog-options';
import { IContactInfo } from './../../models/service/icontact-info';
import { finalize, switchMap, debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { SimpleDialogComponent } from './../../dialogs/simple-dialog/simple-dialog.component';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';
import { SubscriptionAwareComponent } from '../../helper/subscription-aware-component';
import { CustomerService } from '../../services/customer.service';
import { RefreshManagerService } from '../../services/refresh-manager.service';
import { MatSnackBar, MatDialog, MatAutocompleteSelectedEvent } from '@angular/material';
import { Router } from '@angular/router';
import { ICreateContactDialogOptions } from '../../dialogs/create-contact-dialog/icreate-contact-dialog-options';
import { CreateContactDialogComponent } from '../../dialogs/create-contact-dialog/create-contact-dialog.component';
import { Subject } from 'rxjs';

@Component({
  selector: 'besa-contact-overview-page',
  templateUrl: './contact-overview-page.component.html',
  styleUrls: ['./contact-overview-page.component.scss'],
  animations: fuseAnimations
})
export class ContactOverviewPageComponent extends SubscriptionAwareComponent implements OnInit, OnDestroy {

  private _itemPerRequest = 50;
  private _canScroll: boolean = false;
  public loadingMoreDataInProgress: boolean;

  public loadingDataInProgress: boolean = false;
  public contacts: IContactInfo[];
  private _searchTerm: Subject<string>;
  public searchContact: IContactInfo[];

  private _searchString: string;
  get searchString(): string {
    return this._searchString;
  }
  set searchString(search: string) {
    this._searchString = search;
    this._searchTerm.next(search);
  }

  constructor(
    private refreshManager: RefreshManagerService,
    private _snackBar: MatSnackBar,
    private _router: Router,
    private _service: CustomerService,
    private _dialog: MatDialog
  ) {
    super();

    this._searchTerm = new Subject<string>();
    this.searchContact = new Array();

    super.addSubscription(refreshManager.refreshRequested.subscribe((result) => {
      this.loadData();
    }));

    super.addSubscription(this._searchTerm.pipe(
      debounceTime(150),
      distinctUntilChanged(),
      switchMap((item) => {
        return this._service.searchContacts(item);
      })
    ).subscribe((items) => {
      this.searchContact = items;
    }));
  }

  ngOnInit() {
    this.loadData();
  }

  ngOnDestroy() {
    super.unsubcripeAll();
  }

  loadData() {
    this.loadingDataInProgress = true;
    this._service.getContacts(0, this._itemPerRequest).pipe(finalize(() => {
      this.loadingDataInProgress = false;
    })).subscribe((result) => {
      this.contacts = result;
      this._canScroll = this._itemPerRequest == result.length;
    }, (err) => {
      this._snackBar.open("Kontakte wurden nicht gelanden", "Fehler", { duration: 4500 });
    });
  }

  onScroll() {
    if (this._canScroll === false) { return; }
    if (this.loadingMoreDataInProgress === true) { return; }

    this.loadingMoreDataInProgress = true;
    this._service.getContacts(this.contacts.length, this._itemPerRequest).pipe(finalize(() => {
      this.loadingMoreDataInProgress = false;
    })).subscribe((result) => {
      this.contacts.push(...result);
      this._canScroll = this._itemPerRequest == result.length;
    }, (err) => {
      this._snackBar.open("Kontakte wurden nicht gelanden", "Fehler", { duration: 4500 });
    });
  }

  openDetails(contact: IContactInfo) {
    this._router.navigate(['/member/contact-details', contact.id]);
  }

  deleteContact(item: IContactInfo) {
    // checkt ob Kontakt in Verwendung ist
    this._service.checkIfContactIsInUse(item.id).pipe().subscribe((result: Boolean) => {
      // wenn nein löschen
      if (!result) {
        const dialogOptions: ISimpleDialogOptions = {
          title: 'Löschen bestätigen',
          question: `Möchten Sie wirklich den Kontakt ${item.surname} ${item.lastname} löschen?`,
        };

        const dialogRef = this._dialog.open(SimpleDialogComponent, {
          data: dialogOptions
        });

        dialogRef.afterClosed().subscribe((result: boolean) => {
          if (result == true) {
            this.loadingDataInProgress = true;
            this._snackBar.open('Der Kontakt wird gelöscht', 'In Bearbeitung', { duration: -1 });

            this._service.deleteContact(item.id).pipe(finalize(() => {
              this.loadData();
              this.loadingDataInProgress = false;
            })).subscribe((result) => {
              if (result) {
                this._snackBar.open('Der Kontakt wurde gelöscht', 'Erfolg', { duration: 4500 });
              } else {
                this._snackBar.open('Der Kontakt konnte nicht gelöscht werden', 'Fehler', { duration: 4500 });
              }
            }, (err) => {
              this._snackBar.open('Der Kontakt konnte nicht gelöscht werden', 'Fehler', { duration: 4500 });
            });
          } else {
            this.loadingDataInProgress = false;
          }
        })
      } else {
        // Wenn ja explizit nachfragen
        this._snackBar.open('Kontakt konnte nicht gelöscht werden, da dieser in Verwendung ist', 'Fehler', { duration: 4500 });
      }
    }, (err) => {
      // was da tun?
    });

  }

  public editContact(contact: IContactInfo) {
    this._router.navigate(['/member/contact-edit', contact.id]);
  }

  public createNewContact() {
    const options: ICreateContactDialogOptions = {};

    const ref = this._dialog.open<CreateContactDialogComponent>(CreateContactDialogComponent, { data: options });
    ref.afterClosed().subscribe((contact: IContactInfo) => {
      if (contact) {
        this._snackBar.open('Der Kontakt wurde erfolgreich erstellt.', 'Erfolg', { duration: 4500 });
        this.loadData();
      } else {
        this._snackBar.open('Die Erstellung wurde abgebrochen.', 'Abbruch', { duration: 4500 });
      }
    });
  }

  public contactSelected(eventInfo: MatAutocompleteSelectedEvent) {
    const contact: IContactInfo = eventInfo.option.value;
    this._router.navigate(['/member/contact-details', contact.id]);
  }

  public displayContactResult(item?: IContactInfo) {
    return item ? item.surname + " " + item.lastname : '';
  }

}
