import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrepareBranchableForSpliceJobOverviewPageComponent } from './prepare-branchable-for-splice-job-overview-page.component';

describe('PrepareBranchableForSpliceJobOverviewPageComponent', () => {
  let component: PrepareBranchableForSpliceJobOverviewPageComponent;
  let fixture: ComponentFixture<PrepareBranchableForSpliceJobOverviewPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrepareBranchableForSpliceJobOverviewPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrepareBranchableForSpliceJobOverviewPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
