import { fuseAnimations } from  '@fuse/animations';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { JobOverviewPageBase } from '../base/job-overview-page-base';
import { RefreshManagerService } from '../../services/refresh-manager.service';
import { MatSnackBar } from '@angular/material';
import { Router } from '@angular/router';
import { ConstructionStageService } from '../../services/construction-stage.service';
import { Observable } from 'rxjs';
import { IConstructionStageOverviewForJobsModel } from '../../models/service/construction-stage/iconstruction-stage-overview-for-jobs-model';

@Component({
  selector: 'besa-prepare-branchable-for-splice-job-overview-page',
  templateUrl: './prepare-branchable-for-splice-job-overview-page.component.html',
  animations: fuseAnimations,
  styleUrls: ['./prepare-branchable-for-splice-job-overview-page.component.scss']
})
export class PrepareBranchableForSpliceJobOverviewPageComponent extends JobOverviewPageBase implements OnInit, OnDestroy {

  constructor(refreshManger: RefreshManagerService,
    snackBar: MatSnackBar,
    router: Router,
    private _service: ConstructionStageService) {
    super(refreshManger, snackBar, router);
  }

  ngOnInit() {
    super.loadConstructionStages();
  }

  ngOnDestroy() {
    super.unsubcripeAll();
  }

  //#region Methods

  protected loadOverview(onlybounded: boolean): Observable<IConstructionStageOverviewForJobsModel[]> {
    return this._service.loadOverviewForSplice(onlybounded);
  }
  
  protected getNavigationPathForDetails(): string {
    return '/member/prepare-branchable-job-construction-stage-detail';
  }

  //#endregion

}
