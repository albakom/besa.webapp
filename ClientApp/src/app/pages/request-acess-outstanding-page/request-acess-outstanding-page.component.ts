import { fuseAnimations } from  '@fuse/animations';
import { OAuthService } from 'angular-oauth2-oidc';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { NonMemberPageBase } from '../base/non-member-page-base';
import { RequestAccessService } from '../../services/request-access.service';
import { Router } from '@angular/router';
import { MemberStatus } from '../../models/service/member-status.enum';
import { MatSnackBar } from '@angular/material';
import { finalize } from 'rxjs/operators';
import { RequestAccessHubService } from '../../services/hubs/request-access-hub.service';
import { LoginService } from '../../services/login.service';
import { FuseConfigService } from '@fuse/services/config.service';

@Component({
  selector: 'besa-request-acess-outstanding-page',
  templateUrl: './request-acess-outstanding-page.component.html',
  styleUrls: ['./request-acess-outstanding-page.component.scss'],
  animations: fuseAnimations,
})
export class RequestAcessOutstandingPageComponent extends NonMemberPageBase implements OnInit, OnDestroy {

  //#region Properties

  public preperationInProgress: boolean;
  public redirectInit: boolean;

  //#endregion

  constructor(
    private _service: RequestAccessService,
    fuseConfigService: FuseConfigService,
    private _snackBar: MatSnackBar,
    private _oauthService: OAuthService,
    private _router: Router,
    private _hub: RequestAccessHubService,
    private _loginService: LoginService,
  ) {
    super(fuseConfigService);
    super.setDefaultLayout();

    super.addSubscription(_hub.accessGranted.subscribe((result) => {
      if (result === true) {
        this.redirectInit = true;
        this._loginService.checkRoles().pipe(finalize(() => {
          this.redirectInit = false;
        })).subscribe((roleResult) => {
          if (roleResult === true)
            this._router.navigate(['/member/dashboard']);
        })
      }
    }))
  }

  ngOnInit() {
    this._snackBar.dismiss();
    this.preperationInProgress = true;

    if (this._oauthService.hasValidAccessToken() === false) {
      this.redirectInit = true;
      this._router.navigate(['/login']);
    } else {

      this._service.loadMemberStatus().pipe(finalize(() => this.preperationInProgress = false)).subscribe((memberResult) => {
        if (memberResult === MemberStatus.IsMember) {
          this._router.navigate(['/member/dashboard']);
          this.redirectInit = true;
        } else if (memberResult === MemberStatus.NoMember) {
          this._router.navigate(['/requstAccess']);
          this.redirectInit = true;
        } else {
          this.redirectInit = false;

          this._hub.connenct().subscribe( () => {});
        }
      }, (err) => {
        this._snackBar.open('Beim Überprüfen der Mitgliedschaft ist ein Fehler aufgetreten ', 'Fehler', { duration: 4500 });
      });
    }
  }

  ngOnDestroy() {
    super.unsubcripeAll();
    this._hub.disconnect();
  }

}
