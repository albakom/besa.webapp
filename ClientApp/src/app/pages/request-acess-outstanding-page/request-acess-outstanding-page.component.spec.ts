import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RequestAcessOutstandingPageComponent } from './request-acess-outstanding-page.component';

describe('RequestAcessOutstandingPageComponent', () => {
  let component: RequestAcessOutstandingPageComponent;
  let fixture: ComponentFixture<RequestAcessOutstandingPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RequestAcessOutstandingPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequestAcessOutstandingPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
