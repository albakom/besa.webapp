import { SubscriptionAwareComponent } from './../../helper/subscription-aware-component';
import { Component, OnInit } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';
import { RequestAccessService } from '../../services/request-access.service';
import { IUserAccessRequestOverviewModel } from '../../models/service/user/iuser-access-request-overview-model';
import { finalize } from 'rxjs/operators';
import { MatSnackBar } from '@angular/material';
import { RefreshManagerService } from '../../services/refresh-manager.service';
import { Router, ActivatedRoute } from '@angular/router';
import { OAuthService } from 'angular-oauth2-oidc';
import { RequestAccessHubService } from '../../services/hubs/request-access-hub.service';
import { OnDestroy } from '@angular/core/src/metadata/lifecycle_hooks';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'besa-user-access-request-overview-page',
  templateUrl: './user-access-request-overview-page.component.html',
  styleUrls: ['./user-access-request-overview-page.component.scss'],
  animations: fuseAnimations,
})
export class UserAccessRequestOverviewPageComponent extends SubscriptionAwareComponent implements OnInit, OnDestroy {

  //#region Fields

  private _unchangedTitle: string;

  //#endregion

  //#region Properties

  public loadingDataInProgress: boolean;
  public requests: IUserAccessRequestOverviewModel[];

  //#endergion

  constructor(
    private _service: RequestAccessService,
    private _snackBar: MatSnackBar,
    private _refreshManager: RefreshManagerService,
    private _router: Router,
    private _activeRoute: ActivatedRoute,
    private _hub: RequestAccessHubService,
    private _title: Title
  ) {
    super();

    super.addSubscription(_refreshManager.refreshRequested.subscribe((result) => {
      this.loadData();
    }));

    super.addSubscription(_hub.newRequest.subscribe((request) => {
      if (request && this.requests) {
        let exisiting = false;
        this.requests.forEach(element => {
          if (element.id == request.id) {
            exisiting = true;
          }
        });

        if (exisiting === false) {
          this.requests.unshift(request);
          this._title.setTitle(this._unchangedTitle + " - Neue Anfragen");
        }
      }
    }));

    this._unchangedTitle = _title.getTitle();
  }

  ngOnInit() {
    this.loadData();
    this._hub.connenct().subscribe( () => {});
  }

  ngOnDestroy() {
    this._hub.disconnect();
    super.unsubcripeAll();
  }

  //#region Methods

  protected loadData(): void {
    this.loadingDataInProgress = true;

    this._service.loadRequests().pipe(finalize(() => this.loadingDataInProgress = false)).subscribe(
      (result) => {
        this.requests = result;
      }, (err) => {
        this._snackBar.open('Die ausstehenden Anfragen konnte nicht geladen werden', 'Fehler', { duration: 4500 });
      }
    )
  }

  public testFunc(request: IUserAccessRequestOverviewModel): void {
    this._service.requestModel = request;
    this._router.navigate(['../grant-user-acess'], { relativeTo: this._activeRoute });
  }

  //#endregion

}
