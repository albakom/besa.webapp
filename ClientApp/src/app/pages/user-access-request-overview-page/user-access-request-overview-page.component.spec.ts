import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserAccessRequestOverviewPageComponent } from './user-access-request-overview-page.component';

describe('UserAccessRequestOverviewPageComponent', () => {
  let component: UserAccessRequestOverviewPageComponent;
  let fixture: ComponentFixture<UserAccessRequestOverviewPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserAccessRequestOverviewPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserAccessRequestOverviewPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
