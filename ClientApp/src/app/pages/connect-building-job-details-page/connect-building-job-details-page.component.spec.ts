import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConnectBuildingJobDetailsPageComponent } from './connect-building-job-details-page.component';

describe('ConnectBuildingJobDetailsPageComponent', () => {
  let component: ConnectBuildingJobDetailsPageComponent;
  let fixture: ComponentFixture<ConnectBuildingJobDetailsPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConnectBuildingJobDetailsPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConnectBuildingJobDetailsPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
