import { fuseAnimations } from '@fuse/animations';
import { OnDestroy } from '@angular/core/src/metadata/lifecycle_hooks';
import { Component, OnInit } from '@angular/core';
import { JobDetailsPageBase } from '../base/job-details-page-base';
import { RefreshManagerService } from '../../services/refresh-manager.service';
import { ActivatedRoute, Router } from '@angular/router';
import { MatSnackBar, MatDialog } from '@angular/material';
import { LoginService } from '../../services/login.service';
import { UriService } from '../../services/uri.service';
import { JobService } from '../../services/job.service';
import { IFinishConnectBuildingDetailModel } from '../../models/service/jobs/connect-building/ifinish-connect-building-detail-model';
import { IConnectBuildingJobDetailsModel } from '../../models/service/jobs/connect-building/iconnect-building-job-details-model';
import { Observable } from 'rxjs';

@Component({
  selector: 'besa-connect-building-job-details-page',
  templateUrl: './connect-building-job-details-page.component.html',
  styleUrls: ['./connect-building-job-details-page.component.scss'],
  animations: fuseAnimations,
})
export class ConnectBuildingJobDetailsPageComponent extends
  JobDetailsPageBase<IFinishConnectBuildingDetailModel, IConnectBuildingJobDetailsModel>
  implements OnInit, OnDestroy {


  constructor(refreshManager: RefreshManagerService,
    activeRoute: ActivatedRoute,
    snackBar: MatSnackBar,
    dialog: MatDialog,
    loginService: LoginService,
    router: Router,
    uriService: UriService,
    service: JobService,
  ) {
    super(refreshManager, activeRoute, snackBar, dialog, loginService, router, uriService, service);
  }

  ngOnInit() {
    super.loadDetailsInternal();
  }

  ngOnDestroy() {
    super.unsubcripeAll();
  }

  //#region Methods

  protected getDetails(id: number): Observable<IConnectBuildingJobDetailsModel> {
    return  this._service.loadConnectBuildingJobDetails(id);
  }
  protected getFinishUrl(): string {
    return '/member/finish-connect-building-job';
  }

  //#endregion



}
