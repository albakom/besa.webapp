import { Router } from '@angular/router';
import { IContactAfterFinishedFilterModel } from './../../models/service/contacts-after-sale-job/icontact-after-finished-filter-model';
import { duration } from 'moment';
import { finalize, debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { SubscriptionAwareComponent } from './../../helper/subscription-aware-component';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { IContactAfterSalesJobOverview } from '../../models/service/contacts-after-sale-job/icontact-after-sales-job-overview';
import { fuseAnimations } from '@fuse/animations';
import { JobService } from '../../services/job.service';
import { RefreshManagerService } from '../../services/refresh-manager.service';
import { MatSnackBar } from '@angular/material';
import { Subject } from 'rxjs';

@Component({
  selector: 'besa-contact-after-sales-job-overview-page',
  templateUrl: './contact-after-sales-job-overview-page.component.html',
  styleUrls: ['./contact-after-sales-job-overview-page.component.scss'],
  animations: fuseAnimations
})
export class ContactAfterSalesJobOverviewPageComponent extends SubscriptionAwareComponent implements OnInit, OnDestroy {

  public loadingDataInProgress: boolean;
  public contacts: IContactAfterSalesJobOverview[];
  private _filter: IContactAfterFinishedFilterModel;
  private _canScroll: boolean;
  public loadingMoreDataInProgress: boolean;
  public openStates = [
    'offen',
    'geschlossen',
    'alle'
  ];

  private _stateString: string;
  get selectedState(): string {
    return this._stateString;
  }
  set selectedState(state: string) {
    this._stateString = state;
    switch(this._stateString){
      case 'offen':
        this._filter.openState = true;
        break;
      case 'geschlossen':
        this._filter.openState = false;
        break;
      case 'alle': 
        this._filter.openState = null;
        break;
      default: 
        break;
    }

    this._service.getContactAfterSalesJobs(this._filter).subscribe((result) => {
      this.contacts = result;
    });
  }
  
  private _searchTerm: Subject<string>;
  private _searchString: string;
  get searchString(): string {
    return this._searchString;
  }
  set searchString(search: string) {
    this._searchString = search;
    this._searchTerm.next(search);
  }

  constructor(
    private _service: JobService,
    private _refreshManager: RefreshManagerService,
    private _snackBar: MatSnackBar,
    private _router: Router
  ) {
    super();

    this._searchTerm = new Subject<string>();

    super.addSubscription(_refreshManager.refreshRequested.subscribe((result) => {
      this.loadData();

      this._canScroll = true;
    }));

    super.addSubscription(this._searchTerm.pipe(
      debounceTime(150),
      distinctUntilChanged(),
      switchMap((item) => {
        this._filter.query = item;
        return this._service.getContactAfterSalesJobs(this._filter);
      })
    ).subscribe((items) => {
      this.contacts = items;
    }));
   }

  ngOnInit() {
    this._filter = {
      query: '',
      amount: 50,
      start: 0,
    }

    this.loadData();
  }

  ngOnDestroy() {
    super.unsubcripeAll();
  }

  loadData() {
    this.loadingDataInProgress = true;

    this._service.getContactAfterSalesJobs(this._filter).pipe(finalize(() => {
      this.loadingDataInProgress = false;
    })).subscribe((result) => {
      if (result) {
        this.contacts = result;
        this._canScroll = this._filter.amount == result.length;
      }
    }, (err) => {
      this._snackBar.open('Die ausstehende Anfrage konnte nicht geladen werden', 'Fehler', { duration: 4500 });
    })
  }

  openDetails(contact: IContactAfterSalesJobOverview) {
    this._router.navigate(['/member/contact-after-sales-job-details', contact.id]);
  }

  onScroll() {
    console.log('onScroll entered');
    if(this._canScroll === false) { return; }
    if(this.loadingMoreDataInProgress === false) { return; }

    this.loadingMoreDataInProgress = true;
    this._filter.start = this.contacts.length;

    this._service.getContactAfterSalesJobs(this._filter).pipe(finalize(() => {
      this.loadingMoreDataInProgress = false;
    })).subscribe((result) => {
      this.contacts.push(...result);
    }, (err) => {
      this._snackBar.open('weitere Kontakte konnten nicht geladen werden', 'Fehler', { duration: 4500 });
    });
  }



}
