import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContactAfterSalesJobOverviewPageComponent } from './contact-after-sales-job-overview-page.component';

describe('ContactAfterSalesJobOverviewPageComponent', () => {
  let component: ContactAfterSalesJobOverviewPageComponent;
  let fixture: ComponentFixture<ContactAfterSalesJobOverviewPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContactAfterSalesJobOverviewPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContactAfterSalesJobOverviewPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
