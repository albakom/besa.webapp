import { MatSnackBar, MatDialog } from '@angular/material';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';
import { SubscriptionAwareComponent } from '../../helper/subscription-aware-component';
import { ICompanyDetailModel } from '../../models/service/companies/icompany-detail-model';
import { CompanyService } from '../../services/companies/company.service';
import { Router } from '@angular/router';
import { finalize } from 'rxjs/operators';
import { ISimpleDialogOptions } from '../../dialogs/simple-dialog/isimple-dialog-options';
import { SimpleDialogComponent } from '../../dialogs/simple-dialog/simple-dialog.component';

@Component({
  selector: 'besa-company-detail-page',
  templateUrl: './company-detail-page.component.html',
  styleUrls: ['./company-detail-page.component.scss'],
  animations: fuseAnimations
})
export class CompanyDetailPageComponent extends SubscriptionAwareComponent implements OnInit {

  private _id: number;
  public loadingDataInProgress: boolean;
  public company: ICompanyDetailModel;

  constructor(
    private _service: CompanyService,
    private _router: Router,
    private _activeRoute: ActivatedRoute,
    private _snackBar: MatSnackBar,
    private _dialog: MatDialog
  ) {
    super();
   }

  ngOnInit() {
    this.loadData();
  }

  public loadData() {
    this._id = +this._activeRoute.snapshot.paramMap.get('id');
    this.loadingDataInProgress = true;
    this._service.getCompanyDetail(this._id).pipe(finalize(() => {
      this.loadingDataInProgress = false;
    })).subscribe((result) => {
      this.company = result;
    }, (err) => {
      this._snackBar.open('Die ausstehende Anfrage konnte nicht bearbeitet werden', 'Fehler', { duration: 4500 });
    });
  }

  public navigateBack() {
    this._router.navigate(['/member/company-overview']);
  }

  public editCompany() {
    this._router.navigate(['member/company-edit', this._id]);
  }

  public deleteCompany() {
    const dialogOptions: ISimpleDialogOptions = {
      title: 'Löschen bestätigen',
      question: 'Möchten Sie wirklich löschen?'
    };

    const dialogRef = this._dialog.open(SimpleDialogComponent, {
      data: dialogOptions
    });

    dialogRef.afterClosed().subscribe((result: boolean) => {
      if (result === true) {
        this.loadingDataInProgress = true;
        this._snackBar.open('Die Firma wird gelöscht', 'In Bearbeitung', { duration: -1 });
        this._service.deleteCompany(this._id).pipe(finalize(() => {
          this.loadingDataInProgress = false;
          this._router.navigate(['/member/company-overview']);
        })).subscribe(
          (result) => {
            if (result)
              this._snackBar.open('Die Firma wurde gelöscht.', 'Erfolg', { duration: 4500 });
          }, (err) => {
            this._snackBar.open('Die Firma konnte nicht gelöscht werden', 'Fehler', { duration: 4500 });
          }
          );
      }
    })
  }

}
