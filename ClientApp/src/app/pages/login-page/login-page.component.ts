import { Component, OnInit } from '@angular/core';
import { OAuthService } from 'angular-oauth2-oidc';
import { LoginService } from '../../services/login.service';
import { NonMemberPageBase } from '../base/non-member-page-base';
import { Router } from '@angular/router';
import { FuseConfigService } from '@fuse/services/config.service';

@Component({
  selector: 'besa-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss']
})
export class LoginPageComponent extends NonMemberPageBase implements OnInit {

  //#region Properties

  public tryAccessMemberArea: boolean;
  public loginInProgress: boolean;
  public authServiceIsReachable: boolean;
  public autoRedirect = true;

  //#endregion

  constructor(
    private _loginService: LoginService,
    private _oAuthService: OAuthService,
    private _router: Router,
    fuseConfigService: FuseConfigService) {
    super(fuseConfigService);
    super.setDefaultLayout();
  }

  ngOnInit() {

    if (this._oAuthService.hasValidAccessToken() === true) {
      let redirectUri = '/member/dashboard';
      if (this._loginService.redirectUri) {
        redirectUri = this._loginService.redirectUri;
        this._loginService.redirectUri = '';
      }
      this._router.navigateByUrl(redirectUri);

    } else {
      if (this._loginService.redirectUri) {
        this.tryAccessMemberArea = true;
        if (this.autoRedirect === true) {
          this.accessRestrictedArea();
        }
      } else {
        this.tryAccessMemberArea = false;
      }
    }
  }

  public accessRestrictedArea() {
    this.startLogin(false);
  }

  public startLogin(setRedirectUriToDefaults?: boolean) {
    this.loginInProgress = true;
    this._oAuthService.loadDiscoveryDocument().then(() => {
      this.authServiceIsReachable = true;
      if (setRedirectUriToDefaults === true) {
        this._loginService.redirectUri = '/dashboard';
      }
      this._oAuthService.initImplicitFlow();
      this.loginInProgress = false;
    }).catch((err) => {
      this.authServiceIsReachable = false;
      this.loginInProgress = false;
    });
  }

}
