import { UploadFileDialogComponent } from './../../dialogs/upload-file-dialog/upload-file-dialog.component';
import { UriService } from './../../services/uri.service';
import { IFileOverviewModel } from './../../models/service/files/ifile-overview-model';
import { SubscriptionAwareComponent } from '../../helper/subscription-aware-component';
import { Component, OnInit } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';
import { FilesService } from '../../services/files/files.service';
import { RefreshManagerService } from '../../services/refresh-manager.service';
import { OnDestroy } from '@angular/core/src/metadata/lifecycle_hooks';
import { Router } from '@angular/router';
import { finalize } from 'rxjs/operators';
import { MatSnackBar, MatDialog } from '@angular/material';
import { IUploadFileDialogOptions } from '../../dialogs/upload-file-dialog/iupload-file-dialog-options';
import { ISimpleDialogOptions } from '../../dialogs/simple-dialog/isimple-dialog-options';
import { SimpleDialogComponent } from '../../dialogs/simple-dialog/simple-dialog.component';

@Component({
  selector: 'besa-files',
  templateUrl: './files.component.html',
  styleUrls: ['./files.component.scss'],
  animations: fuseAnimations
})
export class FilesComponent extends SubscriptionAwareComponent implements OnInit, OnDestroy {

  private _itemPerRequest = 30;
  private _canScroll: boolean = false;
  public loadingMoreDataInProgress: boolean;

  public files: IFileOverviewModel[];
  public loadingDataInProgress: boolean = false;

  public deleteInProgress: boolean;

  constructor(
    private _service: FilesService,
    private _refreshManager: RefreshManagerService,
    private _router: Router,
    private _snackbar: MatSnackBar,
    private _dialog: MatDialog,
    private _uriService: UriService,
  ) {
    super();

    this.files = new Array();

    super.addSubscription(_refreshManager.refreshRequested.subscribe((result) => {
      this.loadData();
    }));
  }

  ngOnInit() {
    this.loadData();
  }

  ngOnDestroy() {
    super.unsubcripeAll();
  }

  loadData() {
    this.loadingDataInProgress = true;
    this._service.getFiles(0, 30).pipe(finalize(() => {
      this.loadingDataInProgress = false;
    })).subscribe((result) => {
      this.files = result;
    }, (err) => {
      this._snackbar.open('Fehler beim Laden der Dateien', 'Fehler', { duration: 4500 });
    });
  }

  public openDetails(file: IFileOverviewModel) {
    this._router.navigate(['/member/file-detail', file.id]);
  }

  public getFileLink(file: IFileOverviewModel) {
    return this._uriService.getFileUrl(file.id);
  }

  public addFile(): void {
    const options: IUploadFileDialogOptions = {};

    const ref = this._dialog.open<UploadFileDialogComponent>(UploadFileDialogComponent, { data: options });

    ref.afterClosed().subscribe((result: number[]) => {
      if (result && result.length > 0) {
        this._router.navigate(['/member/file-detail', result[0]]);
      }
    });
  }

  public downloadFile(id: number): void {
    this._service.downloadFile(id);
  }

  public deleteFile(file: IFileOverviewModel): void {
    if (this.deleteInProgress === true) { return; }

    const dialogOptions: ISimpleDialogOptions = {
      title: 'Löschen bestätigen',
      question: `Möchten Sie wirklich die Datei ${file.name}  löschen?`,
    };

    const dialogRef = this._dialog.open(SimpleDialogComponent, {
      data: dialogOptions
    });

    dialogRef.afterClosed().subscribe((result: boolean) => {
      if (result === true) {

        this.deleteInProgress = true;
        this._snackbar.open('Die Datei wird gelöscht...', 'Verarbeitung', { duration: -1 });

        this._service.deleteFile(file.id).pipe(finalize(() => {
          this.deleteInProgress = false;
          this.loadData();
        })).subscribe(
          (result) => {
            if (result)
              this._snackbar.open('Die Datei wurde gelöscht.', 'Erfolg', { duration: 4500 });
            else
              this._snackbar.open('Die Datei konnte nicht gelöscht werden', 'Fehler', { duration: 4500 });
          }
          , (err) => {
            this._snackbar.open('Die Datei konnte nicht gelöscht werden', 'Fehler', { duration: 4500 });
          });
      }
    });
  }

  public onScroll() {
    if (this._canScroll === false) { return; }
    if (this.loadingMoreDataInProgress === true) { return; }

    this.loadingMoreDataInProgress = true;
    this._service.getFiles(this.files.length, this._itemPerRequest).pipe(finalize(() => {
      this.loadingMoreDataInProgress = false;
    })).subscribe((result) => {
      this.files.push(...result);
      this._canScroll = this._itemPerRequest == result.length;
    }, (err) => {
      this._snackbar.open("Kontakte wurden nicht gelanden", "Fehler", { duration: 4500 });
    });
  }

}
