import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdatePlanningDataDetailsPageComponent } from './update-planning-data-details-page.component';

describe('UpdatePlanningDataDetailsPageComponent', () => {
  let component: UpdatePlanningDataDetailsPageComponent;
  let fixture: ComponentFixture<UpdatePlanningDataDetailsPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdatePlanningDataDetailsPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdatePlanningDataDetailsPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
