import { IUpdateTaskElementOverviewModel } from './../../models/service/tasks/iupdate-task-element-overview-model';
import { MatSnackBar } from '@angular/material';
import { UpdateTaskService } from './../../services/update-task/update-task.service';
import { ActivatedRoute, Router } from '@angular/router';
import { SubscriptionAwareComponent } from './../../helper/subscription-aware-component';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';
import { finalize } from 'rxjs/operators';
import { IUpdateTaskDetailModel } from '../../models/service/tasks/iupdate-task-detail-model';
import { UpdateTasksHubService } from '../../services/hubs/update-tasks-hub.service';

@Component({
  selector: 'besa-update-planning-data-details-page',
  templateUrl: './update-planning-data-details-page.component.html',
  styleUrls: ['./update-planning-data-details-page.component.scss'],
  animations: fuseAnimations
})
export class UpdatePlanningDataDetailsPageComponent extends SubscriptionAwareComponent implements OnInit, OnDestroy {

  private _id: number;
  loadingDataInProgress: boolean;
  details: IUpdateTaskDetailModel

  constructor(
    private _activeRoute: ActivatedRoute,
    private _service: UpdateTaskService,
    private _snackBar: MatSnackBar,
    private _router: Router,
    private _hubService: UpdateTasksHubService,
  ) {
    super();
  }

  ngOnInit() {
    this.loadData();
    this._hubService.connenct().subscribe(() => {
      if (this._id) {
        this._hubService.joinTaskGroup(this._id);
      }
    });

    this._hubService._taskElementChanged.subscribe((result: IUpdateTaskElementOverviewModel) => {
      this.details.elements.forEach(element => {
        if (element.id === result.id) {
          element = result;
        }
      });
      console.log(result);
    });

    this._hubService._updateTaskHeartbeatChanged.subscribe((result) => {
      if (result.taskId === this.details.overview.id) {
        this.details.overview.lastHearbeat = result.value;
      }
    });

    this._hubService._updateTaskCanceled.subscribe((result) => {
      if (result === this.details.overview.id) {
        this.details.overview.wasCanceled = true;
        this.loadData();
      }
    });

    this._hubService._updateTaskFinishedStateChanged.subscribe((result) => {
      if (result.id == this.details.overview.id) {
        this.details.overview.endend = result.finishedAt;
      }
    });

    this._hubService._taskLockDetected.subscribe((result) => {
      if (result == this.details.overview.id) {
        this._snackBar.open('TaskLock entdeckt', 'Achtung', { duration: 4500 });
      }
    })
  }

  ngOnDestroy() {
    super.unsubcripeAll();
    this._hubService.disconnect();
  }

  loadData() {
    this._id = +this._activeRoute.snapshot.paramMap.get('id');
    this.loadingDataInProgress = true;

    this._service.getDetails(this._id).pipe(finalize(() => {
      this.loadingDataInProgress = false;
    })).subscribe((result) => {
      if (result) {
        this.details = result;
        console.log(result);
      }
    }, (err) => {
      this._snackBar.open('Ein Fehler ist beim Laden der Details aufgetreten', 'Fehler', { duration: 4500 });
    });
  }

  navigateBack() {
    this._router.navigate(['member/update-planning-data']);
  }

  cancelTask() {
    this._service.cancelTask(this._id).subscribe((result) => {
      if (result === true) {
        this._snackBar.open('Task wurde erfolgreich abgebrochen', 'Erfolg', { duration: 4500 });
        this.loadData();
      } else {
        this._snackBar.open('Task konnte nicht erfolgreich abgebrochen werden', 'Fehler', { duration: 4500 });
      }
    }, (err) => {
      this._snackBar.open('Task konnte nicht erfolgreich abgebrochen werden', 'Fehler', { duration: 4500 });
    })
  }

}
