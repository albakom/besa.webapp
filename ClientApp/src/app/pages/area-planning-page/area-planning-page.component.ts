import { IBuildingPolygon } from './../../models/service/building/ibuilding-polygon';
import { IStreetCreateModel } from './../../models/service/building/istreet-create-model';
import { IEmptySpaceCreateModel } from './../../models/service/building/iempty-space-create-model';
import { IBuildingAreaPolygon } from './../../models/service/building/ibuilding-area-polygon';
import { ICreateAreaDialogOptions } from './../../dialogs/create-area-dialog/icreate-area-dialog-options';
import { Router, ActivatedRoute } from '@angular/router/';
import { CommentDialogComponent } from './../../dialogs/comment-dialog/comment-dialog.component';
import { CommentDialogOptions } from './../../dialogs/comment-dialog/comment-dialog-options';
import { duration } from 'moment';
import { CreateAreaItemDialogComponent } from './../../dialogs/create-area-item-dialog/create-area-item-dialog.component';
import { MatSnackBar, MatDialog, MatStep } from '@angular/material';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit, ViewChild } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';
import { SubscriptionAwareComponent } from '../../helper/subscription-aware-component';
import { AreaPlanningService } from '../../services/area-planning/area-planning.service';
import { IBuildingAreaOverviewModel } from '../../models/service/building/ibuilding-area-overview-model';
import { finalize } from 'rxjs/operators';
import { IBuildingAreaCreateModel } from '../../models/service/building/ibuilding-area-create-model';
import { IGPSCoordinate } from '../../models/service/other/igpscoordinate';
import { IBuildingAreaDetailModel } from '../../models/service/building/ibuilding-area-detail-model';
import { ICreateAreaItemDialogOptions } from '../../dialogs/create-area-item-dialog/icreate-area-item-dialog-options';
import { IBuildingDeleteModel } from '../../models/service/building/ibuilding-delete-model';
import { CreateAreaDialogComponent } from '../../dialogs/create-area-dialog/create-area-dialog.component';
import { LatLng, LatLngLiteral, PolyMouseEvent } from '@agm/core';
import { IBuildingCreateModel } from './../../models/service/building/ibuilding-create-model';
import { ICreateAreaItemModel } from '../../models/service/building/icreate-area-item-model';
import { AreaItemType } from '../../models/service/building/area-item-type.enum';
import { ICoordsMouseEvent } from '../../models/service/building/icoords-mouse-event';
import { ISimpleDialogOptions } from '../../dialogs/simple-dialog/isimple-dialog-options';
import { ISimpleInputDialogOptions } from '../../dialogs/simple-input-dialog/isimple-input-dialog-options';
import { SimpleInputDialogComponent } from '../../dialogs/simple-input-dialog/simple-input-dialog.component';
import { IStreetEditModel } from '../../models/service/building/istreet-edit-model';

@Component({
  selector: 'besa-area-planning-page',
  templateUrl: './area-planning-page.component.html',
  styleUrls: ['./area-planning-page.component.scss'],
  animations: fuseAnimations
})
export class AreaPlanningPageComponent extends SubscriptionAwareComponent implements OnInit {

  loadingDataInProgress: boolean;
  selectedArea: IBuildingAreaOverviewModel = {
    id: -1,
    areaName: '',
    coordinates: new Array()
  }
  areas: IBuildingAreaOverviewModel[];

  loadingSecondTabDataInProgress: boolean;
  details: IBuildingAreaDetailModel;
  displayedBuildingColumns: string[] = [
    'id',
    'coordinate',
    'buttons'
  ];
  displayedStreetColumns: string[] = [
    'id',
    'street',
    'buttons'
  ];

  polygons: IBuildingAreaPolygon[];
  createPolygonMode: boolean = false;
  newPolygon: LatLngLiteral[];
  newPolygonName: string;
  buildingPolygons: IBuildingPolygon[];
  emptySpacePolygons: IBuildingPolygon[];

  createAreaItemPolygonMode: boolean = false;
  newAreaItemPolygon: LatLngLiteral[];
  building: IBuildingCreateModel;
  emptySpace: IEmptySpaceCreateModel;
  street: IStreetCreateModel;
  itemType: AreaItemType;

  @ViewChild('step2')
  step2: MatStep;

  constructor(
    private _service: AreaPlanningService,
    private _snackBar: MatSnackBar,
    private _dialog: MatDialog,
    private _router: Router,
    private _route: ActivatedRoute
  ) {
    super();

    this.areas = new Array();
    this.polygons = new Array();
    this.newPolygon = new Array();
    this.buildingPolygons = new Array();
    this.emptySpacePolygons = new Array();
    this.newAreaItemPolygon = new Array();
  }

  ngOnInit() {
    this.loadData();
  }

  loadData() {
    this.loadingDataInProgress = true;
    
    this.newPolygon = new Array();
    this.newPolygonName = '';
    this.areas = new Array();
    this.polygons = new Array();
    this._service.getBuildingAreaOverviews().pipe(finalize(() => {
      this.loadingDataInProgress = false;
    })).subscribe((result) => {
      if (result) {
        this.areas = result;

        result.forEach(element => {
          let coor: LatLngLiteral[] = new Array();
          element.coordinates.forEach(elem => {
            let latlng: LatLngLiteral = {
              lat: elem.latitude,
              lng: elem.longitude
            };
            coor.push(latlng);
          });

          let polygon: IBuildingAreaPolygon = {
            coordinates: coor,
            name: element.areaName,
            id: element.id,
            color: '#000000'
          }
          this.polygons.push(polygon);

          this._route.queryParams.subscribe((params) => {
            if (params.area) {
              let areaItem = result.find(x => x.id == params.area);
              this.selectedArea = {
                id: areaItem.id,
                areaName: areaItem.areaName,
                coordinates: areaItem.coordinates
              };
              this.step2.select();
              this.loadDataForSecondTab();
            } else {
              this.selectedArea = {
                id: -1,
                areaName: '',
                coordinates: new Array()
              }
            }
          })
        });
      }
    });
  }

  addArea() {
    const options: ICreateAreaDialogOptions = {};

    const ref = this._dialog.open<CreateAreaDialogComponent>(CreateAreaDialogComponent, { data: options });
    ref.afterClosed().subscribe((result: string) => {
      if (result) {
        this.createPolygonMode = true;
        this.newPolygonName = result;
      }
    })
  }

  loadDataForSecondTab() {
    this.loadingSecondTabDataInProgress = true;

    this._route.queryParams.subscribe((params) => {
      if (params.area) {
        this.selectedArea.id = params.area;
      }
    })

    if(this.selectedArea) {
      this._router.navigate(['/member/area-planning'], { queryParams: { area: this.selectedArea.id } });
    }

    this._service.getBuildingAreaDetails(this.selectedArea.id).pipe(finalize(() => {
      this.loadingSecondTabDataInProgress = false;
    })).subscribe((result) => {
      if (result) {
        this.details = result;
        result.buildings.forEach(element => {
          let coordinates: LatLngLiteral[] = new Array();
          element.coordinates.forEach(item => {
            let latlngItem: LatLngLiteral = {
              lat: item.latitude,
              lng: item.longitude
            };
            coordinates.push(latlngItem);
          });
          let building: IBuildingPolygon = {
            id: element.id,
            coordinates: coordinates,
            color: '#4286f4',
            type: AreaItemType.building
          }

          this.buildingPolygons.push(building);
        });

        result.emptySpaces.forEach(element => {
          let coordinates: LatLngLiteral[] = new Array();
          element.coordinates.forEach(item => {
            let latlngItem: LatLngLiteral = {
              lat: item.latitude,
              lng: item.longitude
            };
            coordinates.push(latlngItem);
          });
          let emptySpace: IBuildingPolygon = {
            id: element.id,
            coordinates: coordinates,
            color: '#3cbc49',
            type: AreaItemType.emptySpace
          }

          this.emptySpacePolygons.push(emptySpace);
        })
      }
    }, (err) => {
      this._snackBar.open('Details konnten nicht geladen werden', 'Fehler', { duration: 4500 });
    })
  }

  addItem() {
    const options: ICreateAreaItemDialogOptions = {};

    const ref = this._dialog.open<CreateAreaItemDialogComponent>(CreateAreaItemDialogComponent, { data: options });
    ref.afterClosed().subscribe((result) => {
      if (result) {
        this._snackBar.open('Eintrag wurde erfolgreich erstellt', 'Erfolg', { duration: 4500 });
        this.createPolygonMode = true;
        this.newPolygon = new Array();
      }
    })
  }

  deleteBuilding(id: number) {
    const options: CommentDialogOptions = {
      title: 'Warum soll dieses Gebäude gelöscht werden?'
    };

    const ref = this._dialog.open<CommentDialogComponent>(CommentDialogComponent, { data: options });

    ref.afterClosed().subscribe((result) => {
      if (result && result != null && result != "") {
        let deleteModel: IBuildingDeleteModel = {
          id: id,
          comment: result
        };

        this._service.deleteBuilding(deleteModel).subscribe((result) => {
          if (result) {
            this.loadDataForSecondTab();
          }
        })
      }
      else this._snackBar.open('Gebäude löschen wurde abgebrochen', 'Abbruch', { duration: 4500 });
    })
  }

  buildingDetails(id: number) {
    this._router.navigate(['member/area-planning-building-detail', id]);
  }

  emptySpaceDetails(id: number) {
    this._router.navigate(['member/area-planning-emptyspace-detail', id]);
  }

  mapClicked($event: ICoordsMouseEvent) {
    if (this.createPolygonMode) {
      //console.log($event.coords.lat);
      //console.log($event.coords.lng);

      // let latlng: LatLngLiteral = {
      //   lat: $event.coords.lat,
      //   lng: $event.coords.lng
      // };
      let latlng: LatLngLiteral = {
        lat: $event.coords.lat,
        lng: $event.coords.lng
      };

      // this.newPolygon.push(latlng);

      let poly: IBuildingAreaPolygon = {
        name: 'sda',
        coordinates: this.newPolygon,
        id: -1,
        color: '#000000'
      };

      if (this.polygons.find(x => x.id === -1)) {
        this.polygons.splice(this.polygons.findIndex(x => x.id === -1), 1);
      }

      this.polygons.push(poly);
    }
  }

  cancelSelection() {
    if (this.polygons.find(x => x.id === -1)) {
      this.polygons.splice(this.polygons.findIndex(x => x.id === -1), 1);
    }

    this.newPolygon = new Array();
  }

  polyClicked(polygon: IBuildingAreaPolygon) {
    this.polygons.forEach(element => {
      element.color = '#000000';
    });
    this.selectedArea = this.areas.find(x => x.id == polygon.id);
    this.polygons.find(x => x.id === polygon.id).color = '#ff0000';

    //this._router.navigate(['/member/area-planning'], { queryParams: { area: this.selectedArea.id } });
  }

  finishAddArea() {
    let coor: IGPSCoordinate[] = new Array();
    this.newPolygon.forEach(element => {
      let coordinate: IGPSCoordinate = {
        latitude: element.lat,
        longitude: element.lng
      }
      coor.push(coordinate);
    });

    let result: IBuildingAreaCreateModel = {
      areaName: this.newPolygonName,
      coordinates: coor
    }

    this._service.createBuildingArea(result).subscribe((res) => {
      if (res) {

        this.loadData();
        this.createPolygonMode = false;
        this._snackBar.open('Polygon erfolgreich erstellt', 'Erfolg', { duration: 4500 });
      }
      else this._snackBar.open('Polygon konnte nicht erstellt werden', 'Fehler', { duration: 4500 });
    });


  }

  cancelPolygon() {
    this.newPolygon = new Array();
    this.createPolygonMode = false;
    this.newPolygonName = '';
    if (this.polygons.find(x => x.id === -1)) {
      this.polygons.splice(this.polygons.findIndex(x => x.id === -1), 1);
    }
  }

  addAreaItem() {
    const options: ICreateAreaItemDialogOptions = {};

    const ref = this._dialog.open<CreateAreaItemDialogComponent>(CreateAreaItemDialogComponent, { data: options });
    ref.afterClosed().subscribe((result: ICreateAreaItemModel) => {
      if (result) {
        if (result.type === AreaItemType.building) {
        this.createAreaItemPolygonMode = true;
        this.building = result.building;
          this.itemType = AreaItemType.building;
        } else if (result.type === AreaItemType.emptySpace) {
        this.createAreaItemPolygonMode = true;
        this.emptySpace = result.emptySpace;
        this.itemType = AreaItemType.emptySpace;
        } else if (result.type === AreaItemType.street) {
          this.itemType = AreaItemType.street;
          this.street = result.street;
          this.finishItem();
        }
      }
    })
  }

  cancelItemPolygon() {
    this.newAreaItemPolygon = new Array();
    this.createAreaItemPolygonMode = false;
    if (this.buildingPolygons.find(x => x.id === -1)) {
      this.buildingPolygons.splice(this.buildingPolygons.findIndex(x => x.id === -1), 1);
    }

    if (this.emptySpacePolygons.find(x => x.id === -1)) {
      this.emptySpacePolygons.splice(this.emptySpacePolygons.findIndex(x => x.id === -1), 1);
    }
  }

  cancelItemSelection() {
    if (this.buildingPolygons.find(x => x.id === -1)) {
      this.buildingPolygons.splice(this.buildingPolygons.findIndex(x => x.id === -1), 1);
    }

    if (this.emptySpacePolygons.find(x => x.id === -1)) {
      this.emptySpacePolygons.splice(this.emptySpacePolygons.findIndex(x => x.id === -1), 1);
    }

    this.newAreaItemPolygon = new Array();
  }

  finishItem() {
    if (this.itemType === AreaItemType.building) {
      let result: IBuildingCreateModel = this.building;
      this.newAreaItemPolygon.forEach(element => {
        let coor: IGPSCoordinate = {
          latitude: element.lat,
          longitude: element.lng
        };
        result.coordinates.push(coor);
      });

      this._service.createBuilding(result).subscribe((res) => {
        if (res) {
          if (this.buildingPolygons.find(x => x.id === -1)) {
            this.buildingPolygons.splice(this.buildingPolygons.findIndex(x => x.id === -1), 1);
          }
          this.newAreaItemPolygon = new Array();

          this.loadDataForSecondTab();
          this.createAreaItemPolygonMode = false;
          this._snackBar.open('Gebäude erfolgreich erstellt', 'Erfolg', {duration: 4500});
        } else {
          this._snackBar.open('Gebäude konnte nicht erstellt werden', 'Fehler', { duration: 4500});
        }
      })
    } else if (this.itemType === AreaItemType.emptySpace) {
      let result: IEmptySpaceCreateModel = this.emptySpace;
      this.newAreaItemPolygon.forEach(element => {
        let coor: IGPSCoordinate = {
          latitude: element.lat,
          longitude: element.lng
        };
        result.coordinates.push(coor);
      });

      this._service.createEmptySpace(result).subscribe((res) => {
        if (res) {
          if(this.emptySpacePolygons.find(x => x.id === -1)) {
            this.emptySpacePolygons.splice(this.emptySpacePolygons.findIndex(x => x.id === -1),1);
          }
          this.newAreaItemPolygon = new Array();

          this.loadDataForSecondTab();
          this.createAreaItemPolygonMode = false;
          this._snackBar.open('Freie Fläche erfolgreich erstellt', 'Erfolg', {duration: 4500});
        } else {
          this._snackBar.open('Freie Fläche konnte nicht erstellt werden', 'Fehler', { duration: 4500});
        }
      })
    } else if (this.itemType === AreaItemType.street) {
      let result: IStreetCreateModel = this.street;
      this._service.createStreet(result).subscribe((res) => {
        if (res) {
          this.loadDataForSecondTab();
          this._snackBar.open('Straße erfolgreich erstellt', 'Erfolg', {duration: 4500});
        } else {
          this._snackBar.open('Straße konnte nicht erstellt werden', 'Fehler', { duration: 4500});
        }
      })
    }
  }

  areaItemPolygonClicked(poly: IBuildingPolygon) {
    if (poly.type === AreaItemType.building) {
      this._router.navigate(['member/area-planning-building-detail', poly.id]);
    } else if (poly.type === AreaItemType.emptySpace) {
      this._router.navigate(['member/area-planning-emptyspace-detail', poly.id]);
    }
  }

  mapAreaItemClicked($event: ICoordsMouseEvent) {
    if (this.createAreaItemPolygonMode) {
      // console.log($event.coords.lat);
      // console.log($event.coords.lng);

      // let latlng: LatLngLiteral = {
      //   lat: $event.coords.lat,
      //   lng: $event.coords.lng
      // };

      // this.newAreaItemPolygon.push(latlng);
      console.log($event.coords.lat);
      console.log($event.coords.lng);

      this.newAreaItemPolygon.push(latlng);

      let poly: IBuildingPolygon = {
        coordinates: this.newAreaItemPolygon,
        id: -1,
        color: (this.itemType === AreaItemType.building) ? '#4286f4' : '#3cbc49',
        type: this.itemType
      };
      if (this.itemType == AreaItemType.building) {
        if (this.buildingPolygons.find(x => x.id === -1)) {
          this.buildingPolygons.splice(this.buildingPolygons.findIndex(x => x.id === -1), 1);
        }
  
        this.buildingPolygons.push(poly);
      } else if (this.itemType == AreaItemType.emptySpace) {
        if (this.emptySpacePolygons.find(x => x.id === -1)) {
          this.emptySpacePolygons.splice(this.emptySpacePolygons.findIndex(x => x.id === -1), 1);
        }
        this.emptySpacePolygons.push(poly);
      }
      
    }
  }

  deleteStreet(id: number) {
    this._service.deleteStreet(id).subscribe((result) => {
      if (result) {
        this._snackBar.open('Die Straße wurde erfolgreich gelöscht', 'Erfolg', { duration: 4500 });
      } else {
        this._snackBar.open('Die Straße konnte nicht gelöscht werden', 'Fehler', { duration: 4500 });
      }
    }, (err) => {
      this._snackBar.open('Die Straße konnte nicht gelöscht werden', 'Fehler', { duration: 4500 });
    })
  }

  deleteEmptySpace(id: number) {
    this._service.deleteEmptySpace(id).subscribe((result) => {
      if (result) {
        this._snackBar.open('Die Freie Fläche wurde erfolgreich gelöscht', 'Erfolg', { duration: 4500 });
      } else {
        this._snackBar.open('Die Freie Fläche konnte nicht gelöscht werden', 'Fehler', { duration: 4500 });
      }
    }, (err) => {
      this._snackBar.open('Die Freie Fläche konnte nicht gelöscht werden', 'Fehler', { duration: 4500 });
    })
  }

  openEditStreet(id: number) {
    const options: ISimpleInputDialogOptions = {
      title: 'Straße bearbeiten',
      yesLabel: 'Bearbeiten',
      noLabel: 'Abbrechen',
      inputValue: this.details.streets.find(x => x.id == id).streetName
    };

    const ref = this._dialog.open<SimpleInputDialogComponent>(SimpleInputDialogComponent, { data: options });
    ref.afterClosed().subscribe((result) => {
      if (result) {
        let street: IStreetEditModel = {
          id: id,
          name: result
        };
        this._service.editStreet(street).subscribe((result) => {
          if (result) {
            this._snackBar.open('Straße wurde erfolgreich bearbeitet', 'Erfolg', {duration: 4500});
          } else {
            this._snackBar.open('Straße konnte nicht bearbeitet werden', 'Fehler', { duration: 4500});
          }
        }, (err) => {
          this._snackBar.open('Straße konnte nicht bearbeitet werden', 'Fehler', { duration: 4500});
        });
      }
    });    
  }

}
