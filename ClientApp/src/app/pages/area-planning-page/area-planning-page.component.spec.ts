import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AreaPlanningPageComponent } from './area-planning-page.component';

describe('AreaPlanningPageComponent', () => {
  let component: AreaPlanningPageComponent;
  let fixture: ComponentFixture<AreaPlanningPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AreaPlanningPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AreaPlanningPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
