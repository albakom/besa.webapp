import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompanyOverviewPageComponent } from './company-overview-page.component';

describe('CompanyOverviewPageComponent', () => {
  let component: CompanyOverviewPageComponent;
  let fixture: ComponentFixture<CompanyOverviewPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompanyOverviewPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompanyOverviewPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
