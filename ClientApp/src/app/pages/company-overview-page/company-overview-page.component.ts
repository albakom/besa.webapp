import { OnDestroy } from '@angular/core/src/metadata/lifecycle_hooks';
import { ISimpleDialogOptions } from './../../dialogs/simple-dialog/isimple-dialog-options';
import { SimpleDialogComponent } from './../../dialogs/simple-dialog/simple-dialog.component';
import { ICompanieOverviewModel } from './../../models/service/icompanie-overview-model';
import { CompanyService } from './../../services/companies/company.service';
import { Component, OnInit } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';
import { SubscriptionAwareComponent } from '../../helper/subscription-aware-component';
import { MatSnackBar, MatDialog } from '@angular/material';
import { RefreshManagerService } from '../../services/refresh-manager.service';
import { Router, ActivatedRoute } from '@angular/router';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'besa-company-overview-page',
  templateUrl: './company-overview-page.component.html',
  styleUrls: ['./company-overview-page.component.scss'],
  animations: fuseAnimations,
})
export class CompanyOverviewPageComponent extends SubscriptionAwareComponent implements OnInit, OnDestroy {

  public loadingDataInProgress: boolean;
  public companies: ICompanieOverviewModel[];

  // public formValidationErrors: { [key: string]: string } = {};
  // public registerForm: FormGroup;

  constructor(
    private _service: CompanyService,
    private _snackBar: MatSnackBar,
    private _refreshManager: RefreshManagerService,
    private _router: Router,
    private _activeRoute: ActivatedRoute,
    private _dialog: MatDialog
  ) {
    super();

    super.addSubscription(_refreshManager.refreshRequested.subscribe((result) => {
      this.loadData();
    }));
  }

  ngOnInit() {
    this.loadData();
  }

  ngOnDestroy() {
    super.unsubcripeAll();
  }

  protected loadData(): void {
    this.loadingDataInProgress = true;

    this._service.loadCompanies().pipe(finalize(() => this.loadingDataInProgress = false)).subscribe(
      (result) => {
        this.companies = result;
      }, (err) => {
        this._snackBar.open('Die ausstehenden Anfragen konnte nicht geladen werden', 'Fehler', { duration: 4500 });
      }
    )
  }

  public addCompany(): void {
    this._router.navigate(['member/company-create']);
  }

  public editCompany(company: ICompanieOverviewModel) {
    this._router.navigate(['/member/company-edit', company.id]);
  }

  public openDetails(company: ICompanieOverviewModel) {
    this._router.navigate(['/member/company-details', company.id]);
  }

  public deleteCompany(company: ICompanieOverviewModel) {
    const dialogOptions: ISimpleDialogOptions = {
      title: 'Löschen bestätigen',
      question: 'Möchten Sie wirklich löschen?'
    };

    const dialogRef = this._dialog.open(SimpleDialogComponent, {
      data: dialogOptions
    });

    dialogRef.afterClosed().subscribe((result: boolean) => {
      if (result === true) {
        this.loadingDataInProgress = true;

        this._service.deleteCompany(company.id).pipe(finalize(() => {
          this.loadingDataInProgress = false;
          this.loadData();
        })).subscribe(
          (result) => {
            if (result)
              this._snackBar.open('Die Firma wurde gelöscht.', 'Erfolg', { duration: 4500 });
            else
              this._snackBar.open('Die Firma konnte nicht gelöscht werden', 'Fehler', { duration: 4500 });
          }
          );
      }
    })


  }

}
