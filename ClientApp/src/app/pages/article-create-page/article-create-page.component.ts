import { finalize } from 'rxjs/operators';
import { ArticleService } from './../../services/article/article.service';
import { Component, OnInit } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';
import { FormBasedPage } from '../base/form-based-page';
import { OnDestroy } from '@angular/core/src/metadata/lifecycle_hooks';
import { FormBuilder, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material';
import { IArticleCreateModel } from '../../models/service/article/iarticle-create-model';
import { FormErrorMessage } from '../../models/helper/form-error-message';
import { ArticleCreateFormErrorMessage } from '../../models/error-messages/article-create-form-error-message';
import { Router } from '@angular/router';

@Component({
  selector: 'besa-article-create-page',
  templateUrl: './article-create-page.component.html',
  styleUrls: ['./article-create-page.component.scss'],
  animations: fuseAnimations
})
export class ArticleCreatePageComponent extends FormBasedPage implements OnInit, OnDestroy {

  public createInProgress: boolean;

  constructor(
    private _fb: FormBuilder,
    private _router: Router,
    private _snackBar: MatSnackBar,
    private _service: ArticleService
  ) {
    super();

    super.initForm(
      this._fb.group({
        name: ['', [Validators.required, Validators.maxLength(64)]],
        productSoldByMeter: [false, []],
      })
    );
   }

  ngOnInit() {
  }

  ngOnDestroy() {
    super.unsubcripeAll();
  }

  public createArticle(): void {
    if (this.form.valid == false) return;
    if (this.createInProgress == false) return;

    this.createInProgress = true;

    const creationModel: IArticleCreateModel = this.form.value;

    this._snackBar.open('Ware wird erstellt', 'In Bearbeitung', { duration: -1 });

    this._service.createArticle(creationModel).pipe(finalize(() => {
      this.createInProgress = false;
    })).subscribe((result) => {
      this._snackBar.dismiss();
      this._snackBar.open('Ware wurde erfolgreich erstellt', 'Erfolg', { duration: 3500 });
      this._router.navigate(['member/article-overview']);
    }, (err) => {
      this._snackBar.dismiss();
      this._snackBar.open('Feher beim erstellen des Gebiets', 'Fehler', { duration: 3500 });
    });
  }

  protected getErrorMessages(): FormErrorMessage[] {
    throw ArticleCreateFormErrorMessage;
  }

  public cancelAdd(): void {
    this._router.navigate(['member/article-overview']);
  }

}
