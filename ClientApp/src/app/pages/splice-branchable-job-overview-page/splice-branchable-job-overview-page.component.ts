import { Component, OnInit, OnDestroy } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';
import { JobOverviewPageBase } from '../base/job-overview-page-base';
import { RefreshManagerService } from '../../services/refresh-manager.service';
import { MatSnackBar } from '@angular/material';
import { Router } from '@angular/router';
import { ConstructionStageService } from '../../services/construction-stage.service';
import { IConstructionStageOverviewForJobsModel } from '../../models/service/construction-stage/iconstruction-stage-overview-for-jobs-model';
import { Observable } from 'rxjs';

@Component({
  selector: 'besa-splice-branchable-job-overview-page',
  templateUrl: './splice-branchable-job-overview-page.component.html',
  animations: fuseAnimations,
  styleUrls: ['./splice-branchable-job-overview-page.component.scss']
})
export class SpliceBranchableJobOverviewPageComponent extends JobOverviewPageBase implements OnInit, OnDestroy {

  constructor(refreshManger: RefreshManagerService,
    snackBar: MatSnackBar,
    router: Router,
    private _service: ConstructionStageService) {
    super(refreshManger, snackBar, router);
  }

  ngOnInit() {
    super.loadConstructionStages();
  }

  ngOnDestroy() {
    super.unsubcripeAll();
  }

  //#region Methods

  protected loadOverview(onlybounded: boolean): Observable<IConstructionStageOverviewForJobsModel[]> {
    return this._service.loadOverviewForSplice(onlybounded);
  }
  
  protected getNavigationPathForDetails(): string {
    return '/member/splice-branchable-job-construction-stage-detail';
  }

  //#endregion

}
