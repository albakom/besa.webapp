import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SpliceBranchableJobOverviewPageComponent } from './splice-branchable-job-overview-page.component';

describe('SpliceBranchableJobOverviewPageComponent', () => {
  let component: SpliceBranchableJobOverviewPageComponent;
  let fixture: ComponentFixture<SpliceBranchableJobOverviewPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SpliceBranchableJobOverviewPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpliceBranchableJobOverviewPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
