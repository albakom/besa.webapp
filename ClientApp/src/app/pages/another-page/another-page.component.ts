import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'besa-another-page',
  templateUrl: './another-page.component.html',
  styleUrls: ['./another-page.component.scss']
})
export class AnotherPageComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
