import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BindTaskOverviewPageComponent } from './bind-task-overview-page.component';

describe('BindTaskOverviewPageComponent', () => {
  let component: BindTaskOverviewPageComponent;
  let fixture: ComponentFixture<BindTaskOverviewPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BindTaskOverviewPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BindTaskOverviewPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
