import { Component, OnInit } from '@angular/core';
import { ErrorService } from '../../services/error.service';
import { Subscription } from 'rxjs';
import { IErrorModel } from '../../models/helper/ierror-model';
import { SubscriptionAwareComponent } from '../../helper/subscription-aware-component';
import { OnDestroy } from '@angular/core/src/metadata/lifecycle_hooks';
import { NonMemberPageBase } from '../base/non-member-page-base';
import { FuseConfigService } from '@fuse/services/config.service';

@Component({
  selector: 'besa-error-page',
  templateUrl: './error-page.component.html',
  styleUrls: ['./error-page.component.scss']
})
export class ErrorPageComponent extends NonMemberPageBase implements OnInit, OnDestroy {

  //#region Properties

  public error: IErrorModel;

  //#endregion

  //#region Constructor

  constructor(
    private _errorSvc: ErrorService,
    fuseConfigSvc: FuseConfigService,
  ) {
    super(fuseConfigSvc);
    super.setDefaultLayout();
  }

  //#endregion

  //#region life cycle hooks

  ngOnInit() {
    super.addSubscription(this._errorSvc.error.subscribe((err) => {
      this.error = err;
    }));

    super.setDefaultLayout();
  }

  ngOnDestroy() {
    super.unsubcripeAll();
  }

  //#endregion

}
