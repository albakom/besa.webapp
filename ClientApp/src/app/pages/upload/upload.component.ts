import { Observable ,  Subscription ,  Subject } from 'rxjs';
import { FormErrorMessage } from '../../models/helper/form-error-message';
import { Component, OnInit, Input } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';
import { FormBasedPage } from '../base/form-based-page';
import { UploadService } from '../../services/upload/upload.service';
import { map, last, tap, catchError } from 'rxjs/operators';
import { HttpEvent, HttpEventType } from '@angular/common/http';
import { IUploadItemModel } from '../../models/upload/iupload-item-model';
import { UploadStatus } from '../../models/upload/upload-status.enum';

@Component({
  selector: 'besa-upload',
  templateUrl: './upload.component.html',
  styleUrls: ['./upload.component.scss'],
  animations: fuseAnimations
})
export class UploadComponent implements OnInit {

  @Input()
  public canRemoveItems: boolean = true;

  public color = "primary";
  public mode = "determinate";
  public value = 0;

  public selectedUploads: IUploadItemModel[] = new Array();
  public startedUploads: IUploadItemModel[] = new Array();

  private uploadFinished: Subject<number[]> = new Subject<number[]>();

  private fileIds: number[];

  private filesLeft: number;

  showProgress(percentage: number): any {
    this.value = percentage;
  }

  public uploadLog: string = "";
  fileToUpload: File = null;

  constructor(
    private _service: UploadService
  ) {
  }

  ngOnInit() {
  }

  public startUpload(files: File[]): Observable<number[]> {
    this.fileIds = new Array();

    const items: IUploadItemModel[] = files.map((val) => {
      const item: IUploadItemModel = {
        file: val,
        status: UploadStatus.queue,
        percentage: 0,
      }
      return item;
    });
    this.startedUploads = new Array();
    this.selectedUploads = new Array();

    this.selectedUploads.push(...items);
    this.startedUploads.push(...items);

    this.filesLeft = this.startedUploads.length;
    this.uploadFiles();

    return this.uploadFinished;
  }

  private _currentUploadSub: Subscription;

  private checkIfUploadIsFinished(): void {

    if (this.filesLeft === 0) {
      this.uploadFinished.next(this.fileIds);
    }
  }

  private uploadFiles() {


    let isUploading: boolean = false;
    for (let i = 0; i < this.startedUploads.length; i++) {
      let file = this.startedUploads[i];
      if (file.status == UploadStatus.queue && !isUploading) {
        //upload
        isUploading = true;

        // upload running
        //    update status
        this._currentUploadSub = this._service.postFile(file.file).pipe(map(event => {
          // upload status
          let message = "";
          message = this.getEventMessage(event, file.file);
          this.uploadLog += "\r" + message + "\r";
          switch (event.type) {
            case HttpEventType.Sent:
              file.status = UploadStatus.upload;
              return;
            case HttpEventType.UploadProgress:
              file.percentage = Math.round(100 * event.loaded / event.total);
              return;
            case HttpEventType.Response:
              file.status = UploadStatus.finished;
              this.fileIds.push(event.body);
              this.filesLeft -= 1;
              if (this.startedUploads.find(x => x.status == UploadStatus.queue)) {
                this.uploadFiles();
              } else {
                this.checkIfUploadIsFinished();
              }
              return;
            default:
              file.status = UploadStatus.canceled;
              if (this.startedUploads.find(x => x.status == UploadStatus.queue)) {
                this.uploadFiles();
              }
              return;
          }
        })).subscribe((result) => {
        }, (err) => {
          this.filesLeft -= 1;
          // upload error
          file.status = UploadStatus.canceled;
          // start next upload
          if (this.startedUploads.find(x => x.status == UploadStatus.queue)) {
            this.uploadFiles();
          } else {
            this.checkIfUploadIsFinished();
          }
        });
        break;
      }
    }

    this.checkIfUploadIsFinished();
  }

  private getEventMessage(event: HttpEvent<number>, file: File) {
    switch (event.type) {
      case HttpEventType.Sent:
        return `Uploading file "${file.name}" of size ${file.size}.`;

      case HttpEventType.UploadProgress:
        // Compute and show the % done:
        this.showProgress(100 * event.loaded / event.total);
        const percentDone = Math.round(100 * event.loaded / event.total);
        return `File "${file.name}" is ${percentDone}% uploaded.`;

      case HttpEventType.Response:
        return `File "${file.name}" was completely uploaded!`;

      default:
        return `File "${file.name}" surprising upload event: ${event.type}.`;
    }
  }

  public cancel(item: IUploadItemModel) {
    if (this._currentUploadSub) {
      this._currentUploadSub.unsubscribe();
      this.uploadFiles();
    }
    let index: number = this.startedUploads.findIndex(x => x == item);
    this.startedUploads.splice(index, 1);
  }

  public delete(item: IUploadItemModel) {
    let index: number = this.startedUploads.findIndex(x => x == item);
    this.startedUploads.splice(index, 1);
    // TODO: aus dem Service löschen
  }

}
