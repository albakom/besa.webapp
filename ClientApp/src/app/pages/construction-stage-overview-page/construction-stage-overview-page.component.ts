import { ConstructionStageService } from './../../services/construction-stage.service';
import { RefreshManagerService } from './../../services/refresh-manager.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { SubscriptionAwareComponent } from '../../helper/subscription-aware-component';
import { IConstructionStageOverviewModel } from '../../models/service/construction-stage/iconstruction-stage-overview-model';
import { MatSnackBar, MatDialog } from '@angular/material';
import { finalize } from 'rxjs/operators';
import { Router } from '@angular/router';
import { fuseAnimations } from '@fuse/animations';
import { ISimpleDialogOptions } from '../../dialogs/simple-dialog/isimple-dialog-options';
import { SimpleDialogComponent } from '../../dialogs/simple-dialog/simple-dialog.component';

@Component({
  selector: 'besa-construction-stage-overview-page',
  templateUrl: './construction-stage-overview-page.component.html',
  styleUrls: ['./construction-stage-overview-page.component.scss'],
  animations: fuseAnimations,
})
export class ConstructionStageOverviewPageComponent extends SubscriptionAwareComponent implements OnInit, OnDestroy {

  //#region Properties

  public loadingDataInProgress: boolean;
  public overviewItems: IConstructionStageOverviewModel[];

  //#endregion

  constructor(
    private refreshManager: RefreshManagerService,
    private _snackBar: MatSnackBar,
    private _router: Router,
    private _dialog: MatDialog,
    private _service: ConstructionStageService) {
    super();

    super.addSubscription(refreshManager.refreshRequested.subscribe((result) => {
      this.loadItems();
    }));
  }

  ngOnInit() {
    this.loadItems();
  }

  ngOnDestroy() {
    super.unsubcripeAll();
  }

  //#region Mehthods

  public loadItems(): void {

    this.loadingDataInProgress = true;

    this._service.loadOverview().pipe(finalize(() => {
      this.loadingDataInProgress = false;
    })).subscribe((result) => {
      this.overviewItems = result;
    },

      (err) => {
        this._snackBar.open('Die Bauabschnitt konnte nicht geladen werden', 'Fehler', { duration: 4500 });
      });
  }

  public deleteItem(item: IConstructionStageOverviewModel) {
    const dialogOptions: ISimpleDialogOptions = {
      title: 'Löschen bestätigen',
      question: `Möchten Sie den Bauabschitt ${item.name} wirklich löschen?`,
    };

    const dialogRef = this._dialog.open(SimpleDialogComponent, {
      data: dialogOptions
    });

    dialogRef.afterClosed().subscribe((result: boolean) => {
      if (result === true) {
        this.loadingDataInProgress = true;

        this._snackBar.open('Der Bauabschnitt wird gelöscht...', 'In Bearbeitung', { duration: -1 });

        this._service.deleteConstructionStage(item).pipe(finalize(() => {
          this.loadItems();
        })).subscribe(
          (result) => {
            if (result)
              this._snackBar.open('Der Bauabschnitt wurde gelöscht.', 'Erfolg', { duration: 4500 });
            else
              this._snackBar.open('Der Bauabschnitt konnte nicht gelöscht werden', 'Fehler', { duration: 4500 });
          }
          , (err) => {
            this._snackBar.open('Der Bauabschnitt konnte nicht gelöscht werden', 'Fehler', { duration: 4500 });
          });
      }
    })
  }

  public createNewItem(): void {
    this._router.navigate(['./member/new-construciton-stage'])
  }

  public goToEdit(item: IConstructionStageOverviewModel): void {
    this._router.navigate(['./member/construction-stage-update', item.id]);
  }

  //#endregion

}
