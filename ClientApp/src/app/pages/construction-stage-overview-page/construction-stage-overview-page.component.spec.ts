import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConstructionStageOverviewPageComponent } from './construction-stage-overview-page.component';

describe('ConstructionStageOverviewPageComponent', () => {
  let component: ConstructionStageOverviewPageComponent;
  let fixture: ComponentFixture<ConstructionStageOverviewPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConstructionStageOverviewPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConstructionStageOverviewPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
