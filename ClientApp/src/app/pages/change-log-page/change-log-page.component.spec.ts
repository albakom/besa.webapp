import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChangeLogPageComponent } from './change-log-page.component';

describe('ChangeLogPageComponent', () => {
  let component: ChangeLogPageComponent;
  let fixture: ComponentFixture<ChangeLogPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChangeLogPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChangeLogPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
