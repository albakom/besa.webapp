import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BranchOffJobDetailsPageComponent } from './branch-off-job-details-page.component';

describe('BranchOffJobDetailsPageComponent', () => {
  let component: BranchOffJobDetailsPageComponent;
  let fixture: ComponentFixture<BranchOffJobDetailsPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BranchOffJobDetailsPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BranchOffJobDetailsPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
