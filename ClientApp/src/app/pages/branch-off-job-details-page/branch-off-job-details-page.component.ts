import { Observable } from 'rxjs';
import { IBranchOffJobDetailModel } from './../../models/service/jobs/ibranch-off-job-detail-model';
import { UriService } from './../../services/uri.service';
import { IFileOverviewModel } from './../../models/service/files/ifile-overview-model';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { SubscriptionAwareComponent } from '../../helper/subscription-aware-component';
import { RefreshManagerService } from '../../services/refresh-manager.service';
import { ActivatedRoute, Router } from '@angular/router';
import { MatSnackBar, MatDialog } from '@angular/material';
import { JobService } from '../../services/job.service';
import { finalize } from 'rxjs/operators';
import { fuseAnimations } from '@fuse/animations';
import { JobStates } from '../../models/service/jobs/job-states.enum';
import { LoginService } from '../../services/login.service';
import { BesaRoles } from '../../models/service/besa-roles.enum';
import { FileTypes } from '../../models/service/files/file-types.enum';
import { IShowPictureDialogOptions } from '../../dialogs/show-picture-dialog/ishow-picture-dialog-options';
import { ShowPictureDialogComponent } from '../../dialogs/show-picture-dialog/show-picture-dialog.component';
import { IAcknowledgeJobModel } from '../../models/service/jobs/iacknowledge-job-model';
import { JobDetailsPageBase } from '../base/job-details-page-base';
import { IFinishBranchOffDetailModel } from '../../models/service/jobs/branch-off/ifinish-branch-off-detail-model';

@Component({
  selector: 'besa-branch-off-job-details-page',
  templateUrl: './branch-off-job-details-page.component.html',
  styleUrls: ['./branch-off-job-details-page.component.scss'],
  animations: fuseAnimations,
})
export class BranchOffJobDetailsPageComponent extends
  JobDetailsPageBase<IFinishBranchOffDetailModel, IBranchOffJobDetailModel>
  implements OnInit, OnDestroy {


  constructor(refreshManager: RefreshManagerService,
    activeRoute: ActivatedRoute,
    snackBar: MatSnackBar,
    dialog: MatDialog,
    loginService: LoginService,
    router: Router,
    uriService: UriService,
    service: JobService,
  ) {
    super(refreshManager, activeRoute, snackBar, dialog, loginService, router, uriService, service);
  }

  ngOnInit() {
    super.loadDetailsInternal();
  }

  ngOnDestroy() {
    super.unsubcripeAll();
  }

  //#region Methods

  protected getDetails(id: number): Observable<IBranchOffJobDetailModel> {
    return this._service.loadBranchOffJobDetails(id);
  }

  protected getFinishUrl(): string {
    return '/member/finish-branchoff-job';
  }

  //#endregion
}
