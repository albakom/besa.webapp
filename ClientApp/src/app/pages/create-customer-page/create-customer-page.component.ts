import { ISimpleBuildingOverviewModel } from './../../models/service/construction-stage/isimple-building-overview-model';
import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { FormBasedPage } from '../base/form-based-page';
import { FormBuilder, Validators } from '@angular/forms';
import { MatSnackBar, MatAutocompleteSelectedEvent, MatStepper, MatDialog, MatSlideToggleChange } from '@angular/material';
import { ConstructionStageService } from '../../services/construction-stage.service';
import { Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap, finalize } from 'rxjs/operators';
import { CreateCustomerFormMessage } from '../../models/error-messages/create-customer-form-message';
import { FormErrorMessage } from '../../models/helper/form-error-message';
import { StepperSelectionEvent } from '@angular/cdk/stepper';
import { fuseAnimations } from '@fuse/animations';
import { ISimpleFlatOverviewModel } from '../../models/service/flats/isimple-flat-overview-model';
import { CreateFlatDialogComponent } from '../../dialogs/create-flat-dialog/create-flat-dialog.component';
import { ICreateFlatDialogOptions } from '../../dialogs/create-flat-dialog/icreate-flat-dialog-options';
import { IContactInfo } from '../../models/service/icontact-info';
import { PersonTypes } from '../../models/service/person-types.enum';
import { CustomerService } from '../../services/customer.service';
import { ICreateContactDialogOptions } from '../../dialogs/create-contact-dialog/icreate-contact-dialog-options';
import { CreateContactDialogComponent } from '../../dialogs/create-contact-dialog/create-contact-dialog.component';
import { ICustomerCreateModel } from '../../models/service/customers/icustomer-create-model';
import { ICreateCustomerForm } from '../../models/forms/icreate-customer-form';

@Component({
  selector: 'besa-create-customer-page',
  templateUrl: './create-customer-page.component.html',
  styleUrls: ['./create-customer-page.component.scss'],
  animations: fuseAnimations,
})
export class CreateCustomerPageComponent extends FormBasedPage implements OnInit, OnDestroy {

  //#region Fields

  private _buildingSearchTerm: Subject<string>;
  private _customerSearchTerm: Subject<string>;
  private _ownerSearchTerm: Subject<string>;
  private _architectSearchTerm: Subject<string>;
  private _workmanSearchTerm: Subject<string>;

  private _currentOwnerContact: IContactInfo;
  private _currentCustomerContact: IContactInfo;
  private _currentArchitectContact: IContactInfo;
  private _currentWorkmanContact: IContactInfo;


  private _ignoreCustomerSearchEvent: boolean;

  //#endregion

  //#region Properties

  public buildingResult: ISimpleBuildingOverviewModel[];
  public currentBulding: ISimpleBuildingOverviewModel;

  public loadingBuildingDetails: boolean;
  public buildingIsConnected: boolean;

  public loadingFlatNeeded: boolean;

  public loadingFlatsInProgress: boolean;
  public possibleFlats: ISimpleFlatOverviewModel[];

  public ownerResult: IContactInfo[];
  public customerResult: IContactInfo[];
  public architectResult: IContactInfo[];
  public workmanResult: IContactInfo[];

  public createInProgress: boolean;

  @ViewChild('stepper')
  public stepper: MatStepper;

  //#endregion

  constructor(
    private _fb: FormBuilder,
    private _snackBar: MatSnackBar,
    private _dialog: MatDialog,
    private _service: ConstructionStageService,
    private _customerService: CustomerService) {
    super();

    this._buildingSearchTerm = new Subject<string>();
    this._customerSearchTerm = new Subject<string>();
    this._ownerSearchTerm = new Subject<string>();
    this._workmanSearchTerm = new Subject<string>();
    this._architectSearchTerm = new Subject<string>();



    this.prepare();

    super.addSubscription(this._buildingSearchTerm.pipe(
      debounceTime(150),
      distinctUntilChanged(),
      switchMap((item) => {
        return this._service.searchStreets(item);
      })
    ).subscribe((items) => {
      this.buildingResult = items;
    }));

    super.addSubscription(this._customerSearchTerm.pipe(
      debounceTime(150),
      distinctUntilChanged(),
      switchMap((item) => {
        return this._customerService.searchContacts(item);
      })
    ).subscribe((items) => {
      this.customerResult = items;
    }));

    super.addSubscription(this._ownerSearchTerm.pipe(
      debounceTime(150),
      distinctUntilChanged(),
      switchMap((item) => {
        return this._customerService.searchContacts(item);
      })
    ).subscribe((items) => {
      this.ownerResult = items;
    }));

    super.addSubscription(this._workmanSearchTerm.pipe(
      debounceTime(150),
      distinctUntilChanged(),
      switchMap((item) => {
        return this._customerService.searchContacts(item);
      })
    ).subscribe((items) => {
      this.workmanResult = items;
    }));

    super.addSubscription(this._architectSearchTerm.pipe(
      debounceTime(150),
      distinctUntilChanged(),
      switchMap((item) => {
        return this._customerService.searchContacts(item);
      })
    ).subscribe((items) => {
      this.architectResult = items;
    }));

    super.initForm(
      this._fb.group({
        buildingTermValue: ['', []],
        searchCustomermValue: ['', []],
        searchOwnerValue: ['', []],
        searchArchitectValue: ['', []],
        searchWorkmanValue: ['', []],
        buildingAndFlat: this._fb.group(
          {
            buildingId: [null, [Validators.required]],
            flatId: [null, [Validators.required]],
            isActiveConnectionRequired: [false, [Validators.required]],
            ownerContactId: [null, []],
            architectContactId: [null, []],
            workmanContactId: [null, []],
          }),
        houseConnection: this._fb.group(
          {
            description: [null, []],
            civilWorkIsDoneByCustomer: [false, []],
            connectioOfOtherMediaRequired: [false, []],
            informSalesAfterFinish: [false, []],
          }),
        activeConnenction: this._fb.group(
          {
            customerContactId: [null, [Validators.required]],
            ductAmount: [null, []],
            subscriberEndpointLength: [null, []],
            connectionAppointment: [null, []],
            activationAsSoonAsPossible: [false, []],

            subscriberEndpointNearConnectionPoint: this._fb.group({
              value: [true, []],
              description: [null, []],
            }),
            activePointNearSubscriberEndpoint: this._fb.group({
              value: [false, []],
              description: [null, []],
            }),
            powerForActiveEquipment: this._fb.group({
              value: [false, []],
              description: [null, []],
            }),
          }),
      }));

    const fieldsToSubscripeChanges: { [name: string]: Subject<string> } = {
      'buildingTermValue': this._buildingSearchTerm,
      'searchCustomermValue': this._customerSearchTerm,
      'searchOwnerValue': this._ownerSearchTerm,
      'searchArchitectValue': this._architectSearchTerm,
      'searchWorkmanValue': this._workmanSearchTerm,
    };

    for (const key in fieldsToSubscripeChanges) {
      if (fieldsToSubscripeChanges.hasOwnProperty(key)) {
        const element = fieldsToSubscripeChanges[key];

        super.addSubscription(this.form.get(key).valueChanges.subscribe((value: string) => {
          if (this._ignoreCustomerSearchEvent === true) { return; }
          if (typeof (value) === 'string') {
            element.next(value);
          }
        }));
      }
    }
  }

  ngOnInit() {
  }

  ngOnDestroy() {
    super.unsubcripeAll();
  }

  //#region Methods

  private prepare(): void {
    this.ownerResult = new Array();
    this.customerResult = new Array();
    this.architectResult = new Array();
    this.workmanResult = new Array();

    this._currentOwnerContact = null;
    this._currentCustomerContact = null;
    this._currentArchitectContact = null;
    this._currentWorkmanContact = null;

    this.currentBulding = null;
    this.possibleFlats = new Array();
  }

  protected getErrorMessages(): FormErrorMessage[] {
    return CreateCustomerFormMessage;
  }

  public stepChanged(event: StepperSelectionEvent): void {
    if (event.selectedIndex == 1) {
    }
  }

  private setFlatIdToGoToNextStep(flatId: number) {
    this.form.patchValue({
      'buildingAndFlat': {
        flatId: flatId,
      }
    });

    // this.stepper.next();
  }

  public activeConnectionChanged(event: MatSlideToggleChange): void {
    if (event.checked === true) {
      this.getFlats(this.currentBulding);
    }
  }

  private getFlats(building: ISimpleBuildingOverviewModel) {

    if (building.commercialUnits + building.householdUnits == 1) {
      this.loadingFlatNeeded = false;
    }
    else {
      this.loadingFlatNeeded = true;
    }

    this.loadingFlatsInProgress = true;

    this._service.loadFlatsByBuilding(building).pipe(finalize(() => {
      this.loadingFlatsInProgress = false;
    })).subscribe((result) => {
      if (this.loadingFlatNeeded === false) {
        this.setFlatIdToGoToNextStep(result[0].id);
      } else {
        this.possibleFlats = result;
      }
    }, (err) => {
      this._snackBar.open('Wohnungen konnten nicht geladen werden', 'Fehler', { duration: 4500 });
    });
  }

  public buildingSelected(eventInfo: MatAutocompleteSelectedEvent): void {
    const building: ISimpleBuildingOverviewModel = eventInfo.option.value;
    this.currentBulding = building;

    this.form.patchValue({
      'buildingAndFlat': {
        buildingId: building.id
      }
    });

    this.buildingIsConnected = false;
    this.loadingBuildingDetails = true;
    this._service.checkIfBuildingIsConnected(building).pipe(finalize(() => {
      this.loadingBuildingDetails = false;
    })).subscribe((result) => {
      this.buildingIsConnected = result;
    }, (err) => {
      this._snackBar.open('Details zum Gebäude konnten nicht geladen werden', 'Fehler', { duration: 4500 });
    });

    if (this.form.value.buildingAndFlat.isActiveConnectionRequired === true) {
      this.getFlats(building);
    }
  }

  public displayCabinentResult(item?: ISimpleBuildingOverviewModel): string {
    return item ? item.streetName : '';
  }

  public openCreateFlatDialog(): void {
    const options: ICreateFlatDialogOptions = {
      buildingId: this.currentBulding.id,
    };

    const ref = this._dialog.open<CreateFlatDialogComponent>(CreateFlatDialogComponent, { data: options });
    ref.afterClosed().subscribe((flat: ISimpleFlatOverviewModel) => {
      if (flat) {
        if (!this.possibleFlats) {
          this.possibleFlats = new Array();
        }
        this.possibleFlats.push(flat);

        this.setFlatIdToGoToNextStep(flat.id);
      }
    });
  }

  public displayContactResult(item?: IContactInfo): string {
    if (!item) { return '' }
    let result = '';
    if (item.type === PersonTypes.Male) {
      result += 'Herr ';
    } else {
      result += 'Frau ';
    }

    if (item.surname) {
      result += item.surname + ' ';
    }
    if (item.lastname) {
      result += item.lastname + ' ';
    }

    if (item.companyName) {
      result += '(' + item.companyName + ')';
    }

    return result;
  }

  private setContact(type: string, contact: IContactInfo): void {
    let formPropertyName: string = null;
    if (type === 'customer') {
      this._currentCustomerContact = contact;
      this.form.patchValue({
        'activeConnenction': {
          customerContactId: contact.id
        }
      });

    } else if (type === 'owner') {
      this._currentOwnerContact = contact;
      formPropertyName = 'ownerContactId';
    } else if (type === 'architect') {
      this._currentArchitectContact = contact;
      formPropertyName = 'architectContactId';
    } else if (type === 'workman') {
      this._currentWorkmanContact = contact;
      formPropertyName = 'workmanContactId';
    }

    if (formPropertyName) {
      const patchValue: { [property: string]: any } = {};
      patchValue[formPropertyName] = contact.id;

      this.form.patchValue({
        'buildingAndFlat': patchValue,
      });
    }
  }

  public contactSelected(eventInfo: MatAutocompleteSelectedEvent, type: string): void {
    const contact: IContactInfo = eventInfo.option.value;
    this.setContact(type, contact);
  }

  public openCreateContactDialog(type: string): void {
    const options: ICreateContactDialogOptions = {};
    if (this.currentBulding && (type === 'customer' || type === 'owner')) {
      const splitIndex = this.currentBulding.streetName.lastIndexOf(' ');
      if (splitIndex > 0) {
        const streetName = this.currentBulding.streetName.substr(0, splitIndex);
        const houseNumber = this.currentBulding.streetName.substr(splitIndex + 1);

        options.addressToCopy = {
          city: 'Teterow',
          postalCode: '17166',
          street: streetName,
          streetNumber: houseNumber,
        };
      }
    }

    const ref = this._dialog.open<CreateContactDialogComponent>(CreateContactDialogComponent, { data: options });
    ref.afterClosed().subscribe((contact: IContactInfo) => {
      if (contact) {
        this._ignoreCustomerSearchEvent = true;
        let contactArray: IContactInfo[] = null;
        let searchTermValue: string = null;
        if (type === 'customer') {
          contactArray = this.customerResult;
          searchTermValue = 'searchCustomermValue';
        } else if (type === 'owner') {
          contactArray = this.ownerResult;
          searchTermValue = 'searchOwnerValue';
        } else if (type === 'architect') {
          contactArray = this.architectResult;
          searchTermValue = 'searchArchitectValue';
        } else if (type === 'workman') {
          contactArray = this.workmanResult;
          searchTermValue = 'searchWorkmanValue';
        }

        if (contactArray.length > 0) {
          contactArray.splice(0, contactArray.length);
        }

        contactArray.push(contact);

        this.setContact(type, contact);

        const patchValue: { [property: string]: any } = {};
        patchValue[searchTermValue] = this.displayContactResult(contact);

        this.form.patchValue(patchValue);
        this._ignoreCustomerSearchEvent = false;
      }
    });
  }

  public checkIfBuildingAndFlatFormIsValid(): boolean {
    const fromValue: ICreateCustomerForm = this.form.value;
    const model = fromValue.buildingAndFlat;

    if (this.buildingIsConnected === true) {
      if (model.isActiveConnectionRequired === false) {
        return false;
      } else {
        if (!model.flatId) { return false; }
      }
    } else {
      if (!model.ownerContactId) { return false; }

      if (model.isActiveConnectionRequired === true) {
        if (!model.flatId) { return false; }
      }
    }

    return true;
  }

  public checkIfActiveConnenctionIsValid(): boolean {
    const fromValue: ICreateCustomerForm = this.form.value;

    if (fromValue.buildingAndFlat.isActiveConnectionRequired === true) {
      if (!fromValue.activeConnenction.customerContactId) { return false; }
      if (fromValue.activeConnenction.activationAsSoonAsPossible == false) {
        if (!fromValue.activeConnenction.connectionAppointment) { return false; }
      }

      if (fromValue.activeConnenction.subscriberEndpointNearConnectionPoint.value === false) {
        if (!fromValue.activeConnenction.subscriberEndpointLength) { return false; }
      }
    }

    return true;
  }

  public createCustomer(): void {
    if (this.createInProgress === true) { return; }

    const formValue: ICreateCustomerForm = this.form.value;

    if (this.checkIfBuildingAndFlatFormIsValid() === false) { return; }
    if (this.checkIfActiveConnenctionIsValid() === false) { return; }

    const createModel: ICustomerCreateModel = {
      onlyHouseConnection: !formValue.buildingAndFlat.isActiveConnectionRequired,
      buildingId: formValue.buildingAndFlat.buildingId,
      flatId: formValue.buildingAndFlat.flatId,
      customerPersonId: this._currentCustomerContact ? this._currentCustomerContact.id : null,
      propertyOwnerPersonId: this._currentOwnerContact ? this._currentOwnerContact.id : null,
      architectPersonId: this._currentArchitectContact ? this._currentArchitectContact.id : null,
      workmanPersonId: this._currentWorkmanContact ? this._currentWorkmanContact.id : null,

      descriptionForHouseConnection: formValue.houseConnection.description,
      civilWorkIsDoneByCustomer: formValue.houseConnection.civilWorkIsDoneByCustomer,
      connectioOfOtherMediaRequired: formValue.houseConnection.connectioOfOtherMediaRequired,
      informSalesAfterFinish: formValue.houseConnection.informSalesAfterFinish,

      subscriberEndpointNearConnectionPoint: formValue.activeConnenction.subscriberEndpointNearConnectionPoint,
      ductAmount: formValue.activeConnenction.ductAmount,
      subscriberEndpointLength: formValue.activeConnenction.subscriberEndpointLength,
      activePointNearSubscriberEndpoint: formValue.activeConnenction.activePointNearSubscriberEndpoint,
      powerForActiveEquipment: formValue.activeConnenction.powerForActiveEquipment,
      connectionAppointment: formValue.activeConnenction.connectionAppointment,
      activationAsSoonAsPossible: formValue.activeConnenction.activationAsSoonAsPossible,
    }

    if (this.buildingIsConnected === true) {
      createModel.buildingId = null;
    }

    this.createInProgress = true;
    this._snackBar.open("Der Kunden wird erstellt", 'In Bearbeitung', { duration: -1 });

    this._customerService.createCustomer(createModel).pipe(finalize(() => {
      this.createInProgress = false;
    })).subscribe((result) => {
      this._snackBar.open("Der Kunde wurde erstellt", 'Erfolg', { duration: 4500 });

      const emptyForm: ICreateCustomerForm = {
        buildingTermValue: '',
        searchCustomermValue: '',
        searchOwnerValue: '',
        searchArchitectValue: '',
        searchWorkmanValue: '',
        buildingAndFlat: {
          buildingId: null,
          flatId: null,
          isActiveConnectionRequired: false,
          ownerContactId: null,
          architectContactId: null,
          workmanContactId: null,
        },
        houseConnection: {
          description: null,
          civilWorkIsDoneByCustomer: false,
          connectioOfOtherMediaRequired: false,
          informSalesAfterFinish: false,
        },
        activeConnenction: {
          customerContactId: null,
          ductAmount: null,
          subscriberEndpointLength: null,
          connectionAppointment: null,
          activationAsSoonAsPossible: false,
          subscriberEndpointNearConnectionPoint: {
            value: true,
            description: null,
          },
          activePointNearSubscriberEndpoint: {
            value: false,
            description: null,
          },
          powerForActiveEquipment: {
            value: false,
            description: null,
          },
        }
      }

      this.stepper.reset();
      this.form.reset();
      this.form.setValue(emptyForm);
      this.prepare()

    }, (err) => {
      this._snackBar.open("Fehler beim Erstellen des Kunden", 'Fehler', { duration: 4500 });
    })



  }

  //#endregion

}
