import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InjectJobOverviewPageComponent } from './inject-job-overview-page.component';

describe('InjectJobOverviewPageComponent', () => {
  let component: InjectJobOverviewPageComponent;
  let fixture: ComponentFixture<InjectJobOverviewPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InjectJobOverviewPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InjectJobOverviewPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
