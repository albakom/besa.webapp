import { Component, OnInit } from '@angular/core';
import { OnDestroy } from '@angular/core/src/metadata/lifecycle_hooks';
import { fuseAnimations } from '@fuse/animations';
import { JobOverviewPageBase } from '../base/job-overview-page-base';
import { RefreshManagerService } from '../../services/refresh-manager.service';
import { MatSnackBar } from '@angular/material';
import { Router } from '@angular/router';
import { ConstructionStageService } from '../../services/construction-stage.service';
import { IConstructionStageOverviewForJobsModel } from '../../models/service/construction-stage/iconstruction-stage-overview-for-jobs-model';
import { Observable } from 'rxjs';

@Component({
  selector: 'besa-inject-job-overview-page',
  templateUrl: './inject-job-overview-page.component.html',
  styleUrls: ['./inject-job-overview-page.component.scss'],
  animations: fuseAnimations
})
export class InjectJobOverviewPageComponent extends JobOverviewPageBase implements OnInit, OnDestroy {

  constructor(refreshManger: RefreshManagerService,
    snackBar: MatSnackBar,
    router: Router,
    private _service: ConstructionStageService) {
    super(refreshManger, snackBar, router);
  }

  ngOnInit() {
    super.loadConstructionStages();
  }

  ngOnDestroy() {
    super.unsubcripeAll();
  }

  //#region Methods

  protected loadOverview(onlybounded: boolean): Observable<IConstructionStageOverviewForJobsModel[]> {
    return this._service.loadOverviewForInject(onlybounded);
  }
  
  protected getNavigationPathForDetails(): string {
    return '/member/inject-job-construction-stage-detail';
  }

  //#endregion

}
