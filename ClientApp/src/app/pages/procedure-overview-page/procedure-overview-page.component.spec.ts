import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProcedureOverviewPageComponent } from './procedure-overview-page.component';

describe('ProcedureOverviewPageComponent', () => {
  let component: ProcedureOverviewPageComponent;
  let fixture: ComponentFixture<ProcedureOverviewPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProcedureOverviewPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProcedureOverviewPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
