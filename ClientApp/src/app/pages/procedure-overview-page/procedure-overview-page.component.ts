import { Observable, of } from 'rxjs';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { SubscriptionAwareComponent } from '../../helper/subscription-aware-component';
import { RefreshManagerService } from '../../services/refresh-manager.service';
import { MatSnackBar } from '@angular/material';
import { Router } from '@angular/router';
import { finalize, debounceTime, distinctUntilChanged, switchMap, map } from 'rxjs/operators';
import { ProcedureService } from '../../services/procedure.service';
import { fuseAnimations } from '@fuse/animations';
import { IProcedureFilterModel } from '../../models/service/procedure/iprocedure-filter-model';
import { IProcedureOverviewModel } from '../../models/service/procedure/iprocedure-overview-model';
import { ProcedureTypes } from '../../models/service/procedure/procedure-types.enum';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { FormBasedPage } from '../base/form-based-page';
import { FormErrorMessage } from '../../models/helper/form-error-message';
import { IProcedureFilterOperations } from '../../models/service/procedure/iprocedure-filter-operations.enum';
import { ProcedureStates } from '../../models/service/procedure/procedure-states.enum';
import { IProcedureSortProperties } from '../../models/service/procedure/iprocedure-sort-properties.enum';
import { IProcedureSortingDirections } from '../../models/service/procedure/iprocedure-sorting-directions.enum';
import { IProcedureSortingModel } from '../../models/service/procedure/iprocedure-sorting-model';

@Component({
  selector: 'besa-procedure-overview-page',
  templateUrl: './procedure-overview-page.component.html',
  animations: fuseAnimations,
  styleUrls: ['./procedure-overview-page.component.scss']
})
export class ProcedureOverviewPageComponent extends FormBasedPage implements OnInit, OnDestroy {


  private _itemPerRequest = 50;
  private _canScroll: boolean = false;
  public loadingMoreDataInProgress: boolean;

  public loadingDataInProgress: boolean = false;
  public filter: IProcedureFilterModel;
  public sorting: IProcedureSortingModel;
  public procedures: IProcedureOverviewModel[];
  public autoUpdateFilter: boolean;

  public operators: IProcedureFilterOperations[];
  public procedureTypes: ProcedureTypes[];
  public states: ProcedureStates[];

  public sortingForm: FormGroup;
  public sortingDirections: IProcedureSortingDirections[];
  public sortingProperties: IProcedureSortProperties[];

  constructor(
    private refreshManager: RefreshManagerService,
    private _snackBar: MatSnackBar,
    private _router: Router,
    private _fb: FormBuilder,
    private _service: ProcedureService,
  ) {
    super();

    this.autoUpdateFilter = true;
    this.sortingDirections = [IProcedureSortingDirections.Ascending, IProcedureSortingDirections.Descending];
    this.sortingProperties = [IProcedureSortProperties.BuildingName, IProcedureSortProperties.Appointment, IProcedureSortProperties.State];

    this.filter = { start: 0, amount: this._itemPerRequest };
    this.sorting = { direction: IProcedureSortingDirections.Ascending, property: IProcedureSortProperties.BuildingName };

    super.addSubscription(refreshManager.refreshRequested.subscribe((result) => {
      this.loadDataWithSubscripe();
    }));

    this.operators = [IProcedureFilterOperations.Equal, IProcedureFilterOperations.Unqual, IProcedureFilterOperations.Smaller, IProcedureFilterOperations.Greater];
    this.procedureTypes = [ProcedureTypes.CustomerConnection, ProcedureTypes.HouseConnection];
    this.states = _service.getAllPossiblesStates();

    super.initForm(this._fb.group
      (
      {
        start: [0, [Validators.required]],
        amount: [this._itemPerRequest, [Validators.required]],
        openState: [null, []],
        query: [null, []],
        state: [null, []],
        stateOperator: [IProcedureFilterOperations.Greater, []],
        type: [null, []],
        buildingUnitAmount: [null, []],
        buildingUnitAmountOperation: [IProcedureFilterOperations.Equal, []],
        startDate: [null],
        endDate: [null],
        asSoonAsPossibleState: [null],
        informSalesAfterFinishedState: [null],
      }
      ));

    this.sortingForm = this._fb.group(
      {
        property: [IProcedureSortProperties.BuildingName, [Validators.required]],
        direction: [IProcedureSortingDirections.Ascending, [Validators.required]],
      });

    super.addSubscription(this.form.valueChanges.pipe(
      debounceTime(150),
      distinctUntilChanged(),
      switchMap((item: any) => {
        if (this.autoUpdateFilter == true) {
          this.filter = this.form.value;
          return this.loadData();
        }
        else {
          return of(new Array<IProcedureOverviewModel>());
        }
      })
    ).subscribe((result) => {
    }, (err) => {
      this._snackBar.open("Vorgänge wurden nicht gelanden", "Fehler", { duration: 4500 });
    }));


    super.addSubscription(this.sortingForm.valueChanges.pipe(
      debounceTime(150),
      distinctUntilChanged(),
      switchMap((item: any) => {
        if (this.autoUpdateFilter == true) {
          this.sorting = this.sortingForm.value;
          return this.loadData();
        }
        else {
          return of(new Array<IProcedureOverviewModel>());
        }
      })
    ).subscribe((result) => {
    }, (err) => {
      this._snackBar.open("Vorgänge wurden nicht gelanden", "Fehler", { duration: 4500 });
    }));
  }

  ngOnInit() {
    this.loadDataWithSubscripe();
  }

  ngOnDestroy() {
    super.unsubcripeAll();
  }

  protected getErrorMessages(): FormErrorMessage[] {
    return new Array();
  }

  private loadDataWithSubscripe() {
    this.loadData().subscribe((result) => {
    }, (err) => {
      this._snackBar.open("Vorgänge wurden nicht gelanden", "Fehler", { duration: 4500 });
    });
  }

  loadData(): Observable<IProcedureOverviewModel[]> {
    this.loadingDataInProgress = true;
    const obs = this._service.getProcedures(this.filter, this.sorting).pipe(
      map((result) => {
        this.procedures = result;
        this._canScroll = this._itemPerRequest == result.length;
        return result;
      }),
      finalize(() => {
        this.loadingDataInProgress = false;
      }));

    return obs;
  }

  onScroll() {
    if (this._canScroll === false) { return; }
    if (this.loadingMoreDataInProgress === true) { return; }

    this.filter.start = this.procedures.length;

    this.loadingMoreDataInProgress = true;
    this._service.getProcedures(this.filter, this.sorting).pipe(finalize(() => {
      this.loadingMoreDataInProgress = false;
    })).subscribe((result) => {
      this.procedures.push(...result);
      this._canScroll = this._itemPerRequest == result.length;
    }, (err) => {
      this._snackBar.open("Vorgänge wurden nicht gelanden", "Fehler", { duration: 4500 });
    });
  }

  openDetails(procedure: IProcedureOverviewModel) {
    const path = this._service.getPathToProcedure(procedure);
    this._router.navigate([path, procedure.id]);
  }

  public filterItems(): void {
    this.loadDataWithSubscripe();
  }

  public resetFilter(): void {
    this.form.setValue(
      {
        start: 0,
        amount: this._itemPerRequest,
        openState: null,
        query: null,
        state: null,
        stateOperator: IProcedureFilterOperations.Greater,
        type: null,
        buildingUnitAmount: null,
        buildingUnitAmountOperation: IProcedureFilterOperations.Equal,
        startDate: null,
        endDate: null,
        asSoonAsPossibleState: null,
        informSalesAfterFinishedState: null,
      }
    );
  }

  public sortItems(): void {
    this.loadDataWithSubscripe();
  }

  public resetSorting() {
    this.sortingForm.setValue(
      {
        property: IProcedureSortProperties.BuildingName,
        direction: IProcedureSortingDirections.Ascending,
      });
  }
}
