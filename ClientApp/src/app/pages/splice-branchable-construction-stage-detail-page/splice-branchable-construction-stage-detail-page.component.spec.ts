import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SpliceBranchableConstructionStageDetailPageComponent } from './splice-branchable-construction-stage-detail-page.component';

describe('SpliceBranchableConstructionStageDetailPageComponent', () => {
  let component: SpliceBranchableConstructionStageDetailPageComponent;
  let fixture: ComponentFixture<SpliceBranchableConstructionStageDetailPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SpliceBranchableConstructionStageDetailPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpliceBranchableConstructionStageDetailPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
