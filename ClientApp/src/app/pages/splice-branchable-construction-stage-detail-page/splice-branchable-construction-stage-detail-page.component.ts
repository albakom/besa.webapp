import { fuseAnimations } from '@fuse/animations';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { ConstrutionStageJobDetailBasePage } from '../base/constrution-stage-job-detail-base-page';
import { ISpliceBranchableJobOverviewModel } from '../../models/service/jobs/splice/isplice-branchable-job-overview-model';
import { RefreshManagerService } from '../../services/refresh-manager.service';
import { ActivatedRoute, Router } from '@angular/router';
import { MatSnackBar } from '@angular/material';
import { ConstructionStageService } from '../../services/construction-stage.service';
import { Observable } from 'rxjs';
import { IConstructionStageDetailModel } from '../../models/service/construction-stage/iconstruction-stage-detail-for-model';

@Component({
  selector: 'besa-splice-branchable-construction-stage-detail-page',
  templateUrl: './splice-branchable-construction-stage-detail-page.component.html',
  animations: fuseAnimations,
  styleUrls: ['./splice-branchable-construction-stage-detail-page.component.scss']
})
export class SpliceBranchableConstructionStageDetailPageComponent extends ConstrutionStageJobDetailBasePage<ISpliceBranchableJobOverviewModel> implements OnInit, OnDestroy {


  //#region Constructor

  constructor(
    refreshManager: RefreshManagerService,
    activeRoute: ActivatedRoute,
    router: Router,
    snackBar: MatSnackBar,
    private _service: ConstructionStageService,
  ) {
    super(refreshManager, activeRoute, router, snackBar);

    this.initDone = true;
  }

  ngOnInit() {
    super.loadDetailsInternal();
  }

  ngOnDestroy() {
    super.unsubcripeAll();
  }

  //#endregion

  //#region Methods

  protected loadDetailsFromService(id: number, onlyUnboundend: boolean): Observable<IConstructionStageDetailModel<ISpliceBranchableJobOverviewModel>> {
    return this._service.loadDetailsForSpliceJobs(id, onlyUnboundend);
  }

  protected getNavigationPathForDetails(job: ISpliceBranchableJobOverviewModel): string {
    return '/member/splice-branchable-job';
  }

  //#endregion

}

