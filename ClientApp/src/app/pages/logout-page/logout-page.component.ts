import { OAuthService } from 'angular-oauth2-oidc';
import { Component, OnInit } from '@angular/core';
import { LoginService } from '../../services/login.service';
import { NonMemberPageBase } from '../base/non-member-page-base';
import { Router } from '@angular/router';
import { FuseConfigService } from '@fuse/services/config.service';

@Component({
  selector: 'besa-logout-page',
  templateUrl: './logout-page.component.html',
  styleUrls: ['./logout-page.component.scss']
})
export class LogoutPageComponent extends NonMemberPageBase implements OnInit {

  //#region Properties

  public logoutInProgress: boolean;
  public logoutSuccessfully: boolean;

  //#endregion

  constructor(
    private _oAuthService: OAuthService,
    private _loginService: LoginService,
    fuseConfigService: FuseConfigService,
    private _router: Router,
  ) {
    super(fuseConfigService);
    super.setDefaultLayout();

    if (_loginService.logoutRequested === true) {
      this.logoutInProgress = true;
      this.logoutSuccessfully = false;
    } else {
      this.logoutInProgress = false;
      this.logoutSuccessfully = true;
    }
  }

  ngOnInit() {
    if (this.logoutInProgress === true) {
      this._oAuthService.logOut();
    }
  }

  public startLogin(): void {
    this._router.navigate(['/login']);
  }

}
