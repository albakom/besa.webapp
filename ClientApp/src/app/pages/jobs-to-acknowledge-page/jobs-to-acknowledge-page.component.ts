import { ISimpleJobOverview } from './../../models/service/jobs/isimple-job-overview';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { SubscriptionAwareComponent } from '../../helper/subscription-aware-component';
import { RefreshManagerService } from '../../services/refresh-manager.service';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material';
import { JobService } from '../../services/job.service';
import { finalize } from 'rxjs/operators';
import { fuseAnimations } from '@fuse/animations';

@Component({
  selector: 'besa-jobs-to-acknowledge-page',
  templateUrl: './jobs-to-acknowledge-page.component.html',
  styleUrls: ['./jobs-to-acknowledge-page.component.scss'],
  animations: fuseAnimations,
})
export class JobsToAcknowledgePageComponent extends SubscriptionAwareComponent implements OnInit, OnDestroy {

  //#region Properties

  public loadingInProgress: boolean;
  public jobs: ISimpleJobOverview[];

  //#endregion

  constructor(refreshManager: RefreshManagerService,
    private _router: Router,
    private _snackBar: MatSnackBar,
    private _service: JobService) {
    super();

    super.addSubscription(refreshManager.refreshRequested.subscribe((result) => {
      this.loadJobs();
    }))
  }

  ngOnInit() {
    this.loadJobs();
  }

  ngOnDestroy() {
    super.unsubcripeAll();
  }

  //#region  Methods

  private loadJobs(): void {
    if (this.loadingInProgress === true) { return; }

    this.loadingInProgress = true;

    this._service.loadJobsToAcknowledge().pipe(finalize(() => {
      this.loadingInProgress = false;
    })).subscribe((result) => {
      this.jobs = result;
    }, (err) => {
      this._snackBar.open('Die Aufträge konnte nicht geladen werden', 'Fehler', { duration: 4500 });
    });
  }

  public navigateToJob(job: ISimpleJobOverview): void {
    if (!job) { return; }

    const url = this._service.getUrlByJob(job);
    this._router.navigate([url, job.id]);
  }
}
