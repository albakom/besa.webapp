import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JobsToAcknowledgePageComponent } from './jobs-to-acknowledge-page.component';

describe('JobsToAcknowledgePageComponent', () => {
  let component: JobsToAcknowledgePageComponent;
  let fixture: ComponentFixture<JobsToAcknowledgePageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JobsToAcknowledgePageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JobsToAcknowledgePageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
