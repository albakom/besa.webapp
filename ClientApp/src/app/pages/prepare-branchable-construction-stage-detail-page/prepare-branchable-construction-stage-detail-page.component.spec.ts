import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrepareBranchableConstructionStageDetailPageComponent } from './prepare-branchable-construction-stage-detail-page.component';

describe('PrepareBranchableConstructionStageDetailPageComponent', () => {
  let component: PrepareBranchableConstructionStageDetailPageComponent;
  let fixture: ComponentFixture<PrepareBranchableConstructionStageDetailPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrepareBranchableConstructionStageDetailPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrepareBranchableConstructionStageDetailPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
