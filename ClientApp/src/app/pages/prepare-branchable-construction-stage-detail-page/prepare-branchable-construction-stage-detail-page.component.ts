import { fuseAnimations } from  '@fuse/animations';
import { IPrepareBranchableForSpliceJobOverviewModel } from './../../models/service/jobs/splice/iprepare-branchable-for-splice-job-overview-model';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { ConstrutionStageJobDetailBasePage } from '../base/constrution-stage-job-detail-base-page';
import { RefreshManagerService } from '../../services/refresh-manager.service';
import { ActivatedRoute, Router } from '@angular/router';
import { MatSnackBar } from '@angular/material';
import { ConstructionStageService } from '../../services/construction-stage.service';
import { IConstructionStageDetailModel } from '../../models/service/construction-stage/iconstruction-stage-detail-for-model';
import { Observable } from 'rxjs';

@Component({
  selector: 'besa-prepare-branchable-construction-stage-detail-page',
  templateUrl: './prepare-branchable-construction-stage-detail-page.component.html',
  animations: fuseAnimations,
  styleUrls: ['./prepare-branchable-construction-stage-detail-page.component.scss']
})
export class PrepareBranchableConstructionStageDetailPageComponent extends ConstrutionStageJobDetailBasePage<IPrepareBranchableForSpliceJobOverviewModel> implements OnInit, OnDestroy {

  //#region Constructor

  constructor(
    refreshManager: RefreshManagerService,
    activeRoute: ActivatedRoute,
    router: Router,
    snackBar: MatSnackBar,
    private _service: ConstructionStageService,
  ) {
    super(refreshManager, activeRoute, router, snackBar);

    this.initDone = true;
  }

  ngOnInit() {
    super.loadDetailsInternal();
  }

  ngOnDestroy() {
    super.unsubcripeAll();
  }

  //#endregion

  //#region Methods

  protected loadDetailsFromService(id: number, onlyUnboundend: boolean): Observable<IConstructionStageDetailModel<IPrepareBranchableForSpliceJobOverviewModel>> {
    return this._service.loadDetailsForPrepareBranchableJobs(id, onlyUnboundend);
  }

  protected getNavigationPathForDetails(job: IPrepareBranchableForSpliceJobOverviewModel): string {
    return '/member/prepare-branchable-job';
  }

  //#endregion

}

