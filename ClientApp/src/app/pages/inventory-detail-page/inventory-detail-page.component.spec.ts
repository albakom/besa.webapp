import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InventoryDetailPageComponent } from './inventory-detail-page.component';

describe('InventoryDetailPageComponent', () => {
  let component: InventoryDetailPageComponent;
  let fixture: ComponentFixture<InventoryDetailPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InventoryDetailPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InventoryDetailPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
