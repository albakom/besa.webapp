import { ActivatedRoute } from '@angular/router';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';
import { SubscriptionAwareComponent } from '../../helper/subscription-aware-component';
import { IInventoryJobOverviewModel } from '../../models/service/jobs/inventory/iinventory-job-overview-model';
import { RefreshManagerService } from '../../services/refresh-manager.service';
import { MatSnackBar, MatDialog } from '@angular/material';
import { JobService } from '../../services/job.service';
import { Router } from '@angular/router';
import { finalize } from 'rxjs/operators';
import { IFinishInventoryDialogOptions } from '../../dialogs/finish-inventory-job-dialog/ifinish-inventory-dialog-options';
import { FinishInventoryJobDialogComponent } from '../../dialogs/finish-inventory-job-dialog/finish-inventory-job-dialog.component';

@Component({
  selector: 'besa-inventory-detail-page',
  templateUrl: './inventory-detail-page.component.html',
  styleUrls: ['./inventory-detail-page.component.scss'],
  animations: fuseAnimations
})
export class InventoryDetailPageComponent extends SubscriptionAwareComponent implements OnInit, OnDestroy {
  public loadingDataInProgress: boolean;
  public job: IInventoryJobOverviewModel;
  private _id: number;

  constructor(
    private _refreshManager: RefreshManagerService,
    private _snackBar: MatSnackBar,
    private _router: Router,
    private _service: JobService,
    private _activeRoute: ActivatedRoute,
    private _dialog: MatDialog
  ) {
    super();

    super.addSubscription(_refreshManager.refreshRequested.subscribe((result) => {
      this.loadData();
    }))
   }

  ngOnInit() {
    this.loadData();
  }

  ngOnDestroy() {
    super.unsubcripeAll();
  }

  loadData() {
    this.loadingDataInProgress = true;
    this._id = +this._activeRoute.snapshot.paramMap.get('id');
    this._service.getInventoryJobDetails(this._id).pipe(finalize(() => {
      this.loadingDataInProgress = false;
    })).subscribe((result) => {
      if (result) {
        this.job = result;
      } else {
        this._snackBar.open('Die Daten konnten nicht geladen werden', 'Fehler', { duration: 4500 });
      }
    }, (err) => {
      this._snackBar.open('Die Daten konnten nicht geladen werden', 'Fehler', { duration: 4500 });
    });
  }
  
  navigateBack() {
    this._router.navigate(['/member/inventory-overview']);
  }

  finishJob(id: number) {
    const dialogOptions: IFinishInventoryDialogOptions = {
      jobId: id,
      comment: ''
    };

    const dialogRef = this._dialog.open(FinishInventoryJobDialogComponent, {
      data: dialogOptions
    });

    dialogRef.afterClosed().subscribe((result: boolean) => {
      if (result === true) {
        this._snackBar.open('Der Warenauftrag wurde erfolgreich abgeschlossen', 'Erfolg', { duration: 4500 });
        this.loadData();
      } else {
        this._snackBar.open('Der Warenauftrag konnte nicht abgeschlossen werden', 'Fehler', { duration: 4500 });
      }
    }, (err) => {
      this._snackBar.open('Der Warenauftrag konnte nicht abgeschlossen werden', 'Fehler', { duration: 4500 });
    })
  }
}
