import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActivationJobDetailsPageComponent } from './activation-job-details-page.component';

describe('ActivationJobDetailsPageComponent', () => {
  let component: ActivationJobDetailsPageComponent;
  let fixture: ComponentFixture<ActivationJobDetailsPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActivationJobDetailsPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActivationJobDetailsPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
