import { IFinishActivationJobDetailModel } from './../../models/service/jobs/activation/ifinish-activation-job-detail-model';
import { fuseAnimations } from '@fuse/animations';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { RefreshManagerService } from '../../services/refresh-manager.service';
import { ActivatedRoute, Router } from '@angular/router';
import { MatSnackBar, MatDialog } from '@angular/material';
import { LoginService } from '../../services/login.service';
import { UriService } from '../../services/uri.service';
import { JobService } from '../../services/job.service';
import { Observable } from 'rxjs';
import { IActivationJobDetailModel } from '../../models/service/jobs/activation/iactivation-job-detail-model';
import { JobDetailsPageBase } from '../base/job-details-page-base';

@Component({
  selector: 'besa-activation-job-details-page',
  templateUrl: './activation-job-details-page.component.html',
  animations: fuseAnimations,
  styleUrls: ['./activation-job-details-page.component.scss']
})
export class ActivationJobDetailsPageComponent extends
  JobDetailsPageBase<IFinishActivationJobDetailModel, IActivationJobDetailModel>
  implements OnInit, OnDestroy {

  constructor(refreshManager: RefreshManagerService,
    activeRoute: ActivatedRoute,
    snackBar: MatSnackBar,
    dialog: MatDialog,
    loginService: LoginService,
    router: Router,
    uriService: UriService,
    service: JobService,
  ) {
    super(refreshManager, activeRoute, snackBar, dialog, loginService, router, uriService, service);
  }

  ngOnInit() {
    super.loadDetailsInternal();
  }

  ngOnDestroy() {
    super.unsubcripeAll();
  }

  //#region Methods

  protected getDetails(id: number): Observable<IActivationJobDetailModel> {
    return this._service.loadActivationJobDetails(id);
  }

  protected getFinishUrl(): string {
    return '/member/finish-activation-job';
  }

  //#endregion
}

