import { CompanyAddFormErrorMessage } from './../../models/error-messages/company-add-form-error-message';
import { fuseAnimations } from '@fuse/animations';
import { Component, OnInit } from '@angular/core';
import { FormBasedPage } from '../base/form-based-page';
import { Router } from '@angular/router';
import { CompanyService } from '../../services/companies/company.service';
import { MatSnackBar } from '@angular/material';
import { FormBuilder, Validators } from '@angular/forms';
import { FormErrorMessage } from '../../models/helper/form-error-message';
import { ICompanyCreateModel } from '../../models/service/companies/icompany-create-model';
import { IFormCompanyCreateModel } from '../../models/forms/iform-company-create-model';

@Component({
  selector: 'besa-company-create-page',
  templateUrl: './company-create-page.component.html',
  styleUrls: ['./company-create-page.component.scss'],
  animations: fuseAnimations
})
export class CompanyCreatePageComponent extends FormBasedPage implements OnInit {


  constructor(
    private _router: Router,
    private _service: CompanyService,
    private _snackBar: MatSnackBar,
    private _fb: FormBuilder) {
      super();
     }

  ngOnInit() {
    this.initializeFormValues();
  }

  initializeFormValues(): void {
    super.initForm(
      this._fb.group({
        name: ['', [Validators.required, Validators.maxLength(255)]],
        phone: ['', [Validators.required, Validators.maxLength(32)]],
        street: ['', [Validators.required, Validators.maxLength(255)]],
        streetNumber: ['', [Validators.required, Validators.maxLength(8)]],
        city: ['', [Validators.required, Validators.maxLength(64)]],
        postalCode: ['', [Validators.required, Validators.maxLength(5), Validators.minLength(4)]]
      })
    );
  }

  protected getErrorMessages(): FormErrorMessage[] {
    return CompanyAddFormErrorMessage;
  }

  public cancelAdd(): void {
    this._router.navigate(['/member/company-overview']);
  }

  public addCompany(): void {
    const formValue: IFormCompanyCreateModel = this.form.value;

    const addedCompany: ICompanyCreateModel = {
      name: formValue.name,
      phone: formValue.phone,
      address:
        {
          city: formValue.city,
          street: formValue.street,
          streetNumber: formValue.streetNumber,
          postalCode: formValue.postalCode
        }
    };

    this._service.addCompany(addedCompany).pipe().subscribe(
      (result) => {
        if (result && result.id > 0) {
          this._snackBar.open('Die Firma wurde erfolgreich hinzugefügt', 'Erfolg', { duration: 4500 });
          this._router.navigate(['/member/company-overview']);
        }
        else {
          this._snackBar.open('Die Firma konnte nicht hinzugefügt werden', 'Fehler', { duration: 4500 });
        }
      },
      (err) => {
        this._snackBar.open('Die Firma konnte nicht hinzugefügt werden', 'Fehler', { duration: 4500 });
      }
    );
  }

}
