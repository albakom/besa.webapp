import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompanyCreatePageComponent } from './company-create-page.component';

describe('CompanyCreatePageComponent', () => {
  let component: CompanyCreatePageComponent;
  let fixture: ComponentFixture<CompanyCreatePageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompanyCreatePageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompanyCreatePageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
