import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RequestAcessPageComponent } from './request-acess-page.component';

describe('RequestAcessPageComponent', () => {
  let component: RequestAcessPageComponent;
  let fixture: ComponentFixture<RequestAcessPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RequestAcessPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequestAcessPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
