import { LoginService } from './../../services/login.service';
import { MemberStatus } from './../../models/service/member-status.enum';
import { fuseAnimations } from  '@fuse/animations';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { NonMemberPageBase } from '../base/non-member-page-base';



import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { FormErrorMessage } from '../../models/helper/form-error-message';
import { RegisterFormMessage } from '../../models/error-messages/register-form-message';
import { RequestAccessService } from '../../services/request-access.service';
import { ICompanieOverviewModel } from '../../models/service/icompanie-overview-model';

import { finalize } from 'rxjs/operators';
import { MatSnackBar } from '@angular/material';
import { OAuthService } from 'angular-oauth2-oidc';
import { Router } from '@angular/router';
import { IUserAccessRequestCreateModel } from '../../models/service/user/iuser-access-request-create-model';
import { FuseConfigService } from '@fuse/services/config.service';

@Component({
  selector: 'besa-request-acess-page',
  templateUrl: './request-acess-page.component.html',
  styleUrls: ['./request-acess-page.component.scss'],
  animations: fuseAnimations,
})
export class RequestAcessPageComponent extends NonMemberPageBase implements OnInit, OnDestroy {

  //#region Properties

  public formValidationErrors: { [key: string]: string } = {};
  public registerForm: FormGroup;

  public preperationInProgress: boolean;
  public loadingCompaniesInProgress: boolean;
  public companies: ICompanieOverviewModel[];
  public isRegistrationInProgress: boolean;

  //#region Constructor

  constructor(
    private _service: RequestAccessService,
    fuseConfigService: FuseConfigService,
    private _snackBar: MatSnackBar,
    private _loginService: LoginService,
    private _oauthService: OAuthService,
    private _router: Router,
    private _fb: FormBuilder,
  ) {
    super(fuseConfigService);
    super.setDefaultLayout();

    this.registerForm = this._fb.group({
      surname: ['', [Validators.required, Validators.maxLength(64)]],
      lastname: ['', [Validators.required, Validators.maxLength(64)]],
      emailAddress: ['', [Validators.required, Validators.maxLength(255), Validators.email]],
      phone: ['', [Validators.required, Validators.maxLength(32)]],
      companyId: [null, []]
    });

    super.addSubscription(this._loginService.currentUser.subscribe((result) => {
      if (result && result.email) {
        this.registerForm.patchValue({ emailAddress: result.email });
      }
    }));

    super.addSubscription(this.registerForm.statusChanges.subscribe(() => {
      this.updateErrorMessags();
    }));

  }

  ngOnInit() {
    this._snackBar.dismiss();
    this.preperationInProgress = true;

    if (this._oauthService.hasValidAccessToken() === false) {
      this._router.navigate(['/login']);
    } else {

      this._service.loadMemberStatus().pipe(finalize(() => this.preperationInProgress = false)).subscribe((memberResult) => {
        if (memberResult === MemberStatus.IsMember) {
          this._router.navigate(['/member/dashboard']);
        } else if (memberResult === MemberStatus.RequestedOutstanding) {
          this._router.navigate(['/requstAccessOutStanding']);
        } else {
          this.loadingCompaniesInProgress = true;
          this._service.loadCompanies().pipe(finalize(() => {
            this.loadingCompaniesInProgress = false;
          })).subscribe((result) => {
            this.companies = result;
          }, (err) => {
            this._snackBar.open('Beim Laden der Firmen ist ein Fehler aufgetreten ', 'Fehler', { duration: 4500 });
          });
        }
      }, (err) => {
        this._snackBar.open('Beim Überprüfen der Mitgliedschaft ist ein Fehler aufgetreten ', 'Fehler', { duration: 4500 });
      });
    }
  }

  ngOnDestroy() {
    super.unsubcripeAll();
  }

  //#endregion

  //#region Methods

  public updateErrorMessags(): void {
    const messages: FormErrorMessage[] = RegisterFormMessage;

    this.formValidationErrors = {};
    let errorFound = false;
    for (const message of messages) {
      const control = this.registerForm.get(message.forControl);
      if (control && control.dirty && control.invalid && control.errors[message.forValidator] &&
        !this.formValidationErrors[message.forControl]) {
        this.formValidationErrors[message.forControl] = message.errorMessage;
        errorFound = true;
      }
    }
  }

  public startRegistration(): void {
    if (this.registerForm.invalid === true) { return; }
    if (this.isRegistrationInProgress == true) { return; }

    this.isRegistrationInProgress = true;

    const model: IUserAccessRequestCreateModel = this.registerForm.value;

    const snackbarRef = this._snackBar.open('Die Anfrage wird gesendet', 'In Bearbeitung', { duration: -1 });

    this._service.startRegistration(model).pipe(finalize(() => {
      this.isRegistrationInProgress = false;
    })
    ).subscribe((result) => {
      snackbarRef.dismiss();
      this._router.navigate(['/requstAccessOutStanding']);
    }, (err) => {
      this._snackBar.open('Fehler beim versendern Anfrage', 'In Bearbeitung', { duration: 4500 });

    });
  }

  //#endregion

}
