import { fuseAnimations } from  '@fuse/animations';
import { IFinishInjectJobDetails } from './../../models/service/jobs/inject/ifinish-inject-job-detail-model';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { JobDetailsPageBase } from '../base/job-details-page-base';
import { IInjectJobDetailModel } from '../../models/service/jobs/inject/iinject-job-detail-model';
import { RefreshManagerService } from '../../services/refresh-manager.service';
import { ActivatedRoute, Router } from '@angular/router';
import { MatSnackBar, MatDialog } from '@angular/material';
import { LoginService } from '../../services/login.service';
import { UriService } from '../../services/uri.service';
import { JobService } from '../../services/job.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'besa-inject-job-details-page',
  templateUrl: './inject-job-details-page.component.html',
  styleUrls: ['./inject-job-details-page.component.scss'],
  animations: fuseAnimations,
})
export class InjectJobDetailsPageComponent extends
  JobDetailsPageBase<IFinishInjectJobDetails, IInjectJobDetailModel>
  implements OnInit, OnDestroy {

  constructor(refreshManager: RefreshManagerService,
    activeRoute: ActivatedRoute,
    snackBar: MatSnackBar,
    dialog: MatDialog,
    loginService: LoginService,
    router: Router,
    uriService: UriService,
    service: JobService,
  ) {
    super(refreshManager, activeRoute, snackBar, dialog, loginService, router, uriService, service);
  }

  ngOnInit() {
    super.loadDetailsInternal();
  }

  ngOnDestroy() {
    super.unsubcripeAll();
  }

  //#region Methods

  protected getDetails(id: number): Observable<IInjectJobDetailModel> {
    return this._service.loadInjectJobDetails(id);
  }

  protected getFinishUrl(): string {
    return '/member/finish-inejct-job';
  }

  //#endregion



}