import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InjectJobDetailsPageComponent } from './inject-job-details-page.component';

describe('InjectJobDetailsPageComponent', () => {
  let component: InjectJobDetailsPageComponent;
  let fixture: ComponentFixture<InjectJobDetailsPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InjectJobDetailsPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InjectJobDetailsPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
