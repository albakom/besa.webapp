import { CustomerInterestedDialogComponent } from './../../dialogs/customer-interested-dialog/customer-interested-dialog.component';
import { MatDialog } from '@angular/material';
import { MatSnackBar } from '@angular/material';
import { finalize } from 'rxjs/operators';
import {Router, ActivatedRoute} from '@angular/router';
import { JobService } from './../../services/job.service';
import { IContactAfterSalesJobDetails } from './../../models/service/contacts-after-sale-job/icontact-after-sales-job-details';
import { Component, OnInit } from '@angular/core';
import { fuseAnimations } from '../../../@fuse/animations/index';
import { SubscriptionAwareComponent } from '../../helper/subscription-aware-component';
import { duration } from 'moment';
import { ICustomerNotInterestedDialogOptions } from '../../dialogs/customer-not-interested-dialog/icustomer-not-interested-dialog-options';
import { CustomerNotInterestedDialogComponent } from '../../dialogs/customer-not-interested-dialog/customer-not-interested-dialog.component';
import { ICustomerInterestedDialogOptions } from '../../dialogs/customer-interested-dialog/icustomer-interested-dialog-options';

@Component({
  selector: 'besa-contact-after-sales-job-detail-page',
  templateUrl: './contact-after-sales-job-detail-page.component.html',
  styleUrls: ['./contact-after-sales-job-detail-page.component.scss'],
  animations: fuseAnimations
})
export class ContactAfterSalesJobDetailPageComponent extends SubscriptionAwareComponent implements OnInit {

  private _id: number;
  public loadingDataInProgress: boolean;
  public contact: IContactAfterSalesJobDetails;

  constructor(
    private _service: JobService,
    private _router: Router,
    private _activeRoute: ActivatedRoute,
    private _snackBar: MatSnackBar,
    private _dialog: MatDialog
  ) {
    super();
   }

  ngOnInit() {
    this.loadData();
  }

  public loadData() {
    this._id = +this._activeRoute.snapshot.paramMap.get('id');
    this.loadingDataInProgress = true;

    this._service.getContactAfterSaleJobDetails(this._id).pipe(finalize(() => {
      this.loadingDataInProgress = false;
    })).subscribe((result) => {
      if (result) {
        this.contact = result;
      }
    }, (err) => {
      this._snackBar.open('Die ausstehende Anfrage konnte nicht bearbeitet werden', 'Fehler', { duration: 4500 });
    });
  }

  navigateBack() {
    this._router.navigate(['/member/contact-after-sales-job-overview']);
  }

  contactNotInterested() {
    const dialogOptions: ICustomerNotInterestedDialogOptions = {
      jobId: this.contact.id
    };

    const dialogRef = this._dialog.open(CustomerNotInterestedDialogComponent, {
      data: dialogOptions
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result === true) {
        this._router.navigate(['/member/contact-after-sales-job-overview']);
      }
    });
  }

  contactInterested() {
    const dialogOptions: ICustomerInterestedDialogOptions = {
      jobId: this.contact.id
    };

    const dialogRef = this._dialog.open(CustomerInterestedDialogComponent, {
      data: dialogOptions
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result > 0) {
        this._router.navigate(['/member/contact-after-sales-job-overview']);
      }
    });
  }
}
