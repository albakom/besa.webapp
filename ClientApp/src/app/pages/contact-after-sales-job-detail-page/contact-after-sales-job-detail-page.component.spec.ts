import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContactAfterSalesJobDetailPageComponent } from './contact-after-sales-job-detail-page.component';

describe('ContactAfterSalesJobDetailPageComponent', () => {
  let component: ContactAfterSalesJobDetailPageComponent;
  let fixture: ComponentFixture<ContactAfterSalesJobDetailPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContactAfterSalesJobDetailPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContactAfterSalesJobDetailPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
