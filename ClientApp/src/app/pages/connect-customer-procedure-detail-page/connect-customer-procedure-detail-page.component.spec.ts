import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConnectCustomerProcedureDetailPageComponent } from './connect-customer-procedure-detail-page.component';

describe('ConnectCustomerProcedureDetailPageComponent', () => {
  let component: ConnectCustomerProcedureDetailPageComponent;
  let fixture: ComponentFixture<ConnectCustomerProcedureDetailPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConnectCustomerProcedureDetailPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConnectCustomerProcedureDetailPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
