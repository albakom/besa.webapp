import { Pipe, PipeTransform } from '@angular/core';
import { IProcedureSortingDirections } from '../models/service/procedure/iprocedure-sorting-directions.enum';

@Pipe({
  name: 'procedureSortingDirectionsToName'
})
export class ProcedureSortingDirectionsToNamePipe implements PipeTransform {

  transform(value: any, args?: any): any {
    const realValue: IProcedureSortingDirections = value as IProcedureSortingDirections;
    let result = '';
    switch (realValue) {
      case IProcedureSortingDirections.Ascending:
        result = 'Aufsteigend';
        break;
      case IProcedureSortingDirections.Descending:
        result = 'Absteigend';
        break;
    }

    return result;
  }

}
