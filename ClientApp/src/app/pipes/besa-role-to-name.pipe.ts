import { BesaRoles } from './../models/service/besa-roles.enum';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'besaRoleToName'
})
export class BesaRoleToNamePipe implements PipeTransform {

  private _nameLookup: { [role: number]: string }

  constructor() {
    this._nameLookup = {};
    this._nameLookup[BesaRoles.Admin] = "Administrator";
    this._nameLookup[BesaRoles.AirInjector] = "Einbläser";
    this._nameLookup[BesaRoles.CivilWorker] = "Tiefbauer";
    this._nameLookup[BesaRoles.ProjectManager] = "Projektmanager";
    this._nameLookup[BesaRoles.Sales] = "Vertrieb";
    this._nameLookup[BesaRoles.Inventory] = "Lager";
    this._nameLookup[BesaRoles.Splice] = "Spleißer";
    this._nameLookup[BesaRoles.ActiveNetwork] = "Aktives Netz";
    
  }

  transform(value: any, args?: any): any {
    const realValue = +value;

    if(isNaN(realValue) === true) {
      return '';
    } else {
      const result = this._nameLookup[realValue];
      if(result) {
        return result;
      } else {
        return '';
      }
    }
  }

}
