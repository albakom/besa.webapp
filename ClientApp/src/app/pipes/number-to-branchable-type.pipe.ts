import { Pipe, PipeTransform } from '@angular/core';
import { BranchableTypes } from '../models/service/construction-stage/branchable-types.enum';

@Pipe({
  name: 'numberToBranchableType'
})
export class NumberToBranchableTypePipe implements PipeTransform {

  transform(value: any, args?: any): any {
    const realValue = <BranchableTypes>value;

    let result = '';
    if (realValue == BranchableTypes.Cabinet) {
      result = 'KVZ';
    } else if (realValue == BranchableTypes.Sleeve) {
      result = 'Muffe';
    }

    return result;
  }

}
