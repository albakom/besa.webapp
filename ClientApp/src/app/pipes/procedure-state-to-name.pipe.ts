import { ProcedureStates } from './../models/service/procedure/procedure-states.enum';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'procedureStateToName'
})
export class ProcedureStateToNamePipe implements PipeTransform {

  transform(value: any, args?: any): any {
    const realValue = <ProcedureStates>value;
    let result = '';
    switch (realValue) {
      case ProcedureStates.Imported:
        result = 'Importiert';
        break;
      case ProcedureStates.RequestCreated:
        result = 'Anfrage erstellt';
        break;
      case ProcedureStates.RequestDeleted:
        result = 'Anfrage gelöscht';
        break;
      case ProcedureStates.RequestAcknkowledged:
        result = 'Anfrage angenommen';
        break;
      case ProcedureStates.BranchOffJobCreated:
        result = 'Vorstreckaufgabe erstellt';
        break;
      case ProcedureStates.BranchOffJobFinished:
        result = 'Vorstreckaufgabe erledigt';
        break;
      case ProcedureStates.BranchOffJobAcknowledegd:
        result = 'Vorstreckaufgabe bestätigt';
        break;
      case ProcedureStates.ConnectBuildingJobCreated:
        result = 'HA Aufgabe erstellt';
        break;
      case ProcedureStates.ConnectBuildingJobBound:
        result = 'HA Aufgabe gebunden';
        break;
      case ProcedureStates.ConnectBuildingJobUnboud:
        result = 'HA Aufgabe ungebunden';
        break;
      case ProcedureStates.ConnectBuildingJobFinished:
        result = 'HA Aufgabe erledigt';
        break;
      case ProcedureStates.ConnectBuildingJobDiscarded:
        result = 'HA Aufgabe abgelehnt';
        break;
      case ProcedureStates.ConnectBuildingJobAcknlowedged:
        result = 'HA Aufgabe angenommen';
        break;
      case ProcedureStates.InjectJobCreated:
        result = 'Einblasaufgabe erstellt';
        break;
      case ProcedureStates.InjectJobBound:
        result = 'Einblasaufgabe gebunden';
        break;
      case ProcedureStates.InjectJobUnboud:
        result = 'Einblasaufgabe ungebunden';
        break;
      case ProcedureStates.InjectJobFinished:
        result = 'Einblasaufgabe erledigt';
        break;
      case ProcedureStates.InjectJobDiscarded:
        result = 'Einblasaufgabe abgelehnt';
        break;
      case ProcedureStates.InjectJobAcknlowedged:
        result = 'Einblasaufgabe angenommen';
        break;
      case ProcedureStates.PrepareBranchableForSpliceJobCreated:
        result = 'KVZ Vorbereiten erstellt';
        break;
      case ProcedureStates.PrepareBranchableForSpliceJobBound:
        result = 'KVZ Vorbereiten gebunden';
        break;
      case ProcedureStates.PrepareBranchableForSpliceJobUnboud:
        result = 'KVZ Vorbereiten ungebunden';
        break;
      case ProcedureStates.PrepareBranchableForSpliceJobFinished:
        result = 'KVZ Vorbereiten erledigt';
        break;
      case ProcedureStates.PrepareBranchableForSpliceJobDiscarded:
        result = 'KVZ Vorbereiten abgelehnt';
        break;
      case ProcedureStates.PrepareBranchableForSpliceJobAcknlowedged:
        result = 'KVZ Vorbereiten angenommen';
        break;
      case ProcedureStates.SpliceODFJobCreated:
        result = 'ODF Vorbereiten erstellt';
        break;
      case ProcedureStates.SpliceODFJobBound:
        result = 'ODF Vorbereiten gebunden';
        break;
      case ProcedureStates.SpliceODFJobUnboud:
        result = 'ODF Vorbereiten ungebunden';
        break;
      case ProcedureStates.SpliceODFJobFinished:
        result = 'ODF Vorbereiten erledigt';
        break;
      case ProcedureStates.SpliceODFJobDiscarded:
        result = 'ODF Vorbereiten abgelehnt';
        break;
      case ProcedureStates.SpliceODFJobAcknlowedged:
        result = 'ODF Vorbereiten angenommen';
        break;
      case ProcedureStates.SpliceBranchableJobCreated:
        result = 'Spleißaufgabe erstellt';
        break;
      case ProcedureStates.SpliceBranchableJobBound:
        result = 'Spleißaufgabe gebunden';
        break;
      case ProcedureStates.SpliceBranchableJobUnboud:
        result = 'Spleißaufgabe ungebunden';
        break;
      case ProcedureStates.SpliceBranchableJobFinished:
        result = 'Spleißaufgabe erledigt';
        break;
      case ProcedureStates.SpliceBranchableJobDiscarded:
        result = 'Spleißaufgabe abgelehnt';
        break;
      case ProcedureStates.SpliceBranchableJobAcknlowedged:
        result = 'Spleißaufgabe angenommen';
        break;
      case ProcedureStates.ActivatitionJobCreated:
        result = 'Aktivierungsaufgabe erstellt';
        break;
      case ProcedureStates.ActivatitionJobBound:
        result = 'Aktivierungsaufgabe gebunden';
        break;
      case ProcedureStates.ActivatitionJobUnboud:
        result = 'Aktivierungsaufgabe ungebunden';
        break;
      case ProcedureStates.ActivatitionJobFinished:
        result = 'Aktivierungsaufgabe erledigt';
        break;
      case ProcedureStates.ActivatitionJobDiscarded:
        result = 'Aktivierungsaufgabe abgelehnt';
        break;
      case ProcedureStates.ActivatitionJobAcknlowedged:
        result = 'Aktivierungsaufgabe angenommen';
        break;
      default:
        break;
    }
    return result;
  }

}
