import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'amountToArray'
})
export class AmountToArrayPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    const realValue = +value;
    if (isNaN(realValue) === true) { return [0] }
    else {
      const output = new Array();
      for (let index = 0; index < realValue; index++) {
        output.push(index);
      }

      return output;
    }
  }
}
