import { Pipe, PipeTransform } from '@angular/core';
import { UpdateTaskElementStates } from '../models/service/tasks/update-task-element-states.enum';

@Pipe({
  name: 'updateTaskElementStateToString'
})
export class UpdateTaskElementStateToStringPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    const realValue = <UpdateTaskElementStates>value;
    let result = '';
    switch (realValue) {
      case UpdateTaskElementStates.Error:
        result = 'Error';
        break;
      case UpdateTaskElementStates.InProgress:
        result = 'Wird bearbeitet';
        break;
      case UpdateTaskElementStates.NotStarted:
        result = 'in Warteschlange';
        break;
      case UpdateTaskElementStates.Success:
        result = 'Erfolgreich abgeschlossen';
        break;
      case UpdateTaskElementStates.Waring:
        result = 'Warnung';
        break;
      default:
        break;
    }
    return result;
  }

}
