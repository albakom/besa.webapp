import { Pipe, PipeTransform } from '@angular/core';
import { ActivationJobIpAddressRequirements } from '../models/service/jobs/activation/activation-job-ip-address-requirements.enum';

@Pipe({
  name: 'activationJobIpAddressRequirementsToName'
})
export class ActivationJobIpAddressRequirementsToNamePipe implements PipeTransform {

  transform(value: any, args?: any): any {
    const realValue: ActivationJobIpAddressRequirements = <ActivationJobIpAddressRequirements>value;
    let result = '';
    switch (realValue) {
      case ActivationJobIpAddressRequirements.None:
        result = 'Kein Netz';
        break;
      case ActivationJobIpAddressRequirements.Slash27:
        result = '/27';
        break;
      case ActivationJobIpAddressRequirements.Slash28:
        result = '/28';
        break;
      case ActivationJobIpAddressRequirements.Slash29:
        result = '/29';
        break;
    }
    
    return result;
  }

}
