import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'fileSizeToUnit'
})
export class FileSizeToUnitPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    let size: number = value;
    let name: string = "";
    if (size < 1024) {
      name = size.toString() + " B";
    }
    else if ( size < (1024 * 1024)) {
      name = Math.round(size / 1024).toString() + " kB";
    }
    else if (size < (1024 * 1024 * 1024)) {
      name = Math.round(size / (1024 * 1024)).toString() + " MB";
    }
    else if (size < (1024 * 1024 * 1024 * 1024)) {
      name = Math.round(size / (1024 * 1024 * 1024)).toString() + " GB";
    }
    else {
      name = Math.round(size / (1024 * 1024 * 1024 * 1024)).toString() + " TB";
    }
    return name;
  }

}
