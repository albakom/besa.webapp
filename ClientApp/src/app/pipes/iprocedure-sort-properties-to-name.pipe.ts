import { Pipe, PipeTransform } from '@angular/core';
import { IProcedureSortProperties } from '../models/service/procedure/iprocedure-sort-properties.enum';

@Pipe({
  name: 'procedureSortPropertiesToName'
})
export class IprocedureSortPropertiesToNamePipe implements PipeTransform {

  transform(value: any, args?: any): any {
    const realValue: IProcedureSortProperties = value as IProcedureSortProperties;
    let result = '';
    switch (realValue) {
      case IProcedureSortProperties.BuildingName:
        result = 'Straße';
        break;
      case IProcedureSortProperties.Appointment:
        result = 'Datum';
        break;
      case IProcedureSortProperties.State:
        result = 'Status';
        break;
    }

    return result;
  }

}
