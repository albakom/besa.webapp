import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'fileTypeIdtoString'
})
export class FileTypeIdtoStringPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    let val: number = value;
    if (val === 1)
      return "Gebäude";
    else if (val === 2)
      return "BranchOffJob";
    else if (val === 3)
      return "HouseConnectionJob";
    else return "";
  }

}
