import { Pipe, PipeTransform } from '@angular/core';
import { IJobCollectionSortProperties } from '../models/service/jobs/ijob-collection-sort-properties';

@Pipe({
  name: 'jobCollectionSortPropertiesToName'
})
export class JobCollectionSortPropertiesToNamePipe implements PipeTransform {

  transform(value: any, args?: any): any {
    const realValue: IJobCollectionSortProperties = <IJobCollectionSortProperties>value;

    let result = '';
    switch (realValue) {
      case IJobCollectionSortProperties.CompanyName:
        result = 'Firmennamen';
        break;
      case IJobCollectionSortProperties.DoingPercentage:
        result = 'Fertigstellung';
        break;
      case IJobCollectionSortProperties.FinishedTill:
        result = 'Ausführung bis';
        break;
      case IJobCollectionSortProperties.Name:
        result = 'Name';
        break;
    }

    return result;
  }

}
