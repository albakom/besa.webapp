import { Pipe, PipeTransform } from '@angular/core';
import { ContactImportProperties } from '../models/helper/contact-import-properties.enum';

@Pipe({
  name: 'personPropertyToName'
})
export class PersonPropertyToNamePipe implements PipeTransform {

  transform(value: any, args?: any): any {
    const realValue = <ContactImportProperties>value;
    let result = '';
    switch (realValue) {
      case ContactImportProperties.Surname:
        result = 'Vorname';
        break;
      case ContactImportProperties.Lastname:
        result = 'Nachname';
        break;
      case ContactImportProperties.CompanyName:
        result = 'Firma';
        break;
      case ContactImportProperties.Street:
        result = 'Straße';
        break;
      case ContactImportProperties.StreetNumer:
        result = 'Nr.';
        break;
      case ContactImportProperties.Email:
        result = 'EMail';
        break;
      case ContactImportProperties.Phone:
        result = 'Telefon';
        break;
      case ContactImportProperties.Type:
        result = 'Anrede';
        break;
      case ContactImportProperties.City:
        result = 'Ort';
        break;
      case ContactImportProperties.ZipCode:
        result = 'PLZ';
        break;
    }

    return result;
  }

}
