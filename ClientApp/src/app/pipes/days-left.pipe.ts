import { Pipe, PipeTransform } from '@angular/core';

import * as moment from 'moment';

@Pipe({
  name: 'daysLeft'
})
export class DaysLeftPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    const now = moment();
    const date = moment(value);

    const diff = moment.duration(date.diff(now)).asDays();
    const roundedDiff = Math.round(diff);

    let suffix = ' Tage';
    if (roundedDiff == 1) {
      suffix = ' Tag'
    }

    return roundedDiff.toString() + suffix;
  }

}
