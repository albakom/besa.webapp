import { Pipe, PipeTransform } from '@angular/core';
import { IProcedureOverviewModel } from '../models/service/procedure/iprocedure-overview-model';
import { ProcedureStates } from '../models/service/procedure/procedure-states.enum';

@Pipe({
  name: 'procedureStateToColor'
})
export class ProcedureStateToColorPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    const realValue = <IProcedureOverviewModel>value;
    if (realValue.isOpen == false) {
      return '#72bb53';
    }
    let result = '';
    switch (realValue.current) {
      case ProcedureStates.Imported:
        result = 'black';
        break;
      case ProcedureStates.RequestCreated:
      case ProcedureStates.RequestDeleted:
      case ProcedureStates.RequestAcknkowledged:
        result = 'black';
        break;
      case ProcedureStates.BranchOffJobCreated:
      case ProcedureStates.BranchOffJobFinished:
      case ProcedureStates.BranchOffJobAcknowledegd:
        result = 'black';
        break;
      case ProcedureStates.ConnectBuildingJobCreated:
      case ProcedureStates.ConnectBuildingJobBound:
      case ProcedureStates.ConnectBuildingJobUnboud:
      case ProcedureStates.ConnectBuildingJobFinished:
      case ProcedureStates.ConnectBuildingJobDiscarded:
      case ProcedureStates.ConnectBuildingJobAcknlowedged:
        result = 'black';
        break;
      case ProcedureStates.InjectJobCreated:
      case ProcedureStates.InjectJobBound:
      case ProcedureStates.InjectJobUnboud:
      case ProcedureStates.InjectJobFinished:
      case ProcedureStates.InjectJobDiscarded:
      case ProcedureStates.InjectJobAcknlowedged:
        result = 'black';
        break;
      case ProcedureStates.PrepareBranchableForSpliceJobCreated:
      case ProcedureStates.PrepareBranchableForSpliceJobBound:
      case ProcedureStates.PrepareBranchableForSpliceJobUnboud:
      case ProcedureStates.PrepareBranchableForSpliceJobFinished:
      case ProcedureStates.PrepareBranchableForSpliceJobDiscarded:
      case ProcedureStates.PrepareBranchableForSpliceJobAcknlowedged:
        result = 'black';
        break;
      case ProcedureStates.SpliceODFJobCreated:
      case ProcedureStates.SpliceODFJobBound:
      case ProcedureStates.SpliceODFJobUnboud:
      case ProcedureStates.SpliceODFJobFinished:
      case ProcedureStates.SpliceODFJobDiscarded:
      case ProcedureStates.SpliceODFJobAcknlowedged:
        result = 'black';
        break;
      case ProcedureStates.SpliceBranchableJobCreated:
      case ProcedureStates.SpliceBranchableJobBound:
      case ProcedureStates.SpliceBranchableJobUnboud:
      case ProcedureStates.SpliceBranchableJobFinished:
      case ProcedureStates.SpliceBranchableJobDiscarded:
      case ProcedureStates.SpliceBranchableJobAcknlowedged:
        result = 'black';
        break;
      case ProcedureStates.ActivatitionJobCreated:
      case ProcedureStates.ActivatitionJobBound:
      case ProcedureStates.ActivatitionJobUnboud:
      case ProcedureStates.ActivatitionJobFinished:
      case ProcedureStates.ActivatitionJobDiscarded:
      case ProcedureStates.ActivatitionJobAcknlowedged:
        result = 'black';
        break;
      default:
        break;
    }
    return result;
  }
}
