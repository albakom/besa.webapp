import { JobTypes } from './../models/service/jobs/job-types.enum';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'jobTypeToName'
})
export class JobTypeToNamePipe implements PipeTransform {

  transform(value: any, args?: any): any {
    const realValue = <JobTypes>value;

    let result = '';
    if (realValue == JobTypes.BranchOff) {
      result = 'Vorstrecker';
    } else if (realValue == JobTypes.HouseConnenction) {
      result = 'Hausanschluss';
    } else if (realValue== JobTypes.Inject) {
      result = 'Einblasen';
    }

    return result;
  }

}
