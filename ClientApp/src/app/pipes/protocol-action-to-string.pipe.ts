import { MessageActions } from './../models/service/messages/message-actions.enum';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'protocolActionToString'
})
export class ProtocolActionToStringPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    const realValue = <MessageActions>value;
    let result = '';
    switch (realValue) {
      case MessageActions.Create:
        result = 'hinzugefügt';
        break;
      case MessageActions.Delete:
        result = 'gelöscht';
        break;
      case MessageActions.JobAcknowledged:
        result = 'wargenommen';
        break;
      case MessageActions.JobBound:
        result = 'gebunden';
        break;
      case MessageActions.JobDiscared:
        result = 'verworfen';
        break;
      case MessageActions.JobFinished:
        result = 'beendet';
        break;
      case MessageActions.NotSpecified:
        result = 'nicht angegeben';
        break;
      case MessageActions.Update:
        result = 'geändert';
        break;
      default:
        break;
    }
    return result;
  }

}
