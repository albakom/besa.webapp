import { AreaItemType } from './../models/service/building/area-item-type.enum';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'areaItemToString'
})
export class AreaItemToStringPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    const realValue = <AreaItemType>value;
    let result = '';
    switch (realValue) {
      case AreaItemType.building:
        result = 'Gebäude';
        break;
      case AreaItemType.street:
        result = 'Straße';
        break;
      case AreaItemType.emptySpace:
        result = 'Freie Fläche';
        break;
      default:
        break;
    }
    return result;
  }

}
