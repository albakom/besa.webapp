import { Pipe, PipeTransform } from '@angular/core';
import { ProcedureImportProperties } from '../models/helper/procedure-import-properties.enum';

@Pipe({
  name: 'procedurePropertToName'
})
export class ProcedurePropertToNamePipe implements PipeTransform {

  transform(value: any, args?: any): any {
    const realValue = <ProcedureImportProperties>value;
    let result = '';
    switch (realValue) {
      case ProcedureImportProperties.ContactSurname:
        result = 'Vorname';
        break;
      case ProcedureImportProperties.ContactLastname:
        result = 'Nachname';
        break;
      case ProcedureImportProperties.ContactPhone:
        result = 'Telefon';
        break;
      case ProcedureImportProperties.Street:
        result = 'Straße';
        break;
      case ProcedureImportProperties.ContactId:
        result = 'Kontakt Id';
        break;
      case ProcedureImportProperties.StreetNumber:
        result = 'Nr.';
        break;
      case ProcedureImportProperties.Aggrement:
        result = 'Einverständnis';
        break;
      case ProcedureImportProperties.Contract:
        result = 'Tarifvertrag';
        break;
      case ProcedureImportProperties.Appointment:
        result = 'Datum';
        break;
      case ProcedureImportProperties.ConnectBuildingFinished:
        result = 'HA fertig';
        break;
      case ProcedureImportProperties.InjectFinished:
        result = 'Einblase fertig';
        break;
      case ProcedureImportProperties.SpliceFinished:
        result = 'Splice fertig';
        break;
      case ProcedureImportProperties.ActivationFinished:
        result = 'Aktivierung fertig';
        break;
      case ProcedureImportProperties.ActiveCustomer:
        result = 'Aktiver Kunde?';
        break;
      case ProcedureImportProperties.InformSalesAfterFinished:
        result = 'Interesse an Produkten?';
        break;
    }

    return result;
  }

}
