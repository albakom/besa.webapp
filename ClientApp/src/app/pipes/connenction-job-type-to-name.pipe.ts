import { Pipe, PipeTransform } from '@angular/core';
import { BuildingConnenctionTypes } from '../models/service/jobs/building-connenction-types.enum';

@Pipe({
  name: 'connenctionJobTypeToName'
})
export class ConnenctionJobTypeToNamePipe implements PipeTransform {

  transform(value: any, args?: any): any {
    const realValue: BuildingConnenctionTypes = <BuildingConnenctionTypes>value;
    let result = '';
    if (realValue === BuildingConnenctionTypes.BranchOff) {
      result = 'Vorstrecker';
    } else {
      result = 'Hausanschluss';
    }

    return result;
  }

}
