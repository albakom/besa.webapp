import { JobStates } from './../models/service/jobs/job-states.enum';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'jobStateToName'
})
export class JobStateToNamePipe implements PipeTransform {

  transform(value: any, args?: any): any {
    const state: JobStates = <JobStates>value;
    let result = '';
    if (state === JobStates.Acknowledged) {
      result = 'bestätigt';
    } else if (state === JobStates.Finished) {
      result = 'fertig';
    } else if (state === JobStates.Open) {
      result = 'offen';
    }

    return result;
  }

}
