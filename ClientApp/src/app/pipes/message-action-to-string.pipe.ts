import { Pipe, PipeTransform } from '@angular/core';
import { MessageActions } from '../models/service/messages/message-actions.enum';

@Pipe({
  name: 'messageActionToString'
})
export class MessageActionToStringPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    const realValue = <MessageActions>value;
    let result = '';
    switch (realValue) {
      case MessageActions.AddEmployeeToCompany:
        result = 'Mitarbeiter Firma hinzugefügt';
        break;
      case MessageActions.AddFlatsToBuildingWithOneUnit:
        result = 'Standardwohnungen Gebäuden zugewiesen';
        break;
      case MessageActions.AddFlatToBuilding:
        result = 'Wohnungen Gebäuden zugewiesen';
        break;
      case MessageActions.CollectionJobBoundToCompany:
        result = 'Aufababen wurden Firma zugewiesen';
        break;
      case MessageActions.Create:
        result = 'erstellt';
        break;
      case MessageActions.CreateBranchOffJobs:
        result = 'Abzweigaufgaben wurden erstellt';
        break;
      case MessageActions.CreateViaImport:
        result = 'durch import erstellt';
        break;
      case MessageActions.Delete:
        result = 'gelöscht';
        break;
      case MessageActions.FinishedProcedures:
        result = 'abgeschlossen';
        break;
      case MessageActions.InformJobFinisherAboutAcknlowedged:
        result = 'angenommen';
        break;
      case MessageActions.InformJobFinisherAboutDiscard:
        result = 'abgelehnt';
        break;
      case MessageActions.JobAcknowledged:
        result = 'angenommen';
        break;
      case MessageActions.JobBound:
        result = 'gebunden';
        break;
      case MessageActions.JobDiscared:
        result = 'abelehnt';
        break;
      case MessageActions.JobFinished:
        result = 'abgeschlossen';
        break;
      case MessageActions.JobFinishJobAdministrative:
        result = 'administrativ abgeschlossen';
        break;
      case MessageActions.NotSpecified:
        result = 'nicht angegeben';
        break;
      case MessageActions.RemoveEmployeeToCompany:
        result = 'Mitarbeiter von Firma entfernt';
        break;
      case MessageActions.RemoveMemberState:
        result = 'Zugang entfernt';
        break;
      case MessageActions.Update:
        result = 'geändert';
        break;
      case MessageActions.UpdateAllCableTypesFromAdapter:
      case MessageActions.UpdateBuildigsFromAdapter:
      case MessageActions.UpdateBuildingCableInfoFromAdapter:
      case MessageActions.UpdateBuildingNameFromAdapter:
      case MessageActions.UpdateCablesFromAdapter:
      case MessageActions.UpdateFromAdapter:
      case MessageActions.UpdateMainCableForBranchable:
      case MessageActions.UpdateMainCables:
      case MessageActions.UpdatePatchConnections:
      case MessageActions.UpdatePopInformation:
      case MessageActions.UpdateSplices:
        result = 'aus der Planungsdatenbank aktualisiert';
        break;
      default:
        break;
    }
    
    return result;
  }

}
