import { PersonTypes } from './../models/service/person-types.enum';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'personTypeToName'
})
export class PersonTypeToNamePipe implements PipeTransform {

  transform(value: any, args?: any): any {
    const realValue: PersonTypes = <PersonTypes>value;

    let result = 'Herr';
    if (realValue === PersonTypes.Female) {
      result = 'Frau';
    }

    return result;
  }

}
