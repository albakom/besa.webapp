import { Pipe, PipeTransform } from '@angular/core';
import { FileAccessObjects } from '../models/service/files/file-access-objects.enum';

@Pipe({
  name: 'fileAccessObjectTypeToName'
})
export class FileAccessObjectTypeToNamePipe implements PipeTransform {

  transform(value: any, args?: any): any {
    const realValue: FileAccessObjects = <FileAccessObjects>value;

    let result = '';
    switch (realValue) {
      case FileAccessObjects.Building:
        result = 'Gebäude';
        break;
      case FileAccessObjects.BranchOffJob:
        result = 'Vorstreckeraufgabe';
        break;
      case FileAccessObjects.HouseConnectionJob:
        result = 'Hausanschlussaufgabe';
        break;
      case FileAccessObjects.InjectJob:
        result = 'Einblasaufgabe';
        break;
    }

    return result;
  }

}
