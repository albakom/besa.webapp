import { Pipe, PipeTransform } from '@angular/core';

import * as moment from 'moment';

@Pipe({
  name: 'daysLeftToColor'
})
export class DaysLeftToColorPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    const now = moment();
    const date = moment(value);

    const diff = moment.duration(date.diff(now)).asDays();
    const roundedDiff = Math.round(diff);

    if (roundedDiff <= 1) {
      return 'red';
    } else if (roundedDiff <= 5) {
      return 'yellow';
    } else {
      return 'green';
    }
  }

}
