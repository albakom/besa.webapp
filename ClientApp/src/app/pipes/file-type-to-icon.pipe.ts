import { FileTypes } from './../models/service/files/file-types.enum';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'fileTypeToIcon'
})
export class FileTypeToIconPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    let val: FileTypes = value;

    switch (val) {
      case FileTypes.Image:
        return 'image';
      case FileTypes.PDF:
        return 'picture_as_pdf';
      case FileTypes.Video: 
        return  'video_library';
      default:
        return 'note';
    }
  }

}
