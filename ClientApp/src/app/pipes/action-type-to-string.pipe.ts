import {ActionType} from '../models/service/messages/action-type.enum';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'actionTypeToString'
})
export class ActionTypeToStringPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    const realValue = <ActionType>value;
    let result = '';
    switch (realValue) {
      case ActionType.Article:
        result = 'Artikel';
        break;
      case ActionType.Job:
        result = 'Job';
        break;
      case ActionType.User:
        result = 'Benutzer';
        break;
      default:
        break;
    }
    return result;
  }

}
