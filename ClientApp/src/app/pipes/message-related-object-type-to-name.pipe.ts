import { Pipe, PipeTransform } from '@angular/core';
import { MessageRelatedObjectTypes } from '../models/service/tasks/message-related-object-types.enum';

@Pipe({
  name: 'messageRelatedObjectTypeToName'
})
export class MessageRelatedObjectTypeToNamePipe implements PipeTransform {

  transform(value: any, args?: any): any {
    const realValue: MessageRelatedObjectTypes = <MessageRelatedObjectTypes>value;

    let output: string = '';
    switch (realValue) {
      case MessageRelatedObjectTypes.ActivationJob:
        output = 'Aktivierungsaufgabe';
        break;
      case MessageRelatedObjectTypes.Article:
        output = 'Ware';
        break;
      case MessageRelatedObjectTypes.Branchable:
        output = 'Abzweiger';
        break;
      case MessageRelatedObjectTypes.BranchOffJob:
        output = 'Vorstreckeraufgabe';
        break;
      case MessageRelatedObjectTypes.BuildingConnenctionProcedure:
        output = 'Gebäudeanschluss-Vorgang';
        break;
      case MessageRelatedObjectTypes.Buildings:
        output = 'Gebäude';
        break;
      case MessageRelatedObjectTypes.CollectionJob:
        output = 'Sammelaufgabe';
        break;
      case MessageRelatedObjectTypes.Company:
        output = 'Firma';
        break;
      case MessageRelatedObjectTypes.ConnectBuildingJob:
        output = 'Hausanschlussaufgabe';
        break;
      case MessageRelatedObjectTypes.ConstructionStage:
        output = 'Bauabschnitt';
        break;
      case MessageRelatedObjectTypes.ContactAfterFinishedJob:
        output = '"Kontaktieren nach Abschluss-Aufgabe"';
        break;
      case MessageRelatedObjectTypes.ContactInfo:
        output = 'Kontaktinfos';
        break;
      case MessageRelatedObjectTypes.CustomerConnenctionProcedure:
        output = 'Kundenanschluss-Vorgang';
        break;
      case MessageRelatedObjectTypes.CustomerRequest:
        output = 'Kundenanfrage';
        break;
      case MessageRelatedObjectTypes.Fiber:
        output = 'Faser';
        break;
      case MessageRelatedObjectTypes.FiberCable:
        output = 'Glasfaserkabel';
        break;
      case MessageRelatedObjectTypes.FiberCableType:
        output = 'Glasfaserkabel-Typ';
        break;
        case MessageRelatedObjectTypes.FiberConnection:
        output = 'Patchung';
        break;
        case MessageRelatedObjectTypes.File:
        output = 'Datei';
        break;
        case MessageRelatedObjectTypes.FileAccessEntry:
        output = 'Dateizugriffseintrag';
        break;
        case MessageRelatedObjectTypes.Flat:
        output = 'Wohnung';
        break;
        case MessageRelatedObjectTypes.InjectJob:
        output = 'Einblasaufgabe';
        break;
        case MessageRelatedObjectTypes.InventoryJob:
        output = 'Kommisierungsaufgabe';
        break;
        case MessageRelatedObjectTypes.JobGenerell:
        output = 'Aufgabe allgemein';
        break;
        case MessageRelatedObjectTypes.MainCables:
        output = 'Hauptkabel';
        break;
        case MessageRelatedObjectTypes.NotSpecified:
        output = 'nicht angegeben';
        break;
        case MessageRelatedObjectTypes.PoP:
        output = 'PoP';
        break;
        case MessageRelatedObjectTypes.PrepareBranchableJob:
        output = 'Abzweiger vorbereiten';
        break;
        case MessageRelatedObjectTypes.Splice:
        output = 'Spleiß';
        break;
        case MessageRelatedObjectTypes.SpliceInBranchableJob:
        output = 'Spleißen im Verzeiger';
        break;
        case MessageRelatedObjectTypes.SpliceMainCableInPoPJob:
        output = 'Hauptkabel spleißen';
        break;
        case MessageRelatedObjectTypes.User:
        output = 'Benutzer';
        break;
        case MessageRelatedObjectTypes.UserRequests:
        output = 'Benutzeranfrage|';
        break;
    }

    return output;
  }

}
