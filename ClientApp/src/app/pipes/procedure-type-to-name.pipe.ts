import { Pipe, PipeTransform } from '@angular/core';
import { ProcedureTypes } from '../models/service/procedure/procedure-types.enum';

@Pipe({
  name: 'procedureTypeToName'
})
export class ProcedureTypeToNamePipe implements PipeTransform {

  transform(value: any, args?: any): any {
    const realValue = <ProcedureTypes>value;
    let result = '';
    switch (realValue) {
      case ProcedureTypes.HouseConnection:
        result = 'Hausanschluss';
        break;
      case ProcedureTypes.CustomerConnection:
        result = 'Aktiver Kunden';
        break;
    }

    return result;
  }
}
