import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'genderTypeIdToString'
})
export class GenderTypeIdToStringPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    let val: number = value;
    if (val == 0)
      return 'männlich';

    else if (val == 1)
      return 'weiblich';

    else return '';
  }

}
