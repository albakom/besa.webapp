import { Pipe, PipeTransform } from '@angular/core';
import { UpdateTasks } from '../models/service/tasks/update-tasks.enum';

@Pipe({
  name: 'updateTasksToString'
})
export class UpdateTasksToStringPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    const realValue = <UpdateTasks>value;
    let result = '';
    switch (realValue) {
      case UpdateTasks.Cables:
        result = 'Kabel';
        break;
      case UpdateTasks.CableTypes:
        result = 'Kabeltypen';
        break;
      case UpdateTasks.MainCableForBranchable:
        result = 'Hauptkabel für Branchable';
        break;
      case UpdateTasks.MainCableForPops:
        result = 'Hauptkabel für Pop';
        break;
      case UpdateTasks.PatchConnections:
        result = 'Patchverbindungen';
        break;
      case UpdateTasks.PoPs:
        result = 'PoPs';
        break;
      case UpdateTasks.Splices:
        result = 'Spleiße';
        break;
      case UpdateTasks.UpdateBuildings:
        result = 'Gebäude';
        break;
      default:
        break;
    }
    return result;
  }

}
