import { Pipe, PipeTransform } from '@angular/core';
import { IBuildingAreaOverviewModel } from '../models/service/building/ibuilding-area-overview-model';
import { IBuildingAreaPolygon } from '../models/service/building/ibuilding-area-polygon';

@Pipe({
  name: 'areaIdToString'
})
export class AreaIdToStringPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    const realValue = <number>value;
    const realArgs = <IBuildingAreaPolygon[]>args;
    let result = '';

    let buildingArea = realArgs.find(x => x.id === realValue);

    if (buildingArea) {
      result = buildingArea.name;
    }
    
    return result;
  }

}
