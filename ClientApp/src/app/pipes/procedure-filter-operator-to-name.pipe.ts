import { Pipe, PipeTransform } from '@angular/core';
import { IProcedureFilterOperations } from '../models/service/procedure/iprocedure-filter-operations.enum';

@Pipe({
  name: 'procedureFilterOperatorToName'
})
export class ProcedureFilterOperatorToNamePipe implements PipeTransform {

  transform(value: any, args?: any): any {
    const realValue: IProcedureFilterOperations = <IProcedureFilterOperations>value;
    let result = '';

    switch (realValue) {
      case IProcedureFilterOperations.Equal:
        result = 'gleich';
        break;
      case IProcedureFilterOperations.Greater:
        result = 'größer';
        break;
      case IProcedureFilterOperations.Unqual:
        result = 'ungleich';
        break;
      case IProcedureFilterOperations.Smaller:
        result = 'kleiner';
        break;
    }

    return result;
  }

}
