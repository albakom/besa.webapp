import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'toColor'
})
export class ToColorPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    let realValue = <string>value;
    if(realValue.startsWith('#') === false) {
      realValue = '#' + realValue;
    }

    return realValue;
  }

}
