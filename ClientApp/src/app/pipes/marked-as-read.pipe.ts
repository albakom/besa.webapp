import { Pipe, PipeTransform } from '@angular/core';
import { MarkedAsRead } from '../models/service/messages/marked-as-read.enum';

@Pipe({
  name: 'markedAsRead'
})
export class MarkedAsReadPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    const realValue = <MarkedAsRead>value;
    let result = '';
    switch (realValue) {
      case MarkedAsRead.All:
        result = 'alle';
        break;
      case MarkedAsRead.OnlyRead:
        result = 'nur gelesene';
        break;
      case MarkedAsRead.OnlyUnread:
        result = 'nur ungelesene';
        break;
      default:
        break;
    }
    return result;
  }

}
