import { Subject, Observable, BehaviorSubject } from 'rxjs';
import { Injectable } from '@angular/core';
import { LocalStorageService } from 'angular-2-local-storage';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { IIdentityInformationModel } from '../models/helper/iidentity-information-model';
import { OAuthService } from 'angular-oauth2-oidc';
import { Router } from '@angular/router';
import { BesaRoles } from '../models/service/besa-roles.enum';
import { WebServiceClientService } from './web-service-client.service';
import { map } from 'rxjs/operators';
import { IUserInfoModel } from '../models/service/user/iuser-info-model';

@Injectable()
export class LoginService {

  //#region Fields

  private _redirectUriStorageKey = 'redirectUri';

  //#region Properties

  public possibleRoles: BesaRoles[];

  public get redirectUri(): string {
    return this._storage.get<string>(this._redirectUriStorageKey);
  }

  public set redirectUri(value: string) {
    this._storage.set(this._redirectUriStorageKey, value);
  }

  public logoutRequested: boolean;

  public currentUser: BehaviorSubject<IIdentityInformationModel>;
  public currentUserInfo: BehaviorSubject<IUserInfoModel>;

  //#endregion

  //#region Constructor

  constructor(
    private _storage: LocalStorageService,
    private _httpClient: HttpClient,
    private _router: Router,
    private _webService: WebServiceClientService,

  ) {

    this.currentUser = new BehaviorSubject<IIdentityInformationModel>(null);
    this.currentUserInfo = new BehaviorSubject<IUserInfoModel>(null);

    this.possibleRoles = [
      BesaRoles.Admin,
      BesaRoles.AirInjector,
      BesaRoles.CivilWorker,
      BesaRoles.ProjectManager,
      BesaRoles.Sales,
      BesaRoles.Inventory,
      BesaRoles.Splice,
      BesaRoles.ActiveNetwork,
    ];
  }

  //#endregion

  //#region Methods

  public getIdentityInformationen(identityToken: string, url: string): Observable<IIdentityInformationModel> {

    return this._httpClient.get<IIdentityInformationModel>(url, {
      headers: new HttpHeaders()
        .set('Authorization', 'Bearer ' + identityToken)
    }).pipe(map((result) => {
      this.currentUser.next(result);
      return result;
    }));
  }

  public logout() {
    this.logoutRequested = true;
    this._router.navigate(['/logout']);
  }

  public checkRoles(): Observable<boolean> {
    console.log('checkRoles');

    return this._webService.getUserInfo().pipe(map((input) => {
      let result = false;

      if (input.roles !== 0) {
        result = true;
        this.currentUserInfo.next(input);
      }

      return result;
    }));
  }

  public updateUserInfo(input: IUserInfoModel) {
    this.currentUserInfo.next(input);
  }

  public hasRole(role: BesaRoles): boolean {
    const info = this.currentUserInfo.getValue();
    if (!info) { return false; }

    return (info.roles & role) == role;
  }

  public userHasManagementCapabilities(): boolean {
    const info = this.currentUserInfo.getValue();
    if (!info) { return false; }

    return info.isManagement;
  }

  //#endregion

}
