import { IArticleCreateModel } from './../../models/service/article/iarticle-create-model';
import { IArticleOverviewModel } from './../../models/service/article/iarticle-overview-model';
import { WebServiceClientService } from './../web-service-client.service';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { IArticleEditModel } from '../../models/service/article/iarticle-edit-model';
import { IArticleDetailModel } from '../../models/service/article/iarticle-detail-model';

@Injectable()
export class ArticleService {

  constructor(
    private _webServiceClient: WebServiceClientService
  ) { }

  public loadArticle(): Observable<IArticleOverviewModel[]> {
    return this._webServiceClient.getAllArticles();
  }

  public deleteArticle(articleID: number, forced: boolean = false): Observable<Boolean> {
    return this._webServiceClient.deleteArticle(articleID, forced);
  }

  public editArticle(article: IArticleEditModel): Observable<boolean> {
    return this._webServiceClient.editArticle(article);
  }

  public getArticleDetails(articleID: number): Observable<IArticleDetailModel> {
    return this._webServiceClient.getArticleDetails(articleID);
  }

  public createArticle(article: IArticleCreateModel):  Observable<IArticleOverviewModel> {
    return this._webServiceClient.createArticle(article);
  }

  public isArticleInUse(articleID: number): Observable<boolean> {
    return this._webServiceClient.isArticleInUse(articleID);
  }

}
