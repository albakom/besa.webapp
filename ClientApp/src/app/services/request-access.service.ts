import { IGrantUserAccessModel } from './../models/service/user/igrant-user-access-model';
import { MemberStatus } from './../models/service/member-status.enum';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { WebServiceClientService } from './web-service-client.service';
import { ICompanieOverviewModel } from '../models/service/icompanie-overview-model';
import { IUserAccessRequestCreateModel } from '../models/service/user/iuser-access-request-create-model';
import { IUserAccessRequestOverviewModel } from '../models/service/user/iuser-access-request-overview-model';
import { map } from 'rxjs/operators';

@Injectable()
export class RequestAccessService {

  //#region Properties

  public requestModel: IUserAccessRequestOverviewModel;

  //#endregion

  constructor(
    private _webServiceClient: WebServiceClientService,
  ) {

  }

  //#region Methods

  public loadCompanies(): Observable<ICompanieOverviewModel[]> {
    return this._webServiceClient.getAllCompanies();
  }

  public loadMemberStatus(): Observable<MemberStatus> {
    return this._webServiceClient.checkIfUserIsAlreadyMember();
  }

  public startRegistration(model: IUserAccessRequestCreateModel): Observable<boolean> {
    return this._webServiceClient.createUserAccessRequest(model);
  }

  public loadRequests(): Observable<IUserAccessRequestOverviewModel[]> {
    return this._webServiceClient.getAllUserAccessRequests();
  }

  public grantAccess(grantModel: IGrantUserAccessModel): Observable<number> {
    return this._webServiceClient.grantUserAccess(grantModel).pipe(map((input) => {
      if(input > 0) {
        this.requestModel = null;
      }

      return input;
    }))

  }

  //#endregion

}
