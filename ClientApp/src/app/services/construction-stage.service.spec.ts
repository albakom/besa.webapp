import { TestBed, inject } from '@angular/core/testing';

import { ConstructionStageService } from './construction-stage.service';

describe('ConstructionStageService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ConstructionStageService]
    });
  });

  it('should be created', inject([ConstructionStageService], (service: ConstructionStageService) => {
    expect(service).toBeTruthy();
  }));
});
