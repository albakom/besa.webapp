import { MessageRelatedObjectTypes } from 'app/models/service/tasks/message-related-object-types.enum';
import * as moment from 'moment';
import { ProtocolModel } from './../../models/service/messages/protocol-model';
import { Observable, BehaviorSubject } from 'rxjs';
import { WebServiceClientService } from './../web-service-client.service';
import { Injectable } from '@angular/core';
import { ProtocolFilterModel } from '../../models/service/messages/protocol-filter-model';
import { MessageModel } from '../../models/service/messages/message-model';
import { MessageFilterModel } from '../../models/service/messages/message-filter-model';
import { MessageActions } from '../../models/service/messages/message-actions.enum';
import { MarkedAsRead } from '../../models/service/messages/marked-as-read.enum';
import { ISelectedable } from '../../models/helper/iselectedable';
import { IMessageFilterModel } from '../../models/service/messages/imessage-filter-model';

@Injectable({
  providedIn: 'root'
})
export class MessageService {

  //#region Fields

  private _messageFilter: IMessageFilterModel;
  private _currentMessage: MessageModel;

  //#endregion

  //#region Properties

  public onFilterChanged: BehaviorSubject<IMessageFilterModel>;
  public onCurrentMessageChanged: BehaviorSubject<MessageModel>;

  //#endregion

  public getPossibleMessageRelatedTypes(): MessageRelatedObjectTypes[] {
    const types =
      [
        MessageRelatedObjectTypes.NotSpecified,
        MessageRelatedObjectTypes.User,
        MessageRelatedObjectTypes.Article,
        MessageRelatedObjectTypes.Branchable,
        MessageRelatedObjectTypes.Company,
        MessageRelatedObjectTypes.ConstructionStage,
        MessageRelatedObjectTypes.ContactInfo,
        MessageRelatedObjectTypes.FiberCable,
        MessageRelatedObjectTypes.FiberCableType,
        MessageRelatedObjectTypes.FileAccessEntry,
        MessageRelatedObjectTypes.File,
        MessageRelatedObjectTypes.Flat,
        MessageRelatedObjectTypes.PoP,
        MessageRelatedObjectTypes.BuildingConnenctionProcedure,
        MessageRelatedObjectTypes.CustomerConnenctionProcedure,
        MessageRelatedObjectTypes.CollectionJob,
        MessageRelatedObjectTypes.JobGenerell,
        MessageRelatedObjectTypes.ActivationJob,
        MessageRelatedObjectTypes.BranchOffJob,
        MessageRelatedObjectTypes.ConnectBuildingJob,
        MessageRelatedObjectTypes.InjectJob,
        MessageRelatedObjectTypes.InventoryJob,
        MessageRelatedObjectTypes.PrepareBranchableJob,
        MessageRelatedObjectTypes.SpliceInBranchableJob,
        MessageRelatedObjectTypes.SpliceMainCableInPoPJob,
        MessageRelatedObjectTypes.Buildings,
        MessageRelatedObjectTypes.MainCables,
        MessageRelatedObjectTypes.CustomerRequest,
        MessageRelatedObjectTypes.UserRequests,
        MessageRelatedObjectTypes.ContactAfterFinishedJob,
      ];

    return types;
  }
  public getPossibleMessageActions(): MessageActions[] {
    const actions =
      [
        MessageActions.NotSpecified,
        MessageActions.Create,
        MessageActions.Update,
        MessageActions.Delete,
        MessageActions.JobFinished,
        MessageActions.JobDiscared,
        MessageActions.JobAcknowledged,
        MessageActions.JobBound,
        MessageActions.RemoveEmployeeToCompany,
        MessageActions.AddEmployeeToCompany,
        MessageActions.UpdateFromAdapter,
        MessageActions.AddFlatsToBuildingWithOneUnit,
        MessageActions.AddFlatToBuilding,
        MessageActions.UpdateAllCableTypesFromAdapter,
        MessageActions.UpdateBuildingCableInfoFromAdapter,
        MessageActions.UpdateBuildingNameFromAdapter,
        MessageActions.UpdateBuildigsFromAdapter,
        MessageActions.UpdatePopInformation,
        MessageActions.UpdateMainCables,
        MessageActions.UpdateSplices,
        MessageActions.UpdateMainCableForBranchable,
        MessageActions.UpdatePatchConnections,
        MessageActions.UpdateCablesFromAdapter,
        MessageActions.CreateBranchOffJobs,
        MessageActions.CreateViaImport,
        MessageActions.RemoveMemberState,
        MessageActions.FinishedProcedures,
        MessageActions.JobFinishJobAdministrative,
        MessageActions.CollectionJobBoundToCompany,
        MessageActions.InformJobFinisherAboutAcknlowedged,
        MessageActions.InformJobFinisherAboutDiscard,
      ];

    return actions;
  }

  constructor(
    private _webServiceClient: WebServiceClientService,
  ) {

    this._messageFilter = {
      amount: 100,
      start: 0,
      actions: new Array(),
      types: new Array(),
      markedAsRead: MarkedAsRead.All,
    };

    this.onFilterChanged = new BehaviorSubject<IMessageFilterModel>(this._messageFilter);
    this.onCurrentMessageChanged = new BehaviorSubject<MessageModel>(this._currentMessage);

  }

  public getProtocol(filter: ProtocolFilterModel): Observable<ProtocolModel[]> {
    return this._webServiceClient.getProtocol(filter);
  }

  public getDefaultMessages(): Observable<MessageModel[]> {
    return this._webServiceClient.getMessages(this._messageFilter);
  }

  public getMessages(filter: IMessageFilterModel): Observable<MessageModel[]> {
    return this._webServiceClient.getMessages(filter);
  }

  public setCurrentMessage(msg: MessageModel): void {
    this._currentMessage = msg;
    this.onCurrentMessageChanged.next(msg);
  }


  public changeMessageActionTypeOfFilter(item: ISelectedable<MessageActions>): void {
    const index = this._messageFilter.actions.findIndex((x) => x == item.value);

    if (item.isSelected == true) {
      if (index < 0) {
        this._messageFilter.actions.push(item.value);
      }
    }
    else {
      if (index >= 0) {
        this._messageFilter.actions.splice(index, 1);
      }
    }

    this.onFilterChanged.next(this._messageFilter);
  }

  public changeMessageRelatedObjectTypeOfFilter(item: ISelectedable<MessageRelatedObjectTypes>): void {
    const index = this._messageFilter.types.findIndex((x) => x == item.value);

    if (item.isSelected == true) {
      if (index < 0) {
        this._messageFilter.types.push(item.value);
      }
    }
    else {
      if (index >= 0) {
        this._messageFilter.types.splice(index, 1);
      }
    }

    this.onFilterChanged.next(this._messageFilter);
  }

  public setQueryValueForFilter(query: string) {
    this._messageFilter.query = query;
    this.onFilterChanged.next(this._messageFilter);
  }

  public changeEndDateOfFilter(date: Date) {
    if (date == null) {
      this._messageFilter.endTime = null;
    } else {
      this._messageFilter.endTime = date.toISOString();

    }

    this.onFilterChanged.next(this._messageFilter);
  }

  public changeMarkedAsReadStateToFilter(readState?: MarkedAsRead) {
    this._messageFilter.markedAsRead = readState;
    this.onFilterChanged.next(this._messageFilter);
  }

  public changeStartDateOfFilter(date: Date) {
    if (date == null) {
      this._messageFilter.startTime = null;
    } else {
      this._messageFilter.startTime = date.toISOString();

    }

    this.onFilterChanged.next(this._messageFilter);
  }

  public markMessageAsRead(id: number): Observable<boolean> {
    return  this._webServiceClient.markMessageAsRead(id);
  }
}
