import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { IErrorModel } from '../models/helper/ierror-model';

@Injectable()
export class ErrorService {

  //#region Properties

  public error: BehaviorSubject<IErrorModel>;

  //#endregion

  //#region Constructor

  constructor() {
    this.error = new BehaviorSubject<IErrorModel>({ code: -1, title: 'no error' });
  }

  //#endregion

}
