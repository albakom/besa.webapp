import { TestBed, inject } from '@angular/core/testing';

import { AreaPlanningService } from './area-planning.service';

describe('AreaPlanningService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AreaPlanningService]
    });
  });

  it('should be created', inject([AreaPlanningService], (service: AreaPlanningService) => {
    expect(service).toBeTruthy();
  }));
});
