import { IBuildingDetailModel } from './../../models/service/building/ibuilding-detail-model';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { WebServiceClientService } from '../web-service-client.service';
import { IBuildingAreaOverviewModel } from '../../models/service/building/ibuilding-area-overview-model';
import { IBuildingAreaDetailModel } from '../../models/service/building/ibuilding-area-detail-model';
import { IBuildingAreaCreateModel } from '../../models/service/building/ibuilding-area-create-model';
import { IBuildingCreateModel } from '../../models/service/building/ibuilding-create-model';
import { IBuildingDeleteModel } from '../../models/service/building/ibuilding-delete-model';
import { IEmptySpaceCreateModel } from '../../models/service/building/iempty-space-create-model';
import { IEmptySpaceDetailModel } from '../../models/service/building/iempty-space-detail-model';
import { IStreetCreateModel } from '../../models/service/building/istreet-create-model';
import { IStreetEditModel } from '../../models/service/building/istreet-edit-model';
import { IEmptySpaceEditModel } from '../../models/service/building/iempty-space-edit-model';

@Injectable({
  providedIn: 'root'
})
export class AreaPlanningService {

  constructor(
    private _webServiceClient: WebServiceClientService
  ) { }

  public getBuildingAreaOverviews(): Observable<IBuildingAreaOverviewModel[]> {
    return this._webServiceClient.getBuildingAreaOverviews();
  }

  public getBuildingAreaDetails(id: number): Observable<IBuildingAreaDetailModel> {
    return this._webServiceClient.getBuildingAreaDetails(id);
  }

  public createBuildingArea(model: IBuildingAreaCreateModel): Observable<number> {
    return this._webServiceClient.createBuildingArea(model);
  }

  public createBuilding(building: IBuildingCreateModel): Observable<number> {
    return this._webServiceClient.createBuilding(building);
  }

  public editBuilding(building: IBuildingDetailModel): Observable<boolean> {
    return this._webServiceClient.editBuilding(building);
  }

  public deleteBuilding(building: IBuildingDeleteModel): Observable<boolean> {
    return this._webServiceClient.deleteBuilding(building);
  }

  public getBuildingDetails(id: number): Observable<IBuildingDetailModel> {
    return this._webServiceClient.getBuildingDetails(id);
  }

  public createStreet(streetName: IStreetCreateModel): Observable<number> {
    return this._webServiceClient.createStreet(streetName);
  }

  public createEmptySpace(emptySpace: IEmptySpaceCreateModel): Observable<number> {
    return this._webServiceClient.createEmptySpace(emptySpace);
  }

  public getEmptySpaceDetails(id: number): Observable<IEmptySpaceDetailModel> {
    return this._webServiceClient.getEmptySpaceDetails(id);
  }

  public deleteStreet(id: number): Observable<boolean> {
    return this._webServiceClient.deleteStreet(id);
  }

  public deleteEmptySpace(id: number): Observable<boolean> {
    return this._webServiceClient.deleteEmptySpace(id);
  }

  public editStreet(model: IStreetEditModel): Observable<boolean> {
    return this._webServiceClient.editStreet(model);
  }

  public editEmptySpace(model: IEmptySpaceEditModel): Observable<boolean> {
    return this._webServiceClient.editEmptySpace(model);
  }
}
