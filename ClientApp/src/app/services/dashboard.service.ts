import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { WebServiceClientService } from './web-service-client.service';
import { IPersonalJobOverviewModel } from '../models/service/jobs/overview/ipersonal-job-overview-model';

@Injectable()
export class DashboardService {

  //#region Constructor

  constructor(private _service: WebServiceClientService) { }

  //#endregion

  //#region Methods

  public getNextJobs(): Observable<IPersonalJobOverviewModel[]> {
    return this._service.getOpenJobs();
  }

  //#endregion

}
