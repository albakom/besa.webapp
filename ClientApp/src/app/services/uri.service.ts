import { ServiceEndpoints } from './../helper/service-endpoints.enum';
import { Injectable, Inject } from '@angular/core';

@Injectable()
export class UriService {

  //#region  Fields

  private _uriDict: { [endpoint: number]: string };

  //#endregion

  //#region Constructor

  constructor(
    @Inject('API_URL') private _apiUrl: string,
  ) {
    this._uriDict = {};
    this._uriDict[ServiceEndpoints.DashboradOverview] = _apiUrl + '/api/values/get';
    this._uriDict[ServiceEndpoints.UserInfo] = _apiUrl + '/api/personal/UserInfo';
    this._uriDict[ServiceEndpoints.MemberStatus] = _apiUrl + '/api/personal/MemberStatus';
    this._uriDict[ServiceEndpoints.CreateUserAccessRequest] = _apiUrl + '/api/personal/GenerateAccessRequest';
    this._uriDict[ServiceEndpoints.PersonalOpenJobs] = _apiUrl + '/api/personal/OpenJobs';
    this._uriDict[ServiceEndpoints.FinishBranchoffJob] = _apiUrl + '/api/personal/FinishBranchOffJob';
    this._uriDict[ServiceEndpoints.FinishConnectBuildingJob] = _apiUrl + '/api/personal/FinishBuildingConnenctionJob';
    this._uriDict[ServiceEndpoints.FinishInjectJob] = _apiUrl + '/api/personal/FinishInjectJob';
    this._uriDict[ServiceEndpoints.FinishActivationJob] = _apiUrl + '/api/personal/FinishActivationJob';
    this._uriDict[ServiceEndpoints.FinishSpliceBranchableJob] = _apiUrl + '/api/personal/SpliceBranchableJob';
    this._uriDict[ServiceEndpoints.FinishPrepareBranchableJob] = _apiUrl + '/api/personal/PrepareBranchableForSpliceJob';
    this._uriDict[ServiceEndpoints.FinishSpliceODFJob] = _apiUrl + '/api/personal/SpliceODFJob';

    this._uriDict[ServiceEndpoints.CompanieOverviews] = _apiUrl + '/api/companies/overview';
    this._uriDict[ServiceEndpoints.CompanyDetail] = _apiUrl + '/api/companies/details';
    this._uriDict[ServiceEndpoints.DeleteCompany] = _apiUrl + '/api/companies/delete';
    this._uriDict[ServiceEndpoints.EditCompany] = _apiUrl + '/api/companies/edit';
    this._uriDict[ServiceEndpoints.AddCompany] = _apiUrl + '/api/companies/add';
    this._uriDict[ServiceEndpoints.SimpleUsers] = _apiUrl + '/api/user/allEmployees';
    this._uriDict[ServiceEndpoints.UserAccessRequests] = _apiUrl + '/api/user/RequestOverview';
    this._uriDict[ServiceEndpoints.GrantUserAccess] = _apiUrl + '/api/user/GrantAccess';
    this._uriDict[ServiceEndpoints.UserOverviews] = _apiUrl + '/api/user/all';
    this._uriDict[ServiceEndpoints.DeleteUser] = _apiUrl + '/api/user/removeMemberStatus';
    this._uriDict[ServiceEndpoints.EditUser] = _apiUrl + '/api/user/edit';
    this._uriDict[ServiceEndpoints.DataForEditUser] = _apiUrl + '/api/user/DataForEdit';

    this._uriDict[ServiceEndpoints.ConstructionStageOverviews] = _apiUrl + '/api/ConstructionStages/Overview';
    this._uriDict[ServiceEndpoints.DeleteConstructionStage] = _apiUrl + '/api/ConstructionStages/DeleteConstructionStage';
    this._uriDict[ServiceEndpoints.SearchCabinents] = _apiUrl + '/api/ConstructionStages/SearchCabinets';
    this._uriDict[ServiceEndpoints.SearchBuildings] = _apiUrl + '/api/ConstructionStages/SearchBuildings';
    this._uriDict[ServiceEndpoints.BuildingsByBranchable] = _apiUrl + '/api/ConstructionStages/BuldingsByBrachnable';
    this._uriDict[ServiceEndpoints.CreateConstructionStage] = _apiUrl + '/api/ConstructionStages/Create';
    this._uriDict[ServiceEndpoints.CreateBranchOffJobs] = _apiUrl + '/api/ConstructionStages/CreateBranchJobs';
    this._uriDict[ServiceEndpoints.ConstructionStageOverviewForCivilWork] = _apiUrl + '/api/ConstructionStages/OverviewForCivilWork';
    this._uriDict[ServiceEndpoints.ConstrunctionStageDetailForCivilWork] = _apiUrl + '/api/ConstructionStages/DetailsForCivilWork';
    this._uriDict[ServiceEndpoints.ConstructionStageOverviewForProjectManagement] = _apiUrl + '/api/ConstructionStages/OverviewForProjectManangement';
    this._uriDict[ServiceEndpoints.FlatByBuildings] = _apiUrl + '/api/ConstructionStages/FlatsByBuilding';
    this._uriDict[ServiceEndpoints.AddFlatToBuilding] = _apiUrl + '/api/ConstructionStages/CreateFlat';
    this._uriDict[ServiceEndpoints.CheckIfBuildingIsConnected] = _apiUrl + '/api/ConstructionStages/CheckIfBuildingIsConnected';
    this._uriDict[ServiceEndpoints.ConstructionStageOverviewForInjectJobs] = _apiUrl + '/api/ConstructionStages/OverviewForInject';
    this._uriDict[ServiceEndpoints.ConstrunctionStageDetailForInjectJobs] = _apiUrl + '/api/ConstructionStages/DetailsForInject';
    this._uriDict[ServiceEndpoints.ConstructionStageOverviewForSpliceJobs] = _apiUrl + '/api/ConstructionStages/OverviewForSplice';
    this._uriDict[ServiceEndpoints.ConstrunctionStageDetailForSpliceJobs] = _apiUrl + '/api/ConstructionStages/DetailsForSplice';
    this._uriDict[ServiceEndpoints.ConstructionStageOverviewForPrepareBranchableJobs] = _apiUrl + '/api/ConstructionStages/OverviewForPrepareBranchable';
    this._uriDict[ServiceEndpoints.ConstrunctionStageDetailForPrepareBranchableJobs] = _apiUrl + '/api/ConstructionStages/DetailsForPrepareBranchable';
    this._uriDict[ServiceEndpoints.ConstrunctionStageUpdateInfos] = _apiUrl + '/api/ConstructionStages/UpdateInfos';
    this._uriDict[ServiceEndpoints.UpdateConstrunctionStage] = _apiUrl + '/api/ConstructionStages/Update';

    this._uriDict[ServiceEndpoints.MainCables] = _apiUrl + '/api/ConstructionStages/MainCables';

    this._uriDict[ServiceEndpoints.BranchOffJobDetails] = _apiUrl + '/api/Job/BranchOffDetails';
    this._uriDict[ServiceEndpoints.CollectionJobDetails] = _apiUrl + '/api/Job/CollectionDetails';
    this._uriDict[ServiceEndpoints.AcknowledgeJob] = _apiUrl + '/api/Job/AcknowledgeJob';
    this._uriDict[ServiceEndpoints.JobsToAcknowledge] = _apiUrl + '/api/Job/JobsToAcknowledge';
    this._uriDict[ServiceEndpoints.DiscardJob] = _apiUrl + '/api/Job/DiscardJob';
    this._uriDict[ServiceEndpoints.OpenCustomerRequests] = _apiUrl + '/api/Job/OpenRequests';
    this._uriDict[ServiceEndpoints.DeleteCustomerRequest] = _apiUrl + '/api/Job/DeleteRequest';
    this._uriDict[ServiceEndpoints.CustomerRequestDetails] = _apiUrl + '/api/Job/RequestDetails';
    this._uriDict[ServiceEndpoints.CreateHouseConnenctionJob] = _apiUrl + '/api/Job/CreateHouseConnenction';
    this._uriDict[ServiceEndpoints.ConnectBuildingJobDetails] = _apiUrl + '/api/Job/ConnectBuildingDetails';
    this._uriDict[ServiceEndpoints.InjectJobDetails] = _apiUrl + '/api/Job/InjectDetails';
    this._uriDict[ServiceEndpoints.SpliceJobDetails] = _apiUrl + '/api/Job/SpliceBranchableDetails';
    this._uriDict[ServiceEndpoints.PrepareBranchableJobDetails] = _apiUrl + '/api/Job/PrepareBranchableForSpliceDetails';
    this._uriDict[ServiceEndpoints.SpliceODFJobDetails] = _apiUrl + '/api/Job/SplicODFDetails';
    this._uriDict[ServiceEndpoints.CollectionJobOverview] = _apiUrl + '/api/Job/CollectionJobOverview';

    this._uriDict[ServiceEndpoints.CreateConnectBuildingJob] = _apiUrl + '/api/Job/CreateConnectBuildingJob';
    this._uriDict[ServiceEndpoints.CreateInjectJob] = _apiUrl + '/api/Job/CreateInjectJob';
    this._uriDict[ServiceEndpoints.BindJobs] = _apiUrl + '/api/Job/BindJobs';

    this._uriDict[ServiceEndpoints.ArticleOverviews] = _apiUrl + '/api/article/overview';
    this._uriDict[ServiceEndpoints.DeleteArticle] = _apiUrl + '/api/article/remove';
    this._uriDict[ServiceEndpoints.EditArticle] = _apiUrl + '/api/article/edit';
    this._uriDict[ServiceEndpoints.CreateArticle] = _apiUrl + '/api/article/create';
    this._uriDict[ServiceEndpoints.ArticleDetails] = _apiUrl + '/api/article/details';
    this._uriDict[ServiceEndpoints.ArticleInUse] = _apiUrl + '/api/article/isInUse';

    this._uriDict[ServiceEndpoints.SearchContacts] = _apiUrl + '/api/Customer/SearchContacts';
    this._uriDict[ServiceEndpoints.CreateContact] = _apiUrl + '/api/Customer/CreateContact';
    this._uriDict[ServiceEndpoints.CreateCustomer] = _apiUrl + '/api/Customer/CreateCustomer';
    this._uriDict[ServiceEndpoints.DeleteContact] = _apiUrl + '/api/Customer/DeleteContact';
    this._uriDict[ServiceEndpoints.CheckIfIsContactInUse] = _apiUrl + '/api/Customer/CheckIfContactIsInUse';
    this._uriDict[ServiceEndpoints.GetContact] = _apiUrl + '/api/Customer/Contact';
    this._uriDict[ServiceEndpoints.EditContact] = _apiUrl + '/api/Customer/EditContact';
    this._uriDict[ServiceEndpoints.GetContacts] = _apiUrl + '/api/Customer/GetContacts';
    this._uriDict[ServiceEndpoints.ImportContacts] = _apiUrl + '/api/Customer/ImportContacts';

    this._uriDict[ServiceEndpoints.UploadFile] = _apiUrl + '/api/file/upload';
    this._uriDict[ServiceEndpoints.SearchFile] = _apiUrl + '/api/file/search';
    this._uriDict[ServiceEndpoints.GetFile] = _apiUrl + '/api/file/get';
    this._uriDict[ServiceEndpoints.GetFileDetail] = _apiUrl + '/api/file/details';
    this._uriDict[ServiceEndpoints.SearchFileAccess] = _apiUrl + '/api/file/accessObjects';
    this._uriDict[ServiceEndpoints.EditFile] = _apiUrl + '/api/file/edit';
    this._uriDict[ServiceEndpoints.GetFiles] = _apiUrl + '/api/file/Overview';
    this._uriDict[ServiceEndpoints.DeleteFile] = _apiUrl + '/api/file/Delete';
    this._uriDict[ServiceEndpoints.DownloadFile] = _apiUrl + '/api/file/Get';

    this._uriDict[ServiceEndpoints.CreateSpliceBranchBranchableJob] = _apiUrl + '/api/Job/createSpliceBranchableJob';
    this._uriDict[ServiceEndpoints.CreatePrepareBranchableForSpliceJob] = _apiUrl + '/api/job/createPrepareBranchableForSpliceJob';
    this._uriDict[ServiceEndpoints.CreateSpliceODFJob] = _apiUrl + '/api/job/createSpliceODFJob';
    this._uriDict[ServiceEndpoints.CreateActivationJob] = _apiUrl + '/api/job/CreateActivationJob';
    
    this._uriDict[ServiceEndpoints.SpliceBranchableDetails] = _apiUrl + '/api/job/spliceBranchableDetails';
    this._uriDict[ServiceEndpoints.PrepareBranchableForSpliceDetails] = _apiUrl + '/api/job/prepareBranchableForSpliceDetails';
    this._uriDict[ServiceEndpoints.SplicODFDetails] = _apiUrl + '/api/job/splicODFDetails';
    this._uriDict[ServiceEndpoints.SpliceBranchableJob] = _apiUrl + '/api/personal/SpliceBranchableJob';
    this._uriDict[ServiceEndpoints.PrepareBranchableForSpliceJob] = _apiUrl + '/api/personal/PrepareBranchableForSpliceJob';
    this._uriDict[ServiceEndpoints.SpliceODFJob] = _apiUrl + '/api/personal/SpliceODFJob';
    this._uriDict[ServiceEndpoints.CheckIfBuildingHasCable] = _apiUrl + '/api/job/CheckIfBuildingHasCable';
    this._uriDict[ServiceEndpoints.OpenPrepareBranchableForSpliceJobs] = _apiUrl + '/api/job/OpenPrepareBranchableForSpliceJobs';
    this._uriDict[ServiceEndpoints.OpenSpliceODFJobs] = _apiUrl + '/api/job/OpenSpliceODFJobs';
    this._uriDict[ServiceEndpoints.OpenActivationJobs] = _apiUrl + '/api/job/OpenActivationJobs';
    this._uriDict[ServiceEndpoints.SpliceODFJobOverview] = _apiUrl + '/api/job/OpenSpliceODFJobsAsOverview';
    this._uriDict[ServiceEndpoints.ActivationJobDetails] = _apiUrl + '/api/job/ActivationJobDetails';
    this._uriDict[ServiceEndpoints.ActivationJobOverview] = _apiUrl + '/api/job/OpenAcitvationJobsAsOverview';
    this._uriDict[ServiceEndpoints.FinishJobAdministrative] = _apiUrl + '/api/job/FinishJobAdministrative';
    this._uriDict[ServiceEndpoints.SearchBranchOffJobs] = _apiUrl + '/api/job/SearchBranchOffJobByBuildingName';
    this._uriDict[ServiceEndpoints.CopyFinishBranchOffToConnectBuilding] = _apiUrl + '/api/job/CopyFinishBranchOffToConnectBuilding';
    this._uriDict[ServiceEndpoints.CreateJobByRequestId] = _apiUrl + '/api/job/CreateJobByRequestId';
    this._uriDict[ServiceEndpoints.DeleteCollectionJob] = _apiUrl + '/api/job/UnboundJobs';

    this._uriDict[ServiceEndpoints.ImportProcedures] = _apiUrl + '/api/import/Procedure';

    this._uriDict[ServiceEndpoints.ProcedureOverview] = _apiUrl + '/api/Procedure/Overview';
    this._uriDict[ServiceEndpoints.ConnectBuildingProcedureDetails] = _apiUrl + '/api/Procedure/ConnectBuildingDetails';
    this._uriDict[ServiceEndpoints.ConnectCustomerProcedureDetail] = _apiUrl + '/api/Procedure/ConnectCustomerDetails';

    this._uriDict[ServiceEndpoints.CreateInventoryJob] = _apiUrl + '/api/job/CreateInventoryJob';
    this._uriDict[ServiceEndpoints.GetInventoryJobOverviews] = _apiUrl + '/api/Job/GetInventoryJobOverviews';
    this._uriDict[ServiceEndpoints.InventoryJobDetails] = _apiUrl + '/api/Job/InventoryJobDetails';
    this._uriDict[ServiceEndpoints.FinishInventoryJob] = _apiUrl + '/api/Personal/FinishInventoryJob';

    this._uriDict[ServiceEndpoints.UpdateBuildingCableInfoFromAdapter] = _apiUrl + '/api/ConstructionStages/UpdateBuildingCableInfoFromAdapter';

    this._uriDict[ServiceEndpoints.UpdateBuildingNameFromAdapter] = _apiUrl + '/api/ConstructionStages/UpdateBuildingNameFromAdapter';
    
    this._uriDict[ServiceEndpoints.UpdateBuildigsFromAdapter] = _apiUrl + '/api/ConstructionStages/UpdateBuildigsFromAdapter';
    
    this._uriDict[ServiceEndpoints.UpdateCablesFromAdapter] = _apiUrl + '/api/ConstructionStages/UpdateCablesFromAdapter';

    this._uriDict[ServiceEndpoints.UpdatePopInformation] = _apiUrl + '/api/ConstructionStages/UpdatePopInformation';

    this._uriDict[ServiceEndpoints.UpdateMainCables] = _apiUrl + '/api/ConstructionStages/UpdateMainCables';

    this._uriDict[ServiceEndpoints.UpdatePatchConnections] = _apiUrl + '/api/ConstructionStages/UpdatePatchConnections';

    this._uriDict[ServiceEndpoints.UpdateSplices] = _apiUrl + '/api/ConstructionStages/UpdateSplices';

    this._uriDict[ServiceEndpoints.UpdateCableTypesFromAdapter] = _apiUrl + '/api/ConstructionStages/UpdateCableTypesFromAdapter';
    this._uriDict[ServiceEndpoints.UpdateMainCableForBranchable] = _apiUrl + '/api/ConstructionStages/UpdateMainCableForBranchable';
    this._uriDict[ServiceEndpoints.UpdateAllDataFromProjectAdapter] = _apiUrl + '/api/ConstructionStages/UpdateAllDataFromProjectAdapter';

    this._uriDict[ServiceEndpoints.GetProtocol] = _apiUrl + '/api/Protocol/GetProtocol';
    this._uriDict[ServiceEndpoints.GetMessages] = _apiUrl + '/api/Personal/GetMessages';
    this._uriDict[ServiceEndpoints.ReadMessage] = _apiUrl + '/api/Personal/ReadMessage';

    this._uriDict[ServiceEndpoints.GetContactAfterSaleJobOverview] = _apiUrl + '/api/Job/ContactAfterSalesJobs';
    this._uriDict[ServiceEndpoints.GetContactAfterSaleJobDetail] = _apiUrl + '/api/Job/ContactAfterSalesJobDetail';
    this._uriDict[ServiceEndpoints.FinishContactAfterFinished] = _apiUrl + '/api/Job/FinishContactAfterFinished';
    this._uriDict[ServiceEndpoints.CreateRequestFromContactCustomerJob] = _apiUrl + '/api/Job/CreateRequestFromContactCustomerJob';

    this._uriDict[ServiceEndpoints.UpdateTaskOverview] = _apiUrl + '/api/UpdateTask/Overview';
    this._uriDict[ServiceEndpoints.UpdateTaskDetails] = _apiUrl + '/api/UpdateTask/Details';
    this._uriDict[ServiceEndpoints.UpdateTaskCancelTask] = _apiUrl + '/api/UpdateTask/CancelTask';
    this._uriDict[ServiceEndpoints.UpdateTaskObjectForTask] = _apiUrl + '/api/UpdateTask/ObjectForTask';
    this._uriDict[ServiceEndpoints.UpdateTaskStartTask] = _apiUrl + '/api/UpdateTask/StartTask';

    this._uriDict[ServiceEndpoints.GetBuildingAreaOverviews] = _apiUrl + '/api/Building/GetBuildingAreaOverviews';
    this._uriDict[ServiceEndpoints.GetBuildingAreaDetails] = _apiUrl + '/api/Building/GetBuildingAreaDetails';
    this._uriDict[ServiceEndpoints.CreateBuildingArea] = _apiUrl + '/api/Building/CreateBuildingArea';
    this._uriDict[ServiceEndpoints.CreateBuilding] = _apiUrl + '/api/Building/CreateBuilding';
    this._uriDict[ServiceEndpoints.EditBuilding] = _apiUrl + '/api/Building/EditBuilding';
    this._uriDict[ServiceEndpoints.DeleteBuilding] = _apiUrl + '/api/Building/DeleteBuilding';
    this._uriDict[ServiceEndpoints.BuildingDetails] = _apiUrl + '/api/Building/BuildingDetails';
    this._uriDict[ServiceEndpoints.CreateStreet] = _apiUrl + '/api/Building/CreateStreet';
    this._uriDict[ServiceEndpoints.CreateEmptySpace] = _apiUrl + '/api/Building/CreateEmptySpace';
    this._uriDict[ServiceEndpoints.EmptySpaceDetails] = _apiUrl + '/api/Building/EmptySpaceDetails';
    this._uriDict[ServiceEndpoints.DeleteStreet] = _apiUrl + '/api/Building/DeleteStreet';
    this._uriDict[ServiceEndpoints.DeleteEmptySpace] = _apiUrl + '/api/Building/DeleteEmptySpace';
    this._uriDict[ServiceEndpoints.EditStreet] = _apiUrl +'/api/Building/EditStreet';
    this._uriDict[ServiceEndpoints.EditEmptySpace] = _apiUrl + '/api/Building/EditEmptySpace';
  }


  //#endregion

  //#region Methods

  public getUri(endpoint: ServiceEndpoints): string {
    return this._uriDict[endpoint];
  }

  public getFileUrl(id: number): string {
    const url = this._uriDict[ServiceEndpoints.GetFile] + '/' + id.toString();
    return url;
  }

  //#endregion



}
