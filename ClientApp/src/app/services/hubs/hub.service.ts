import { Injectable, Inject } from '@angular/core';
import { OAuthService } from 'angular-oauth2-oidc';
import { HubConnection, LogLevel } from '@aspnet/signalr';

import * as signalR from '@aspnet/signalr';

@Injectable()
export class HubService {

  constructor(
    private _oauthService: OAuthService,
    @Inject('IS_IN_PRODUCTION') private _isProduction: boolean,
    @Inject('API_URL') private _apiUrl: string) { }

  public getHubConnection(hubUrl: string): HubConnection {
    const url = this._apiUrl + '/' + hubUrl;

    let logLevel = LogLevel.Information;

    if (this._isProduction) {
      logLevel = LogLevel.Error;
    }

    const connection = new signalR.HubConnectionBuilder().withUrl(url, {
      accessTokenFactory: () => this._oauthService.getAccessToken()
    }).configureLogging(logLevel).build();

    return connection;
  }
}
