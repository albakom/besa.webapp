import { Observable } from "rxjs";

export interface IHubService {
    connenct(): Observable<void>;
    disconnect();
}