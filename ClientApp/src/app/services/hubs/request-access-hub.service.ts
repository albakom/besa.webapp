import { Observable, Subject, from } from 'rxjs';
import { IUserAccessRequestOverviewModel } from './../../models/service/user/iuser-access-request-overview-model';
import { Injectable } from '@angular/core';
import { HubService } from './hub.service';
import { HubConnection } from '@aspnet/signalr';
import { IHubService } from './ihub-service';

@Injectable()
export class RequestAccessHubService implements IHubService {

  //#region Fields

  private _connection: HubConnection;

  //#endregion

  public newRequest: Subject<IUserAccessRequestOverviewModel>;
  public accessGranted: Subject<boolean>;

  constructor(private _hubService: HubService) {
    this.newRequest = new Subject<IUserAccessRequestOverviewModel>();
    this.accessGranted = new Subject<boolean>();
  }

  //#region Methods

  public connenct(): Observable<void> {
    this._connection = this._hubService.getHubConnection('access');

    const promise = this._connection.start()
      .then(() => {
      })
      .catch(err => {
      });

    this._connection.on('NewAccessRequest', (data: IUserAccessRequestOverviewModel) => {
      this.newRequest.next(data);
    });

    this._connection.on('AccessGranted', (data: IUserAccessRequestOverviewModel) => {
      this.accessGranted.next(true);
    });

    return from(promise);
  }

  public disconnect() {
    if (this._connection) {
      this._connection.stop();
    }
  }

  //#endregion

}
