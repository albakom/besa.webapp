import { TestBed, inject } from '@angular/core/testing';

import { UpdateTasksHubService } from './update-tasks-hub.service';

describe('UpdateTasksHubService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [UpdateTasksHubService]
    });
  });

  it('should be created', inject([UpdateTasksHubService], (service: UpdateTasksHubService) => {
    expect(service).toBeTruthy();
  }));
});
