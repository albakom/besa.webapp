import { IUpdateTasFinishedStateChangedModel } from './../../models/service/tasks/iupdate-tas-finished-state-changed-model';
import { IUpdateTaskHeartbeatChangedModel } from './../../models/service/tasks/iupdate-task-heartbeat-changed-model';
import { Injectable } from '@angular/core';
import { IUpdateTaskElementOverviewModel } from '../../models/service/tasks/iupdate-task-element-overview-model';
import { Subject, Observable, from as fromPromise } from 'rxjs';
import { HubService } from './hub.service';
import { IHubService } from './ihub-service';
import { HubConnection } from '@aspnet/signalr';

import { from } from 'rxjs';
import { IUpdateTaskOverviewModel } from '../../models/service/tasks/iupdate-task-overview-model';

@Injectable({
  providedIn: 'root'
})
export class UpdateTasksHubService implements IHubService {

  //#region Properties

  private _connection: HubConnection;
  public _taskElementChanged: Subject<IUpdateTaskElementOverviewModel>;
  public _updateTaskAdded: Subject<IUpdateTaskOverviewModel>;
  public _updateTaskCanceled: Subject<number>;
  public _updateTaskHeartbeatChanged: Subject<IUpdateTaskHeartbeatChangedModel>;
  public _updateTaskFinishedStateChanged: Subject<IUpdateTasFinishedStateChangedModel>;
  public _taskLockDetected: Subject<number>;

  private _hubConnectionEstablished: boolean = false;

  //#endregion

  constructor(private _hubService: HubService) {
    this._taskElementChanged = new Subject<IUpdateTaskElementOverviewModel>();
    this._updateTaskAdded = new Subject<IUpdateTaskOverviewModel>();
    this._updateTaskCanceled = new Subject<number>();
    this._updateTaskHeartbeatChanged = new Subject<IUpdateTaskHeartbeatChangedModel>();
    this._updateTaskFinishedStateChanged = new Subject<IUpdateTasFinishedStateChangedModel>();
    this._taskLockDetected = new Subject<number>();
  }

  //#region Methods

  public connenct(): Observable<void> {
    this._connection = this._hubService.getHubConnection('updateTasks');

    const promise = this._connection.start()
      .then(() => {
        this._hubConnectionEstablished = true;
      })
      .catch(err => {
      });

    this._connection.on('TaskElementChanged', (data: IUpdateTaskElementOverviewModel) => {
      this._taskElementChanged.next(data);
    });

    this._connection.on('UpdateTaskAdded', (data: IUpdateTaskOverviewModel) => {
      this._updateTaskAdded.next(data);
    });

    this._connection.on('UpdateTaskCanceled', (taskId: number) => {
      this._updateTaskCanceled.next(taskId);
    });

    this._connection.on('HeartbeatChanged', (data: IUpdateTaskHeartbeatChangedModel) => {
      this._updateTaskHeartbeatChanged.next(data);
    });

    this._connection.on('UpdateTaskFinished', (data: IUpdateTasFinishedStateChangedModel) => {
      this._updateTaskFinishedStateChanged.next(data);
    });

    this._connection.on('TaskLockDetected', (taskId: number) => {
      this._taskLockDetected.next(taskId);
    })

    return from(promise);
  }

  public joinTaskGroup(id: number): Observable<void> {
   const promise = this._connection.invoke('JoinTask', id).then(() => {
    }).catch((err) => {
      console.log(err);
    });

    return from(promise);
  }

  public leaveTaskGroup(id: number): Observable<void> {
    const promise = this._connection.invoke('LeaveTask', id).then(() => {
    }).catch((err) => {
      console.log(err);
    });

    return from(promise);
  }

  public disconnect() {
    this._connection.stop();
  }

  //#endregion
}
