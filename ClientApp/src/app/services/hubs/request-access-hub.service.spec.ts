import { TestBed, inject } from '@angular/core/testing';

import { RequestAccessHubService } from './request-access-hub.service';

describe('RequestAccessHubService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RequestAccessHubService]
    });
  });

  it('should be created', inject([RequestAccessHubService], (service: RequestAccessHubService) => {
    expect(service).toBeTruthy();
  }));
});
