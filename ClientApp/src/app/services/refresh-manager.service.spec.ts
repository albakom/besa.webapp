import { TestBed, inject } from '@angular/core/testing';

import { RefreshManagerService } from './refresh-manager.service';

describe('RefreshManagerService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RefreshManagerService]
    });
  });

  it('should be created', inject([RefreshManagerService], (service: RefreshManagerService) => {
    expect(service).toBeTruthy();
  }));
});
