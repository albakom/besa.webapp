import { filter } from 'rxjs/operators';
import { IStreetCreateModel } from './../models/service/building/istreet-create-model';
import { IBuildingDeleteModel } from './../models/service/building/ibuilding-delete-model';
import { IBuildingAreaOverviewModel } from './../models/service/building/ibuilding-area-overview-model';
import { IUpdateTaskObjectOverviewModel } from './../models/service/tasks/iupdate-task-object-overview-model';
import { IContactAfterFinishedUnsuccessfullyModel } from './../models/service/contacts-after-sale-job/icontact-after-finished-unsuccessfully-model';
import { IContactAfterSalesJobDetails } from './../models/service/contacts-after-sale-job/icontact-after-sales-job-details';
import { IContactAfterSalesJobOverview } from './../models/service/contacts-after-sale-job/icontact-after-sales-job-overview';
import { ProtocolModel } from './../models/service/messages/protocol-model';
import { ProtocolFilterModel } from './../models/service/messages/protocol-filter-model';
import { Observable } from 'rxjs';
import { IFinishActivationJobModel } from './../models/service/jobs/activation/ifinish-activation-job-model';
import { IFinishJobModel } from './../models/service/jobs/ifinish-job-model';
import { IPrepareBranchableForSpliceJobDetailModel } from './../models/service/jobs/splice/iprepare-branchable-for-splice-job-detail-model';
import { ISpliceBranchableJobDetailModel } from './../models/service/jobs/splice/isplice-branchable-job-detail-model';
import { IPrepareBranchableForSpliceJobCreateModel } from './../models/service/jobs/splice/iprepare-branchable-for-splice-job-create-model';
import { ISpliceBranchableJobCreateModel } from './../models/service/jobs/splice/isplice-branchable-job-create-model';
import { IContactInfo } from './../models/service/icontact-info';
import { IFileDetailModel } from './../models/service/files/ifile-detail-model';
import { IFileOverviewModel } from './../models/service/files/ifile-overview-model';
import { IFinishBranchOffJobModel } from './../models/service/jobs/ifinish-branch-off-job-model';
import { IArticleDetailModel } from './../models/service/article/iarticle-detail-model';
import { IArticleEditModel } from './../models/service/article/iarticle-edit-model';
import { IArticleOverviewModel } from './../models/service/article/iarticle-overview-model';
import { IUserEditModel } from './../models/service/user/iuser-edit-model';
import { ICompanieOverviewModel } from './../models/service/icompanie-overview-model';
import { ICompanyCreateModel } from './../models/service/companies/icompany-create-model';
import { ICompanyDetailModel } from './../models/service/companies/icompany-detail-model';
import { HttpClient, HttpRequest, HttpEvent, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { UriService } from './uri.service';
import { ServiceEndpoints } from '../helper/service-endpoints.enum';
import { BesaRoles } from '../models/service/besa-roles.enum';
import { MemberStatus } from '../models/service/member-status.enum';
import { IUserAccessRequestCreateModel } from '../models/service/user/iuser-access-request-create-model';
import { IUserAccessRequestOverviewModel } from '../models/service/user/iuser-access-request-overview-model';
import { IGrantUserAccessModel } from '../models/service/user/igrant-user-access-model';
import { IConstructionStageOverviewModel } from '../models/service/construction-stage/iconstruction-stage-overview-model';
import { ISimpleCabinetOverviewModel } from '../models/service/construction-stage/isimple-cabinet-overview-model';
import { ISimpleBuildingOverviewModel } from '../models/service/construction-stage/isimple-building-overview-model';
import { IConstructionStageCreateModel } from '../models/service/construction-stage/iconstruction-stage-create-model';
import { ICompanyEditModel } from '../models/service/companies/icompany-edit-model';
import { ISimpleEmployeeModel } from '../models/service/companies/isimple-employee-model';
import { IUserOverviewModel } from '../models/service/user/iuser-overview-model';
import { IConstructionStageForProjectManagementOverviewModel } from '../models/service/construction-stage/iconstruction-stage-for-project-management-overview-model';
import { IPersonalJobOverviewModel } from '../models/service/jobs/overview/ipersonal-job-overview-model';
import { IBranchOffJobDetailModel } from '../models/service/jobs/ibranch-off-job-detail-model';
import { ICollectionJobDetailsModel } from '../models/service/jobs/icollection-job-details-model';
import { IArticleCreateModel } from '../models/service/article/iarticle-create-model';
import { ISimpleFlatOverviewModel } from '../models/service/flats/isimple-flat-overview-model';
import { IFlatCreateModel } from '../models/service/flats/iflat-create-model';
import { ICustomerCreateModel } from '../models/service/customers/icustomer-create-model';
import { query } from '@angular/animations';
import { IAcknowledgeJobModel } from '../models/service/jobs/iacknowledge-job-model';
import { ISimpleJobOverview } from '../models/service/jobs/isimple-job-overview';
import { IFileObjectOverviewModel } from '../models/service/files/ifile-object-overview-model';
import { FileAccessObjects } from '../models/service/files/file-access-objects.enum';
import { IRequestOverviewModel } from '../models/service/jobs/irequest-overview-model';
import { ICustomerRequestDetails } from '../models/service/jobs/icustomer-request-details';
import { IFileEditModel } from '../models/service/files/ifile-edit-model';
import { IConnectBuildingJobDetailsModel } from '../models/service/jobs/connect-building/iconnect-building-job-details-model';
import { IFinishConnectBuildingModel } from '../models/service/jobs/connect-building/ifinish-connect-building-model';
import { IConstructionStageDetailModel } from '../models/service/construction-stage/iconstruction-stage-detail-for-model';
import { ISimpleBuildingConnenctionJobOverview } from '../models/service/jobs/overview/isimple-building-connenction-job-overview';
import { IConstructionStageOverviewForJobsModel } from '../models/service/construction-stage/iconstruction-stage-overview-for-jobs-model';
import { IInejectJobOverviewModel } from '../models/service/jobs/inject/iinject-job-overview-model';
import { IInjectJobDetailModel } from '../models/service/jobs/inject/iinject-job-detail-model';
import { IFinishInjectJobModel } from '../models/service/jobs/inject/ifinish-inject-job-model';
import { IConnectBuildingJobCreateModel } from '../models/service/jobs/connect-building/iconnect-building-job-create-model';
import { IInjectJobCreateModel } from '../models/service/jobs/inject/iinject-job-create-model';
import { IBindJobsModel } from '../models/service/jobs/ibind-jobs-model';
import { ICSVImportInfoModel } from '../models/service/helper/icsvimport-info-model';
import { ISpliceODFJobCreateModel } from '../models/service/jobs/splice/isplice-odf-job-create-model';
import { ISpliceBranchableJobFinishModel } from '../models/service/jobs/splice/isplice-branchable-job-finish-model';
import { IPrepareBranchableForSpliceJobFinishModel } from '../models/service/jobs/splice/iprepare-branchable-for-splice-job-finish-model';
import { ISpliceODFJobFinishModel } from '../models/service/jobs/splice/isplice-odf-job-finish-model';
import { ISpliceODFJobDetailModel } from '../models/service/jobs/splice/isplice-odf-job-detail-model';
import { ICSVImportProcedureModel } from '../models/service/import/icsvimport-procedure-model';
import { ICSVImportProcedureResultModel } from '../models/service/import/icsvimport-procedure-result-model';
import { IProcedureFilterModel } from '../models/service/procedure/iprocedure-filter-model';
import { IProcedureOverviewModel } from '../models/service/procedure/iprocedure-overview-model';
import { ICustomerConnectionProcedureDetailModel } from '../models/service/procedure/icustomer-connection-procedure-detail-model';
import { IBuildingConnectionProcedureDetailModel } from '../models/service/procedure/ibuilding-connection-procedure-detail-model';
import { IInventoryJobCreateModel } from '../models/service/jobs/inventory/iinventory-job-create-model';
import { IMainCableOverviewModel } from '../models/service/splice/imain-cable-overview-model';
import { IActivationJobCreateModel } from '../models/service/jobs/activation/activation-job-create-model';
import { IInventoryJobOverviewModel } from '../models/service/jobs/inventory/iinventory-job-overview-model';
import { IProcedureSortingModel } from '../models/service/procedure/iprocedure-sorting-model';
import { ISimplePrepareBranchableForSpliceJobOverviewModel } from '../models/service/jobs/splice/isimple-prepare-branchable-for-splice-job-overview-model';
import { ISimpleSpliceODFJobOverviewModel } from '../models/service/jobs/splice/isimple-splice-odfjob-overview-model';
import { ISimpleActivationJobOverviewModel } from '../models/service/jobs/activation/isimple-activation-job-overview-model';
import { IInventoryJobDetailModel } from '../models/service/jobs/inventory/iinventory-job-detail-model';
import { IInventoryFinishedJobModel } from '../models/service/jobs/inventory/iinventory-finished-job-model';
import { ISpliceBranchableJobOverviewModel } from '../models/service/jobs/splice/isplice-branchable-job-overview-model';
import { IPrepareBranchableForSpliceJobOverviewModel } from '../models/service/jobs/splice/iprepare-branchable-for-splice-job-overview-model';
import { ISpliceODFJobOverviewModel } from '../models/service/jobs/splice/isplice-odf-job-overview-model';
import { IActivationJobOverviewModel } from '../models/service/jobs/activation/iactivation-job-overview-model';
import { IActivationJobDetailModel } from '../models/service/jobs/activation/iactivation-job-detail-model';
import { IFinishSpliceBranchableJobModel } from '../models/service/jobs/splice/ifinish-splice-branchable-job-model';
import { IFinishPrepareBranchableJobModel } from '../models/service/jobs/splice/ifinish-prepare-branchable-job-model';
import { IFinishSpliceODFJobModel } from '../models/service/jobs/splice/ifinish-splice-odfjob-model';
import { IFinishJobAdministrativeModel } from '../models/service/jobs/ifinish-job-administrative-model';
import { IUserInfoModel } from '../models/service/user/iuser-info-model';
import { JobStates } from '../models/service/jobs/job-states.enum';
import { IConstructionStageDetailsForEditModel } from '../models/service/construction-stage/iconstruction-stage-details-for-edit-model';
import { IConstructionStageUpdateModel } from '../models/service/construction-stage/Iconstruction-stage-update-model';
import { IJobCollectionFilterModel } from '../models/service/jobs/ijob-collection-filter-model';
import { IJobCollectionOverviewModel } from '../models/service/jobs/overview/ijob-collection-overview-model';
import { MessageModel } from '../models/service/messages/message-model';
import { MessageFilterModel } from '../models/service/messages/message-filter-model';
import { IContactAfterFinishedFilterModel } from '../models/service/contacts-after-sale-job/icontact-after-finished-filter-model';
import { IRequestCustomerConnectionByContactModel } from '../models/service/contacts-after-sale-job/irequest-customer-connection-by-contact-model';
import { IUpdateTaskOverviewModel } from '../models/service/tasks/iupdate-task-overview-model';
import { IUpdateTaskDetailModel } from '../models/service/tasks/iupdate-task-detail-model';
import { UpdateTasks } from '../models/service/tasks/update-tasks.enum';
import { IStartUpdateTaskModel } from '../models/service/tasks/istart-update-task-model';
import { IBuildingAreaDetailModel } from '../models/service/building/ibuilding-area-detail-model';
import { IBuildingAreaCreateModel } from '../models/service/building/ibuilding-area-create-model';
import { IBuildingCreateModel } from '../models/service/building/ibuilding-create-model';
import { IBuildingDetailModel } from '../models/service/building/ibuilding-detail-model';
import { IEmptySpaceCreateModel } from '../models/service/building/iempty-space-create-model';
import { IEmptySpaceDetailModel } from '../models/service/building/iempty-space-detail-model';
import { IMessageFilterModel } from '../models/service/messages/imessage-filter-model';
import { IStreetEditModel } from '../models/service/building/istreet-edit-model';
import { IEmptySpaceEditModel } from '../models/service/building/iempty-space-edit-model';

@Injectable()
export class WebServiceClientService {

  //#region Constructor

  constructor(
    private _httpClient: HttpClient,
    private _uriService: UriService) { }

  //#endregion

  //#region Dashboard

  public getDashboardOverview(): Observable<string[]> {
    const url = this._uriService.getUri(ServiceEndpoints.DashboradOverview);
    return this._httpClient.get<string[]>(url);
  }

  //#endregion

  //#region Personal-Info

  public getUserInfo(): Observable<IUserInfoModel> {
    const url = this._uriService.getUri(ServiceEndpoints.UserInfo);
    return this._httpClient.get<IUserInfoModel>(url);
  }

  public checkIfUserIsAlreadyMember(): Observable<MemberStatus> {
    const url = this._uriService.getUri(ServiceEndpoints.MemberStatus);
    return this._httpClient.get<MemberStatus>(url);
  }

  public createUserAccessRequest(request: IUserAccessRequestCreateModel): Observable<boolean> {
    const url = this._uriService.getUri(ServiceEndpoints.CreateUserAccessRequest);
    return this._httpClient.post<boolean>(url, request);
  }

  public getOpenJobs(): Observable<IPersonalJobOverviewModel[]> {
    const url = this._uriService.getUri(ServiceEndpoints.PersonalOpenJobs);
    return this._httpClient.get<IPersonalJobOverviewModel[]>(url);
  }

  //#endregion

  //#region User 

  public getAllUserAccessRequests(): Observable<IUserAccessRequestOverviewModel[]> {
    const url = this._uriService.getUri(ServiceEndpoints.UserAccessRequests);
    return this._httpClient.get<IUserAccessRequestOverviewModel[]>(url);
  }

  public grantUserAccess(grantModel: IGrantUserAccessModel): Observable<number> {
    const url = this._uriService.getUri(ServiceEndpoints.GrantUserAccess);
    return this._httpClient.post<number>(url, grantModel);
  }

  public getAllSimpleUsers(): Observable<ISimpleEmployeeModel[]> {
    const url = this._uriService.getUri(ServiceEndpoints.SimpleUsers);
    return this._httpClient.get<ISimpleEmployeeModel[]>(url);
  }

  public getAllUsers(): Observable<IUserOverviewModel[]> {
    const url = this._uriService.getUri(ServiceEndpoints.UserOverviews);
    return this._httpClient.get<IUserOverviewModel[]>(url);
  }

  public deleteUser(userID: number): Observable<boolean> {
    const url = this._uriService.getUri(ServiceEndpoints.DeleteUser) + '/' + userID.toString();

    return this._httpClient.delete<boolean>(url);
  }

  public editUser(user: IUserEditModel): Observable<boolean> {
    const url = this._uriService.getUri(ServiceEndpoints.EditUser);
    return this._httpClient.post<boolean>(url, user);
  }

  public dataForEditUser(userID: number): Observable<IUserEditModel> {
    const url = this._uriService.getUri(ServiceEndpoints.DataForEditUser) + '/' + userID.toString();
    return this._httpClient.get<IUserEditModel>(url);
  }

  //#endregion

  //#region Companies

  public getAllCompanies(): Observable<ICompanieOverviewModel[]> {
    const url = this._uriService.getUri(ServiceEndpoints.CompanieOverviews);
    return this._httpClient.get<ICompanieOverviewModel[]>(url);
  }

  public deleteCompany(companyID: number): Observable<boolean> {
    const url = this._uriService.getUri(ServiceEndpoints.DeleteCompany) + '/' + companyID.toString();

    return this._httpClient.delete<boolean>(url);
  }

  public getCompanyDetail(companyID: number): Observable<ICompanyDetailModel> {
    const url = this._uriService.getUri(ServiceEndpoints.CompanyDetail) + '/' + companyID.toString();
    return this._httpClient.get<ICompanyDetailModel>(url);
  }

  public editCompany(company: ICompanyEditModel): Observable<boolean> {
    const url = this._uriService.getUri(ServiceEndpoints.EditCompany);
    return this._httpClient.post<boolean>(url, company);
  }

  public addCompany(company: ICompanyCreateModel): Observable<ICompanieOverviewModel> {
    const url = this._uriService.getUri(ServiceEndpoints.AddCompany);
    return this._httpClient.post<ICompanieOverviewModel>(url, company);
  }

  //#endregion

  //#region Articles

  public getAllArticles(): Observable<IArticleOverviewModel[]> {
    const url = this._uriService.getUri(ServiceEndpoints.ArticleOverviews);
    return this._httpClient.get<IArticleOverviewModel[]>(url);
  }

  public deleteArticle(articleID: number, forced: boolean = false): Observable<boolean> {
    const url = this._uriService.getUri(ServiceEndpoints.DeleteArticle) + '/' + articleID.toString() + "?forceDeleted=" + forced;

    return this._httpClient.delete<boolean>(url);
  }

  public editArticle(article: IArticleEditModel): Observable<boolean> {
    const url = this._uriService.getUri(ServiceEndpoints.EditArticle);
    return this._httpClient.post<boolean>(url, article);
  }

  public getArticleDetails(articleID: number): Observable<IArticleDetailModel> {
    const url = this._uriService.getUri(ServiceEndpoints.ArticleDetails) + '/' + articleID.toString();
    return this._httpClient.get<IArticleDetailModel>(url);
  }

  public createArticle(article: IArticleCreateModel): Observable<IArticleOverviewModel> {
    const url = this._uriService.getUri(ServiceEndpoints.CreateArticle);
    return this._httpClient.post<IArticleOverviewModel>(url, article);
  }

  public isArticleInUse(articleID: number): Observable<boolean> {
    const url = this._uriService.getUri(ServiceEndpoints.ArticleInUse) + '/' + articleID.toString();
    return this._httpClient.get<boolean>(url);
  }

  //#endregion

  //#region ConstructionStages

  public getAllConstructionStages(): Observable<IConstructionStageOverviewModel[]> {
    const url = this._uriService.getUri(ServiceEndpoints.ConstructionStageOverviews);
    return this._httpClient.get<IConstructionStageOverviewModel[]>(url);
  }

  public deleteConstructionStage(id: number): Observable<boolean> {
    let url = this._uriService.getUri(ServiceEndpoints.DeleteConstructionStage);
    url += '/' + id.toString();

    return this._httpClient.delete<boolean>(url);
  }

  public getAllConstructionStagesForCivilWork(onlyUnboundedJob: boolean): Observable<IConstructionStageOverviewForJobsModel[]> {
    const url = this._uriService.getUri(ServiceEndpoints.ConstructionStageOverviewForCivilWork) + "?onlyUnboundedJob=" + onlyUnboundedJob;
    return this._httpClient.get<IConstructionStageOverviewForJobsModel[]>(url);
  }

  public getAllConstructionStageForProjectManagement(): Observable<IConstructionStageForProjectManagementOverviewModel[]> {
    const url = this._uriService.getUri(ServiceEndpoints.ConstructionStageOverviewForProjectManagement);
    return this._httpClient.get<IConstructionStageForProjectManagementOverviewModel[]>(url);
  }

  public searchCabinetsByQuery(query: string): Observable<ISimpleCabinetOverviewModel[]> {
    let url = this._uriService.getUri(ServiceEndpoints.SearchCabinents);
    url += "?query=" + query;
    return this._httpClient.get<ISimpleCabinetOverviewModel[]>(url);
  }

  public searchBuildingsByQuery(query: string): Observable<ISimpleBuildingOverviewModel[]> {
    let url = this._uriService.getUri(ServiceEndpoints.SearchBuildings);
    url += "?query=" + query;
    return this._httpClient.get<ISimpleBuildingOverviewModel[]>(url);
  }

  public getAllBuildingsByBranchableId(id: number): Observable<ISimpleBuildingOverviewModel[]> {
    let url = this._uriService.getUri(ServiceEndpoints.BuildingsByBranchable);
    url += '/' + id.toString();

    return this._httpClient.get<ISimpleBuildingOverviewModel[]>(url);
  }

  public createConstructionStage(model: IConstructionStageCreateModel): Observable<IConstructionStageOverviewModel> {
    let url = this._uriService.getUri(ServiceEndpoints.CreateConstructionStage);
    return this._httpClient.post<IConstructionStageOverviewModel>(url, model);
  }

  public createBranchOffJobs(constructionStageId: number): Observable<boolean> {
    let url = this._uriService.getUri(ServiceEndpoints.CreateBranchOffJobs);
    url += '/' + constructionStageId.toString();

    return this._httpClient.post<boolean>(url, {});
  }

  public getConstructionStageDetailsForCivilWork(constructionStageId: number, onlyUnbouded: boolean): Observable<IConstructionStageDetailModel<ISimpleBuildingConnenctionJobOverview>> {
    let url = this._uriService.getUri(ServiceEndpoints.ConstrunctionStageDetailForCivilWork);
    url += '/' + constructionStageId.toString();
    url += "?onlyUnboundedJob=" + onlyUnbouded;

    return this._httpClient.get<IConstructionStageDetailModel<ISimpleBuildingConnenctionJobOverview>>(url);
  }

  public getFlatsByBuldingId(id: number): Observable<ISimpleFlatOverviewModel[]> {
    let url = this._uriService.getUri(ServiceEndpoints.FlatByBuildings);
    url += '/' + id.toString();

    return this._httpClient.get<ISimpleFlatOverviewModel[]>(url);
  }

  public checkIfBuildingIsConnected(id: number): Observable<boolean> {
    let url = this._uriService.getUri(ServiceEndpoints.CheckIfBuildingIsConnected);
    url += '/' + id.toString();

    return this._httpClient.get<boolean>(url);
  }


  public addFlatToBuilding(model: IFlatCreateModel): Observable<ISimpleFlatOverviewModel> {
    const url = this._uriService.getUri(ServiceEndpoints.AddFlatToBuilding);

    return this._httpClient.post<ISimpleFlatOverviewModel>(url, model);
  }

  public getAllConstructionStagesFoInjectJobs(onlyUnboundedJob: boolean): Observable<IConstructionStageOverviewForJobsModel[]> {
    const url = this._uriService.getUri(ServiceEndpoints.ConstructionStageOverviewForInjectJobs) + "?onlyUnboundedJob=" + onlyUnboundedJob;
    return this._httpClient.get<IConstructionStageOverviewForJobsModel[]>(url);
  }

  public getConstructionStageDetailsForInjectJobs(constructionStageId: number, onlyUnbouded: boolean): Observable<IConstructionStageDetailModel<IInejectJobOverviewModel>> {
    let url = this._uriService.getUri(ServiceEndpoints.ConstrunctionStageDetailForInjectJobs);
    url += '/' + constructionStageId.toString();
    url += "?onlyUnboundedJob=" + onlyUnbouded;

    return this._httpClient.get<IConstructionStageDetailModel<IInejectJobOverviewModel>>(url);
  }

  public getAllConstructionStagesForSpliceJobs(onlyUnboundedJob: boolean): Observable<IConstructionStageOverviewForJobsModel[]> {
    const url = this._uriService.getUri(ServiceEndpoints.ConstructionStageOverviewForSpliceJobs) + "?onlyUnboundedJob=" + onlyUnboundedJob;
    return this._httpClient.get<IConstructionStageOverviewForJobsModel[]>(url);
  }

  public getConstructionStageDetailsForSpliceJobs(constructionStageId: number, onlyUnbouded: boolean): Observable<IConstructionStageDetailModel<ISpliceBranchableJobOverviewModel>> {
    let url = this._uriService.getUri(ServiceEndpoints.ConstrunctionStageDetailForSpliceJobs);
    url += '/' + constructionStageId.toString();
    url += "?onlyUnboundedJob=" + onlyUnbouded;

    return this._httpClient.get<IConstructionStageDetailModel<ISpliceBranchableJobOverviewModel>>(url);
  }


  public getAllConstructionStagesForPrepareBranchableJobs(onlyUnboundedJob: boolean): Observable<IConstructionStageOverviewForJobsModel[]> {
    const url = this._uriService.getUri(ServiceEndpoints.ConstructionStageOverviewForPrepareBranchableJobs) + "?onlyUnboundedJob=" + onlyUnboundedJob;
    return this._httpClient.get<IConstructionStageOverviewForJobsModel[]>(url);
  }

  public getConstructionStageDetailsForPrepareBranchableJobs(constructionStageId: number, onlyUnbouded: boolean): Observable<IConstructionStageDetailModel<IPrepareBranchableForSpliceJobOverviewModel>> {
    let url = this._uriService.getUri(ServiceEndpoints.ConstrunctionStageDetailForPrepareBranchableJobs);
    url += '/' + constructionStageId.toString();
    url += "?onlyUnboundedJob=" + onlyUnbouded;

    return this._httpClient.get<IConstructionStageDetailModel<IPrepareBranchableForSpliceJobOverviewModel>>(url);
  }

  public getMainCables(): Observable<IMainCableOverviewModel[]> {
    const url = this._uriService.getUri(ServiceEndpoints.MainCables);
    return this._httpClient.get<IMainCableOverviewModel[]>(url);
  }

  public updateCableTypesFromAdapter(): Observable<string> {
    const url = this._uriService.getUri(ServiceEndpoints.UpdateCableTypesFromAdapter);
    return this._httpClient.post<string>(url, null);
  }

  public updateBuildingCableInfoFromAdapter(): Observable<string> {
    const url = this._uriService.getUri(ServiceEndpoints.UpdateBuildingCableInfoFromAdapter);
    return this._httpClient.post<string>(url, null);
  }

  public updateBuildingNameFromAdapter(): Observable<string> {
    const url = this._uriService.getUri(ServiceEndpoints.UpdateBuildingNameFromAdapter);
    return this._httpClient.post<string>(url, null);
  }

  public updateBuildigsFromAdapter(): Observable<string> {
    const url = this._uriService.getUri(ServiceEndpoints.UpdateBuildigsFromAdapter);
    return this._httpClient.post<string>(url, null);
  }

  public updateCablesFromAdapter(): Observable<string> {
    const url = this._uriService.getUri(ServiceEndpoints.UpdateCablesFromAdapter);
    return this._httpClient.post<string>(url, null);
  }

  public updatePopInformation(): Observable<string> {
    const url = this._uriService.getUri(ServiceEndpoints.UpdatePopInformation);
    return this._httpClient.post<string>(url, null);
  }

  public updateMainCables(): Observable<string> {
    const url = this._uriService.getUri(ServiceEndpoints.UpdateMainCables);
    return this._httpClient.post<string>(url, null);
  }

  public updatePatchConnections(): Observable<string> {
    const url = this._uriService.getUri(ServiceEndpoints.UpdatePatchConnections);
    return this._httpClient.post<string>(url, null);
  }

  public updateSplices(): Observable<string> {
    const url = this._uriService.getUri(ServiceEndpoints.UpdateSplices);
    return this._httpClient.post<string>(url, null);
  }

  public updateMainCableForBranchable(): Observable<string> {
    const url = this._uriService.getUri(ServiceEndpoints.UpdateMainCableForBranchable);
    return this._httpClient.post<string>(url, null);
  }

  public updateAllDataFromProjectAdapter(): Observable<string> {
    const url = this._uriService.getUri(ServiceEndpoints.UpdateAllDataFromProjectAdapter);
    return this._httpClient.post<string>(url, null);
  }

  public getConstructionStageUpdateInfos(constructionStageId: number): Observable<IConstructionStageDetailsForEditModel> {
    let url = this._uriService.getUri(ServiceEndpoints.ConstrunctionStageUpdateInfos);
    url += '/' + constructionStageId.toString();

    return this._httpClient.get<IConstructionStageDetailsForEditModel>(url);
  }

  public updateConstructionStage(model: IConstructionStageUpdateModel): Observable<IConstructionStageDetailsForEditModel> {
    const url = this._uriService.getUri(ServiceEndpoints.UpdateConstrunctionStage);
    return this._httpClient.post<IConstructionStageDetailsForEditModel>(url, model);
  }


  //#endregion

  //#region Jobs

  public getBranchOffJobDetails(jobId: number): Observable<IBranchOffJobDetailModel> {
    let url = this._uriService.getUri(ServiceEndpoints.BranchOffJobDetails);
    url += '/' + jobId.toString();

    return this._httpClient.get<IBranchOffJobDetailModel>(url);
  }

  public getCollectionJobDetails(collectionJobId: number): Observable<ICollectionJobDetailsModel> {
    let url = this._uriService.getUri(ServiceEndpoints.CollectionJobDetails);
    url += '/' + collectionJobId.toString();

    return this._httpClient.get<ICollectionJobDetailsModel>(url);

  }

  public postFinishBranchoffJob(model: IFinishBranchOffJobModel): Observable<number> {
    const url = this._uriService.getUri(ServiceEndpoints.FinishBranchoffJob);
    return this._httpClient.post<number>(url, model);
  }

  public postFinishConnectBuildingJob(model: IFinishConnectBuildingModel): Observable<number> {
    const url = this._uriService.getUri(ServiceEndpoints.FinishConnectBuildingJob);
    return this._httpClient.post<number>(url, model);
  }

  public acknowledgeJob(model: IAcknowledgeJobModel): Observable<boolean> {
    const url = this._uriService.getUri(ServiceEndpoints.AcknowledgeJob);
    return this._httpClient.post<boolean>(url, model);
  }

  public discardJob(jobId: number): Observable<boolean> {
    const url = this._uriService.getUri(ServiceEndpoints.DiscardJob) + '/' + jobId.toString();
    return this._httpClient.delete<boolean>(url);
  }

  public getJobsToAcknowledge(): Observable<ISimpleJobOverview[]> {
    const url = this._uriService.getUri(ServiceEndpoints.JobsToAcknowledge);
    return this._httpClient.get<ISimpleJobOverview[]>(url);
  }

  public getOpenCustomerRequests(): Observable<IRequestOverviewModel[]> {
    const url = this._uriService.getUri(ServiceEndpoints.OpenCustomerRequests);
    return this._httpClient.get<IRequestOverviewModel[]>(url);
  }

  public deleteCustomerRequest(requestId: number): Observable<boolean> {
    let url = this._uriService.getUri(ServiceEndpoints.DeleteCustomerRequest);
    url += '/' + requestId.toString();

    return this._httpClient.delete<boolean>(url);
  }

  public getCustomerRequestDetails(requestId: number): Observable<ICustomerRequestDetails> {
    let url = this._uriService.getUri(ServiceEndpoints.CustomerRequestDetails);
    url += '/' + requestId.toString();

    return this._httpClient.get<ICustomerRequestDetails>(url);
  }

  public createHouseConnenctionJobByRequestId(requestId: number, boundLikeBranchOff = true): Observable<number> {
    let url = this._uriService.getUri(ServiceEndpoints.CreateHouseConnenctionJob);
    url += '/' + requestId.toString() + "?boundLikeBranchOff=" + boundLikeBranchOff;

    return this._httpClient.post<number>(url, {});
  }

  public getConnectBuildingJobDetails(requestId: number): Observable<IConnectBuildingJobDetailsModel> {
    let url = this._uriService.getUri(ServiceEndpoints.ConnectBuildingJobDetails);
    url += '/' + requestId.toString();

    return this._httpClient.get<IConnectBuildingJobDetailsModel>(url);
  }

  public getInjectJobDetails(requestId: number): Observable<IInjectJobDetailModel> {
    let url = this._uriService.getUri(ServiceEndpoints.InjectJobDetails);
    url += '/' + requestId.toString();

    return this._httpClient.get<IInjectJobDetailModel>(url);
  }

  public getSpliceBranchableJobDetails(requestId: number): Observable<ISpliceBranchableJobDetailModel> {
    let url = this._uriService.getUri(ServiceEndpoints.SpliceJobDetails);
    url += '/' + requestId.toString();

    return this._httpClient.get<ISpliceBranchableJobDetailModel>(url);
  }

  public getSpliceODFJobDetails(jobId: number): Observable<ISpliceODFJobDetailModel> {
    let url = this._uriService.getUri(ServiceEndpoints.SpliceODFJobDetails);
    url += '/' + jobId.toString();

    return this._httpClient.get<ISpliceODFJobDetailModel>(url);
  }

  public getPrepareBranchableJobDetails(requestId: number): Observable<IPrepareBranchableForSpliceJobDetailModel> {
    let url = this._uriService.getUri(ServiceEndpoints.PrepareBranchableJobDetails);
    url += '/' + requestId.toString();

    return this._httpClient.get<IPrepareBranchableForSpliceJobDetailModel>(url);
  }

  public postFinishInjectJob(model: IFinishInjectJobModel): Observable<number> {
    const url = this._uriService.getUri(ServiceEndpoints.FinishInjectJob);
    return this._httpClient.post<number>(url, model);
  }

  public createConnectBuildingJob(model: IConnectBuildingJobCreateModel): Observable<number> {
    const url = this._uriService.getUri(ServiceEndpoints.CreateConnectBuildingJob);
    return this._httpClient.post<number>(url, model);
  }

  public createInjectJob(model: IInjectJobCreateModel): Observable<number> {
    const url = this._uriService.getUri(ServiceEndpoints.CreateInjectJob);
    return this._httpClient.post<number>(url, model);
  }

  public bindJobs(model: IBindJobsModel): Observable<boolean> {
    const url = this._uriService.getUri(ServiceEndpoints.BindJobs);
    return this._httpClient.post<boolean>(url, model);
  }

  public createSpliceBranchBranchableJob(model: ISpliceBranchableJobCreateModel): Observable<number> {
    const url = this._uriService.getUri(ServiceEndpoints.CreateSpliceBranchBranchableJob);
    return this._httpClient.post<number>(url, model);
  }

  public createPrepareBranchableForSpliceJob(model: IPrepareBranchableForSpliceJobCreateModel): Observable<number> {
    const url = this._uriService.getUri(ServiceEndpoints.CreatePrepareBranchableForSpliceJob);
    return this._httpClient.post<number>(url, model);
  }

  public createSpliceODFJob(model: ISpliceODFJobCreateModel): Observable<number> {
    const url = this._uriService.getUri(ServiceEndpoints.CreateSpliceODFJob);
    return this._httpClient.post<number>(url, model);
  }

  public createActivationJob(model: IActivationJobCreateModel): Observable<number> {
    const url = this._uriService.getUri(ServiceEndpoints.CreateActivationJob);
    return this._httpClient.post<number>(url, model);
  }

  public spliceBranchableDetails(jobId: number): Observable<ISpliceBranchableJobDetailModel> {
    const url = this._uriService.getUri(ServiceEndpoints.SpliceBranchableDetails) + '/' + jobId;
    return this._httpClient.get<ISpliceBranchableJobDetailModel>(url);
  }

  public prepareBranchableForSpliceDetails(jobId: number): Observable<IPrepareBranchableForSpliceJobDetailModel> {
    const url = this._uriService.getUri(ServiceEndpoints.PrepareBranchableForSpliceDetails) + '/' + jobId;
    return this._httpClient.get<IPrepareBranchableForSpliceJobDetailModel>(url);
  }

  public spliceODFDetails(jobId: number): Observable<ISpliceODFJobDetailModel> {
    const url = this._uriService.getUri(ServiceEndpoints.SplicODFDetails) + '/' + jobId;
    return this._httpClient.get<ISpliceODFJobDetailModel>(url);
  }

  public spliceBranchableJob(model: ISpliceBranchableJobFinishModel): Observable<boolean> {
    const url = this._uriService.getUri(ServiceEndpoints.SpliceBranchableJob);
    return this._httpClient.post<boolean>(url, model);
  }

  public prepareBranchableForSpliceJob(model: IPrepareBranchableForSpliceJobFinishModel): Observable<boolean> {
    const url = this._uriService.getUri(ServiceEndpoints.PrepareBranchableForSpliceJob);
    return this._httpClient.post<boolean>(url, model);
  }

  public spliceODFJob(model: ISpliceODFJobFinishModel): Observable<boolean> {
    const url = this._uriService.getUri(ServiceEndpoints.SpliceODFJob);
    return this._httpClient.post<boolean>(url, model);
  }

  public checkIfBuildngHasCable(id: number): Observable<boolean> {
    const url = this._uriService.getUri(ServiceEndpoints.CheckIfBuildingHasCable) + '/' + id;
    return this._httpClient.get<boolean>(url);
  }

  public createInventoryJob(model: IInventoryJobCreateModel): Observable<boolean> {
    const url = this._uriService.getUri(ServiceEndpoints.CreateInventoryJob);
    return this._httpClient.post<boolean>(url, model);
  }

  public getInventoryOverviews(): Observable<IInventoryJobOverviewModel[]> {
    const url = this._uriService.getUri(ServiceEndpoints.GetInventoryJobOverviews);
    return this._httpClient.get<IInventoryJobOverviewModel[]>(url);
  }

  public getOpenPrepareBranchableForSpliceJobs(): Observable<ISimplePrepareBranchableForSpliceJobOverviewModel[]> {
    const url = this._uriService.getUri(ServiceEndpoints.OpenPrepareBranchableForSpliceJobs);
    return this._httpClient.get<ISimplePrepareBranchableForSpliceJobOverviewModel[]>(url);
  }

  public getOpenSpliceODFJobs(): Observable<ISimpleSpliceODFJobOverviewModel[]> {
    const url = this._uriService.getUri(ServiceEndpoints.OpenSpliceODFJobs);
    return this._httpClient.get<ISimpleSpliceODFJobOverviewModel[]>(url);
  }

  public getOpenActivationJobs(): Observable<ISimpleActivationJobOverviewModel[]> {
    const url = this._uriService.getUri(ServiceEndpoints.OpenActivationJobs);
    return this._httpClient.get<ISimpleActivationJobOverviewModel[]>(url);
  }

  public getActivationJobDetails(jobId: number): Observable<IActivationJobDetailModel> {
    const url = this._uriService.getUri(ServiceEndpoints.ActivationJobDetails) + '/' + jobId;
    return this._httpClient.get<IActivationJobDetailModel>(url);
  }

  /*    public getOpenActivationJobs(): Observable<ISimpleActivationJobOverviewModel[]> {
    return this._webServiceClient.getOpenActivationJobs();
  }*/

  public getInventoryJobDetails(id: number): Observable<IInventoryJobDetailModel> {
    const url = this._uriService.getUri(ServiceEndpoints.InventoryJobDetails) + "/" + id;
    return this._httpClient.get<IInventoryJobDetailModel>(url);
  }

  public postFinishInventoryJob(model: IInventoryFinishedJobModel): Observable<boolean> {
    const url = this._uriService.getUri(ServiceEndpoints.FinishInventoryJob);
    return this._httpClient.post<boolean>(url, model);
  }

  public getSpliceODFJobsOverview(): Observable<ISpliceODFJobOverviewModel[]> {
    const url = this._uriService.getUri(ServiceEndpoints.SpliceODFJobOverview);
    return this._httpClient.get<ISpliceODFJobOverviewModel[]>(url);
  }

  public getOpenAcitvationJobsAsOverview(): Observable<IActivationJobOverviewModel[]> {
    const url = this._uriService.getUri(ServiceEndpoints.ActivationJobOverview);
    return this._httpClient.get<IActivationJobOverviewModel[]>(url);
  }

  public postFinishActivationJob(model: IFinishActivationJobModel): Observable<number> {
    const url = this._uriService.getUri(ServiceEndpoints.FinishActivationJob);
    return this._httpClient.post<number>(url, model);
  }

  public postFinishSpliceBranchableJob(model: IFinishSpliceBranchableJobModel): Observable<number> {
    const url = this._uriService.getUri(ServiceEndpoints.FinishSpliceBranchableJob);
    return this._httpClient.post<number>(url, model);
  }

  public postFinishPrepareBranchableJob(model: IFinishPrepareBranchableJobModel): Observable<number> {
    const url = this._uriService.getUri(ServiceEndpoints.PrepareBranchableForSpliceJob);
    return this._httpClient.post<number>(url, model);
  }

  public postFinishSpliceODFJob(model: IFinishSpliceODFJobModel): Observable<number> {
    const url = this._uriService.getUri(ServiceEndpoints.FinishSpliceODFJob);
    return this._httpClient.post<number>(url, model);
  }

  public postFinishJobAdministrative(model: IFinishJobAdministrativeModel): Observable<boolean> {
    const url = this._uriService.getUri(ServiceEndpoints.FinishJobAdministrative);
    return this._httpClient.post<boolean>(url, model);
  }

  public searchBranchOffJobs(query: string): Observable<ISimpleJobOverview[]> {
    let url = this._uriService.getUri(ServiceEndpoints.SearchBranchOffJobs);
    url += "?query=" + query + "&state=" + (JobStates.Acknowledged).toString();
    return this._httpClient.get<ISimpleJobOverview[]>(url);
  }

  public postCopyFinishBranchOffToConnectBuilding(jobId: number): Observable<number> {
    let url = this._uriService.getUri(ServiceEndpoints.CopyFinishBranchOffToConnectBuilding);
    url += '/' + jobId.toString();

    return this._httpClient.post<number>(url, {});
  }

  public postCreateJobByRequestId(requestId: number): Observable<ISimpleJobOverview> {
    let url = this._uriService.getUri(ServiceEndpoints.CreateJobByRequestId);
    url += '/' + requestId.toString();

    return this._httpClient.post<ISimpleJobOverview>(url, {});
  }

  public getCollectionJoboverview(filterModel: IJobCollectionFilterModel): Observable<IJobCollectionOverviewModel[]> {
    let url = this._uriService.getUri(ServiceEndpoints.CollectionJobOverview);

    url += `?amount=${filterModel.amount.toString()}&start=${filterModel.start.toString()}`;
    if (filterModel.query) {
      url += `&query=${filterModel.query}`;
    }
    url += `&sortProperty=${filterModel.sortProperty}`;
    url += `&sortDirection=${filterModel.sortDirection}`;

    return this._httpClient.get<IJobCollectionOverviewModel[]>(url);
  }

  public deleteCollectionJob(id: number): Observable<boolean> {
    let url = this._uriService.getUri(ServiceEndpoints.DeleteCollectionJob);
    url += '/' + id.toString();

    return this._httpClient.delete<boolean>(url);
  }

  public getContactAfterSaleJobs(filter: IContactAfterFinishedFilterModel): Observable<IContactAfterSalesJobOverview[]> {
    let url = this._uriService.getUri(ServiceEndpoints.GetContactAfterSaleJobOverview);
    url += '?query=' + filter.query + '&amount=' + filter.amount + '&start=' + filter.start + '&openState=' + filter.openState;

    return this._httpClient.get<IContactAfterSalesJobOverview[]>(url);
  }

  public getContactAfterSaleJobDetails(id: number): Observable<IContactAfterSalesJobDetails> {
    let url = this._uriService.getUri(ServiceEndpoints.GetContactAfterSaleJobDetail);
    url += '/' + id.toString();

    return this._httpClient.get<IContactAfterSalesJobDetails>(url);
  }

  public finishContactAfterFinished(model: IContactAfterFinishedUnsuccessfullyModel): Observable<boolean> {
    let url = this._uriService.getUri(ServiceEndpoints.FinishContactAfterFinished);

    return this._httpClient.post<boolean>(url, model);
  }

  public createRequestFromContactCustomerJob(model: IRequestCustomerConnectionByContactModel): Observable<number> {
    let url = this._uriService.getUri(ServiceEndpoints.CreateRequestFromContactCustomerJob);
    return this._httpClient.post<number>(url, model);
  }

  // 

  //#endregion

  //#region Customers

  public searchContactsByQuery(query: string): Observable<IContactInfo[]> {
    let url = this._uriService.getUri(ServiceEndpoints.SearchContacts);
    url += "?query=" + query;
    return this._httpClient.get<IContactInfo[]>(url);
  }

  public createContact(model: IContactInfo): Observable<number> {
    const url = this._uriService.getUri(ServiceEndpoints.CreateContact);
    return this._httpClient.post<number>(url, model);
  }

  public createCustomer(model: ICustomerCreateModel): Observable<number> {
    const url = this._uriService.getUri(ServiceEndpoints.CreateCustomer);
    return this._httpClient.post<number>(url, model);
  }

  public searchContactsByQueryAndAmount(query: string, amount: number): Observable<IContactInfo[]> {
    let url = this._uriService.getUri(ServiceEndpoints.SearchContacts);
    url += "?query=" + query + "&amount=" + amount;
    return this._httpClient.get<IContactInfo[]>(url);
  }

  public getContacts(start: number, amount: number): Observable<IContactInfo[]> {
    let url = this._uriService.getUri(ServiceEndpoints.GetContacts);
    url += "?start=" + start.toString() + "&amount=" + amount.toString();
    return this._httpClient.get<IContactInfo[]>(url);
  }

  public deleteContact(id: number): Observable<Boolean> {
    let url = this._uriService.getUri(ServiceEndpoints.DeleteContact);
    url += '/' + id;
    return this._httpClient.delete<Boolean>(url);
  }

  public checkIfContactIsInUse(id: number): Observable<Boolean> {
    let url = this._uriService.getUri(ServiceEndpoints.CheckIfIsContactInUse);
    url += '/' + id;
    return this._httpClient.get<Boolean>(url);
  }

  public getContact(id: number): Observable<IContactInfo> {
    let url = this._uriService.getUri(ServiceEndpoints.GetContact) + '/' + id;
    return this._httpClient.get<IContactInfo>(url);
  }

  public editContact(contact: IContactInfo): Observable<IContactInfo> {
    let url = this._uriService.getUri(ServiceEndpoints.EditContact);
    return this._httpClient.post<IContactInfo>(url, contact);
  }

  public importContacts(info: ICSVImportInfoModel): Observable<number[]> {
    const url = this._uriService.getUri(ServiceEndpoints.ImportContacts);

    return this._httpClient.post<number[]>(url, info);
  }

  //#endregion

  //#region Files

  public uploadFile(file: File): Observable<HttpEvent<number>> {
    let url = this._uriService.getUri(ServiceEndpoints.UploadFile);

    const formData: FormData = new FormData();

    formData.append('file', file, file.name);

    const req = new HttpRequest('POST', url, file, {
      reportProgress: true
    });
    const result: Observable<HttpEvent<number>> =
      this._httpClient.post<number>(url, formData, {
        responseType: 'json',
        observe: 'events',
        reportProgress: true
      });
    return result;
    // return this._httpClient.request(req);
  }

  public searchFiles(query: string = "", start: number, amount: number): Observable<IFileOverviewModel[]> {
    let url = this._uriService.getUri(ServiceEndpoints.SearchFile) + "?query=" + query + "&start=" + start.toString() + "&amount=" + amount.toString();
    return this._httpClient.get<IFileOverviewModel[]>(url);
  }

  public getFiles(start: number, amount: number): Observable<IFileOverviewModel[]> {
    let url = this._uriService.getUri(ServiceEndpoints.GetFiles) + "?start=" + start.toString() + "&amount=" + amount.toString();
    return this._httpClient.get<IFileOverviewModel[]>(url);
  }

  public getFileDetail(id: number): Observable<IFileDetailModel> {
    let url = this._uriService.getUri(ServiceEndpoints.GetFileDetail) + "/" + id.toString();
    return this._httpClient.get<IFileDetailModel>(url);
  }

  public searchFileAccess(type: FileAccessObjects, query: string, amount: number): Observable<IFileObjectOverviewModel[]> {
    let url = this._uriService.getUri(ServiceEndpoints.SearchFileAccess) + "?type=" + type.toString() + "&query=" + query + "&amount=" + amount.toString();
    return this._httpClient.get<IFileObjectOverviewModel[]>(url);
  }

  public editFile(file: IFileEditModel): Observable<IFileDetailModel> {
    let url = this._uriService.getUri(ServiceEndpoints.EditFile);
    return this._httpClient.post<IFileDetailModel>(url, file);
  }

  public deleteFile(fileId: number): Observable<boolean> {
    let url = this._uriService.getUri(ServiceEndpoints.DeleteFile);
    url += '/' + fileId.toString();

    return this._httpClient.delete<boolean>(url);
  }

  public downloadFile(fileId: number): Observable<HttpResponse<Blob>> {
    const url = this._uriService.getUri(ServiceEndpoints.DownloadFile) + "/" + fileId.toString();
    return this._httpClient.get(url, {
      observe: 'response',
      responseType: 'blob',

    });
  }

  //#endregion

  //#region Imports

  public importProcedures(input: ICSVImportProcedureModel): Observable<ICSVImportProcedureResultModel> {
    const url = this._uriService.getUri(ServiceEndpoints.ImportProcedures);

    return this._httpClient.post<ICSVImportProcedureResultModel>(url, input);
  }

  //#endregion

  //#region Procedures

  public getProcedures(filter: IProcedureFilterModel, sorting: IProcedureSortingModel): Observable<IProcedureOverviewModel[]> {
    let url = this._uriService.getUri(ServiceEndpoints.ProcedureOverview);

    url += `?amount=${filter.amount.toString()}&start=${filter.start.toString()}`;
    if (filter.openState) {
      url += `&openState=${filter.openState}`;
    }

    if (filter.query) {
      url += `&query=${filter.query}`;
    }

    if (filter.state) {
      url += `&state=${filter.state}&stateOperator=${filter.stateOperator}`;
    }

    if (filter.buildingUnitAmount) {
      url += `&buildingUnitAmount=${filter.buildingUnitAmount}&buildingUnitAmountOperation=${filter.buildingUnitAmountOperation}`;
    }

    if (filter.type) {
      url += `&type=${filter.type}`;
    }

    if (filter.startDate) {
      url += `&startDate=${filter.startDate.toISOString()}`;
    }

    if (filter.endDate) {
      url += `&endDate=${filter.endDate.toISOString()}`;
    }

    // if (filter.asSoonAsPossibleState) {
    //   url += `&asSoonAsPossibleState=${filter.asSoonAsPossibleState}`;
    // }

    // if (filter.informSalesAfterFinishedState) {
    //   url += `&informSalesAfterFinishedState=${filter.informSalesAfterFinishedState}`;
    // }

    if (filter)

      url += `&property=${sorting.property}`;
    url += `&direction=${sorting.direction}`;

    return this._httpClient.get<IProcedureOverviewModel[]>(url);
  }

  public getConnectBuildingProcedureDetails(procedureId: number): Observable<IBuildingConnectionProcedureDetailModel> {
    let url = this._uriService.getUri(ServiceEndpoints.ConnectBuildingProcedureDetails);
    url += '/' + procedureId.toString();

    return this._httpClient.get<IBuildingConnectionProcedureDetailModel>(url);
  }

  public getConnectCustomerProcedureDetails(procedureId: number): Observable<ICustomerConnectionProcedureDetailModel> {
    let url = this._uriService.getUri(ServiceEndpoints.ConnectCustomerProcedureDetail);
    url += '/' + procedureId.toString();

    return this._httpClient.get<ICustomerConnectionProcedureDetailModel>(url);
  }

  //#endregion

  public getProtocol(filter: ProtocolFilterModel): Observable<ProtocolModel[]> {
    let url = this._uriService.getUri(ServiceEndpoints.GetProtocol) + '?amount=' + filter.amount + '&start=' + filter.start;
    if (filter.actions.length > 0) {
      let actionAsString: string = '';
      filter.actions.forEach(element => {
        actionAsString += element + '-';
      });

      url += '&actionList=' + actionAsString;
    }
    if (filter.types.length > 0) {
      let typesAsString: string = '';
      filter.types.forEach(element => {
        typesAsString += element + '-';
      });

      url += '&typeList=' + typesAsString;
    }
    if (filter.startTime) {
      url += '&startTime=' + filter.startTime.toISOString();
    }
    if (filter.endTime) {
      url += '&endTime=' + filter.endTime.toISOString();

    }

    return this._httpClient.get<ProtocolModel[]>(url);
  }

  public getMessages(filter: IMessageFilterModel): Observable<MessageModel[]> {
    let url = this._uriService.getUri(ServiceEndpoints.GetMessages) + '?amount=' + filter.amount + '&start=' + filter.start;
    if (filter.query) {
      url += '&query=' + filter.query;
    }
    if (filter.startTime) {
      url += "&startTime=" + filter.startTime;
    }
    if (filter.endTime) {
      url += "&endTime=" + filter.endTime;
    }
    if (filter.actions.length > 0) {
      let actions: string = '';
      filter.actions.forEach(element => {
        actions += element + '-';
      });
      url += "&actionList=" + actions;
    }
    if (filter.types.length > 0) {
      let value: string = '';
      filter.actions.forEach(element => {
        value += element + '-';
      });
      url += "&typeList=" + value;
    }
    if (filter.markedAsRead) {
      url += "&markedAsRead=" + filter.markedAsRead;
    }

    return this._httpClient.get<MessageModel[]>(url);
  }

  public markMessageAsRead(id: number): Observable<boolean> {
    let url = this._uriService.getUri(ServiceEndpoints.ReadMessage);
    return this._httpClient.post<boolean>(url, id);
  }


  public updateTaskOverview(): Observable<IUpdateTaskOverviewModel[]> {
    let url = this._uriService.getUri(ServiceEndpoints.UpdateTaskOverview);
    return this._httpClient.get<IUpdateTaskOverviewModel[]>(url);
  }

  public updateTaskDetails(id: number): Observable<IUpdateTaskDetailModel> {
    let url = this._uriService.getUri(ServiceEndpoints.UpdateTaskDetails);
    url += '/' + id.toString();
    return this._httpClient.get<IUpdateTaskDetailModel>(url);
  }

  public updateTaskCancelTask(id: number): Observable<Boolean> {
    let url = this._uriService.getUri(ServiceEndpoints.UpdateTaskCancelTask);
    url += '/' + id.toString();
    return this._httpClient.delete<Boolean>(url);
  }

  public updateTaskObjectForTask(taskType: UpdateTasks): Observable<IUpdateTaskObjectOverviewModel[]> {
    let url = this._uriService.getUri(ServiceEndpoints.UpdateTaskObjectForTask);
    url += '?taskType=' + taskType;
    return this._httpClient.get<IUpdateTaskObjectOverviewModel[]>(url);
  }

  public updateTaskStartTask(model: IStartUpdateTaskModel): Observable<number> {
    let url = this._uriService.getUri(ServiceEndpoints.UpdateTaskStartTask);
    return this._httpClient.post<number>(url, model);
  }

  //#region AreaPlanning

  public getBuildingAreaOverviews(): Observable<IBuildingAreaOverviewModel[]> {
    let url = this._uriService.getUri(ServiceEndpoints.GetBuildingAreaOverviews);
    return this._httpClient.get<IBuildingAreaOverviewModel[]>(url);
  }

  public getBuildingAreaDetails(id: number): Observable<IBuildingAreaDetailModel> {
    let url = this._uriService.getUri(ServiceEndpoints.GetBuildingAreaDetails);
    url += '/' + id.toString();
    return this._httpClient.get<IBuildingAreaDetailModel>(url);
  }

  public createBuildingArea(createModel: IBuildingAreaCreateModel): Observable<number> {
    let url = this._uriService.getUri(ServiceEndpoints.CreateBuildingArea);
    return this._httpClient.post<number>(url, createModel);
  }

  public createBuilding(building: IBuildingCreateModel): Observable<number> {
    let url = this._uriService.getUri(ServiceEndpoints.CreateBuilding);
    return this._httpClient.post<number>(url, building);
  }

  public editBuilding(building: IBuildingDetailModel): Observable<boolean> {
    let url = this._uriService.getUri(ServiceEndpoints.EditBuilding);
    return this._httpClient.post<boolean>(url, building);
  }

  public deleteBuilding(building: IBuildingDeleteModel): Observable<boolean> {
    let url = this._uriService.getUri(ServiceEndpoints.DeleteBuilding);
    return this._httpClient.post<boolean>(url, building);
  }

  public getBuildingDetails(id: number): Observable<IBuildingDetailModel> {
    let url = this._uriService.getUri(ServiceEndpoints.BuildingDetails);
    url += '/' + id.toString();
    return this._httpClient.get<IBuildingDetailModel>(url);
  }

  public createStreet(streetName: IStreetCreateModel): Observable<number> {
    let url = this._uriService.getUri(ServiceEndpoints.CreateStreet);
    return this._httpClient.post<number>(url, streetName);
  }

  public createEmptySpace(emptySpace: IEmptySpaceCreateModel): Observable<number> {
    let url = this._uriService.getUri(ServiceEndpoints.CreateEmptySpace);
    return this._httpClient.post<number>(url, emptySpace);
  }

  public getEmptySpaceDetails(id: number): Observable<IEmptySpaceDetailModel> {
    let url = this._uriService.getUri(ServiceEndpoints.EmptySpaceDetails);
    url += '/' + id.toString();
    return this._httpClient.get<IEmptySpaceDetailModel>(url);
  }

  public deleteStreet(id: number): Observable<boolean> {
    let url = this._uriService.getUri(ServiceEndpoints.DeleteStreet);
    url += '/' + id.toString();
    return this._httpClient.delete<boolean>(url);
  }

  public deleteEmptySpace(id: number): Observable<boolean> {
    let url = this._uriService.getUri(ServiceEndpoints.DeleteEmptySpace);
    url += '/' + id.toString();
    return this._httpClient.delete<boolean>(url);
  }

  public editStreet(model: IStreetEditModel): Observable<boolean> {
    let url = this._uriService.getUri(ServiceEndpoints.EditStreet);
    return this._httpClient.post<boolean>(url, model);
  }

  public editEmptySpace(model: IEmptySpaceEditModel): Observable<boolean> {
    let url = this._uriService.getUri(ServiceEndpoints.EditEmptySpace);
    return this._httpClient.post<boolean>(url, model);
  }

  //#endregion

}
