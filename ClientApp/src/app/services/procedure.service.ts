import { Injectable } from '@angular/core';
import { WebServiceClientService } from './web-service-client.service';
import { IProcedureOverviewModel } from '../models/service/procedure/iprocedure-overview-model';
import { Observable } from 'rxjs';
import { IProcedureFilterModel } from '../models/service/procedure/iprocedure-filter-model';
import { IBuildingConnectionProcedureDetailModel } from '../models/service/procedure/ibuilding-connection-procedure-detail-model';
import { ICustomerConnectionProcedureDetailModel } from '../models/service/procedure/icustomer-connection-procedure-detail-model';
import { ProcedureStates } from '../models/service/procedure/procedure-states.enum';
import { IProcedureSortingModel } from '../models/service/procedure/iprocedure-sorting-model';
import { ProcedureTypes } from '../models/service/procedure/procedure-types.enum';

@Injectable()
export class ProcedureService {
 

  constructor(
    private _webServiceClient: WebServiceClientService
  ) { }

  public getProcedures(filter: IProcedureFilterModel, sorting: IProcedureSortingModel): Observable<IProcedureOverviewModel[]> {
    return this._webServiceClient.getProcedures(filter, sorting);
  }

  public getConnectBuildingProcedureDetails(procedureId: number): Observable<IBuildingConnectionProcedureDetailModel> {
    return this._webServiceClient.getConnectBuildingProcedureDetails(procedureId);
  }

  public getConnectCustomerProcedureDetails(procedureId: number): Observable<ICustomerConnectionProcedureDetailModel> {
    return this._webServiceClient.getConnectCustomerProcedureDetails(procedureId);
  }

  public getAllPossiblesStates(): ProcedureStates[] {
    return [
      ProcedureStates.Imported,
      ProcedureStates.RequestCreated,
      ProcedureStates.RequestDeleted,
      ProcedureStates.RequestAcknkowledged,
      ProcedureStates.BranchOffJobCreated,
      ProcedureStates.BranchOffJobFinished,
      ProcedureStates.BranchOffJobAcknowledegd,
      ProcedureStates.ConnectBuildingJobCreated,
      ProcedureStates.ConnectBuildingJobBound,
      ProcedureStates.ConnectBuildingJobUnboud,
      ProcedureStates.ConnectBuildingJobFinished,
      ProcedureStates.ConnectBuildingJobDiscarded,
      ProcedureStates.ConnectBuildingJobAcknlowedged,
      ProcedureStates.InjectJobCreated,
      ProcedureStates.InjectJobBound,
      ProcedureStates.InjectJobUnboud,
      ProcedureStates.InjectJobFinished,
      ProcedureStates.InjectJobDiscarded,
      ProcedureStates.InjectJobAcknlowedged,
      ProcedureStates.PrepareBranchableForSpliceJobCreated,
      ProcedureStates.PrepareBranchableForSpliceJobBound,
      ProcedureStates.PrepareBranchableForSpliceJobUnboud,
      ProcedureStates.PrepareBranchableForSpliceJobFinished,
      ProcedureStates.PrepareBranchableForSpliceJobDiscarded,
      ProcedureStates.PrepareBranchableForSpliceJobAcknlowedged,
      ProcedureStates.SpliceODFJobCreated,
      ProcedureStates.SpliceODFJobBound,
      ProcedureStates.SpliceODFJobUnboud,
      ProcedureStates.SpliceODFJobFinished,
      ProcedureStates.SpliceODFJobDiscarded,
      ProcedureStates.SpliceODFJobAcknlowedged,
      ProcedureStates.SpliceBranchableJobCreated,
      ProcedureStates.SpliceBranchableJobBound,
      ProcedureStates.SpliceBranchableJobUnboud,
      ProcedureStates.SpliceBranchableJobFinished,
      ProcedureStates.SpliceBranchableJobDiscarded,
      ProcedureStates.SpliceBranchableJobAcknlowedged,
      ProcedureStates.ActivatitionJobCreated,
      ProcedureStates.ActivatitionJobBound,
      ProcedureStates.ActivatitionJobUnboud,
      ProcedureStates.ActivatitionJobFinished,
      ProcedureStates.ActivatitionJobDiscarded,
      ProcedureStates.ActivatitionJobAcknlowedged,
    ];
  }

  public getPathToProcedure(procedure: IProcedureOverviewModel): any {
    let path = 'connect-building-procedure-details';
    if (procedure.type === ProcedureTypes.CustomerConnection) {
      path = 'connect-customer-procedure-details';
    }

    return ('/member/' + path);
  }
}
