import { Observable, of } from 'rxjs';
import { IContactInfo } from './../models/service/icontact-info';
import { Injectable } from '@angular/core';
import { WebServiceClientService } from './web-service-client.service';
import { ICustomerCreateModel } from '../models/service/customers/icustomer-create-model';
import { PersonTypes } from '../models/service/person-types.enum';
import { ICSVImportInfoModel } from '../models/service/helper/icsvimport-info-model';

export function displayContactResultInternal(item?: IContactInfo): string {
  if (!item) { return '' }
  let result = '';
  if (item.type === PersonTypes.Male) {
    result += 'Herr ';
  } else {
    result += 'Frau ';
  }

  if (item.surname) {
    result += item.surname + ' ';
  }
  if (item.lastname) {
    result += item.lastname + ' ';
  }
  if (item.companyName) {
    result += '(' + item.companyName + ')';
  }

  return result;
}

@Injectable()
export class CustomerService {

  constructor(private _webServiceClient: WebServiceClientService) { }


  //#region Methods

  public displayContactResult(item?: IContactInfo): string {
    return displayContactResultInternal(item);
  }

  public searchContacts(query: string): Observable<IContactInfo[]> {
    if (!query) {
      return of(new Array<IContactInfo>());
    } else {
      return this._webServiceClient.searchContactsByQuery(query);
    }
  }

  public searchContactsWithAmount(query: string, amount: number): Observable<IContactInfo[]> {
    return this._webServiceClient.searchContactsByQueryAndAmount(query, amount);
  }

  public getContacts(start: number = 0, amount: number = 50): Observable<IContactInfo[]> {
    return this._webServiceClient.getContacts(start, amount);
  }

  public createContact(model: IContactInfo): Observable<number> {
    return this._webServiceClient.createContact(model);
  }

  public createCustomer(model: ICustomerCreateModel): Observable<number> {
    return this._webServiceClient.createCustomer(model);
  }

  public deleteContact(id: number): Observable<Boolean> {
    return this._webServiceClient.deleteContact(id);
  }

  public checkIfContactIsInUse(id: number): Observable<Boolean> {
    return this._webServiceClient.checkIfContactIsInUse(id);
  }

  public getContact(id: number): Observable<IContactInfo> {
    return this._webServiceClient.getContact(id);
  }

  public editContact(contact: IContactInfo): Observable<IContactInfo> {
    return this._webServiceClient.editContact(contact);
  }

  public importContacts(info: ICSVImportInfoModel): Observable<number[]> {
    return this._webServiceClient.importContacts(info);
  }

  //#endregion


}
