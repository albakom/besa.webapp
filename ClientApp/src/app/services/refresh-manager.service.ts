import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable()
export class RefreshManagerService {

  //#region Fields

  private _refreshRequestedSubject: Subject<boolean>;

  //#endregion

  //#region Properties

  public get refreshRequested(): Subject<boolean> {
    return this._refreshRequestedSubject;
  }

  //#endregion

  //#region Constructor

  constructor() {
    this._refreshRequestedSubject = new Subject<boolean>();

  }

  //#endregion

  //#region Methods

  public requestRefresh(): void {
    this._refreshRequestedSubject.next(true);
  }

  //#endregion

}
