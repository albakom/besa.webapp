import { IRequestCustomerConnectionByContactModel } from './../models/service/contacts-after-sale-job/irequest-customer-connection-by-contact-model';
import { IContactAfterFinishedUnsuccessfullyModel } from './../models/service/contacts-after-sale-job/icontact-after-finished-unsuccessfully-model';
import { IContactAfterSalesJobDetails } from './../models/service/contacts-after-sale-job/icontact-after-sales-job-details';
import { IContactAfterSalesJobOverview } from './../models/service/contacts-after-sale-job/icontact-after-sales-job-overview';
import { IFinishPrepareBranchableJobModel } from './../models/service/jobs/splice/ifinish-prepare-branchable-job-model';
import { Observable } from 'rxjs';
import { ISpliceBranchableJobCreateModel } from './../models/service/jobs/splice/isplice-branchable-job-create-model';
import { IInjectJobDetailModel } from './../models/service/jobs/inject/iinject-job-detail-model';
import { IBranchOffJobDetailModel } from './../models/service/jobs/ibranch-off-job-detail-model';
import { Injectable } from '@angular/core';
import { WebServiceClientService } from './web-service-client.service';
import { ICollectionJobDetailsModel } from '../models/service/jobs/icollection-job-details-model';
import { IFinishBranchOffJobModel } from '../models/service/jobs/ifinish-branch-off-job-model';
import { IAcknowledgeJobModel } from '../models/service/jobs/iacknowledge-job-model';
import { ISimpleJobOverview } from '../models/service/jobs/isimple-job-overview';
import { JobTypes } from '../models/service/jobs/job-types.enum';
import { IRequestOverviewModel } from '../models/service/jobs/irequest-overview-model';
import { ICustomerRequestDetails } from '../models/service/jobs/icustomer-request-details';
import { IConnectBuildingJobDetailsModel } from '../models/service/jobs/connect-building/iconnect-building-job-details-model';
import { IFinishConnectBuildingModel } from '../models/service/jobs/connect-building/ifinish-connect-building-model';
import { IFinishInjectJobModel } from '../models/service/jobs/inject/ifinish-inject-job-model';
import { IConnectBuildingJobCreateModel } from '../models/service/jobs/connect-building/iconnect-building-job-create-model';
import { IInjectJobCreateModel } from '../models/service/jobs/inject/iinject-job-create-model';
import { IBindJobsModel } from '../models/service/jobs/ibind-jobs-model';
import { IPrepareBranchableForSpliceJobCreateModel } from '../models/service/jobs/splice/iprepare-branchable-for-splice-job-create-model';
import { IInventoryJobCreateModel } from '../models/service/jobs/inventory/iinventory-job-create-model';
import { ISpliceODFJobCreateModel } from '../models/service/jobs/splice/isplice-odf-job-create-model';
import { IActivationJobCreateModel } from '../models/service/jobs/activation/activation-job-create-model';
import { IInventoryJobOverviewModel } from '../models/service/jobs/inventory/iinventory-job-overview-model';
import { ISimplePrepareBranchableForSpliceJobOverviewModel } from '../models/service/jobs/splice/isimple-prepare-branchable-for-splice-job-overview-model';
import { ISimpleSpliceODFJobOverviewModel } from '../models/service/jobs/splice/isimple-splice-odfjob-overview-model';
import { ISimpleActivationJobOverviewModel } from '../models/service/jobs/activation/isimple-activation-job-overview-model';
import { IInventoryJobDetailModel } from '../models/service/jobs/inventory/iinventory-job-detail-model';
import { IInventoryFinishedJobModel } from '../models/service/jobs/inventory/iinventory-finished-job-model';
import { ISpliceBranchableJobDetailModel } from '../models/service/jobs/splice/isplice-branchable-job-detail-model';
import { IPrepareBranchableForSpliceJobDetailModel } from '../models/service/jobs/splice/iprepare-branchable-for-splice-job-detail-model';
import { ISpliceODFJobOverviewModel } from '../models/service/jobs/splice/isplice-odf-job-overview-model';
import { ISpliceODFJobDetailModel } from '../models/service/jobs/splice/isplice-odf-job-detail-model';
import { IActivationJobOverviewModel } from '../models/service/jobs/activation/iactivation-job-overview-model';
import { IActivationJobDetailModel } from '../models/service/jobs/activation/iactivation-job-detail-model';
import { IFinishActivationJobModel } from '../models/service/jobs/activation/ifinish-activation-job-model';
import { IFinishSpliceBranchableJobModel } from '../models/service/jobs/splice/ifinish-splice-branchable-job-model';
import { IFinishSpliceODFJobModel } from '../models/service/jobs/splice/ifinish-splice-odfjob-model';
import { IFinishJobAdministrativeModel } from '../models/service/jobs/ifinish-job-administrative-model';
import { IJobCollectionOverviewModel } from '../models/service/jobs/overview/ijob-collection-overview-model';
import { IJobCollectionFilterModel } from '../models/service/jobs/ijob-collection-filter-model';
import { IContactAfterFinishedFilterModel } from '../models/service/contacts-after-sale-job/icontact-after-finished-filter-model';

@Injectable()
export class JobService {


  constructor(private _webServiceClient: WebServiceClientService) { }


  //#region Methods

  public getUrlByJobType(jobType: JobTypes): string {
    let url = '/member/';
    if (jobType == JobTypes.BranchOff) {
      url += 'branch-off-job';
    } else if (jobType == JobTypes.HouseConnenction) {
      url += 'connection-building-job';
    } else if (jobType == JobTypes.Inject) {
      url += 'inject-job';
    } else if (jobType == JobTypes.SpliceInBranchable) {
      url += 'splice-branchable-job';
    } else if (jobType == JobTypes.PrepareBranchableForSplice) {
      url += 'prepare-branchable-job';
    } else if (jobType == JobTypes.SpliceInPoP) {
      url += 'splice-odf-details';
    } else if (jobType == JobTypes.ActivationJob) {
      url += 'activation-job-details';
    }

    else {

    }

    return url;
  }

  public getUrlByJobIdAndType(jobid: number, jobType: JobTypes): string {
    let url = this.getUrlByJobType(jobType);
    return url + '/' + jobid.toString();
  }

  public getUrlByJob(job: ISimpleJobOverview): string {
    const url = this.getUrlByJobType(job.jobType);
    return url;
  }

  public loadBranchOffJobDetails(jobId: number): Observable<IBranchOffJobDetailModel> {
    return this._webServiceClient.getBranchOffJobDetails(jobId);
  }

  public loadCollectionJobDetails(collectionJobId: number): Observable<ICollectionJobDetailsModel> {
    return this._webServiceClient.getCollectionJobDetails(collectionJobId);
  }

  public finishBranchoffJob(model: IFinishBranchOffJobModel): Observable<number> {
    return this._webServiceClient.postFinishBranchoffJob(model);
  }

  public acknowledgeJob(model: IAcknowledgeJobModel): Observable<boolean> {
    return this._webServiceClient.acknowledgeJob(model);
  }

  public discardJob(jobId: number): Observable<boolean> {
    return this._webServiceClient.discardJob(jobId);
  }

  public loadJobsToAcknowledge(): Observable<ISimpleJobOverview[]> {
    return this._webServiceClient.getJobsToAcknowledge();
  }

  public loadOpenRequest(): Observable<IRequestOverviewModel[]> {
    return this._webServiceClient.getOpenCustomerRequests();
  }

  public deleteRequest(model: IRequestOverviewModel): Observable<boolean> {
    return this._webServiceClient.deleteCustomerRequest(model.id);
  }

  public deleteRequestById(id: number): Observable<boolean> {
    return this._webServiceClient.deleteCustomerRequest(id);
  }

  public loadRequestDetails(requestId: number): Observable<ICustomerRequestDetails> {
    return this._webServiceClient.getCustomerRequestDetails(requestId);
  }

  public createBuildingConnenctionJob(requestId: number): Observable<number> {
    return this._webServiceClient.createHouseConnenctionJobByRequestId(requestId);
  }

  public loadConnectBuildingJobDetails(jobId: number): Observable<IConnectBuildingJobDetailsModel> {
    return this._webServiceClient.getConnectBuildingJobDetails(jobId);
  }

  public finishConnectBuildingJob(model: IFinishConnectBuildingModel): Observable<number> {
    return this._webServiceClient.postFinishConnectBuildingJob(model);
  }

  public loadInjectJobDetails(jobId: number): Observable<IInjectJobDetailModel> {
    return this._webServiceClient.getInjectJobDetails(jobId);
  }

  public loadSpliceJobDetails(jobId: number): Observable<ISpliceBranchableJobDetailModel> {
    return this._webServiceClient.getSpliceBranchableJobDetails(jobId);
  }

  public loadPrepareBranchableJobDetails(jobId: number): Observable<IPrepareBranchableForSpliceJobDetailModel> {
    return this._webServiceClient.getPrepareBranchableJobDetails(jobId);
  }

  public finishInjectJob(model: IFinishInjectJobModel): Observable<number> {
    return this._webServiceClient.postFinishInjectJob(model);
  }

  public createConnectBuildingJob(model: IConnectBuildingJobCreateModel): Observable<number> {
    return this._webServiceClient.createConnectBuildingJob(model);
  }

  public createInjectJob(model: IInjectJobCreateModel): Observable<number> {
    return this._webServiceClient.createInjectJob(model);
  }

  public createPrepareBranchableForSpliceJob(model: IPrepareBranchableForSpliceJobCreateModel): Observable<number> {
    return this._webServiceClient.createPrepareBranchableForSpliceJob(model);
  }

  public bindJobs(model: IBindJobsModel): Observable<boolean> {
    return this._webServiceClient.bindJobs(model);
  }

  public checkIfBuildingHasCable(id: number): Observable<boolean> {
    return this._webServiceClient.checkIfBuildngHasCable(id);
  }

  public createSpliceBranchableJob(model: ISpliceBranchableJobCreateModel): Observable<number> {
    return this._webServiceClient.createSpliceBranchBranchableJob(model);
  }

  public createInventoryJob(model: IInventoryJobCreateModel): Observable<boolean> {
    return this._webServiceClient.createInventoryJob(model);
  }

  public createSpliceODFJob(model: ISpliceODFJobCreateModel): Observable<number> {
    return this._webServiceClient.createSpliceODFJob(model);
  }

  public createActivationJob(model: IActivationJobCreateModel): Observable<number> {
    return this._webServiceClient.createActivationJob(model);
  }

  public getInventoryOverviews(): Observable<IInventoryJobOverviewModel[]> {
    return this._webServiceClient.getInventoryOverviews();
  }

  public getOpenPrepareBranchableForSpliceJobs(): Observable<ISimplePrepareBranchableForSpliceJobOverviewModel[]> {
    return this._webServiceClient.getOpenPrepareBranchableForSpliceJobs();
  }

  public getOpenSpliceODFJobs(): Observable<ISimpleSpliceODFJobOverviewModel[]> {
    return this._webServiceClient.getOpenSpliceODFJobs();
  }

  public getOpenActivationJobs(): Observable<ISimpleActivationJobOverviewModel[]> {
    return this._webServiceClient.getOpenActivationJobs();
  }
  public getInventoryJobDetails(id: number): Observable<IInventoryJobDetailModel> {
    return this._webServiceClient.getInventoryJobDetails(id);
  }

  public finishInventoryJob(model: IInventoryFinishedJobModel): Observable<boolean> {
    return this._webServiceClient.postFinishInventoryJob(model);
  }

  public loadSpliceODFJobsOverview(): Observable<ISpliceODFJobOverviewModel[]> {
    return this._webServiceClient.getSpliceODFJobsOverview();
  }

  public loadSpliceODFJobDetails(jobId: number): Observable<ISpliceODFJobDetailModel> {
    return this._webServiceClient.getSpliceODFJobDetails(jobId);
  }

  public loadActivationJobsOverview(): Observable<IActivationJobOverviewModel[]> {
    return this._webServiceClient.getOpenAcitvationJobsAsOverview();
  }
  public loadActivationJobDetails(id: number): Observable<IActivationJobDetailModel> {
    return this._webServiceClient.getActivationJobDetails(id);
  }

  public finishActivationJob(model: IFinishActivationJobModel): Observable<number> {
    return this._webServiceClient.postFinishActivationJob(model);
  }

  public finishSpliceBranchableJob(model: IFinishSpliceBranchableJobModel): Observable<number> {
    return this._webServiceClient.postFinishSpliceBranchableJob(model);
  }

  public finishPrepareBranchableJob(model: IFinishPrepareBranchableJobModel): Observable<number> {
    return this._webServiceClient.postFinishPrepareBranchableJob(model);
  }

  public finishSpliceODFJob(model: IFinishSpliceODFJobModel): Observable<number> {
    return this._webServiceClient.postFinishSpliceODFJob(model);
  }

  public finishJobAdministrative(model: IFinishJobAdministrativeModel): Observable<boolean> {
    return this._webServiceClient.postFinishJobAdministrative(model);
  }

  public searchBranchOffJobs(query: string): Observable<ISimpleJobOverview[]> {
    return this._webServiceClient.searchBranchOffJobs(query);
  }

  public copyFinishBranchOffToConnectBuilding(jobId: number): Observable<number> {
    return this._webServiceClient.postCopyFinishBranchOffToConnectBuilding(jobId);
  }

  public createJobByRequestId(requestId: number): Observable<ISimpleJobOverview> {
    return this._webServiceClient.postCreateJobByRequestId(requestId);
  }

  public getCollectionJobOverview(filterModel: IJobCollectionFilterModel): Observable<IJobCollectionOverviewModel[]> {
    return this._webServiceClient.getCollectionJoboverview(filterModel);
  }

  public deleteCollectionJob(id: number): Observable<boolean> {
    return this._webServiceClient.deleteCollectionJob(id);
  }

  public getContactAfterSalesJobs(filter: IContactAfterFinishedFilterModel): Observable<IContactAfterSalesJobOverview[]> {
    return this._webServiceClient.getContactAfterSaleJobs(filter);
  }

  public getContactAfterSaleJobDetails(id: number): Observable<IContactAfterSalesJobDetails> {
    return this._webServiceClient.getContactAfterSaleJobDetails(id);
  }

  public finishContactAfterFinished(model: IContactAfterFinishedUnsuccessfullyModel): Observable<boolean> {
    return this._webServiceClient.finishContactAfterFinished(model);
  }

  public createRequestFromContactCustomerJob(model: IRequestCustomerConnectionByContactModel): Observable<number> {
    return this._webServiceClient.createRequestFromContactCustomerJob(model);
  }
  
  //#endregion

}
