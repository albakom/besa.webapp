import { WebServiceClientService } from './../web-service-client.service';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpRequest, HttpEvent } from '@angular/common/http';

@Injectable()
export class UploadService {

  constructor(
    private _webserviceclient: WebServiceClientService
  ) { }

  

  public postFile(fileToUpload: File): Observable<HttpEvent<number>> {
    
    return this._webserviceclient.uploadFile(fileToUpload);
  }

}
