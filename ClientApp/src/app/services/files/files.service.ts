import { FileAccessObjects } from './../../models/service/files/file-access-objects.enum';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { WebServiceClientService } from '../web-service-client.service';
import { IFileOverviewModel } from '../../models/service/files/ifile-overview-model';
import { IFileDetailModel } from '../../models/service/files/ifile-detail-model';
import { IFileObjectOverviewModel } from '../../models/service/files/ifile-object-overview-model';
import { IFileEditModel } from '../../models/service/files/ifile-edit-model';
import { HttpResponse } from '@angular/common/http';
import { saveAs } from 'file-saver/FileSaver';

@Injectable()
export class FilesService {

  constructor(
    private _webservice: WebServiceClientService
  ) { }

  public searchFile(query: string = "", start: number = 0, amount: number = 30): Observable<IFileOverviewModel[]> {
    return this._webservice.searchFiles(query, start, amount);
  }

  public getFiles(start: number = 0, amount: number = 30): Observable<IFileOverviewModel[]> {
    return this._webservice.getFiles(start, amount);
  }

  public getFileDetail(id: number): Observable<IFileDetailModel> {
    return this._webservice.getFileDetail(id);
  }

  public searchAccess(type: FileAccessObjects, query: string, amount: number): Observable<IFileObjectOverviewModel[]> {
    return this._webservice.searchFileAccess(type, query, amount);
  }

  public editFile(file: IFileEditModel): Observable<IFileDetailModel> {
    return this._webservice.editFile(file);
  }

  public deleteFile(fileId: number): Observable<boolean> {
    return this._webservice.deleteFile(fileId);
  }

    public downloadFile(fileId: number) {
    this._webservice.downloadFile(fileId).subscribe(res => {
      const fileName = this.getFileNameFromResponseContentDisposition(res);
      this.saveFile(res.body, fileName);
    })    
  }

  public saveFile(blobContent: Blob, fileName: string): void {
    const blob = new Blob([blobContent], { type:  blobContent.type });
   saveAs(blob, fileName);
  }

  public getFileNameFromResponseContentDisposition(res: HttpResponse<Blob>): string {
    const contentDisposition = res.headers.get('Content-Disposition') || '';
    const matches = /filename=([^;]+)/ig.exec(contentDisposition);
   if(!matches) { return '' };
   
    const fileName = (matches[1] || 'untitled').trim();
    return fileName;
  }

}
