import { IUpdateTaskOverviewModel } from './../../models/service/tasks/iupdate-task-overview-model';
import { Injectable } from '@angular/core';
import { WebServiceClientService } from '../web-service-client.service';
import { Observable } from 'rxjs';
import { IUpdateTaskDetailModel } from '../../models/service/tasks/iupdate-task-detail-model';
import { UpdateTasks } from '../../models/service/tasks/update-tasks.enum';
import { IUpdateTaskObjectOverviewModel } from '../../models/service/tasks/iupdate-task-object-overview-model';
import { IStartUpdateTaskModel } from '../../models/service/tasks/istart-update-task-model';

@Injectable({
  providedIn: 'root'
})
export class UpdateTaskService {

  constructor(private _service: WebServiceClientService) { }

  public getOverview(): Observable<IUpdateTaskOverviewModel[]> {
    return this._service.updateTaskOverview();
  }

  public getDetails(id: number): Observable<IUpdateTaskDetailModel> {
    return this._service.updateTaskDetails(id);
  }

  public cancelTask(id: number): Observable<Boolean> {
    return this._service.updateTaskCancelTask(id);
  }

  public getObjectForTask(taskType: UpdateTasks): Observable<IUpdateTaskObjectOverviewModel[]> {
    return this._service.updateTaskObjectForTask(taskType);
  }

  public startTask(model: IStartUpdateTaskModel): Observable<number> {
    return this._service.updateTaskStartTask(model);
  }
}
