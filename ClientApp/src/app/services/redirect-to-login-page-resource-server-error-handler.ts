import { OAuthResourceServerErrorHandler } from "angular-oauth2-oidc";
import { HttpResponse } from "@angular/common/http";
import { Observable ,  throwError as _throw ,  from as fromPromise } from "rxjs";
import { Injectable } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { LoginService } from "./login.service";

@Injectable()
export class RedirectToLoginPageResourceServerErrorHandler implements OAuthResourceServerErrorHandler {

    constructor(private _router: Router,
        private _activeRoute: ActivatedRoute,
        private _loginService: LoginService
    ) {

    }

    handleError(err: HttpResponse<any>): Observable<any> {
        if (err.status == 401) {
            this._loginService.redirectUri = document.location.href;
            return fromPromise<any>(this._router.navigate(['/login']));
        } else {
            return _throw(err);
        }
    }

}