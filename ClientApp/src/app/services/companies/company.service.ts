import { ICompanieOverviewModel } from './../../models/service/icompanie-overview-model';
import {WebServiceClientService} from '../web-service-client.service';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ICompanyDetailModel } from '../../models/service/companies/icompany-detail-model';
import { ICompanyEditModel } from '../../models/service/companies/icompany-edit-model';
import { ICompanyCreateModel } from '../../models/service/companies/icompany-create-model';
import { ISimpleEmployeeModel } from '../../models/service/companies/isimple-employee-model';

@Injectable()
export class CompanyService {

  constructor(
    private _webServiceClient: WebServiceClientService,
  ) { }

  public loadCompanies(): Observable<ICompanieOverviewModel[]> {
    return this._webServiceClient.getAllCompanies();
  }

  public deleteCompany(companyID: number): Observable<Boolean> {
    return this._webServiceClient.deleteCompany(companyID);
  }

  public getCompanyDetail(companyID: number): Observable<ICompanyDetailModel> {
    return this._webServiceClient.getCompanyDetail(companyID);
  }

  public getAllEmployees(): Observable<ISimpleEmployeeModel[]> {
    return this._webServiceClient.getAllSimpleUsers();
  }

  public editCompany(company: ICompanyEditModel): Observable<boolean> {
    return this._webServiceClient.editCompany(company);
  }

  public addCompany(company: ICompanyCreateModel): Observable<ICompanieOverviewModel> {
    return this._webServiceClient.addCompany(company);
  }

}
