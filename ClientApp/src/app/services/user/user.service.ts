import { IUserEditModel } from './../../models/service/user/iuser-edit-model';
import { WebServiceClientService } from '../web-service-client.service';
import { Injectable } from '@angular/core';
import { IUserOverviewModel } from '../../models/service/user/iuser-overview-model';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable()
export class UserService {
  private users: IUserOverviewModel[] = new Array();

  constructor(
    private _webServiceClient: WebServiceClientService,
  ) { }

  public loadUsers(): Observable<IUserOverviewModel[]> {
    return this._webServiceClient.getAllUsers().pipe(map((result) => {
      result.forEach(element => {
        this.users.push(element);
      });
      return result;
    }));
  }

  public deleteUser(userID: number): Observable<Boolean> {
    return this._webServiceClient.deleteUser(userID);
  }

  public getUser(userID: number): Observable<IUserEditModel> {
    return this._webServiceClient.dataForEditUser(userID);
  }

  // public getCompanyDetail(companyID: number): Observable<ICompanyDetailModel> {
  //   return this._webServiceClient.getCompanyDetail(companyID);
  // }

  // public getAllEmployees(): Observable<ISimpleEmployeeModel[]> {
  //   return this._webServiceClient.getAllSimpleUsers();
  // }

  public editUser(user: IUserEditModel): Observable<boolean> {
    return this._webServiceClient.editUser(user);
  }

  // public addCompany(company: ICompanyCreateModel): Observable<ICompanieOverviewModel> {
  //   return this._webServiceClient.addCompany(company);
  // }
}
