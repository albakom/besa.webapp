import { IPrepareBranchableForSpliceJobOverviewModel } from './../models/service/jobs/splice/iprepare-branchable-for-splice-job-overview-model';
import { ISimpleBuildingConnenctionJobOverview } from '../models/service/jobs/overview/isimple-building-connenction-job-overview';
import { ISimpleBuildingOverviewModel } from './../models/service/construction-stage/isimple-building-overview-model';
import { Injectable } from '@angular/core';
import { WebServiceClientService } from './web-service-client.service';
import { Observable, of } from 'rxjs';
import { IConstructionStageOverviewModel } from '../models/service/construction-stage/iconstruction-stage-overview-model';
import { ISimpleCabinetOverviewModel } from '../models/service/construction-stage/isimple-cabinet-overview-model';
import { IConstructionStageCreateModel } from '../models/service/construction-stage/iconstruction-stage-create-model';
import { IConstructionStageForProjectManagementOverviewModel } from '../models/service/construction-stage/iconstruction-stage-for-project-management-overview-model';
import { ISimpleFlatOverviewModel } from '../models/service/flats/isimple-flat-overview-model';
import { IFlatCreateModel } from '../models/service/flats/iflat-create-model';
import { IContactInfo } from '../models/service/icontact-info';
import { IConstructionStageDetailModel } from '../models/service/construction-stage/iconstruction-stage-detail-for-model';
import { IConstructionStageOverviewForJobsModel } from '../models/service/construction-stage/iconstruction-stage-overview-for-jobs-model';
import { IInejectJobOverviewModel } from '../models/service/jobs/inject/iinject-job-overview-model';
import { IMainCableOverviewModel } from '../models/service/splice/imain-cable-overview-model';
import { ISpliceBranchableJobOverviewModel } from '../models/service/jobs/splice/isplice-branchable-job-overview-model';
import { IConstructionStageDetailsForEditModel } from '../models/service/construction-stage/iconstruction-stage-details-for-edit-model';
import { IConstructionStageUpdateModel } from '../models/service/construction-stage/Iconstruction-stage-update-model';

@Injectable()
export class ConstructionStageService {

  //#region Constructor

  constructor(
    private _webServiceClient: WebServiceClientService
  ) { }

  //#endregion

  //#region Mehtods

  public loadOverview(): Observable<IConstructionStageOverviewModel[]> {
    return this._webServiceClient.getAllConstructionStages();
  }

  public deleteConstructionStage(model: IConstructionStageOverviewModel): Observable<boolean> {
    return this._webServiceClient.deleteConstructionStage(model.id);

  }

  public loadOverviewForProjectManagement(): Observable<IConstructionStageForProjectManagementOverviewModel[]> {
    return this._webServiceClient.getAllConstructionStageForProjectManagement();
  }

  public loadOverviewForCivilWork(onlyUnbouded: boolean): Observable<IConstructionStageOverviewForJobsModel[]> {
    return this._webServiceClient.getAllConstructionStagesForCivilWork(onlyUnbouded);
  }

  public loadBuldingsByBrancableId(branchableId: number): Observable<ISimpleBuildingOverviewModel[]> {
    return this._webServiceClient.getAllBuildingsByBranchableId(branchableId);
  }

  public searchCabinents(query: string): Observable<ISimpleCabinetOverviewModel[]> {
    if (!query) {
      return of(new Array<ISimpleCabinetOverviewModel>());
    } else {
      return this._webServiceClient.searchCabinetsByQuery(query);
    }
  }

  public searchStreets(query: string): Observable<ISimpleBuildingOverviewModel[]> {
    if (!query) {
      return of(new Array<ISimpleBuildingOverviewModel>());
    } else {
      return this._webServiceClient.searchBuildingsByQuery(query);
    }
  }

  public createConstructionStage(model: IConstructionStageCreateModel): Observable<IConstructionStageOverviewModel> {
    return this._webServiceClient.createConstructionStage(model);
  }

  public createBranchOffJobs(constructionStageId: number): Observable<boolean> {
    return this._webServiceClient.createBranchOffJobs(constructionStageId);
  }

  public loadDetailsForCivilWork(constructionStageId: number, onlyUnbouded: boolean): Observable<IConstructionStageDetailModel<ISimpleBuildingConnenctionJobOverview>> {
    return this._webServiceClient.getConstructionStageDetailsForCivilWork(constructionStageId, onlyUnbouded);
  }

  public loadFlatsByBuilding(building: ISimpleBuildingOverviewModel): Observable<ISimpleFlatOverviewModel[]> {
    return this._webServiceClient.getFlatsByBuldingId(building.id);
  }

  public checkIfBuildingIsConnected(building: ISimpleBuildingOverviewModel): Observable<boolean> {
    return this._webServiceClient.checkIfBuildingIsConnected(building.id);
  }


  public creatFlat(model: IFlatCreateModel): Observable<ISimpleFlatOverviewModel> {
    return this._webServiceClient.addFlatToBuilding(model);
  }

  public loadOverviewForInject(onlyUnbouded: boolean): Observable<IConstructionStageOverviewForJobsModel[]> {
    return this._webServiceClient.getAllConstructionStagesFoInjectJobs(onlyUnbouded);
  }

  public loadDetailsForInjectJobs(constructionStageId: number, onlyUnbouded: boolean): Observable<IConstructionStageDetailModel<IInejectJobOverviewModel>> {
    return this._webServiceClient.getConstructionStageDetailsForInjectJobs(constructionStageId, onlyUnbouded);
  }

  public loadOverviewForSplice(onlyUnbouded: boolean): Observable<IConstructionStageOverviewForJobsModel[]> {
    return this._webServiceClient.getAllConstructionStagesForSpliceJobs(onlyUnbouded);
  }

  public loadDetailsForSpliceJobs(constructionStageId: number, onlyUnbouded: boolean): Observable<IConstructionStageDetailModel<ISpliceBranchableJobOverviewModel>> {
    return this._webServiceClient.getConstructionStageDetailsForSpliceJobs(constructionStageId, onlyUnbouded);
  }

  public loadOverviewForPrepareBranchable(onlyUnbouded: boolean): Observable<IConstructionStageOverviewForJobsModel[]> {
    return this._webServiceClient.getAllConstructionStagesForPrepareBranchableJobs(onlyUnbouded);
  }

  public loadDetailsForPrepareBranchableJobs(constructionStageId: number, onlyUnbouded: boolean): Observable<IConstructionStageDetailModel<IPrepareBranchableForSpliceJobOverviewModel>> {
    return this._webServiceClient.getConstructionStageDetailsForPrepareBranchableJobs(constructionStageId, onlyUnbouded);
  }

  public loadMainCables(): Observable<IMainCableOverviewModel[]> {
    return this._webServiceClient.getMainCables();
  }

  public updateCableTypesFromAdapter(): Observable<string> {
    return this._webServiceClient.updateCableTypesFromAdapter();
  }

  public updateBuildingCableInfoFromAdapter(): Observable<string> {
    return this._webServiceClient.updateBuildingCableInfoFromAdapter();
  }

  public updateBuildingNameFromAdapter(): Observable<string> {
    return this._webServiceClient.updateBuildingNameFromAdapter();
  }

  public updateBuildigsFromAdapter(): Observable<string> {
    return this._webServiceClient.updateBuildigsFromAdapter();
  }

  public updateCablesFromAdapter(): Observable<string> {
    return this._webServiceClient.updateCablesFromAdapter();
  }

  public updatePopInformation(): Observable<string> {
    return this._webServiceClient.updatePopInformation();
  }

  public updateMainCables(): Observable<string> {
    return this._webServiceClient.updateMainCables();
  }

  public updatePatchConnections(): Observable<string> {
    return this._webServiceClient.updatePatchConnections();
  }

  public updateSplices(): Observable<string> {
    return this._webServiceClient.updateSplices();
  }

  public updateMainCableForBranchable(): Observable<string> {
    return this._webServiceClient.updateMainCableForBranchable();
  }

  public updateAllDataFromProjectAdapter(): Observable<string> {
    return this._webServiceClient.updateAllDataFromProjectAdapter();
  }

  public loadDetailsForEdit(id: number): Observable<IConstructionStageDetailsForEditModel> {
    return this._webServiceClient.getConstructionStageUpdateInfos(id);
  }

  public updateConstructionStage(model: IConstructionStageUpdateModel): Observable<IConstructionStageDetailsForEditModel> {
    return this._webServiceClient.updateConstructionStage(model);
  }
  
  //#endregion

}
