import { Injectable } from '@angular/core';
import { WebServiceClientService } from './web-service-client.service';
import { Observable } from 'rxjs';
import { ICSVImportProcedureModel } from '../models/service/import/icsvimport-procedure-model';
import { ICSVImportProcedureResultModel } from '../models/service/import/icsvimport-procedure-result-model';

@Injectable()
export class ImportService {

  constructor(
    private _webServiceClient: WebServiceClientService
  ) { }

  public importProcedures(input: ICSVImportProcedureModel): Observable<ICSVImportProcedureResultModel> {
    return this._webServiceClient.importProcedures(input);
  }
}
