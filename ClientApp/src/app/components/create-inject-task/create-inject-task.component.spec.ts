import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateInjectTaskComponent } from './create-inject-task.component';

describe('CreateInjectTaskComponent', () => {
  let component: CreateInjectTaskComponent;
  let fixture: ComponentFixture<CreateInjectTaskComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateInjectTaskComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateInjectTaskComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
