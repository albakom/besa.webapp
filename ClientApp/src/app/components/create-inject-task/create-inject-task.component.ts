import { Component, OnInit, OnDestroy } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';
import { IContactInfo } from '../../models/service/icontact-info';
import { ISimpleBuildingOverviewModel } from '../../models/service/construction-stage/isimple-building-overview-model';
import { Subject } from 'rxjs';
import { FormBasedPage } from '../../pages/base/form-based-page';
import { MatSnackBar, MatDialog, MatAutocompleteSelectedEvent } from '@angular/material';
import { JobService } from '../../services/job.service';
import { CustomerService } from '../../services/customer.service';
import { FormBuilder, Validators } from '@angular/forms';
import { ConstructionStageService } from '../../services/construction-stage.service';
import { debounceTime, distinctUntilChanged, switchMap, finalize } from 'rxjs/operators';
import { CreateInjectJobErrorMessages } from '../../models/error-messages/create-inject-job-error-message';
import { FormErrorMessage } from '../../models/helper/form-error-message';
import { ICreateContactDialogOptions } from '../../dialogs/create-contact-dialog/icreate-contact-dialog-options';
import { CreateContactDialogComponent } from '../../dialogs/create-contact-dialog/create-contact-dialog.component';
import { PersonTypes } from '../../models/service/person-types.enum';
import { IInjectJobCreateModel } from '../../models/service/jobs/inject/iinject-job-create-model';

@Component({
  selector: 'besa-create-inject-task',
  templateUrl: './create-inject-task.component.html',
  styleUrls: ['./create-inject-task.component.scss'],
  animations: fuseAnimations,
})
export class CreateInjectTaskComponent extends FormBasedPage implements OnInit, OnDestroy {

  //#region Properties

  public buildingResult: ISimpleBuildingOverviewModel[];
  public customerResult: IContactInfo[];

  public createTaskInProgress: boolean;

  //#endregion

  //#region Fields

  private _buildingSearchTerm: Subject<string>;
  private _customerSearchTerm: Subject<string>;

  private _ignoreCustomerSearchEvent: boolean;
  private _currentBulding: ISimpleBuildingOverviewModel;

  //#endregion

  constructor(
    private _snachBar: MatSnackBar,
    private _dialog: MatDialog,
    private _customerService: CustomerService,
    private _jobService: JobService,
    private _fb: FormBuilder,
    private _service: ConstructionStageService) {
    super();

    this._buildingSearchTerm = new Subject<string>();
    this._customerSearchTerm = new Subject<string>();

    super.addSubscription(this._buildingSearchTerm.pipe(
      debounceTime(150),
      distinctUntilChanged(),
      switchMap((item) => {
        return this._service.searchStreets(item);
      })
    ).subscribe((items) => {
      this.buildingResult = items;
    }));

    super.addSubscription(this._customerSearchTerm.pipe(
      debounceTime(150),
      distinctUntilChanged(),
      switchMap((item) => {
        return this._customerService.searchContacts(item);
      })
    ).subscribe((items) => {
      this.customerResult = items;
    }));


    this.prepare();

    super.initForm(
      this._fb.group({
        searchTerms: this._fb.group(
          {
            buildingTermValue: ['', []],
            searchCustomermValue: ['', []],
          }
        ),
        buildingId: [null, [Validators.required]],
        customerContactId: [0, [Validators.required]],
      }));

    const fieldsToSubscripeChanges: { [name: string]: Subject<string> } = {
      'buildingTermValue': this._buildingSearchTerm,
      'searchCustomermValue': this._customerSearchTerm,
    };

    for (const key in fieldsToSubscripeChanges) {
      if (fieldsToSubscripeChanges.hasOwnProperty(key)) {
        const element = fieldsToSubscripeChanges[key];

        super.addSubscription(this.form.get('searchTerms').get(key).valueChanges.subscribe((value: string) => {
          if (this._ignoreCustomerSearchEvent === true) { return; }
          if (typeof (value) === 'string') {
            element.next(value);
          }
        }));
      }
    }
  }

  ngOnInit() {
  }

  ngOnDestroy() {
    super.unsubcripeAll();
  }

  private prepare(): void {
    this.customerResult = new Array();
    this._currentBulding = null;

    if (this.form) {
      this.form.reset();
      this.form.setValue(
        {
          searchTerms: {
            buildingTermValue: '',
            searchCustomermValue: '',
          },
          buildingId: null,
          customerContactId: null,
        }
      );
    }
  }

  //#region Methods

  protected getErrorMessages(): FormErrorMessage[] {
    return CreateInjectJobErrorMessages;
  }

  public buildingSelected(eventInfo: MatAutocompleteSelectedEvent): void {
    const building: ISimpleBuildingOverviewModel = eventInfo.option.value;
    this._currentBulding = building;
    this.form.patchValue({ buildingId: building.id });
  }

  public contactSelected(eventInfo: MatAutocompleteSelectedEvent): void {
    const contact: IContactInfo = eventInfo.option.value;
    this.form.patchValue({ customerContactId: contact.id });
  }

  public openCreateContactDialog(): void {
    const options: ICreateContactDialogOptions = {};
    if (this._currentBulding) {
      const splitIndex = this._currentBulding.streetName.lastIndexOf(' ');
      if (splitIndex > 0) {
        const streetName = this._currentBulding.streetName.substr(0, splitIndex);
        const houseNumber = this._currentBulding.streetName.substr(splitIndex + 1);

        options.addressToCopy = {
          city: 'Teterow',
          postalCode: '17166',
          street: streetName,
          streetNumber: houseNumber,
        };
      }
    }

    const ref = this._dialog.open<CreateContactDialogComponent>(CreateContactDialogComponent, { data: options });
    ref.afterClosed().subscribe((contact: IContactInfo) => {
      if (contact) {
        this._ignoreCustomerSearchEvent = true;

        if (this.customerResult.length > 0) {
          this.customerResult.splice(0, this.customerResult.length);
        }

        this.customerResult.push(contact);
        this.form.patchValue({ customerContactId: contact.id });
        this.form.get('searchTerms').patchValue({ searchCustomermValue: this.displayContactResult(contact) });
        this._ignoreCustomerSearchEvent = false;
      }
    });
  }

  public displayBuildingResult(item?: ISimpleBuildingOverviewModel): string {
    return item ? item.streetName : '';
  }

  public displayContactResult(item?: IContactInfo): string {
    if (!item) { return '' }
    let result = '';
    if (item.type === PersonTypes.Male) {
      result += 'Herr ';
    } else {
      result += 'Frau ';
    }

    if (item.surname) {
      result += item.surname + ' ';
    }
    if (item.lastname) {
      result += item.lastname + ' ';
    }

    if (item.companyName) {
      result += '(' + item.companyName + ')';
    }

    return result;
  }

  public createInjectJob(): void {

    if (this.createTaskInProgress === true) { return; }

    this.createTaskInProgress = true;
    this._snachBar.open('Aufgabe wird erstellt', 'In Bearbeitung', { duration: -1 });
    const value: IInjectJobCreateModel = this.form.value;

    this._jobService.createInjectJob(value).pipe(finalize(() => {
      this.createTaskInProgress = false;
    })).subscribe((result) => {
      this._snachBar.dismiss();
      if (result > 0) {
        this._snachBar.open('Aufgabe erfolgreich erstellt', 'Erfolg', { duration: 4500 });
        this.prepare();
      } else {
        this._snachBar.open('Einblasaufgabe konnte nicht erstellt werden', 'Fehler', { duration: 4500 });
      }
    }, (err) => {
      this._snachBar.dismiss();
      this._snachBar.open('Einblasaufgabe konnte nicht erstellt werden', 'Fehler', { duration: 4500 });
    });
  }

  //#endregion

}
