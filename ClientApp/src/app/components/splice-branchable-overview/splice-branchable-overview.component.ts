import { Component, OnInit, Input } from '@angular/core';
import { ISpliceEntryModel } from '../../models/service/jobs/splice/isplice-entry-model';
import { MatSelectionListChange, AUTOCOMPLETE_OPTION_HEIGHT } from '@angular/material';
import { SpliceBranchableOverviewDataSource } from './splice-branchable-overview-data-source';
import { SpliceTypes } from '../../models/service/construction-stage/splice-types.enum';

@Component({
  selector: 'besa-splice-branchable-overview',
  templateUrl: './splice-branchable-overview.component.html',
  styleUrls: ['./splice-branchable-overview.component.scss']
})
export class SpliceBranchableOverviewComponent implements OnInit {

  private _splices: ISpliceEntryModel[];

  @Input()
  public set splices(input: ISpliceEntryModel[]) {
    this._splices = input;
    if (input) {

      this.possibleSpliceTypes = new Array();
      this.possibleBundles = new Array();
      this.possibleNames = new Array();

      input.forEach(item => {
        if (this.possibleSpliceTypes.indexOf(item.type) < 0) {
          this.possibleSpliceTypes.push(item.type);
        }
        if (this.possibleBundles.indexOf(item.firstFiber.bundleNumber) < 0) {
          this.possibleBundles.push(item.firstFiber.bundleNumber);
        }
        if (this.possibleNames.indexOf(item.firstFiber.name) < 0) {
          this.possibleNames.push(item.firstFiber.name);
        }
        if (item.secondFiber && this.possibleNames.indexOf(item.secondFiber.name) < 0) {
          this.possibleNames.push(item.secondFiber.name);
        }

      });

      this.possibleBundles.sort((a, b) => a === b ? 0 : (a < b ? - 1 : 1));
      this.possibleNames.sort();

      this.spliceDataSource = new SpliceBranchableOverviewDataSource(input);
    }
  }

  public get splices(): ISpliceEntryModel[] {
    return this._splices;
  }

  public spliceDataSource: SpliceBranchableOverviewDataSource;

  public columnsToDisplay: string[];
  public possibleColumns = [
    'firstCableName',
    'firstFiber',
    'firstFiberNumber',
    'firstFiberColor',
    'secondCableName',
    'secondFiber',
    'secondFiberNumber',
    'secondFiberColor',
    'spliceNumber',
    'spliceTray',
    'spliceType',
  ];

  public possibleSpliceTypes: SpliceTypes[]
  public possibleBundles: number[];
  public possibleNames: string[];

  private _spliceTypeToName: { [type: number]: string };
  private _niceColumnNames: { [type: string]: string } = {
    'firstCableName' : 'Kabelname (1. Faser)',
    'firstFiber': 'Bezeichnung (1. Faser)',
    'firstFiberNumber' : 'Numerierung (1. Faser)',
    'firstFiberColor': 'Farbe (1. Faser)',
    'secondCableName':  'Kabelname (2. Faser)',
    'secondFiber': 'Bezeichnung (2. F2aser)',
    'secondFiberNumber': 'Numerierung (1. Faser)',
    'secondFiberColor': 'Farbe (2. Faser)',
    'spliceNumber': 'Spleißplatz',
    'spliceTray': 'Spleißkassette',
    'spliceType': 'Spleißart',
  }

  constructor() {
    this.columnsToDisplay = new Array();

    this.possibleColumns.forEach(element => {
      this.columnsToDisplay.push(element);
    });

    this._spliceTypeToName = {};
    this._spliceTypeToName[SpliceTypes.Unkown] = 'Unbekannt';
    this._spliceTypeToName[SpliceTypes.Buffer] = 'Reserve';
    this._spliceTypeToName[SpliceTypes.LoopTouched] = 'Geschnitten';
    this._spliceTypeToName[SpliceTypes.LoopUntouched] = 'Ungeschnitten';
    this._spliceTypeToName[SpliceTypes.Normal] = 'Spleiß';
    this._spliceTypeToName[SpliceTypes.Deposit] = 'Ablage';

  }

  ngOnInit() {
  }

  public columnsChanged(event: MatSelectionListChange): void {
    const column: string = event.option.value;
    const indexOfItem: number = this.columnsToDisplay.indexOf(column);
    const wishedIndex: number = this.possibleColumns.indexOf(column);

    if (event.option.selected === true) {
      if (indexOfItem < 0) {

        let indexToInsert = 0;
        for (let index = 0; index < this.columnsToDisplay.length; index++) {
          const column = this.columnsToDisplay[index];
          const expectedIndex = this.possibleColumns.indexOf(column);
          if (expectedIndex > wishedIndex) { break; }
          indexToInsert += 1;
        }

        this.columnsToDisplay.splice(indexToInsert, 0, column);
      }
    } else {
      if (indexOfItem >= 0) {
        this.columnsToDisplay.splice(indexOfItem, 1);
      }
    }
  }

  private getValues<T>(event: MatSelectionListChange): T[] {
    const items: T[] = new Array();

    event.source.options.forEach(item => {
      if (item.selected === true) {
        items.push(item.value);
      }
    });

    return items;
  }

  public typesChanged(event: MatSelectionListChange): void {
    const items: SpliceTypes[] = this.getValues(event);
    this.spliceDataSource.filterByType(items);
  }

  public bundleChanged(event: MatSelectionListChange): void {
    const items: number[] = this.getValues(event);
    this.spliceDataSource.filterByBundle(items);
  }

  public nameChanged(event: MatSelectionListChange): void {
    const items: string[] = this.getValues(event);
    this.spliceDataSource.filterByName(items);
  }

  //#region View-Helper

  public getSpliceTypeName(input: SpliceTypes): string {
    return this._spliceTypeToName[input];
  }

  public getNiceColumName(input: string): string {
    return this._niceColumnNames[input];
  }

  //#endregion

}
