import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SpliceBranchableOverviewComponent } from './splice-branchable-overview.component';

describe('SpliceBranchableOverviewComponent', () => {
  let component: SpliceBranchableOverviewComponent;
  let fixture: ComponentFixture<SpliceBranchableOverviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SpliceBranchableOverviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpliceBranchableOverviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
