import { ISpliceEntryModel } from "../../models/service/jobs/splice/isplice-entry-model";
import { CollectionViewer, DataSource } from "@angular/cdk/collections";
import { Observable, BehaviorSubject } from "rxjs";
import { SpliceTypes } from "../../models/service/construction-stage/splice-types.enum";

export class SpliceBranchableOverviewDataSource extends DataSource<ISpliceEntryModel>
{
    private _collectionSubject: BehaviorSubject<ISpliceEntryModel[]>
    private _filterTypes: SpliceTypes[];
    private _filterBundles: number[];
    private _filterNames: string[];

    constructor(private _entries: ISpliceEntryModel[]) {
        super();

        this._collectionSubject = new BehaviorSubject<ISpliceEntryModel[]>(_entries);

        this._filterTypes = new Array();
        this._filterBundles = new Array();
        this._filterNames = new Array();
    }

    public connect(collectionViewer: CollectionViewer): Observable<ISpliceEntryModel[]> {
        return this._collectionSubject;
    }

    public disconnect(collectionViewer: CollectionViewer): void {
    }

    public filter(): void {
        const result: ISpliceEntryModel[] = new Array();

        for (let index = 0; index < this._entries.length; index++) {
            const item = this._entries[index];

            let validItem = true;
            if (this._filterTypes.length > 0) {
                validItem = this._filterTypes.indexOf(item.type) >= 0;
            }

            if (validItem === false) { continue; }

            if (this._filterBundles.length > 0) {
                validItem = item.firstFiber != null && this._filterBundles.indexOf(item.firstFiber.bundleNumber) >= 0;
            }

            if (validItem === false) { continue; }

            if (this._filterNames.length > 0) {
                const firstFiberResult = (item.firstFiber != null && this._filterNames.indexOf(item.firstFiber.name) >= 0);
                const secondFiberResult = (item.secondFiber != null && this._filterNames.indexOf(item.secondFiber.name) >= 0);

                validItem = firstFiberResult || secondFiberResult;
            }

            if (validItem === true) {
                result.push(item);
            }
        }

        this._collectionSubject.next(result);
    }

    public filterByType(types: SpliceTypes[]) {
        if (types) { this._filterTypes = types }
        else { this._filterTypes = new Array(); }

        this.filter();
    }

    public filterByBundle(bundles: number[]) {
        if (bundles) { this._filterBundles = bundles }
        else { this._filterBundles = new Array(); }

        this.filter();
    }

    public filterByName(names: string[]) {
        if (names) { this._filterNames = names }
        else { this._filterNames = new Array(); }

        this.filter();
    }
}