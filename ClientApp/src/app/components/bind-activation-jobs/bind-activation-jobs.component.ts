import { Component, OnInit } from '@angular/core';
import { SimpleOverviewBasedBindJobBase } from '../base/simple-overview-based-bind-jobs-base';
import { ISimpleActivationJobOverviewModel } from '../../models/service/jobs/activation/isimple-activation-job-overview-model';
import { RefreshManagerService } from '../../services/refresh-manager.service';
import { CompanyService } from '../../services/companies/company.service';
import { JobService } from '../../services/job.service';
import { MatSnackBar } from '@angular/material';
import { FormBuilder } from '@angular/forms';
import { Observable } from 'rxjs';
import { fuseAnimations } from '@fuse/animations';

@Component({
  selector: 'besa-bind-activation-jobs',
  templateUrl: './bind-activation-jobs.component.html',
  animations: fuseAnimations,
  styleUrls: ['./bind-activation-jobs.component.scss']
})
export class BindActivationJobsComponent extends SimpleOverviewBasedBindJobBase<ISimpleActivationJobOverviewModel> implements OnInit {

  //#region Properties

  //#endregion

  constructor(
    refreshManager: RefreshManagerService,
    companyService: CompanyService,
    jobService: JobService,
    snackBar: MatSnackBar,
    fb: FormBuilder) {
    super(refreshManager, companyService, jobService, snackBar, fb);

  }

  ngOnInit() {
    this.loadOpenJobs();
  }

  //#endregion

  //#region Methods

  protected loadPossibleJobs(): Observable<ISimpleActivationJobOverviewModel[]> {
    return this._jobService.getOpenActivationJobs();
  }

  //#endregion

}