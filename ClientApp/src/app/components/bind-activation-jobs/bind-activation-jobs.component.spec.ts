import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BindActivationJobsComponent } from './bind-activation-jobs.component';

describe('BindActivationJobsComponent', () => {
  let component: BindActivationJobsComponent;
  let fixture: ComponentFixture<BindActivationJobsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BindActivationJobsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BindActivationJobsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
