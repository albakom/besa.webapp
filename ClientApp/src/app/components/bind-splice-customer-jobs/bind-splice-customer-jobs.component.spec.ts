import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BindSpliceCustomerJobsComponent } from './bind-splice-customer-jobs.component';

describe('BindSpliceCustomerJobsComponent', () => {
  let component: BindSpliceCustomerJobsComponent;
  let fixture: ComponentFixture<BindSpliceCustomerJobsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BindSpliceCustomerJobsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BindSpliceCustomerJobsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
