import {CompanyService} from '../../services/companies/company.service';
import { Component, OnInit } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';
import { ConstructionBasedBindJobBase } from '../base/construction-based-bind-jobs-base';
import { Observable } from 'rxjs';
import { RefreshManagerService } from '../../services/refresh-manager.service';
import { JobService } from '../../services/job.service';
import { ConstructionStageService } from '../../services/construction-stage.service';
import { MatSnackBar } from '@angular/material';
import { FormBuilder } from '@angular/forms';
import { IConstructionStageDetailModel } from '../../models/service/construction-stage/iconstruction-stage-detail-for-model';
import { ISpliceBranchableJobOverviewModel } from '../../models/service/jobs/splice/isplice-branchable-job-overview-model';

@Component({
  selector: 'besa-bind-splice-customer-jobs',
  templateUrl: './bind-splice-customer-jobs.component.html',
  animations: fuseAnimations,
  styleUrls: ['./bind-splice-customer-jobs.component.scss']
})
export class BindSpliceCustomerJobsComponent extends ConstructionBasedBindJobBase<ISpliceBranchableJobOverviewModel> {

  //#region Properties

  protected loadDetails(id: number, onlyunbounded: boolean): Observable<IConstructionStageDetailModel<ISpliceBranchableJobOverviewModel>> {
    return this._constructionStageService.loadDetailsForSpliceJobs(id, onlyunbounded);
  }

  //#endregion

  constructor(
    refreshManager: RefreshManagerService,
    companyService: CompanyService,
    jobService: JobService,
    constructionStageService: ConstructionStageService,
    snackBar: MatSnackBar,
    fb: FormBuilder) {
    super(refreshManager, companyService, jobService, constructionStageService, snackBar, fb);
    

    }

    //#endregion

  }
