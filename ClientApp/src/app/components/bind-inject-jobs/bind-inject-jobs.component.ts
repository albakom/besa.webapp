import { Component, OnInit, OnDestroy } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';
import { FormBasedPage } from '../../pages/base/form-based-page';
import { IConstructionStageOverviewForJobsModel } from '../../models/service/construction-stage/iconstruction-stage-overview-for-jobs-model';
import { IConstructionStageDetailModel } from '../../models/service/construction-stage/iconstruction-stage-detail-for-model';
import { IInejectJobOverviewModel } from '../../models/service/jobs/inject/iinject-job-overview-model';
import { ICompanieOverviewModel } from '../../models/service/icompanie-overview-model';
import { MatStepper, MatSnackBar } from '@angular/material';
import { BindJobsBase } from '../base/bind-jobs-base';
import { RefreshManagerService } from '../../services/refresh-manager.service';
import { CompanyService } from '../../services/companies/company.service';
import { JobService } from '../../services/job.service';
import { ConstructionStageService } from '../../services/construction-stage.service';
import { FormBuilder } from '@angular/forms';
import { Observable } from 'rxjs';
import { ConstructionBasedBindJobBase } from '../base/construction-based-bind-jobs-base';

@Component({
  selector: 'besa-bind-inject-jobs',
  templateUrl: './bind-inject-jobs.component.html',
  styleUrls: ['./bind-inject-jobs.component.scss'],
  animations: fuseAnimations,
})
export class BindInjectJobsComponent extends ConstructionBasedBindJobBase<IInejectJobOverviewModel> {

  //#region Properties

  protected loadDetails(id: number, onlyunbounded: boolean): Observable<IConstructionStageDetailModel<IInejectJobOverviewModel>> {
    return this._constructionStageService.loadDetailsForInjectJobs(id, onlyunbounded);
  }

  //#endregion

  constructor(
    refreshManager: RefreshManagerService,
    companyService: CompanyService,
    jobService: JobService,
    constructionStageService: ConstructionStageService,
    snackBar: MatSnackBar,
    fb: FormBuilder) {
    super(refreshManager, companyService, jobService, constructionStageService, snackBar, fb);
    

    }

    //#endregion

  }
