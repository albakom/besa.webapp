import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BindInjectJobsComponent } from './bind-inject-jobs.component';

describe('BindInjectJobsComponent', () => {
  let component: BindInjectJobsComponent;
  let fixture: ComponentFixture<BindInjectJobsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BindInjectJobsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BindInjectJobsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
