import { finalize } from 'rxjs/operators';
import { fuseAnimations } from '@fuse/animations';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBasedPage } from '../../pages/base/form-based-page';
import { FormBuilder, Validators } from '@angular/forms';
import { FormErrorMessage } from '../../models/helper/form-error-message';
import { CreateSpliceOdfJobErrorMessages } from '../../models/error-messages/create-splice-odf-job-error-message';
import { RefreshManagerService } from '../../services/refresh-manager.service';
import { IMainCableOverviewModel } from '../../models/service/splice/imain-cable-overview-model';
import { ConstructionStageService } from '../../services/construction-stage.service';
import { MatSnackBar, MatSelectChange, } from '@angular/material';
import { JobService } from '../../services/job.service';
import { ISpliceODFJobCreateModel } from '../../models/service/jobs/splice/isplice-odf-job-create-model';

@Component({
  selector: 'besa-create-splice-odf-task',
  templateUrl: './create-splice-odf-task.component.html',
  animations: fuseAnimations,
  styleUrls: ['./create-splice-odf-task.component.scss']
})
export class CreateSpliceODFTaskComponent extends FormBasedPage implements OnInit, OnDestroy {

  public cables: IMainCableOverviewModel[];
  public selectedCable: IMainCableOverviewModel;
  public loadingCablesInProgress: boolean;
  public createTaskInProgress: boolean;

  constructor(
    private _fb: FormBuilder,
    private _snackBar: MatSnackBar,
    private _service: ConstructionStageService,
    private _jobService: JobService,
    refreshManger: RefreshManagerService) {
    super();

    super.addSubscription(refreshManger.refreshRequested.subscribe((result) => this.loadCables()));

    super.initForm(_fb.group(
      {
        mainCableId: [-1, [Validators.required, Validators.min(1)]]
      }
    ));
  }

  ngOnInit() {
    this.loadCables();
  }

  ngOnDestroy() {
    super.unsubcripeAll();
  }

  //#region Methods

  protected getErrorMessages(): FormErrorMessage[] {
    return CreateSpliceOdfJobErrorMessages;
  }

  public loadCables(): void {
    this.loadingCablesInProgress = true;

    this._service.loadMainCables().pipe(finalize(() => {
      this.loadingCablesInProgress = false;
    })).subscribe((result) => {
      this.cables = result;
    }, (err) => {
      this._snackBar.open('Fehler beim Laden der Hauptkabel', 'Fehler', { duration: 4500 });
    })
  }

  public cableChanged(event: MatSelectChange): void {
    const id: number = event.value;
    for (let i = 0; i < this.cables.length; i++) {
      const item = this.cables[i];
      if (item.id === id) {
        this.selectedCable = item;
        break;
      }
    }
  }

  public createJob(): void {
    if (this.form.invalid === true) { return; }
    if (!this.selectedCable || this.selectedCable.jobExists === true) { return; }
    if (this.createTaskInProgress === true) { return; }

    this.createTaskInProgress = true;
    const model: ISpliceODFJobCreateModel = this.form.value;

    this._snackBar.open('In Bearbeitung', 'Auftrag wird erstellt', { duration: -1 });

    this._jobService.createSpliceODFJob(model).pipe(finalize(() => {
      this.createTaskInProgress = false;
    })).subscribe((result) => {
      this._snackBar.open('Erfolg', 'Auftrag wurde erstellt', { duration: 4500 });
      this.selectedCable.jobExists = true;
      this.form.reset();
      this.selectedCable = null;

    }, (err) => {
      this._snackBar.open('Fehler', 'Auftrag konnte nicht erstellt werden', { duration: 4500 });
    });
  }

  //#endregion

}
