import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateSpliceOdftaskComponent } from './create-splice-odftask.component';

describe('CreateSpliceOdftaskComponent', () => {
  let component: CreateSpliceOdftaskComponent;
  let fixture: ComponentFixture<CreateSpliceOdftaskComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateSpliceOdftaskComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateSpliceOdftaskComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
