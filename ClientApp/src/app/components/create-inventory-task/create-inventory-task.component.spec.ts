import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateInventoryTaskComponent } from './create-inventory-task.component';

describe('CreateInventoryTaskComponent', () => {
  let component: CreateInventoryTaskComponent;
  let fixture: ComponentFixture<CreateInventoryTaskComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateInventoryTaskComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateInventoryTaskComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
