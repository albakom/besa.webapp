import * as moment from 'moment';
import { finalize ,  debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { IArticleFormModel } from './../../models/service/jobs/inventory/iarticle-form-model';
import { FormArray } from '@angular/forms/src/model';
import { IArticleOverviewModel } from './../../models/service/article/iarticle-overview-model';
import { JobService } from './../../services/job.service';
import { Component, OnInit } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';
import { FormBasedPage } from '../../pages/base/form-based-page';
import { OnDestroy } from '@angular/core/src/metadata/lifecycle_hooks';
import { FormErrorMessage } from '../../models/helper/form-error-message';
import { ArticleService } from '../../services/article/article.service';
import { MatSnackBar, MatAutocompleteSelectedEvent, MatSelectChange } from '@angular/material';
import { FormBuilder, Validators } from '@angular/forms';
import { CreateInventoryJobErrorMessage } from '../../models/error-messages/create-inventory-job-error-message';
import { Subject } from 'rxjs';
import { IContactInfo } from '../../models/service/icontact-info';
import { CustomerService } from '../../services/customer.service';
import { IInventoryJobCreateModel } from '../../models/service/jobs/inventory/iinventory-job-create-model';

@Component({
  selector: 'besa-create-inventory-task',
  templateUrl: './create-inventory-task.component.html',
  styleUrls: ['./create-inventory-task.component.scss'],
  animations: fuseAnimations
})
export class CreateInventoryTaskComponent extends FormBasedPage implements OnInit, OnDestroy {

  public articleResult: IArticleOverviewModel[];
  public createTaskInProgress: boolean;
  public contactResult: IContactInfo[];

  public articlesForm: FormArray;

  private _contactSearchTerm: Subject<string>;

  constructor(
    private _articleService: ArticleService,
    private _snackBar: MatSnackBar,
    private _jobService: JobService,
    private _fb: FormBuilder,
    private _customerService: CustomerService
  ) {
    super();

    this._contactSearchTerm = new Subject<string>();

    super.addSubscription(this._contactSearchTerm.pipe(
      debounceTime(150),
      distinctUntilChanged(),
      switchMap((item) => {
        return this._customerService.searchContacts(item);
      })
    ).subscribe((items) => {
      this.contactResult = items;
    }));

    this.articlesForm = this._fb.array([]);

    super.initForm(
      this._fb.group({
        searchTerms: this._fb.group({
          contactTermValue: ['', []],
        }),
        articles: this.articlesForm,
        contactId: [0, [Validators.required]],
        date: [null, [Validators.required]],
        time: [null, [Validators.required]],
      })
    );

    this.prepare();

    const fieldsToSubscripeChanges: { [name: string]: Subject<string> } = {
      'contactTermValue': this._contactSearchTerm,
    };

    for (const key in fieldsToSubscripeChanges) {
      if (fieldsToSubscripeChanges.hasOwnProperty(key)) {
        const element = fieldsToSubscripeChanges[key];

        super.addSubscription(this.form.get('searchTerms').get(key).valueChanges.subscribe((value: string) => {
          if (typeof (value) === 'string' && value) {
            element.next(value);
          }
        }));
      }
    }

  }

  ngOnInit() {
  }

  ngOnDestroy() {
    super.unsubcripeAll();
  }

  private prepare(): void {
    this.contactResult = new Array();
    if (this.articlesForm) {
      while (this.articlesForm.length > 0) {
        this.articlesForm.removeAt(0);
      }
    }

    this.form.reset();

    this.articleResult = new Array();
    this._articleService.loadArticle().pipe().subscribe((result) => {
      this.articleResult = result;
    }, (err) => {

    })
  }

  protected getErrorMessages(): FormErrorMessage[] {
    return CreateInventoryJobErrorMessage;
  }

  public articleSelected(eventInfo: MatSelectChange) {
    const article: IArticleOverviewModel = this.articleResult.find(x => x.name == eventInfo.value);
    this.articleResult.splice(this.articleResult.indexOf(article), 1);

    this.articlesForm.push(this._fb.group({
      name: [article.name, [Validators.required]],
      id: [article.id, [Validators.required]],
      amount: [1, [Validators.required, Validators.min(0)]],
    }));
  }

  public contactSelected(eventInfo: MatAutocompleteSelectedEvent) {
    const contact: IContactInfo = eventInfo.option.value;
    this.form.patchValue({ contactId: contact.id });
  }

  public displayContactResult(item?: IContactInfo): string {
    return item ? item.surname + ' ' + item.lastname : '';
  }

  public removeArticle(article: any, index: number) {
    this.articleResult.push(article.value);
    this.articlesForm.removeAt(index);
  }

  public createInventoryJob() {
    if (this.createTaskInProgress === true) { return; }
    this.createTaskInProgress = true;

    const valuesAsDict: { [id: number]: number } = {};
    const value: any = this.form.value;

    for (let i = 0; i < value.articles.length; i++) {
      const item: IArticleFormModel = value.articles[i];

      valuesAsDict[item.id] = item.amount;
    }

    var time = moment(this.form.value.time, 'HH:mm').toDate();
    var resultTime = moment(this.form.value.date).add(time.getHours(), 'h').add(time.getMinutes(), 'm').toDate();


    let model: IInventoryJobCreateModel = {
      articles: valuesAsDict,
      issuanceToId: this.form.value.contactId,
      pickupAt: resultTime
    };

    this._snackBar.open('Aufgabe wird erstellt', 'In Bearbeitung', { duration: -1 });

    this._jobService.createInventoryJob(model).pipe(finalize(() => {
      this.createTaskInProgress = false;
    })).subscribe((result) => {
      this._snackBar.dismiss();
      if (result) {
        this._snackBar.open('Kommissionierung wurde erfolgreich erstellt', 'Erfolg', { duration: 4500 });
      } else {
        this._snackBar.open('Kommissionierung konnte nicht erstellt werden', 'Fehler', { duration: 4500 });
      }
      this.prepare();
    }, (err) => {
      this._snackBar.open('Kommissionierung konnte nicht erstellt werden', 'Fehler', { duration: 4500 });
    })
  }

}
