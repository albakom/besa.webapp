import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreatePrepareBranchableForSpliceTaskComponent } from './create-prepare-branchable-for-splice-task.component';

describe('CreatePrepareBranchableForSpliceTaskComponent', () => {
  let component: CreatePrepareBranchableForSpliceTaskComponent;
  let fixture: ComponentFixture<CreatePrepareBranchableForSpliceTaskComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreatePrepareBranchableForSpliceTaskComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreatePrepareBranchableForSpliceTaskComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
