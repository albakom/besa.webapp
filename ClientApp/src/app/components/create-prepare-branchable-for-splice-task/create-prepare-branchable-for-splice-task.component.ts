import { element } from 'protractor';
import { ISimpleCabinetOverviewModel } from './../../models/service/construction-stage/isimple-cabinet-overview-model';
import { debounceTime, distinctUntilChanged, switchMap, finalize } from 'rxjs/operators';
import { MatDialog, MatAutocompleteSelectedEvent } from '@angular/material';
import { MatSnackBar } from '@angular/material';
import { ConstructionStageService } from './../../services/construction-stage.service';
import { fuseAnimations } from '@fuse/animations';
import { Component, OnInit } from '@angular/core';
import { FormBasedPage } from '../../pages/base/form-based-page';
import { OnDestroy } from '@angular/core/src/metadata/lifecycle_hooks';
import { FormErrorMessage } from '../../models/helper/form-error-message';
import { JobService } from '../../services/job.service';
import { FormBuilder } from '@angular/forms';
import { Subject } from 'rxjs';
import { CreatePrepareBranchableForSpliceJobErrorMessages } from '../../models/error-messages/create-prepare-branchable-for-splice-job-error-messages';
import { IPrepareBranchableForSpliceJobCreateModel } from '../../models/service/jobs/splice/iprepare-branchable-for-splice-job-create-model';

@Component({
  selector: 'besa-create-prepare-branchable-for-splice-task',
  templateUrl: './create-prepare-branchable-for-splice-task.component.html',
  styleUrls: ['./create-prepare-branchable-for-splice-task.component.scss'],
  animations: fuseAnimations
})
export class CreatePrepareBranchableForSpliceTaskComponent extends FormBasedPage implements OnInit, OnDestroy {

  public prepareBranchableResult: ISimpleCabinetOverviewModel[];
  public createTaskInProgress: boolean;

  private _prepareBranchableSearchTerm: Subject<string>;
  private _currentBranchable: ISimpleCabinetOverviewModel;

  constructor(
    private _constructionStageService: ConstructionStageService,
    private _snackBar: MatSnackBar,
    private _jobService: JobService,
    private _fb: FormBuilder
  ) {
    super();

    this._prepareBranchableSearchTerm = new Subject<string>();

    super.addSubscription(this._prepareBranchableSearchTerm.pipe(
      debounceTime(150),
      distinctUntilChanged(),
      switchMap((item) => {
        return this._constructionStageService.searchCabinents(item);
      })
    ).subscribe((items) => {
      this.prepareBranchableResult = items;
    }))

    this.prepare();

    super.initForm(
      this._fb.group({
        searchTerms: this._fb.group(
          {
            prepareBranchableTermValue: ['', []],
          }
        ),
        branchableId: null
      }));

      const fieldsToSubscripeChanges: { [name: string]: Subject<string> } = {
        'prepareBranchableTermValue': this._prepareBranchableSearchTerm
      };

      for (const key in fieldsToSubscripeChanges) {
        if (fieldsToSubscripeChanges.hasOwnProperty(key)) {
          const element = fieldsToSubscripeChanges[key];

          super.addSubscription(this.form.get('searchTerms').get(key).valueChanges.subscribe((value: string) => {
            if (typeof(value) == 'string') {
              element.next(value);
            }
          }));
        }
      }
   }

  ngOnInit() {
  }

  ngOnDestroy() {
    super.unsubcripeAll();
  }

  private prepare(): void {
    this.prepareBranchableResult = new Array();

    if (this.form) {
      this.form.reset();
      this.form.setValue({
        searchTerms: {
          prepareBranchableTermValue: '',
        },
        branchableId: null
      });
    }
  }

  protected getErrorMessages(): FormErrorMessage[] {
    return CreatePrepareBranchableForSpliceJobErrorMessages;
  }

  public prepareBranchableSelected(eventInfo: MatAutocompleteSelectedEvent): void {
    const branchable: ISimpleCabinetOverviewModel = eventInfo.option.value;
    this._currentBranchable = branchable;
    this.form.patchValue({branchableId: branchable.id});
  }

  displayBranchableResult(item?: ISimpleCabinetOverviewModel): string {
    return item ? item.name : '';
  }

  public createPrepareBranchableForSpliceJob() {
    if (this.createTaskInProgress === true) { return; }

    this.createTaskInProgress = true;
    this._snackBar.open('KVZ-Vorbereitung wird erstellt', 'In Bearbeitung', { duration: -1 });

    const value: IPrepareBranchableForSpliceJobCreateModel = this.form.value;

    this._jobService.createPrepareBranchableForSpliceJob(value).pipe(finalize(() => {
      this.createTaskInProgress = false;
    })).subscribe((result) => {
      this._snackBar.dismiss();
      if (result > 0) {
        this._snackBar.open('Die KVZ-Vorbereitung wurde erfolgreich erstellt', 'Erfolg', { duration: 4500 });
        this.prepare();
      } else {
        this._snackBar.open('Die KVZ-Vorbereitung konnte nicht erstellt werden', 'Fehler', { duration: 4500 });
      }
    }, (err) => {
      this._snackBar.dismiss();
      this._snackBar.open('Die KVZ-Vorbereitung konnte nicht erstellt werden', 'Fehler', { duration: 4500 });
    });
  }

}
