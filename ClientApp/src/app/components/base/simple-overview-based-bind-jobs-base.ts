import { FormBasedPage } from "../../pages/base/form-based-page";
import { IConstructionStageOverviewForJobsModel } from "../../models/service/construction-stage/iconstruction-stage-overview-for-jobs-model";
import { IConstructionStageDetailModel } from "../../models/service/construction-stage/iconstruction-stage-detail-for-model";
import { IJobOverviewModel } from '../../models/service/jobs/ijobs-overview-model';
import { ICompanieOverviewModel } from "../../models/service/icompanie-overview-model";
import { RefreshManagerService } from "../../services/refresh-manager.service";
import { CompanyService } from "../../services/companies/company.service";
import { ConstructionStageService } from "../../services/construction-stage.service";
import { MatSnackBar, MatSelectionListChange, MatStepper, MatSelectionList } from "@angular/material";
import { FormBuilder, Validators } from "@angular/forms";
import { BindJobsFormErrorMessages } from "../../models/error-messages/bind-jobs-form-error-message";
import { FormErrorMessage } from "../../models/helper/form-error-message";
import { finalize } from "rxjs/operators";
import { Observable } from "rxjs";
import { StepperSelectionEvent } from "@angular/cdk/stepper";
import { IFormBindJobsModel } from "../../models/forms/iform-bind-jobs-model";
import { ViewChild, OnInit, OnDestroy } from '@angular/core';
import { IBindJobsModel } from "../../models/service/jobs/ibind-jobs-model";
import { JobService } from '../../services/job.service';
import { BindJobsBase } from "./bind-jobs-base";

export abstract class SimpleOverviewBasedBindJobBase<TJobOverview extends IJobOverviewModel> extends BindJobsBase<TJobOverview> implements OnInit {

    //#region Fields

    //#endregon

    //#region Properties

    @ViewChild('matSelectionList')
    public selectionList: MatSelectionList;

    //#endregion

    constructor(
        refreshManager: RefreshManagerService,
        companyService: CompanyService,
        jobService: JobService,
        snackBar: MatSnackBar,
        fb: FormBuilder) {
        super(refreshManager, companyService, jobService, snackBar, fb);

        super.addSubscription(refreshManager.refreshRequested.subscribe((result) => {
            this.loadPossibleJobs();
        }));

    }

    ngOnInit() {
        this.loadPossibleJobs();
    }

    //#region Methods

    protected abstract loadPossibleJobs(): Observable<TJobOverview[]>;

    protected loadOpenJobs(): void {
        this.loadingJobsInProgress = true;

        this.loadPossibleJobs().pipe(finalize(() => {
            this.loadingJobsInProgress = false;
        })).subscribe((result) => {
            this.possibleJobs = result;
        }, (err) => {
            this._snackBar.open('Fehler beim Laden der offenen Aufträge', 'Fehler', { duration: 4500 });
        })
    }

    protected restoreInternal(): void {
        if (this.selectionList) {
            this.selectionList.deselectAll();
        }

        this.loadOpenJobs();
    }

    //#endregion

}