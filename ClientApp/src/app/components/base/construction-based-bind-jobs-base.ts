import { FormBasedPage } from "../../pages/base/form-based-page";
import { IConstructionStageOverviewForJobsModel } from "../../models/service/construction-stage/iconstruction-stage-overview-for-jobs-model";
import { IConstructionStageDetailModel } from "../../models/service/construction-stage/iconstruction-stage-detail-for-model";
import { IJobOverviewModel } from '../../models/service/jobs/ijobs-overview-model';
import { ICompanieOverviewModel } from "../../models/service/icompanie-overview-model";
import { RefreshManagerService } from "../../services/refresh-manager.service";
import { CompanyService } from "../../services/companies/company.service";
import { ConstructionStageService } from "../../services/construction-stage.service";
import { MatSnackBar, MatSelectionListChange, MatStepper } from "@angular/material";
import { FormBuilder, Validators } from "@angular/forms";
import { BindJobsFormErrorMessages } from "../../models/error-messages/bind-jobs-form-error-message";
import { FormErrorMessage } from "../../models/helper/form-error-message";
import { finalize } from "rxjs/operators";
import { Observable } from "rxjs";
import { StepperSelectionEvent } from "@angular/cdk/stepper";
import { IFormBindJobsModel } from "../../models/forms/iform-bind-jobs-model";
import { ViewChild, OnInit, OnDestroy } from '@angular/core';
import { IBindJobsModel } from "../../models/service/jobs/ibind-jobs-model";
import { JobService } from '../../services/job.service';
import { BindJobsBase } from "./bind-jobs-base";

export abstract class ConstructionBasedBindJobBase<TJobOverview extends IJobOverviewModel> extends BindJobsBase<TJobOverview> implements OnInit {

    //#region Fields

    //#endregon

    //#region Properties

    public loadingConstructionStages: boolean;
    public constructionStages: IConstructionStageOverviewForJobsModel[];

    public constructionStageDetails: { [constructionStageId: number]: IConstructionStageDetailModel<TJobOverview> };
    public loadingDetailsInProgress: boolean;

    //#endregion

    constructor(
        refreshManager: RefreshManagerService,
        companyService: CompanyService,
        jobService: JobService,
        protected _constructionStageService: ConstructionStageService,
        snackBar: MatSnackBar,
        fb: FormBuilder) {
        super(refreshManager, companyService, jobService, snackBar, fb);

        super.addSubscription(refreshManager.refreshRequested.subscribe((result) => {
            this.loadConstructionStages();
        }));

        this.constructionStageDetails = {};
    }

    ngOnInit() {
        this.loadConstructionStages();
    }

    //#region Methods

    private loadConstructionStages(): void {
        this.loadingConstructionStages = true;

        this._constructionStageService.loadOverviewForCivilWork(true).pipe(finalize(() => {
            this.loadingConstructionStages = false;
        })).subscribe((result) => {
            this.constructionStageDetails = {};
            this.constructionStages = result;
        }, (err) => {
            this._snackBar.open('Bauabschnitte konnte nicht geladen werden', 'Fehler', { duration: 4500 });
        })
    }

    protected abstract loadDetails(id: number, onlyunbounded: boolean): Observable<IConstructionStageDetailModel<TJobOverview>>;

    public loadConstructionStageDetails(overview: IConstructionStageOverviewForJobsModel) {
        const existingOnes = this.constructionStageDetails[overview.id];
        if (existingOnes) { return; }

        this.loadingDetailsInProgress = true;
        this.loadDetails(overview.id, true).pipe(finalize(() => {
            this.loadingDetailsInProgress = false;
        })).subscribe((result) => {
            this.constructionStageDetails[overview.id] = result;
        }, (err) => {
            this._snackBar.open('Details konnten nicht geladen werden', 'Fehler', { duration: 4500 });
        });
    }

    protected restoreInternal(): void {
        this.constructionStages = new Array();
        this.loadConstructionStages();
    }

    //#endregion

}