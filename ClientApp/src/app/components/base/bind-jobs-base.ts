import { IJobOverviewModel } from "../../models/service/jobs/ijobs-overview-model";
import { FormBasedPage } from "../../pages/base/form-based-page";
import { OnDestroy, ViewChild } from "@angular/core";
import { ICompanieOverviewModel } from "../../models/service/icompanie-overview-model";
import { MatStepper, MatSnackBar, MatSelectionListChange } from "@angular/material";
import { RefreshManagerService } from "../../services/refresh-manager.service";
import { CompanyService } from "../../services/companies/company.service";
import { JobService } from "../../services/job.service";
import { FormBuilder, Validators } from "@angular/forms";
import { StepperSelectionEvent } from "@angular/cdk/stepper";
import { finalize } from "rxjs/operators";
import { IFormBindJobsModel } from "../../models/forms/iform-bind-jobs-model";
import { IBindJobsModel } from "../../models/service/jobs/ibind-jobs-model";
import { FormErrorMessage } from "../../models/helper/form-error-message";
import { BindJobsFormErrorMessages } from "../../models/error-messages/bind-jobs-form-error-message";


export abstract class BindJobsBase<TJobOverivew extends IJobOverviewModel> extends FormBasedPage implements OnDestroy {

    //#region Fields

    //#endregon

    //#region Properties

    public loadingJobsInProgress: boolean;

    public loadingCompaniesInProgress: boolean;

    public companies: ICompanieOverviewModel[];
    public bindingInProgress: boolean;

    public possibleJobs: TJobOverivew[];
    public jobs: TJobOverivew[];

    @ViewChild('stepper')
    public stepper: MatStepper;

    //#endregion

    constructor(
        refreshManager: RefreshManagerService,
        protected _companyService: CompanyService,
        protected _jobService: JobService,
        protected _snackBar: MatSnackBar,
        protected _fb: FormBuilder) {
        super();

        this.jobs = new Array();

        super.addSubscription(refreshManager.refreshRequested.subscribe((result) => {
            if (this.companies) {
                this.loadCompanies();
            }
        }));

        super.initForm(
            this._fb.group({
                companyId: ['', [Validators.required]],
                finishTill: ['', [Validators.required]],
                name: ['', [Validators.required, Validators.maxLength(128)]],
            }));
    }

    ngOnDestroy() {
        super.unsubcripeAll();
    }

    //#region Methods

    protected getErrorMessages(): FormErrorMessage[] {
        return BindJobsFormErrorMessages;
    }

    public jobAdded(event: MatSelectionListChange): void {
        const option: TJobOverivew = event.option.value;
        if (event.option.selected == true) {
            this.jobs.unshift(option);
        } else {
            const index = this.jobs.indexOf(option);
            if (index >= 0) {
                this.jobs.splice(index, 1);
            }
        }
    }

    public stepChanged(event: StepperSelectionEvent): void {
        if (event.selectedIndex == 1) {
            if (!this.companies) {
                this.loadCompanies();
            }
        }
    }

    private loadCompanies(): void {
        if (this.loadingCompaniesInProgress === true) { return; }

        this.loadingCompaniesInProgress = true;

        this._companyService.loadCompanies().pipe(finalize(() => {
            this.loadingCompaniesInProgress = false;
        })).subscribe((result) => {
            this.companies = result;
        }, (err) => {
            this._snackBar.open('Firmen konnten nicht geladen werden', 'Fehler', { duration: 4500 });
        });

    }

    public getCompanyName(): string {
        if (!this.companies) { return ''; }

        const value: IFormBindJobsModel = this.form.value;

        for (let index = 0; index < this.companies.length; index++) {
            const element = this.companies[index];
            if (element.id == value.companyId) {
                return element.name;
            }
        }

        return '';
    }

    public getFinishedDate(): string {
        const value: IFormBindJobsModel = this.form.value;
        return value.finishTill;
    }

    public getTaskName(): string {
        const value: IFormBindJobsModel = this.form.value;
        return value.name;
    }

    public bindJobs(): void {
        if (this.form.valid === false) { return; }
        if (this.bindingInProgress === true) { return; }

        this.bindingInProgress = true;

        const value: IFormBindJobsModel = this.form.value;

        const model: IBindJobsModel = {
            companyId: value.companyId,
            endDate: value.finishTill,
            name: value.name,
            jobIds: this.jobs.map((elem) => elem.jobId),
        }

        this._snackBar.open('Aufträge werden gebunden', 'in Bearbeitung', { duration: -1 });

        this._jobService.bindJobs(model).pipe(finalize(() => {
            this.bindingInProgress = false;
        })).subscribe((result) => {
            this._snackBar.dismiss();
            if (result === true) {
                this._snackBar.open('Aufträge wurden erfolgreich gebunden', 'Erfolg', { duration: 4500 });

                this.restore();

            } else {
                this._snackBar.open('Aufträge konnte nicht gebunden werden', 'Fehler', { duration: 4500 });
            }
        }, (err) => {
            this._snackBar.dismiss();
            this._snackBar.open('Aufträge konnte nicht gebunden werden', 'Fehler', { duration: 4500 });
        });
    }

    protected abstract restoreInternal(): void;

    private restore(): void {
        this.formValidationErrors = {};
        this.jobs = new Array();

        if (this.stepper) {
            this.stepper.reset();
        }

        this.restoreInternal();
    }

    //#endregion

}