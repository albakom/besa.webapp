import { Component, OnInit } from '@angular/core';
import { SimpleOverviewBasedBindJobBase } from '../base/simple-overview-based-bind-jobs-base';
import { ISimpleSpliceODFJobOverviewModel } from '../../models/service/jobs/splice/isimple-splice-odfjob-overview-model';
import { RefreshManagerService } from '../../services/refresh-manager.service';
import { CompanyService } from '../../services/companies/company.service';
import { JobService } from '../../services/job.service';
import { MatSnackBar } from '@angular/material';
import { FormBuilder } from '@angular/forms';
import { fuseAnimations } from '@fuse/animations';
import { Observable } from 'rxjs';

@Component({
  selector: 'besa-bind-splice-odf-jobs',
  templateUrl: './bind-splice-odf-jobs.component.html',
  animations: fuseAnimations,
  styleUrls: ['./bind-splice-odf-jobs.component.scss']
})
export class BindSpliceODFJobsComponent extends SimpleOverviewBasedBindJobBase<ISimpleSpliceODFJobOverviewModel> implements OnInit {

  //#region Properties

  //#endregion

  constructor(
    refreshManager: RefreshManagerService,
    companyService: CompanyService,
    jobService: JobService,
    snackBar: MatSnackBar,
    fb: FormBuilder) {
    super(refreshManager, companyService, jobService, snackBar, fb);

  }

  ngOnInit() {
    this.loadOpenJobs();
  }

  //#endregion

  //#region Methods

  protected loadPossibleJobs(): Observable<ISimpleSpliceODFJobOverviewModel[]> {
    return this._jobService.getOpenSpliceODFJobs();
  }

  //#endregion

}