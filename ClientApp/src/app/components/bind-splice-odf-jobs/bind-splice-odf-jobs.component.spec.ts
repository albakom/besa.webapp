import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BindSpliceOdfjobsComponent } from './bind-splice-odfjobs.component';

describe('BindSpliceOdfjobsComponent', () => {
  let component: BindSpliceOdfjobsComponent;
  let fixture: ComponentFixture<BindSpliceOdfjobsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BindSpliceOdfjobsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BindSpliceOdfjobsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
