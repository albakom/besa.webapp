import {CompanyService} from '../../services/companies/company.service';
import { Component, OnInit } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';
import { BindJobsBase } from '../base/bind-jobs-base';
import { ISimpleBuildingConnenctionJobOverview } from '../../models/service/jobs/overview/isimple-building-connenction-job-overview';
import { Observable } from 'rxjs';
import { IConstructionStageDetailModel } from '../../models/service/construction-stage/iconstruction-stage-detail-for-model';
import { RefreshManagerService } from '../../services/refresh-manager.service';
import { JobService } from '../../services/job.service';
import { ConstructionStageService } from '../../services/construction-stage.service';
import { MatSnackBar } from '@angular/material';
import { FormBuilder } from '@angular/forms';
import { ConstructionBasedBindJobBase } from '../base/construction-based-bind-jobs-base';

@Component({
  selector: 'besa-bind-connection-jobs',
  templateUrl: './bind-connection-jobs.component.html',
  styleUrls: ['./bind-connection-jobs.component.scss'],
  animations: fuseAnimations,
})
export class BindConnectionJobsComponent extends ConstructionBasedBindJobBase<ISimpleBuildingConnenctionJobOverview> {

  //#region Properties

  protected loadDetails(id: number, onlyunbounded: boolean): Observable<IConstructionStageDetailModel<ISimpleBuildingConnenctionJobOverview>> {
    return this._constructionStageService.loadDetailsForCivilWork(id, onlyunbounded);
  }

  //#endregion

  constructor(
    refreshManager: RefreshManagerService,
    companyService: CompanyService,
    jobService: JobService,
    constructionStageService: ConstructionStageService,
    snackBar: MatSnackBar,
    fb: FormBuilder) {
    super(refreshManager, companyService, jobService, constructionStageService, snackBar, fb);

    }

    //#endregion

  }
