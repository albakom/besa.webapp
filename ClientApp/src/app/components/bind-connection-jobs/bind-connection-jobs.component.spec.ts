import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BindConnectionJobsComponent } from './bind-connection-jobs.component';

describe('BindConnectionJobsComponent', () => {
  let component: BindConnectionJobsComponent;
  let fixture: ComponentFixture<BindConnectionJobsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BindConnectionJobsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BindConnectionJobsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
