import { FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';
import { FormBasedPage } from '../../pages/base/form-based-page';
import { RefreshManagerService } from '../../services/refresh-manager.service';
import { MatSnackBar, MatAutocompleteSelectedEvent, MatDialog } from '@angular/material';
import { ConstructionStageService } from '../../services/construction-stage.service';
import { Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap, finalize } from 'rxjs/operators';
import { ISimpleBuildingOverviewModel } from '../../models/service/construction-stage/isimple-building-overview-model';
import { IContactInfo } from '../../models/service/icontact-info';
import { CustomerService } from '../../services/customer.service';
import { ICreateContactDialogOptions } from '../../dialogs/create-contact-dialog/icreate-contact-dialog-options';
import { PersonTypes } from '../../models/service/person-types.enum';
import { CreateContactDialogComponent } from '../../dialogs/create-contact-dialog/create-contact-dialog.component';
import { JobService } from '../../services/job.service';
import { IConnectBuildingJobCreateModel } from '../../models/service/jobs/connect-building/iconnect-building-job-create-model';
import { FormErrorMessage } from '../../models/helper/form-error-message';
import { CreateConnectBuildingJobErrorMessages } from '../../models/error-messages/create-connect-building-job-error-message';

@Component({
  selector: 'besa-create-connect-building-task',
  templateUrl: './create-connect-building-task.component.html',
  styleUrls: ['./create-connect-building-task.component.scss'],
  animations: fuseAnimations,
})
export class CreateConnectBuildingTaskComponent extends FormBasedPage implements OnInit, OnDestroy {

  //#region Properties

  public buildingResult: ISimpleBuildingOverviewModel[];

  public ownerResult: IContactInfo[];
  public customerResult: IContactInfo[];
  public architectResult: IContactInfo[];
  public workmanResult: IContactInfo[];

  public createTaskInProgress: boolean;

  //#endregion

  //#region Fields

  private _buildingSearchTerm: Subject<string>;
  private _customerSearchTerm: Subject<string>;
  private _ownerSearchTerm: Subject<string>;
  private _architectSearchTerm: Subject<string>;
  private _workmanSearchTerm: Subject<string>;
  private _ignoreCustomerSearchEvent: boolean;
  private _currentBulding: ISimpleBuildingOverviewModel;
  //#endregion

  constructor(
    private _snachBar: MatSnackBar,
    private _dialog: MatDialog,
    private _customerService: CustomerService,
    private _jobService: JobService,
    private _fb: FormBuilder,
    private _service: ConstructionStageService) {
    super();

    this._buildingSearchTerm = new Subject<string>();
    this._customerSearchTerm = new Subject<string>();
    this._ownerSearchTerm = new Subject<string>();
    this._workmanSearchTerm = new Subject<string>();
    this._architectSearchTerm = new Subject<string>();

    super.addSubscription(this._buildingSearchTerm.pipe(
      debounceTime(150),
      distinctUntilChanged(),
      switchMap((item) => {
        return this._service.searchStreets(item);
      })
    ).subscribe((items) => {
      this.buildingResult = items;
    }));

    super.addSubscription(this._customerSearchTerm.pipe(
      debounceTime(150),
      distinctUntilChanged(),
      switchMap((item) => {
        return this._customerService.searchContacts(item);
      })
    ).subscribe((items) => {
      this.customerResult = items;
    }));

    super.addSubscription(this._ownerSearchTerm.pipe(
      debounceTime(150),
      distinctUntilChanged(),
      switchMap((item) => {
        return this._customerService.searchContacts(item);
      })
    ).subscribe((items) => {
      this.ownerResult = items;
    }));

    super.addSubscription(this._workmanSearchTerm.pipe(
      debounceTime(150),
      distinctUntilChanged(),
      switchMap((item) => {
        return this._customerService.searchContacts(item);
      })
    ).subscribe((items) => {
      this.workmanResult = items;
    }));

    super.addSubscription(this._architectSearchTerm.pipe(
      debounceTime(150),
      distinctUntilChanged(),
      switchMap((item) => {
        return this._customerService.searchContacts(item);
      })
    ).subscribe((items) => {
      this.architectResult = items;
    }));

    this.prepare();

    super.initForm(
      this._fb.group({
        searchTerms: this._fb.group(
          {
            buildingTermValue: ['', []],
            searchCustomermValue: ['', []],
            searchOwnerValue: ['', []],
            searchArchitectValue: ['', []],
            searchWorkmanValue: ['', []],
          }
        ),
        buildingId: [null, [Validators.required]],
        onlyHouseConnection: [true, [Validators.required]],
        customerContactId: [null, []],
        workmanContactId: [null, []],
        architectContactId: [null, []],
        ownerContactId: [null, [Validators.required]],
        description: [null, [Validators.maxLength(1024)]],
        civilWorkIsDoneByCustomer: [false, [Validators.required]],
        connectionOfOtherMediaRequired: [false, [Validators.required]],
        connectionAppointment: [null, []],
      }));

    const fieldsToSubscripeChanges: { [name: string]: Subject<string> } = {
      'buildingTermValue': this._buildingSearchTerm,
      'searchCustomermValue': this._customerSearchTerm,
      'searchOwnerValue': this._ownerSearchTerm,
      'searchArchitectValue': this._architectSearchTerm,
      'searchWorkmanValue': this._workmanSearchTerm,
    };

    for (const key in fieldsToSubscripeChanges) {
      if (fieldsToSubscripeChanges.hasOwnProperty(key)) {
        const element = fieldsToSubscripeChanges[key];

        super.addSubscription(this.form.get('searchTerms').get(key).valueChanges.subscribe((value: string) => {
          if (this._ignoreCustomerSearchEvent === true) { return; }
          if (typeof (value) === 'string') {
            element.next(value);
          }
        }));
      }
    }
  }

  ngOnInit() {
  }

  ngOnDestroy() {
    super.unsubcripeAll();
  }

  private prepare(): void {
    this.ownerResult = new Array();
    this.customerResult = new Array();
    this.architectResult = new Array();
    this.workmanResult = new Array();

    this._currentBulding = null;
    if (this.form) {
      this.form.reset();
      this.form.setValue(
        {
          searchTerms: {
            buildingTermValue: '',
            searchCustomermValue: '',
            searchOwnerValue: '',
            searchArchitectValue: '',
            searchWorkmanValue: '',
          },
          buildingId: null,
          onlyHouseConnection: true,
          customerContactId: null,
          workmanContactId: null,
          architectContactId: null,
          ownerContactId: null,
          description: null,
          civilWorkIsDoneByCustomer: false,
          connectionOfOtherMediaRequired: false,
          connectionAppointment: null,
        }
      );
    }
  }

  //#region Methods

  protected getErrorMessages(): FormErrorMessage[] {
    return CreateConnectBuildingJobErrorMessages;
  }

  public buildingSelected(eventInfo: MatAutocompleteSelectedEvent): void {
    const building: ISimpleBuildingOverviewModel = eventInfo.option.value;
    this._currentBulding = building;
    this.form.patchValue({ buildingId: building.id });
  }

  private setContact(type: string, contact: IContactInfo): void {
    let formPropertyName: string = null;
    if (type === 'customer') {
      formPropertyName = 'customerContactId';
    } else if (type === 'owner') {
      formPropertyName = 'ownerContactId';
    } else if (type === 'architect') {
      formPropertyName = 'architectContactId';
    } else if (type === 'workman') {
      formPropertyName = 'workmanContactId';
    }

    if (formPropertyName) {
      const patchValue: { [property: string]: any } = {};
      patchValue[formPropertyName] = contact.id;
      this.form.patchValue(patchValue);
    }
  }

  public contactSelected(eventInfo: MatAutocompleteSelectedEvent, type: string): void {
    const contact: IContactInfo = eventInfo.option.value;
    this.setContact(type, contact);
  }

  public openCreateContactDialog(type: string): void {
    const options: ICreateContactDialogOptions = {};
    if (this._currentBulding && (type === 'customer' || type === 'owner')) {
      const splitIndex = this._currentBulding.streetName.lastIndexOf(' ');
      if (splitIndex > 0) {
        const streetName = this._currentBulding.streetName.substr(0, splitIndex);
        const houseNumber = this._currentBulding.streetName.substr(splitIndex + 1);

        options.addressToCopy = {
          city: 'Teterow',
          postalCode: '17166',
          street: streetName,
          streetNumber: houseNumber,
        };
      }
    }

    const ref = this._dialog.open<CreateContactDialogComponent>(CreateContactDialogComponent, { data: options });
    ref.afterClosed().subscribe((contact: IContactInfo) => {
      if (contact) {
        this._ignoreCustomerSearchEvent = true;
        let contactArray: IContactInfo[] = null;
        let searchTermValue: string = null;
        if (type === 'customer') {
          contactArray = this.customerResult;
          searchTermValue = 'searchCustomermValue';
        } else if (type === 'owner') {
          contactArray = this.ownerResult;
          searchTermValue = 'searchOwnerValue';
        } else if (type === 'architect') {
          contactArray = this.architectResult;
          searchTermValue = 'searchArchitectValue';
        } else if (type === 'workman') {
          contactArray = this.workmanResult;
          searchTermValue = 'searchWorkmanValue';
        }

        if (contactArray.length > 0) {
          contactArray.splice(0, contactArray.length);
        }

        contactArray.push(contact);

        this.setContact(type, contact);

        const patchValue: { [property: string]: any } = {};
        patchValue[searchTermValue] = this.displayContactResult(contact);

        this.form.get('searchTerms').patchValue(patchValue);
        this._ignoreCustomerSearchEvent = false;
      }
    });
  }

  public displayBuildingResult(item?: ISimpleBuildingOverviewModel): string {
    return item ? item.streetName : '';
  }

  public displayContactResult(item?: IContactInfo): string {
    if (!item) { return '' }
    let result = '';
    if (item.type === PersonTypes.Male) {
      result += 'Herr ';
    } else {
      result += 'Frau ';
    }

    if (item.surname) {
      result += item.surname + ' ';
    }
    if (item.lastname) {
      result += item.lastname + ' ';
    }

    if (item.companyName) {
      result += '(' + item.companyName + ')';
    }

    return result;
  }

  //#endregion

  public checkForm(): boolean {
    if (this.form.invalid === true) { return false; }
    const value: IConnectBuildingJobCreateModel = this.form.value;

    if (value.onlyHouseConnection === false && !value.customerContactId) {
      return false;
    } else {
      return true;
    }
  }

  public createConnectBuildingJob(): void {

    if (this.createTaskInProgress === true) { return; }
    if (this.checkForm() === false) { return; }

    this.createTaskInProgress = true;
    this._snachBar.open('Aufgabe wird erstellt', 'In Bearbeitung', { duration: -1 });
    const value: IConnectBuildingJobCreateModel = this.form.value;

    this._jobService.createConnectBuildingJob(value).pipe(finalize(() => {
      this.createTaskInProgress = false;
    })).subscribe((result) => {
      this._snachBar.dismiss();
      if (result > 0) {
        this._snachBar.open('Aufgabe erfolgreich erstellt', 'Erfolg', { duration: 4500 });
        this.prepare();
      } else {
        this._snachBar.open('Anschlussaufgabe konnte nicht erstellt werden', 'Fehler', { duration: 4500 });
      }
    }, (err) => {
      this._snachBar.dismiss();
      this._snachBar.open('Anschlussaufgabe konnte nicht erstellt werden', 'Fehler', { duration: 4500 });
    });
  }
}
