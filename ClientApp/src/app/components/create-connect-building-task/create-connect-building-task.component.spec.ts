import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateConnectBuildingTaskComponent } from './create-connect-building-task.component';

describe('CreateConnectBuildingTaskComponent', () => {
  let component: CreateConnectBuildingTaskComponent;
  let fixture: ComponentFixture<CreateConnectBuildingTaskComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateConnectBuildingTaskComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateConnectBuildingTaskComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
