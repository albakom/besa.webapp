import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonInfoShortViewComponent } from './person-info-short-view.component';

describe('PersonInfoShortViewComponent', () => {
  let component: PersonInfoShortViewComponent;
  let fixture: ComponentFixture<PersonInfoShortViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PersonInfoShortViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonInfoShortViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
