import { IContactInfo } from './../../models/service/icontact-info';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'besa-person-info-short-view',
  templateUrl: './person-info-short-view.component.html',
  styleUrls: ['./person-info-short-view.component.scss']
})
export class PersonInfoShortViewComponent implements OnInit {

  @Input()
  public contact: IContactInfo;

  constructor() { }

  ngOnInit() {
  }

}
