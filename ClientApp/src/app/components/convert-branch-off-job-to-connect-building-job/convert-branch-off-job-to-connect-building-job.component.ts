import { finalize } from 'rxjs/operators';
import { MatSnackBar } from '@angular/material';
import { JobService } from './../../services/job.service';
import { SubscriptionAwareComponent } from './../../helper/subscription-aware-component';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';
import { BehaviorSubject } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { ISimpleJobOverview } from '../../models/service/jobs/isimple-job-overview';
import { IStateHelperModel } from '../../models/helper/istate-helper-model';
import { MatAutocompleteSelectedEvent } from '@angular/material';
import { ProgressStates } from '../../models/helper/progress-states.enum';
import { element } from 'protractor';
import { Router } from '@angular/router';

@Component({
  selector: 'besa-convert-branch-off-job-to-connect-building-job',
  templateUrl: './convert-branch-off-job-to-connect-building-job.component.html',
  animations: fuseAnimations,
  styleUrls: ['./convert-branch-off-job-to-connect-building-job.component.scss']
})
export class ConvertBranchOffJobToConnectBuildingJobComponent extends SubscriptionAwareComponent implements OnInit, OnDestroy {

  //#region Fields

  private _jobNameQuery: BehaviorSubject<string>;

  //#endregion

  //#region Properties

  get jobNameQuery(): string {
    return this._jobNameQuery.getValue();
  }

  set jobNameQuery(search: string) {
    this._jobNameQuery.next(search);
  }

  public queriedJobs: ISimpleJobOverview[];
  public selectedItems: IStateHelperModel<ISimpleJobOverview, number>[];

  public convertInProgress: boolean;

  //#endregion

  constructor(
    private _snackbar: MatSnackBar,
    private _router: Router,
    private _jobService: JobService) {
    super();

    this.selectedItems = new Array();
    this._jobNameQuery = new BehaviorSubject<string>('');

    super.addSubscription(this._jobNameQuery.pipe(
      debounceTime(150),
      distinctUntilChanged(),
      switchMap((item) => {
        return this._jobService.searchBranchOffJobs(item);
      })
    ).subscribe((items) => {
      this.queriedJobs = items;
    }));

  }

  ngOnInit() {
  }

  ngOnDestroy() {
    super.unsubcripeAll();
  }

  //#region Methods

  public jobSelected(eventInfo: MatAutocompleteSelectedEvent): void {
    const itemToAdd: ISimpleJobOverview = eventInfo.option.value;
    const existingItem = this.selectedItems.find(x => x.item.id == itemToAdd.id);

    if (existingItem) {
      return;
    }

    this.selectedItems.push({ item: itemToAdd, state: ProgressStates.Untouched });
  }

  public removeItem(item: IStateHelperModel<ISimpleJobOverview, number>): void {
    const index = this.selectedItems.indexOf(item);
    if (index >= 0) {
      this.selectedItems.splice(index, 1);
    }
  }

  private convertItem() {
    let item: IStateHelperModel<ISimpleJobOverview, number> = null;
    for (let index = 0; index < this.selectedItems.length; index++) {
      const element = this.selectedItems[index];
      if (element.state === ProgressStates.Untouched) {
        item = element;
      }
    }

    if (!item) {
      const indexToRemove = new Array();
      for (let index = this.selectedItems.length - 1; index >= 0; index--) {
        const element = this.selectedItems[index];
        if (element.state === ProgressStates.Success) {
          indexToRemove.push(index);
        }
      }

      for (let index = 0; index < indexToRemove.length; index++) {
        this.selectedItems.splice(indexToRemove[index], 1);
      }

      this._snackbar.open('Konvertierung abgeschlossen', 'Erfolg', { duration: 4500 });
      this.convertInProgress = false;
    }
    else {
      item.state = ProgressStates.InProgress;
      this._jobService.copyFinishBranchOffToConnectBuilding(item.item.id).pipe(finalize(() => {
        this.convertItem();
      })).subscribe((result) => {
        item.state = ProgressStates.Success;
        item.result = result;
      }, (err) => {
        item.state = ProgressStates.Error;
      })
    }
  }

  public changeJobStates(): void {
    if (this.selectedItems.length === 0) { return; }
    if (this.convertInProgress === true) { return; }

    this.convertInProgress = true;

    for (let index = 0; index < this.selectedItems.length; index++) {
      const element = this.selectedItems[index];
      element.state = ProgressStates.Untouched;
    }

    this._snackbar.open('Aufträge werden konvertiert', 'In Bearbeitung', { duration: -1 });

    this.convertItem();
  }

  public navigateToJobLink(item: IStateHelperModel<ISimpleJobOverview, number>): void {
    const url = this._jobService.getUrlByJob(item.item);
    this._router.navigate([url, item.item.id]);
  }

  //#endregion

  //#region View-Helper

  public displayJobResult(item?: ISimpleJobOverview): string {
    if (item) { return item.name } else { return '' };
  }

  //#endregion

}
