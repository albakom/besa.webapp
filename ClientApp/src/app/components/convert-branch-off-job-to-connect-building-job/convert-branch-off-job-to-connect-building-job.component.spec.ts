import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConvertBranchOffJobToConnectBuildingJobComponent } from './convert-branch-off-job-to-connect-building-job.component';

describe('ConvertBranchOffJobToConnectBuildingJobComponent', () => {
  let component: ConvertBranchOffJobToConnectBuildingJobComponent;
  let fixture: ComponentFixture<ConvertBranchOffJobToConnectBuildingJobComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConvertBranchOffJobToConnectBuildingJobComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConvertBranchOffJobToConnectBuildingJobComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
