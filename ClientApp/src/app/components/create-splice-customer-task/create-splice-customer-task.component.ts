import { CreateFlatDialogComponent } from './../../dialogs/create-flat-dialog/create-flat-dialog.component';
import { ISimpleFlatOverviewModel } from './../../models/service/flats/isimple-flat-overview-model';
import { ConstructionStageService } from './../../services/construction-stage.service';
import { ISimpleBuildingOverviewModel } from './../../models/service/construction-stage/isimple-building-overview-model';
import { debounceTime, switchMap, distinctUntilChanged, finalize } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { MatSnackBar, MatAutocompleteSelectedEvent, MatDialog } from '@angular/material';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';
import { FormBasedPage } from '../../pages/base/form-based-page';
import { FormErrorMessage } from '../../models/helper/form-error-message';
import { FormBuilder, Validators } from '@angular/forms';
import { IContactInfo } from '../../models/service/icontact-info';
import { CustomerService, displayContactResultInternal } from '../../services/customer.service';
import { CreateSpliceCustomerErrorMessages } from '../../models/error-messages/create-splice-customer-error-messages';
import { ICreateContactDialogOptions } from '../../dialogs/create-contact-dialog/icreate-contact-dialog-options';
import { CreateContactDialogComponent } from '../../dialogs/create-contact-dialog/create-contact-dialog.component';
import { PersonTypes } from '../../models/service/person-types.enum';
import { JobService } from '../../services/job.service';
import { ICreateFlatDialogOptions } from '../../dialogs/create-flat-dialog/icreate-flat-dialog-options';
import { ISpliceBranchableJobCreateModel } from '../../models/service/jobs/splice/isplice-branchable-job-create-model';



@Component({
  selector: 'besa-create-splice-customer-task',
  templateUrl: './create-splice-customer-task.component.html',
  styleUrls: ['./create-splice-customer-task.component.scss'],
  animations: fuseAnimations
})
export class CreateSpliceCustomerTaskComponent extends FormBasedPage implements OnInit, OnDestroy {

  public customerResult: IContactInfo[];
  public buildingResult: ISimpleBuildingOverviewModel[];
  public createTaskInProgress: boolean;

  private _customerSearchTerm: Subject<string>;
  private _buildingSearchTerm: Subject<string>;

  private _currentBuilding: ISimpleBuildingOverviewModel;
  public currentFlats: ISimpleFlatOverviewModel[];
  public selectedFlat: ISimpleFlatOverviewModel;
  public createFlat: boolean;
  public showFlatSelect: boolean;

  public customerIsSelected: boolean = false;
  public buildingIsSelected: boolean = false;
  public injectJobIsCompleted: boolean = false;

  public loadBuildingDataInProgress: boolean = false;

  constructor(
    private _snackBar: MatSnackBar,
    private _fb: FormBuilder,
    private _customerService: CustomerService,
    private _constructionStageService: ConstructionStageService,
    private _dialog: MatDialog,
    private _jobService: JobService
  ) {
    super();

    this._customerSearchTerm = new Subject<string>();
    this._buildingSearchTerm = new Subject<string>();

    super.addSubscription(this._customerSearchTerm.pipe(
      debounceTime(150),
      distinctUntilChanged(),
      switchMap((item) => {
        return this._customerService.searchContacts(item);
      })
    ).subscribe((items) => {
      this.customerResult = items;
    }));

    super.addSubscription(this._buildingSearchTerm.pipe(
      debounceTime(150),
      distinctUntilChanged(),
      switchMap((item) => {
        return this._constructionStageService.searchStreets(item);
      })
    ).subscribe((items) => {
      this.buildingResult = items;
    }));

    this.prepare();

    super.initForm(
      this._fb.group({
        searchTerms: this._fb.group({
          searchCustomerValue: ['', []],
          buildingTermValue: ['', []],
        }),
        customerId: [true, [Validators.required]],
        buildingId: [null, [Validators.required]],
      })
    );

    const fieldsToSubscribeChanges: { [name: string]: Subject<string> } = {
      'searchCustomerValue': this._customerSearchTerm,
      'buildingTermValue': this._buildingSearchTerm,
    };

    for (const key in fieldsToSubscribeChanges) {
      if (fieldsToSubscribeChanges.hasOwnProperty(key)) {
        const element = fieldsToSubscribeChanges[key];

        super.addSubscription(this.form.get('searchTerms').get(key).valueChanges.subscribe((value: string) => {
          if (typeof (value) === 'string') {
            element.next(value);
          }
        }));
      }
    }
  }

  ngOnInit() {
  }

  ngOnDestroy() {
    super.unsubcripeAll();
  }

  private prepare() {
    this.customerResult = new Array();
    this.buildingResult = new Array();
    this.createTaskInProgress = false;
    this._currentBuilding = null;
    this.currentFlats = new Array();
    this.selectedFlat = null;
    this.createFlat = false;
    this.showFlatSelect = false;
    this.customerIsSelected = false;
    this.buildingIsSelected = false;
    this.injectJobIsCompleted = false;

    if (this.form) {
      this.form.reset();
      this.form.setValue({
        searchTerms: {
          searchCustomerValue: '',
          buildingTermValue: '',
        },
        customerId: null,
        buildingId: null,
      });
    }

  }

  protected getErrorMessages(): FormErrorMessage[] {
    return CreateSpliceCustomerErrorMessages;
  }

  public customerSelected(eventInfo: MatAutocompleteSelectedEvent): void {
    const contact: IContactInfo = eventInfo.option.value;
    this.form.patchValue({ customerId: contact.id });
    this.customerIsSelected = true;
  }

  public buildingSelected(eventInfo: MatAutocompleteSelectedEvent): void {
    const building: ISimpleBuildingOverviewModel = eventInfo.option.value;
    this._currentBuilding = building;
    this.form.patchValue({ buildingId: building.id });
    this.buildingIsSelected = true;
    this.currentFlats = null;
    this.selectedFlat = null;
    this.checkIfCreateFlatIsNeeded();

    this.loadBuildingDataInProgress = true;
    this._jobService.checkIfBuildingHasCable(building.id).pipe().subscribe((result) => {
      this.loadBuildingDataInProgress = false;
      if (result) { // wieder umändern, nur zum testen
        this.injectJobIsCompleted = true;

        this._constructionStageService.loadFlatsByBuilding(building).pipe().subscribe((result) => {
          if (result) {
            if ((building.commercialUnits + building.householdUnits) > 1) {
              this.showFlatSelect = true;
              this.currentFlats = result;
              this.checkIfCreateFlatIsNeeded();
            } else {
              this.showFlatSelect = false;
              this.selectedFlat = result[0];
            }
          }
        }, (err) => {
          this._snackBar.open('Ein Fehler ist beim Laden der Wohnungen aufgetreten', 'Fehler', { duration: 4500 });
        });

      } else {
        this.injectJobIsCompleted = false;
      }
    }, (err) => {
      this.loadBuildingDataInProgress = false;
      this._snackBar.open('Es konnte nicht überprüft werden ob das Einblasen schon abgeschlossen wurde', 'Fehler', { duration: 4500 });
    });
  }

  public openCreateFlatDialog(): void {
    const options: ICreateFlatDialogOptions = {
      buildingId: this._currentBuilding.id,
    };

    const ref = this._dialog.open<CreateFlatDialogComponent>(CreateFlatDialogComponent, { data: options });
    ref.afterClosed().subscribe((flat: ISimpleFlatOverviewModel) => {
      if (flat) {
        if (!this.currentFlats) {
          this.currentFlats = new Array();
        }
        this.currentFlats.push(flat);
        this.checkIfCreateFlatIsNeeded();
      }
    })
  }

  private checkIfCreateFlatIsNeeded(): void {
    if (this.currentFlats && this._currentBuilding && this.currentFlats.length < (this._currentBuilding.commercialUnits + this._currentBuilding.householdUnits)) {
      this.createFlat = true;
    }
    else this.createFlat = false;
  }

  public openCreateContactDialog(): void {
    const options: ICreateContactDialogOptions = {};
    const ref = this._dialog.open<CreateContactDialogComponent>(CreateContactDialogComponent, { data: options });
    ref.afterClosed().subscribe((contact: IContactInfo) => {
      if (contact) {
        if (this.customerResult.length > 0) {
          this.customerResult.splice(0, this.customerResult.length);
        }

        this.customerResult.push(contact);
        this.form.patchValue({ customerContactId: contact.id });
        this.form.get('searchTerms').patchValue({ searchCustomerValue: this.displayContactResult(contact) });
      }
    });
  }

  public displayContactResult(item?: IContactInfo): string {
   return displayContactResultInternal(item);
  }

  public displayBuildingResult(item?: ISimpleBuildingOverviewModel): string {
    return item ? item.streetName : '';
  }

  public createSpliceCustomerTask() {
    if (this.createTaskInProgress === true || !this.selectedFlat) { return; }

    this.createTaskInProgress = true;
    this._snackBar.open('Kunde splicen wird erstellt', 'In Bearbeitung', { duration: -1 });

    const value: ISpliceBranchableJobCreateModel = {
      flatId: this.selectedFlat.id,
      customerContactId: this.form.value.customerId,
    }

    this._jobService.createSpliceBranchableJob(value).pipe(finalize(() => {
      this.createTaskInProgress = false;
      this.prepare();
    })).subscribe((result) => {
      this._snackBar.dismiss();
      if (result > 0) {
        this._snackBar.open('Kunden splicen wurde erfolgreich erstellt', 'Erfolg', { duration: 4500 });
      } else {
        this._snackBar.open('Kunden konnte nicht erstellt werden', 'Fehler', { duration: 4500 });
      }
    }, (err) => {
      this._snackBar.open('Kunden splicen konnte nicht erstellt werden', 'Fehler', { duration: 4500 });
    });
  }

}
