import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateSpliceCustomerTaskComponent } from './create-splice-customer-task.component';

describe('CreateSpliceCustomerTaskComponent', () => {
  let component: CreateSpliceCustomerTaskComponent;
  let fixture: ComponentFixture<CreateSpliceCustomerTaskComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateSpliceCustomerTaskComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateSpliceCustomerTaskComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
