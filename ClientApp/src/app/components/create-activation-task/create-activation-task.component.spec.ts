import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateActivationTaskComponent } from './create-activation-task.component';

describe('CreateActivationTaskComponent', () => {
  let component: CreateActivationTaskComponent;
  let fixture: ComponentFixture<CreateActivationTaskComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateActivationTaskComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateActivationTaskComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
