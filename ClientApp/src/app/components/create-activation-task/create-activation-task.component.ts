import { ActivationJobIpAddressRequirements } from './../../models/service/jobs/activation/activation-job-ip-address-requirements.enum';
import { fuseAnimations } from '@fuse/animations';
import { CreateFlatDialogComponent } from '../../dialogs/create-flat-dialog/create-flat-dialog.component';
import { IArticleOverviewModel } from './../../models/service/article/iarticle-overview-model';
import { ISimpleFlatOverviewModel } from '../../models/service/flats/isimple-flat-overview-model';
import { MatSnackBar, MatDialog } from '@angular/material';
import { finalize ,  debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { ConstructionStageService } from './../../services/construction-stage.service';
import { CustomerService, displayContactResultInternal } from './../../services/customer.service';
import { FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBasedPage } from '../../pages/base/form-based-page';
import { FormErrorMessage } from '../../models/helper/form-error-message';
import { CreateActivationJobErrorMesagges } from '../../models/error-messages/create-activation-job-error-message';
import { Subject } from 'rxjs';
import { IContactInfo } from '../../models/service/icontact-info';
import { ISimpleBuildingOverviewModel } from '../../models/service/construction-stage/isimple-building-overview-model';
import { MatAutocompleteSelectedEvent } from '@angular/material';
import { JobService } from '../../services/job.service';
import { ArticleService } from '../../services/article/article.service';
import { ICreateFlatDialogOptions } from '../../dialogs/create-flat-dialog/icreate-flat-dialog-options';
import { ICreateContactDialogOptions } from '../../dialogs/create-contact-dialog/icreate-contact-dialog-options';
import { CreateContactDialogComponent } from '../../dialogs/create-contact-dialog/create-contact-dialog.component';
import { IActivationJobCreateModel } from '../../models/service/jobs/activation/activation-job-create-model';

@Component({
  selector: 'besa-create-activation-task',
  templateUrl: './create-activation-task.component.html',
  animations: fuseAnimations,
  styleUrls: ['./create-activation-task.component.scss']
})
export class CreateActivationTaskComponent extends FormBasedPage implements OnInit, OnDestroy {

  //#region Fields

  private _currentBuilding: ISimpleBuildingOverviewModel;
  private _customerSearchTerm: Subject<string>;
  private _buildingSearchTerm: Subject<string>;

  //#endregion

  //#region Properties

  public customerResult: IContactInfo[];
  public buildingResult: ISimpleBuildingOverviewModel[];
  public createTaskInProgress: boolean;
  public currentFlats: ISimpleFlatOverviewModel[];
  public networkOptions: ActivationJobIpAddressRequirements[];

  public loadBuildingDataInProgress: boolean;
  public createFlat: boolean;
  public showFlatSelect: boolean;

  public loadingDeviesInProgress: boolean;
  public devices: IArticleOverviewModel[];

  //#endregion

  constructor(private _fb: FormBuilder,
    private _customerService: CustomerService,
    private _jobService: JobService,
    private _articleService: ArticleService,
    private _snackBar: MatSnackBar,
    private _dialog: MatDialog,
    private _constructionStageService: ConstructionStageService) {
    super();

    this.networkOptions = [
      ActivationJobIpAddressRequirements.None, 
      ActivationJobIpAddressRequirements.Slash29, 
      ActivationJobIpAddressRequirements.Slash28, 
      ActivationJobIpAddressRequirements.Slash27];

    this._customerSearchTerm = new Subject<string>();
    this._buildingSearchTerm = new Subject<string>();

    super.initForm(
      _fb.group(
        {
          searchTerms: this._fb.group({
            searchCustomerValue: [null, []],
            buildingTermValue: [null, []],
          }),
          flatId: [null, [Validators.required]],
          customerContactId: [null, [Validators.required]],
          deviceId: [null, []],
          unchagedIpAdress: [false, [Validators.required]],
          addressRequirement: [ActivationJobIpAddressRequirements.None, [Validators.required]],
        }
      )
    );

    super.addSubscription(this._customerSearchTerm.pipe(
      debounceTime(150),
      distinctUntilChanged(),
      switchMap((item) => {
        return this._customerService.searchContacts(item);
      })
    ).subscribe((items) => {
      this.customerResult = items;
    }));

    super.addSubscription(this._buildingSearchTerm.pipe(
      debounceTime(150),
      distinctUntilChanged(),
      switchMap((item) => {
        return this._constructionStageService.searchStreets(item);
      })
    ).subscribe((items) => {
      this.buildingResult = items;
    }));

    const fieldsToSubscribeChanges: { [name: string]: Subject<string> } = {
      'searchCustomerValue': this._customerSearchTerm,
      'buildingTermValue': this._buildingSearchTerm,
    };

    for (const key in fieldsToSubscribeChanges) {
      if (fieldsToSubscribeChanges.hasOwnProperty(key)) {
        const element = fieldsToSubscribeChanges[key];

        super.addSubscription(this.form.get('searchTerms').get(key).valueChanges.subscribe((value: string) => {
          if (typeof (value) === 'string') {
            element.next(value);
          }
        }));
      }
    }
  }

  private prepare() {
    this.customerResult = new Array();
    this.buildingResult = new Array();
    this.createTaskInProgress = false;
    this._currentBuilding = null;
    this.currentFlats = new Array();
    //this.selectedFlat = null;
    this.createFlat = false;
    this.showFlatSelect = false;
    //this.buildingIsSelected = false;
    //this.injectJobIsCompleted = false;

    if (this.form) {
      this.form.reset();
      this.form.setValue({
        searchTerms: {
          searchCustomerValue: null,
          buildingTermValue: null,
        },
        flatId: null,
        customerContactId: null,
        deviceId: null,
        unchagedIpAdress: false,
        addressRequirement: ActivationJobIpAddressRequirements.None,
      });
    }

  }

  ngOnInit() {
    this.loadDevices();
  }

  ngOnDestroy() {
    super.unsubcripeAll();
  }

  //#region Methods

  public loadDevices(): void {
    this.loadingDeviesInProgress = true;

    this._articleService.loadArticle().pipe(finalize(() => {
      this.loadingDeviesInProgress = false;
    })).subscribe((result) => {
      this.devices = result;
    }, (err) => {
      this._snackBar.open('Fehler', 'Geräte konnten nicht geladen werden', { duration: 4500 });
    })
  }

  public customerSelected(eventInfo: MatAutocompleteSelectedEvent): void {
    const contact: IContactInfo = eventInfo.option.value;
    this.form.patchValue({ customerContactId: contact.id });
  }

  public buildingSelected(eventInfo: MatAutocompleteSelectedEvent): void {
    const building: ISimpleBuildingOverviewModel = eventInfo.option.value;
    this.currentFlats = null;
    this._currentBuilding = building;

    const maxFlatAmount = building.commercialUnits + building.householdUnits;
    this.loadBuildingDataInProgress = true;
    this._constructionStageService.loadFlatsByBuilding(building).pipe(
      finalize(() => {
        this.loadBuildingDataInProgress = false;
      })
    ).subscribe((result) => {
      this.currentFlats = result;

      if (maxFlatAmount > 1) {
        this.showFlatSelect = true;
        this.checkIfCreateFlatIsNeeded();
      } else {
        this.showFlatSelect = false;
        this.form.patchValue({ flatId: result[0].id });
      }

    }, (err) => {
      this._snackBar.open('Ein Fehler ist beim Laden der Wohnungen aufgetreten', 'Fehler', { duration: 4500 });
    });
  }

  public openCreateFlatDialog(): void {
    const options: ICreateFlatDialogOptions = {
      buildingId: this._currentBuilding.id,
    };

    const ref = this._dialog.open<CreateFlatDialogComponent>(CreateFlatDialogComponent, { data: options });
    ref.afterClosed().subscribe((flat: ISimpleFlatOverviewModel) => {
      if (flat) {
        if (!this.currentFlats) {
          this.currentFlats = new Array();
        }
        this.currentFlats.push(flat);
        this.checkIfCreateFlatIsNeeded();
      }
    });
  }

  private checkIfCreateFlatIsNeeded(): void {
    if (this.currentFlats && this._currentBuilding && this.currentFlats.length < (this._currentBuilding.commercialUnits + this._currentBuilding.householdUnits)) {
      this.createFlat = true;
    }
    else {
      this.createFlat = false;
    }
  }

  protected getErrorMessages(): FormErrorMessage[] {
    return CreateActivationJobErrorMesagges;
  }

  public openCreateContactDialog(): void {
    const options: ICreateContactDialogOptions = {};
    const ref = this._dialog.open<CreateContactDialogComponent>(CreateContactDialogComponent, { data: options });
    ref.afterClosed().subscribe((contact: IContactInfo) => {
      if (contact) {
        if (this.customerResult.length > 0) {
          this.customerResult.splice(0, this.customerResult.length);
        }

        this.customerResult.push(contact);
        this.form.patchValue({ customerContactId: contact.id });
        this.form.get('searchTerms').patchValue({ searchCustomerValue: this.displayContactResult(contact) });
      }
    });
  }


  public createTask() {
    if (this.createTaskInProgress === true || this.form.invalid == true) { return; }

    this.createTaskInProgress = true;
    this._snackBar.open('Auftrag wird erstellt', 'In Bearbeitung', { duration: -1 });
    const value: IActivationJobCreateModel = this.form.value;

    this._jobService.createActivationJob(value).pipe(finalize(() => {
      this.createTaskInProgress = false;

    })).subscribe((result) => {
      this._snackBar.dismiss();
      if (result > 0) {
        this._snackBar.open('Aufgabe erfolgreich erstellt', 'Erfolg', { duration: 4500 });
        this.prepare();
      } else {
        this._snackBar.open('Aufgabe konnten nicht erstellt werden', 'Fehler', { duration: 4500 });
      }
    }, (err) => {
      this._snackBar.open('Aufgabe konnten nicht erstellt werden', 'Fehler', { duration: 4500 });
    });
  }

  //#endregion

  //#region View-helper

  public displayContactResult(item?: IContactInfo): string {
    return displayContactResultInternal(item);
  }

  public displayBuildingResult(item?: ISimpleBuildingOverviewModel): string {
    return item ? item.streetName : '';
  }

  //#endregion
}
