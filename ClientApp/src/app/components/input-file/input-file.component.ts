import { Component, OnInit, Input, Output, ViewChild, ElementRef, EventEmitter, AfterViewInit } from '@angular/core';

@Component({
  selector: 'besa-input-file',
  templateUrl: './input-file.component.html',
  styleUrls: ['./input-file.component.scss']
})
export class InputFileComponent implements OnInit, AfterViewInit {

  @Input() disabled: boolean = false;

  @Input() accept: string;
  @Output() onFileSelect: EventEmitter<File[]> = new EventEmitter();

  private _capture;

  @Input()
  public get capture(): string {
    return this._capture;
  }

  public set capture(value: string) {
    this._capture = value;
    this.setCapture();
  }

  @Input()
  public mutliple = false;

  @Input()
  public icon = 'file_upload';

  @Input()
  public showIcon: boolean = true;

  @Input()
  public caption: string = 'Hochladen';

  @ViewChild('inputFile') nativeInputFile: ElementRef;

  private _files: File[];

  get fileCount(): number { return this._files && this._files.length || 0; }

  onNativeInputFileSelect($event) {
    if ($event.srcElement) {
      this._files = $event.srcElement.files;
    }
    else if ($event.originalTarget) {
      this._files = $event.originalTarget.files;
    }
    
    this.onFileSelect.emit(this._files);
  }

  selectFile() {
    if (this.disabled === true) { return; }
    this.nativeInputFile.nativeElement.click();
  }

  ngOnInit() {
  }

  ngAfterViewInit() {
    this.setCapture();
  }

  private setCapture(): void {
    if (!this.nativeInputFile) { return; }
    const inputElement: HTMLInputElement = this.nativeInputFile.nativeElement;
    if (this._capture) {
      inputElement.setAttribute('capture', this._capture);
    }
    else {
      inputElement.removeAttribute('capture');
    }
  }

}
