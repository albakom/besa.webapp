import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BindPreapreBranchableJobsComponent } from './bind-preapre-branchable-jobs.component';

describe('BindPreapreBranchableJobsComponent', () => {
  let component: BindPreapreBranchableJobsComponent;
  let fixture: ComponentFixture<BindPreapreBranchableJobsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BindPreapreBranchableJobsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BindPreapreBranchableJobsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
