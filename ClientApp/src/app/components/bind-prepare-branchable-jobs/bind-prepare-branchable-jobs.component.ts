import { finalize } from 'rxjs/operators';
import { OnDestroy } from '@angular/core/src/metadata/lifecycle_hooks';
import { Component, OnInit } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';
import { SubscriptionAwareComponent } from '../../helper/subscription-aware-component';
import { BindJobsBase } from '../base/bind-jobs-base';
import { IJobOverviewModel } from '../../models/service/jobs/ijobs-overview-model';
import { IConstructionStageDetailModel } from '../../models/service/construction-stage/iconstruction-stage-detail-for-model';
import { Observable } from 'rxjs';
import { RefreshManagerService } from '../../services/refresh-manager.service';
import { CompanyService } from '../../services/companies/company.service';
import { JobService } from '../../services/job.service';
import { ConstructionStageService } from '../../services/construction-stage.service';
import { MatSnackBar } from '@angular/material';
import { FormBuilder } from '@angular/forms';
import { ISimplePrepareBranchableForSpliceJobOverviewModel } from '../../models/service/jobs/splice/isimple-prepare-branchable-for-splice-job-overview-model';
import { SimpleOverviewBasedBindJobBase } from '../base/simple-overview-based-bind-jobs-base';

@Component({
  selector: 'besa-bind-prepare-branchable-jobs',
  templateUrl: './bind-prepare-branchable-jobs.component.html',
  styleUrls: ['./bind-prepare-branchable-jobs.component.scss'],
  animations: fuseAnimations,
})
export class BindPrepareBranchableJobsComponent extends SimpleOverviewBasedBindJobBase<ISimplePrepareBranchableForSpliceJobOverviewModel> implements OnInit {

  //#region Properties

  //#endregion

  constructor(
    refreshManager: RefreshManagerService,
    companyService: CompanyService,
    jobService: JobService,
    snackBar: MatSnackBar,
    fb: FormBuilder) {
    super(refreshManager, companyService, jobService, snackBar, fb);

  }

  ngOnInit() {
    this.loadOpenJobs();
  }

  //#endregion

  //#region Methods

  protected loadPossibleJobs(): Observable<ISimplePrepareBranchableForSpliceJobOverviewModel[]> {
    return this._jobService.getOpenPrepareBranchableForSpliceJobs();
  }

  //#endregion

}
