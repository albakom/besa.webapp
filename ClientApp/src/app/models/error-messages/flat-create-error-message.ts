import { FormErrorMessage } from '../helper/form-error-message';
export const FlatCreateErrorMessage: FormErrorMessage[] = [
    new FormErrorMessage('buildingId', 'required', 'Es muss ein Gebäude angegeben werden'),
    new FormErrorMessage('number', 'required', 'Es muss eine Wohnungsnummer angegeben werden'),
    new FormErrorMessage('number', 'maxlength', 'Der Name der Wohnung ist zu lang'),
    new FormErrorMessage('description', 'maxlength', 'Die Beschreibung der Wohnung ist zu lang'),
    new FormErrorMessage('floor', 'maxlength', 'Der Name des Geschosses ist zu lang'),
]
