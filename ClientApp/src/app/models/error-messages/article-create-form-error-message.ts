import { FormErrorMessage } from '../helper/form-error-message';
export const ArticleCreateFormErrorMessage: FormErrorMessage[] = [
    new FormErrorMessage('name', 'required', 'Eine Ware benötigt einen Namen'),
    new FormErrorMessage('name', 'maxlength', 'Der Name ist zu lang'),
];
