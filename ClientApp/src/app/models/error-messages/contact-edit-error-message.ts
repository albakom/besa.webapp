import { FormErrorMessage } from './../helper/form-error-message';
export const ContactEditErrorMessage: FormErrorMessage[] = [
    new FormErrorMessage('surname', 'maxlength', 'Der Vorname ist zu lang'),
    new FormErrorMessage('lastname', 'required', 'Es muss ein Nachname angegeben werden'),
    new FormErrorMessage('lastname', 'maxlength', 'Der Nachname ist zu lang'),
    new FormErrorMessage('type', 'required', 'Eine Anrede ist notwendig'),new FormErrorMessage('companyName', 'maxlength', 'Der Firmenname ist zu lang'),
    new FormErrorMessage('emailAddress', 'required', 'Es muss eine E-Mail vorhanden sein'),
    new FormErrorMessage('emailAddress', 'email', 'Ungültige Adresse'),
    new FormErrorMessage('emailAddress', 'maxlength', 'Die E-Mail-Adresse ist zu lang'),
    new FormErrorMessage('phone', 'required', 'Es muss eine Telefonnummer angegeben werden'),
    new FormErrorMessage('phone', 'maxlength', 'Die Telefonnummer ist zu lang'),
    
    new FormErrorMessage('street', 'required', 'Der Straßenname wird benötigt'),
    new FormErrorMessage('street', 'maxlength', 'Der Straßenname ist zu lang'),
    new FormErrorMessage('streetNumber', 'required', 'Die Hausnummer wird benötigt'),
    new FormErrorMessage('streetNumber', 'maxlength', 'Die Hausnummer ist zu lang'),
    new FormErrorMessage('city', 'required', 'Der Stadtname wird benötigt'),
    new FormErrorMessage('city', 'maxlength', 'Der Stadtname ist zu lang'),
    new FormErrorMessage('postalCode', 'required', 'Die Postleitszahl wird benötigt'),
    new FormErrorMessage('postalCode', 'maxlength', 'Die Postleitszahl ist zu lang'),
    new FormErrorMessage('postalCode', 'minlength', 'Die Postleitszahl ist zu kurz'),
];