import { FormErrorMessage } from "../helper/form-error-message";

export const CreateActivationJobErrorMesagges: FormErrorMessage[] =
    [
        new FormErrorMessage('buildingId', 'required', 'Es muss ein Gebäude angegeben werden'),
        new FormErrorMessage('customerContactId', 'required', 'Es muss ein Kunde angeben werden'),
    ];
