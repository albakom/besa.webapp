import { FormErrorMessage } from '../helper/form-error-message';
export const ContactCreateErrorMessage: FormErrorMessage[] = [
    new FormErrorMessage('type', 'required', 'Eine Anrede ist notwendig'),
    new FormErrorMessage('surname', 'maxlength', 'Der Vorname ist zu lang'),
    new FormErrorMessage('lastname', 'required', 'Es muss ein Nachname angegeben werden'),
    new FormErrorMessage('companyName', 'maxlength', 'Der Firmenname ist zu lang'),
    // new FormErrorMessage('emailAddress', 'required', 'Es muss eine E-Mail vorhanden sein'),
    new FormErrorMessage('emailAddress', 'email', 'Ungültige Adresse'),
    new FormErrorMessage('emailAddress', 'maxlength', 'Die E-Mail-Adresse ist zu lang'),
    new FormErrorMessage('phone', 'required', 'Es muss eine Telefonnummer angegeben werden'),
    new FormErrorMessage('phone', 'maxlength', 'Die Telefonnummer ist zu lang'),
    
    new FormErrorMessage('address.street', 'maxlength', 'Der Straßenanme ist zu lang'),
]
