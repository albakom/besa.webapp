import { FormErrorMessage } from "../helper/form-error-message";

export const CreateSpliceOdfJobErrorMessages: FormErrorMessage[] =
    [
        new FormErrorMessage('mainCableId', 'required', 'Es muss ein Kabel angegeben werden'),
        new FormErrorMessage('mainCableId', 'min', 'Es muss ein Kabel angegeben werden'),
    ];
