import { FormErrorMessage } from "../helper/form-error-message";

export const CreateConstructionStageFormMessages: FormErrorMessage[] =
    [
        new FormErrorMessage('name', 'required', 'Ein Baugebiet benötigt einen Namen'),
        new FormErrorMessage('name', 'maxlength', 'Der Name ist zu lang'),
    ];
