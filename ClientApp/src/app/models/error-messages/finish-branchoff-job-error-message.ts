import { FormErrorMessage } from '../helper/form-error-message';
export const FinishBranchOffJobErrorMessage: FormErrorMessage[] = [
    new FormErrorMessage('jobId', 'required', 'Es muss kein Auftrag angegeben'),
    new FormErrorMessage('comment', 'required', 'Es muss eine Wohnungsnummer angegeben werden'),
]
