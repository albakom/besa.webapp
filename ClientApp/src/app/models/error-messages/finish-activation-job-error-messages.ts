import { FormErrorMessage } from './../helper/form-error-message';
export const FinishActivationJobErrorMessages: FormErrorMessage[] = [
    new FormErrorMessage('jobId', 'required', 'Es muss kein Auftrag angegeben'),
]
