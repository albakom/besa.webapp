import { FormErrorMessage } from "../helper/form-error-message";

export const FinishJobAdministrativeFormErrorMessage: FormErrorMessage[] =
    [
        new FormErrorMessage('jobId', 'required', 'Es muss ein Job angebene werden'),
        new FormErrorMessage('finishedAt', 'date', 'Es muss ein Datum angegeben werden'),
        new FormErrorMessage('createNextJobInPipeLine', 'required', 'Soll ein Folgeauftrag erstellt werden'),
    ];
