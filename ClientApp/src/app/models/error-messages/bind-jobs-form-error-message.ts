import { FormErrorMessage } from "../helper/form-error-message";

export const BindJobsFormErrorMessages: FormErrorMessage[] =
    [
        new FormErrorMessage('companyId', 'required', 'Es muss eine Firma angegeben werden'),
        new FormErrorMessage('finishTill', 'required', 'Es muss ein Enddatum angegeben werden'),
        new FormErrorMessage('name', 'required', 'Der Auftrag benötigt einen Namen'),
        new FormErrorMessage('name', 'maxLength', 'Der Name für den Auftrag ist zu lang'),
    ];
