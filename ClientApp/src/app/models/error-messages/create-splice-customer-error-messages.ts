import { FormErrorMessage } from './../helper/form-error-message';
export const CreateSpliceCustomerErrorMessages: FormErrorMessage[] = [
    new FormErrorMessage('customerId', 'required', 'Es muss ein Kunde angegeben werden'),
    new FormErrorMessage('buildingId', 'required', 'Es muss ein Gebäude angegeben werden')
];
