import { FormErrorMessage } from "../helper/form-error-message";

export const CreateInjectJobErrorMessages: FormErrorMessage[] =
    [
        new FormErrorMessage('flatId', 'required', 'Es muss eine Wohnung angegeben werden'),
        new FormErrorMessage('customerId', 'required', 'Es muss ein Kunde angeben werden'),
    ];
