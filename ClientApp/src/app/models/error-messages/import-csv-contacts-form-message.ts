import { FormErrorMessage } from "../helper/form-error-message";

export const ImportCSVContactsFormMessage: FormErrorMessage[] =
    [
        new FormErrorMessage('seperationChar', 'required', 'Es muss ein Trennzeichen angeben werden'),
        new FormErrorMessage('seperationChar', 'minLength', 'Es darf nur ein Zeichen angegeben werden'),
        new FormErrorMessage('seperationChar', 'maxLength', 'Es darf nur ein Zeichen angegeben werden'),
        new FormErrorMessage('ignoreFirstLine', 'required', 'Soll die erste Zeile übersprungen werden?'),
        new FormErrorMessage('encoding', 'required', 'Es muss die Zeichenkodierung angeben werden'),
        new FormErrorMessage('defaultPostalCode', 'minLength', 'PLZ muss aus fünf Zeichen bestehen'),
        new FormErrorMessage('defaultPostalCode', 'maxLength', 'PLZ muss aus fünf Zeichen bestehen'),
        new FormErrorMessage('defaultCity', 'maxlength', 'Maximal 64 Zeichen für den Namen erlaubt'),
    ];
