import { FormErrorMessage } from '../helper/form-error-message';
export const FinishInjectJobErrorMessage: FormErrorMessage[] = [
    new FormErrorMessage('jobId', 'required', 'Es muss kein Auftrag angegeben'),
    new FormErrorMessage('length', 'required', 'Es muss eine Einblaslänge angegeben werden'),
    new FormErrorMessage('length', 'min', 'Die Länge muss mindestens einen Meter betragen'),
    new FormErrorMessage('length', 'max', 'Die Länge darf maximal 4500 Meter betragen'),
]
