import { FormErrorMessage } from "../helper/form-error-message";

export const ImportCSVProcedureFormMessage: FormErrorMessage[] =
    [
        new FormErrorMessage('ignoreFirstLine', 'required', 'Soll die erste Zeile übersprungen werden?'),
        new FormErrorMessage('rolebackIfErrorHappend', 'required', 'Soll im Fehlerfall, der Import rückgängig gemacht werden?'),
        new FormErrorMessage('skipError', 'required', 'Soll bei einem Fehler der Import abbrechen?'),
    ];
