import { FormErrorMessage } from '../helper/form-error-message';
export const CreateConnectBuildingJobErrorMessages: FormErrorMessage[] = [
    new FormErrorMessage('buildingId', 'required', 'Ein Gebäude muss angegeben werden'),
    new FormErrorMessage('onlyHouseConnection', 'required', 'Ist erforderlich'),
    new FormErrorMessage('ownerContactId', 'required', 'Es muss ein Eigentümer angeben werden'),
    new FormErrorMessage('description', 'maxLength', 'Die Beschreibung darf maximal 1024 zeichen lang sein'),
    new FormErrorMessage('civilWorkIsDoneByCustomer', 'required', 'Ist erforderlich'),
    new FormErrorMessage('connectionOfOtherMediaRequired', 'required', 'Ist erforderlich'),
];
