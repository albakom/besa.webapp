import { FormErrorMessage } from '../helper/form-error-message';
export const CreatePrepareBranchableForSpliceJobErrorMessages: FormErrorMessage[] = [
    new FormErrorMessage('prepareBranchableId', 'required', 'Es muss ein KVZ angegeben werden'),
]