import { FormErrorMessage } from '../helper/form-error-message';
export const FinishSpliceODFJobErrorMessage: FormErrorMessage[] = [
    new FormErrorMessage('jobId', 'required', 'Es muss kein Auftrag angegeben'),
    new FormErrorMessage('cableMetric', 'required', 'Es muss eine Einblaslänge angegeben werden'),
    new FormErrorMessage('cableMetric', 'min', 'Die Länge muss größer als null Meter betragen'),
]
