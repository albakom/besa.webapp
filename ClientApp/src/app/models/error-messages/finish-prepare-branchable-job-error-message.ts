import { FormErrorMessage } from "../helper/form-error-message";

export const FinishPrepareBranchableJobErrorMessage: FormErrorMessage[] = [
    new FormErrorMessage('jobId', 'required', 'Es muss kein Auftrag angegeben'),
]