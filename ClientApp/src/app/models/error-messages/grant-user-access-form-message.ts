import { FormErrorMessage } from '../helper/form-error-message';

export const GrantUserAccessFormMessage: FormErrorMessage[] =
    [
        new FormErrorMessage('surname', 'required', 'Es muss ein Vorname angegeben werden'),
        new FormErrorMessage('surname', 'maxlength', 'Der Vorname ist zu lang'),
        new FormErrorMessage('lastname', 'required', 'Es muss ein Nachname angegeben werden'),
        new FormErrorMessage('lastname', 'maxlength', 'Der Nachname ist zu lang'),
        new FormErrorMessage('eMailAddress', 'required', 'Es muss eine Mail-Adresse angebene werden'),
        new FormErrorMessage('eMailAddress', 'maxlength', 'Die Mail Adresse ist zu lang'),
        new FormErrorMessage('eMailAddress', 'email', 'Keine gültige Mail-Adresse'),
        new FormErrorMessage('phoneNumber', 'required', 'Es muss eine Telefonnummer angegeben werden'),
        new FormErrorMessage('phoneNumber', 'maxlength', 'Die Telefonnummer ist zu lang'),
        new FormErrorMessage('phoneNumber', 'pattern', 'Keine gültige Telefonnummer'),
        new FormErrorMessage('role', 'required', 'Es muss eine Rolle angegben werden'),
        new FormErrorMessage('role', 'min', 'Es wurde eine ungültige Rolle angegeben'),
    ];
