import { FormErrorMessage } from '../helper/form-error-message';
export const ArticleEditErrorMessage: FormErrorMessage[] = [
    new FormErrorMessage('name', 'required', 'Es muss ein Name angegeben werden')
]
