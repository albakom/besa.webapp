import { FormErrorMessage } from './../helper/form-error-message';

export const CompanyAddFormErrorMessage: FormErrorMessage[] = [
    new FormErrorMessage('name', 'required', 'Es muss ein Name angegeben werden'),
    new FormErrorMessage('name', 'maxlength', 'Der Name ist zu lang'),
    new FormErrorMessage('phone', 'required', 'Es muss eine Telefonnummer angegeben werden'),
    new FormErrorMessage('phone', 'maxlength', 'Die Telefonnummer ist zu lang'),
    new FormErrorMessage('street', 'required', 'Es muss eine Straße angegeben werden'),
    new FormErrorMessage('street', 'maxlength', 'Die Straße ist zu lang'),
    new FormErrorMessage('streetNumber', 'required', 'Es muss eine Hausnummer angegeben werden'),
    new FormErrorMessage('streetNumber', 'maxlength', 'Die Hausnummer ist zu lang'),
    new FormErrorMessage('city', 'required', 'Es muss eine Stadt angegeben werden'),
    new FormErrorMessage('city', 'maxlength', 'Die Stadt ist zu lang'),
    new FormErrorMessage('postalCode', 'required', 'Es muss eine Postleitszahl angegeben werden'),
    new FormErrorMessage('postalCode', 'min', 'Die Postleitzahl ist zu kurz'),
    new FormErrorMessage('postalCode', 'maxlength', 'Die Postleitzahl ist zu lang'),
];
