import { FormErrorMessage } from './../helper/form-error-message';

export const UserEditFormErrorMessage: FormErrorMessage[] = [
    new FormErrorMessage('surname', 'required', 'Es muss ein Vorname angegeben werden'),
    new FormErrorMessage('lastname', 'required', 'Es muss ein Name angegeben werden'),
    new FormErrorMessage('phone', 'required', 'Es muss eine Telefonnummer angegeben werden'),
    new FormErrorMessage('eMailAddress', 'required', 'Es muss eine E-Mail-Adresse angegeben werden'),
];