import { FileAccessObjects } from './file-access-objects.enum';
export interface IFileAccessModel {
    objectId: number;
    objectType: FileAccessObjects;
}