import { FileAccessObjects } from "./file-access-objects.enum";

export interface IFileAccessOverviewModel {
    objectId: number;
    objectType: FileAccessObjects;
    objectName: string;
}