export enum FileAccessObjects {
    Building = 1,
    BranchOffJob = 2,
    HouseConnectionJob = 3,
    InjectJob = 4,
}