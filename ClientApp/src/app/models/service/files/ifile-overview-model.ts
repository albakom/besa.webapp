import { IUniqueElement } from './../iunique-element';
import { FileTypes } from './file-types.enum';
export interface IFileOverviewModel extends IUniqueElement {
    name: string;
    extention: string;
    type: FileTypes;
    size: number;
}
