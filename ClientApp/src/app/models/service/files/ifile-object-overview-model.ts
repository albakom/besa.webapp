import { IUniqueElement } from './../iunique-element';
export interface IFileObjectOverviewModel extends IUniqueElement {
    name: string;
}
