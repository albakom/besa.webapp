import { IUniqueElement } from './../iunique-element';
import { IFileAccessModel } from './ifile-access-model';
export interface IFileEditModel extends IUniqueElement {
    name: string;
    accessList: IFileAccessModel[];
}
