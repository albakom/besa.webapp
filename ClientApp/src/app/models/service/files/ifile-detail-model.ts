import { IUniqueElement } from './../iunique-element';
import { FileTypes } from './file-types.enum';
import { IFileAccessOverviewModel } from './ifile-access-overview-model';
export interface IFileDetailModel extends IUniqueElement {
    name: string;
    extention: string;
    type: FileTypes;
    size: number;
    accessControl: IFileAccessOverviewModel[];
}