import { IUniqueElement } from "../iunique-element";
import { ISimpleCabinetOverviewModel } from "./isimple-cabinet-overview-model";
import { ISimpleBuildingOverviewModel } from "./isimple-building-overview-model";

export interface IConstructionStageDetailsForEditModel extends IUniqueElement {
    name: string;
    branchables: ISimpleCabinetOverviewModel[];
    buildings: { [brachableId: number]: ISimpleBuildingOverviewModel[] };
}