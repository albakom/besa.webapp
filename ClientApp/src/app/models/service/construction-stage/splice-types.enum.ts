export enum SpliceTypes {
    LoopUntouched = 100,
    LoopTouched = 85,
    Normal = 1 ,
    Buffer = 90,
    Deposit = 80,
    Unkown = 101,
}
