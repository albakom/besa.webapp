export interface IFiberBundleModel {
    number: number;
    color?: string;
}