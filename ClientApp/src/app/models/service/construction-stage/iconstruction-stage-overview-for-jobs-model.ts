import { IUniqueElement } from './../iunique-element';
import { ISimpleCabinetOverviewModel } from './isimple-cabinet-overview-model';
export interface IConstructionStageOverviewForJobsModel extends IUniqueElement  {
    name: string;
    doingPercentage: number;
    branchables: ISimpleCabinetOverviewModel[];
}
