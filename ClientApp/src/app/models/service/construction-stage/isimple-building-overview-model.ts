import { IUniqueElement } from '../iunique-element';

export interface ISimpleBuildingOverviewModel extends IUniqueElement {
    streetName: string;
    householdUnits: number;
    commercialUnits: number;
}
