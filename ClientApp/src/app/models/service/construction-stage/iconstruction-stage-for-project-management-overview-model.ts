import { IUniqueElement } from "../iunique-element";

export interface IConstructionStageForProjectManagementOverviewModel extends IUniqueElement {
    name: string;
    buildingsWithoutBranchOffJobs: number;
}
