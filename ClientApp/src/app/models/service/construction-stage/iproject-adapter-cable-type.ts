import { IUniqueElement } from "../iunique-element";

export interface IProjectAdapterCableType extends IUniqueElement {
    adapterId: string;
    name: string;
    fiberAmount: number;
    fiberPerBundle: number;
}