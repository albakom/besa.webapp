import { IUniqueElement } from "../iunique-element";
import { IGPSCoordinate } from "../other/igpscoordinate";

export interface ISimpleBuildingOverviewWithGPSModel extends IUniqueElement {
    streetName: string;
    householdUnits: number;
    commercialUnits: number;
    coordinate: IGPSCoordinate;
}