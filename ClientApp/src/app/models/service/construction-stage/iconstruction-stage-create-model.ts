export interface IConstructionStageCreateModel {
    name: string;
    cabinets?: number[];
    sleeves: number[];
    createJobs: boolean;
}
