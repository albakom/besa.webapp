import { IUniqueElement } from './../iunique-element';
import { IBranchableOverviewWithJobs } from './ibranchable-overview-with-jobs';
import { IJobOverviewModel } from '../jobs/ijobs-overview-model';
export interface IConstructionStageDetailModel<TJob extends IJobOverviewModel> extends IUniqueElement {
    name: string;
    doingPercentage: number;
    branchables: IBranchableOverviewWithJobs<TJob>[];
}
