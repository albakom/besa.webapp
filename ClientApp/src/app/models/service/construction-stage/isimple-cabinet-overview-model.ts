import { IUniqueElement } from '../iunique-element';

export interface ISimpleCabinetOverviewModel extends IUniqueElement {
    name: string;
}
