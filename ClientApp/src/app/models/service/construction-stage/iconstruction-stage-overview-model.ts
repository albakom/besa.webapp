import { IUniqueElement } from '../iunique-element';
export interface IConstructionStageOverviewModel extends IUniqueElement {
   name: string;
   doingPercentage: number;
   householdsUnits: number;
   commercialUnits: number;
   cabinetAmount: number;
   sleeveAmount: number;
}
