import { IUniqueElement } from "../iunique-element";
import { IGPSCoordinate } from "../other/igpscoordinate";
import { ISimpleBuildingConnenctionJobOverview } from "../jobs/overview/isimple-building-connenction-job-overview";
import { IJobOverviewModel } from "../jobs/ijobs-overview-model";

export interface IBranchableOverviewWithJobs<TJob extends IJobOverviewModel> extends IUniqueElement {
    name: string;
    coordinate: IGPSCoordinate;
    doingPercentage: number;
    jobs: TJob[];
}
