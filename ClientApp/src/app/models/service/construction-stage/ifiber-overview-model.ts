import { IUniqueElement } from './../iunique-element';
export interface IFiberOverviewModel extends IUniqueElement {
    name: string;
    bundleNumber: number;
    numberinBundle: number;
    color: string;
    fiberNumber: number;
    cableId: number;
}