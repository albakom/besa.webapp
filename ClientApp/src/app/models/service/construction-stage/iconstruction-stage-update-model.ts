import { IUniqueElement } from '../iunique-element';
export interface IConstructionStageUpdateModel extends IUniqueElement {
    name: string;
    createBranchOffJobs: boolean;
    addedBranchables: number[];
    removedBranchables: number[];
}