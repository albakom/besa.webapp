import { IUniqueElement } from "../iunique-element";
import { IGPSCoordinate } from '../other/igpscoordinate';
import { BranchableTypes } from "./branchable-types.enum";

export interface ISimpleBranchableWithGPSModel extends IUniqueElement {
    name: string;
    type: BranchableTypes;
    coordinate: IGPSCoordinate;
}