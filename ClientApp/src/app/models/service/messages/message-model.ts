import { MessageActionDetails } from './message-action-details';
import { ISimpleUserOverviewModel } from './../user/isimple-user-overview-model';
import { ISimpleEmployeeModel } from '../companies/isimple-employee-model';
import { MessageActions } from './message-actions.enum';
import { IMessageActionDetailsModel } from './details/imessage-action-details-model';
import { IUserOverviewModel } from '../user/iuser-overview-model';
import { MessageRelatedObjectTypes } from 'app/models/service/tasks/message-related-object-types.enum';
export class MessageModel {
    timeStamp: Date;
    relatedObjectType: MessageRelatedObjectTypes
    sourceUser?: IUserOverviewModel
    action: MessageActions;
    actionDetails: IMessageActionDetailsModel;
    markedAsRead: boolean;
    id: number;
}
