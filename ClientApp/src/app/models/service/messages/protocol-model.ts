import { IUniqueElement } from './../iunique-element';
import { MessageActionDetails } from './message-action-details';
import { MessageActions } from './message-actions.enum';
import { MessageRelatedObjectTypes } from '../tasks/message-related-object-types.enum';
import { IUserOverviewModel } from '../user/iuser-overview-model';
export interface ProtocolModel extends IUniqueElement {
    timestamp: string;
    action: MessageActions;
    type: MessageRelatedObjectTypes;
    relatedObjectId?: number;
    sourcedBy: IUserOverviewModel;
    name: string;
}
