import { ISimpleJobOverview } from '../jobs/isimple-job-overview';
import { MessageModel } from './message-model';
export class JobMessageModel extends MessageModel {
    jobs: ISimpleJobOverview[];
}
