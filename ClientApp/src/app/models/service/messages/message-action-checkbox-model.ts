import { MessageActions } from "./message-actions.enum";

export class MessageActionCheckboxModel {
    public action: MessageActions;
    public isSelected: boolean;
}
