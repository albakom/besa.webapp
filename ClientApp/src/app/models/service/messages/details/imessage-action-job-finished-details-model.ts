import { IMessageActionJobBasedDetailsModel } from "./imessage-action-job-based-details-model";
import { IUserOverviewModel } from "../../user/iuser-overview-model";
import { IExplainedBooleanModel } from "../../iexplained-boolean-model";

export interface IMessageActionJobFinishedDetailsModel extends IMessageActionJobBasedDetailsModel {
    finishedBy: IUserOverviewModel;
    finishedAt: string;
    problemHappend: IExplainedBooleanModel;
}
