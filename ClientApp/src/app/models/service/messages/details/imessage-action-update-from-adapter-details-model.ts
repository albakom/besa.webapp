import { IMessageActionDetailsModel } from "./imessage-action-details-model";
import { MessageActions } from "../message-actions.enum";

export interface IMessageActionUpdateFromAdapterDetailsModel extends IMessageActionDetailsModel {
    //level: ProtocolLevels;
    action: MessageActions;
}
