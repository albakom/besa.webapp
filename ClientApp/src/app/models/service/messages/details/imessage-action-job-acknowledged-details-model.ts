import { IMessageActionJobBasedDetailsModel } from "./imessage-action-job-based-details-model";
import { IUserOverviewModel } from '../../user/iuser-overview-model';

export interface IMessageActionJobAcknowledgedDetailsModel extends IMessageActionJobBasedDetailsModel {
    acepptedBy: IUserOverviewModel;
    acceptedAt: string;
}
