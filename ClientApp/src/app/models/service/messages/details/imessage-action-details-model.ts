import { IUniqueElement } from '../../iunique-element';
export interface IMessageActionDetailsModel extends IUniqueElement {
    name: string;
}
