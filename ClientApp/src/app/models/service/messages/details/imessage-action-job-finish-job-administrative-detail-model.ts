import { IMessageActionJobBasedDetailsModel } from "./imessage-action-job-based-details-model";
import { IUserOverviewModel } from "../../user/iuser-overview-model";

export interface IMessageActionJobFinishJobAdministrativeDetailModel extends IMessageActionJobBasedDetailsModel {
    finishedBy: IUserOverviewModel;
    finishedAt: string;
}
