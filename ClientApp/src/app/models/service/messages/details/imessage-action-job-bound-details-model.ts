import { IMessageActionJobBasedDetailsModel } from "./imessage-action-job-based-details-model";
import { IJobCollectionOverviewModel } from "../../jobs/overview/ijob-collection-overview-model";

export interface IMessageActionJobBoundDetailsModel extends IMessageActionJobBasedDetailsModel {
    collectionJob: IJobCollectionOverviewModel;
}
