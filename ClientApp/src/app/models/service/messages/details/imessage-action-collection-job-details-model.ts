import { IJobCollectionOverviewModel } from "../../jobs/overview/ijob-collection-overview-model";
import { IMessageActionDetailsModel } from "./imessage-action-details-model";

export interface IMessageActionCollectionJobDetailsModel extends IMessageActionDetailsModel {
    collectionJob: IJobCollectionOverviewModel;
}
