import { IMessageActionDetailsModel } from "./imessage-action-details-model";
import { IProcedureOverviewModel } from "../../procedure/iprocedure-overview-model";

export interface IMessageActionProcedureFinishedDetailsModel extends IMessageActionDetailsModel {
    procedure: IProcedureOverviewModel;
}
