import { IUserOverviewModel } from './../../user/iuser-overview-model';
import { IMessageActionDetailsModel } from "./imessage-action-details-model";

export interface IMessageActionRemoveMemberDetailsModel extends IMessageActionDetailsModel {
    member: IUserOverviewModel;
}
