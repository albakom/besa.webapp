import { IMessageActionJobBasedDetailsModel } from "./imessage-action-job-based-details-model";
import { IUserOverviewModel } from "../../user/iuser-overview-model";

export interface IMessageActionJobDiscardDetailsModel extends IMessageActionJobBasedDetailsModel {
    discaredBy: IUserOverviewModel;
    discardedAt: string;
    reason: string;
}
