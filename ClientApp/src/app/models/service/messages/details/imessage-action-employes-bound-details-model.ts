import { IUserOverviewModel } from "../../user/iuser-overview-model";
import { ISimpleCompanyOverviewModel } from '../../companies/isimple-company-overview-model';
import { IMessageActionDetailsModel } from "./imessage-action-details-model";

export interface IMessageActionEmployesBoundDetailsModel extends IMessageActionDetailsModel {
    removed: boolean;
     employee: IUserOverviewModel;
     company: ISimpleCompanyOverviewModel;
}
