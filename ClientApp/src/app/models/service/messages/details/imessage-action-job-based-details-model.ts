import { IMessageActionDetailsModel } from "./imessage-action-details-model";
import { ISimpleJobOverview } from '../../jobs/isimple-job-overview';

export interface IMessageActionJobBasedDetailsModel extends IMessageActionDetailsModel {
    jobOverview: ISimpleJobOverview;
}
