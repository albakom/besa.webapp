export enum MarkedAsRead {
    All,
    OnlyRead,
    OnlyUnread,
}
