import { ActionType } from './action-type.enum';
export class MessageActionDetails {
    type: ActionType;
    id: number;
    name: string;
}
