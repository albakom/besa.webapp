import { MessageActions } from "./message-actions.enum";
import { ActionType } from "./action-type.enum";
import { MarkedAsRead } from "./marked-as-read.enum";

export class MessageFilterModel {
    startTime: Date;
    endTime: Date;
    actions: MessageActions[];
    types: ActionType[];
    amount: number;
    start: number;
    query: string;
    markedAsRead: MarkedAsRead;
}
