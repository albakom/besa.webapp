import { ActionType } from "./action-type.enum";

export class ActionTypeCheckboxModel {
    isSelected: boolean;
    actionType: ActionType;
}
