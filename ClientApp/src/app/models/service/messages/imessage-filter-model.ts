import { MarkedAsRead } from './marked-as-read.enum';
import { MessageRelatedObjectTypes } from 'app/models/service/tasks/message-related-object-types.enum';
import { MessageActions } from "./message-actions.enum";

export interface IMessageFilterModel {
    startTime?: string;
    endTime?: string;
    actions: MessageActions[];
    types: MessageRelatedObjectTypes[];
    amount: number;
    start: number;
    query?: string;
    markedAsRead?: MarkedAsRead;
}
