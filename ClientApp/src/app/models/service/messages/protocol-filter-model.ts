import { MessageActions } from './message-actions.enum';
import { MessageRelatedObjectTypes } from '../tasks/message-related-object-types.enum';

export interface ProtocolFilterModel {
    startTime?: Date;
    endTime?: Date;
    actions: MessageActions[];
    types: MessageRelatedObjectTypes[];
    amount: number;
    start: number;
}
