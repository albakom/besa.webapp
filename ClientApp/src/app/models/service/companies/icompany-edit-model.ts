import { IAddressModel } from './../iaddress-model';
import { IUniqueElement } from './../iunique-element';
export interface ICompanyEditModel extends IUniqueElement {
    name: string;
    phone: string;
    address: IAddressModel;

    addedEmployees: number[];
    removedEmployees: number[];
}
