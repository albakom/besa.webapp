import { IUniqueElement } from '../iunique-element';
export interface ISimpleCompanyOverviewModel extends IUniqueElement {
    name: string;
}
