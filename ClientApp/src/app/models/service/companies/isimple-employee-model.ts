export interface ISimpleEmployeeModel {
    id: number;
    surname: string;
    lastname: string;
}
