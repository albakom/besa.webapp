import { IAddressModel } from './../iaddress-model';
import { IUniqueElement } from '../iunique-element';
import { ISimpleEmployeeModel } from './isimple-employee-model';
export interface ICompanyDetailModel extends IUniqueElement {
    name: string;
    phone: string;
    address: IAddressModel;
    employees: Array<ISimpleEmployeeModel>;
}
