import { IAddressModel } from './../iaddress-model';

export interface ICompanyCreateModel {
    name: string;
    phone: string;
    address: IAddressModel;
}
