import { IUniqueElement } from './../iunique-element';
export interface IMainCableOverviewModel extends IUniqueElement {
    name: string;
    jobExists: boolean;
}
