import { ISimpleBuildingOverviewModel } from './../construction-stage/isimple-building-overview-model';
import { IContactInfo } from '../icontact-info';
export interface IContactAfterSalesJobOverview {
    id: number;
    isFinished: boolean;
    contact: IContactInfo;
    building: ISimpleBuildingOverviewModel;
}
