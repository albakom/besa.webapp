export interface IContactAfterFinishedUnsuccessfullyModel {
    jobId: number;
    comment: string;
}
