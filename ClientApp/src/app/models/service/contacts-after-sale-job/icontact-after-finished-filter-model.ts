export interface IContactAfterFinishedFilterModel {
    query: string;
    amount: number;
    start: number;
    openState?: boolean;
}
