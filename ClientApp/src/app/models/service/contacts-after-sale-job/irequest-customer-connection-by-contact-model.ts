import { IExplainedBooleanModel } from './../iexplained-boolean-model';
export interface IRequestCustomerConnectionByContactModel {
    jobId: number;
    subscriberEndpointNearConnectionPoint: IExplainedBooleanModel;
    ductAmount?: number;
    subscriberEndpointLength?: number;
    activePointNearSubscriberEndpoint: IExplainedBooleanModel;
    powerForActiveEquipment: IExplainedBooleanModel;
    connectionAppointment?: Date;
    activationAsSoonAsPossible: boolean;
}
