import { ISimpleFlatOverviewModel } from './../flats/isimple-flat-overview-model';
import { ISimpleBuildingOverviewWithGPSModel } from './../construction-stage/isimple-building-overview-with-gps-model';
import { IContactInfo } from '../icontact-info';
export interface IContactAfterSalesJobDetails {
    id: number;
    closed: boolean;
    building: ISimpleBuildingOverviewWithGPSModel;
    flat: ISimpleFlatOverviewModel;
    closeComment: string;
    relatedProcedureId: number;
    finshedAt: Date;
    contact: IContactInfo;
}
