export interface IGPSCoordinate {
    latitude: number;
    longitude: number;
}
