export interface IColorModel {
    name: string;
    rgbHexValue: string;
}
