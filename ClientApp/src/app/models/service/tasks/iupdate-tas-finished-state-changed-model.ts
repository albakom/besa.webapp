export interface IUpdateTasFinishedStateChangedModel {
    id: number;
    finishedAt: Date;
}
