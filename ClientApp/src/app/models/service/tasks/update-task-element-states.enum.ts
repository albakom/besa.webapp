export enum UpdateTaskElementStates {
    NotStarted = 0,
    InProgress = 1,
    Error = 2,
    Waring = 3,
    Success = 4,
}
