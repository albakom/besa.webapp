import { UpdateTasks } from './update-tasks.enum';
import { ISimpleUserOverviewModel } from '../user/isimple-user-overview-model';
export interface IUpdateTaskOverviewModel {
    id: number;
    taskType: UpdateTasks;
    started: Date;
    endend?: Date;
    lastHearbeat: Date;
    wasCanceled: boolean;
    createdBy: ISimpleUserOverviewModel;
}
