import { UpdateTasks } from './update-tasks.enum';
export interface IStartUpdateTaskModel {
    objectIds: number[];
    taskType: UpdateTasks;
}
