import { MessageRelatedObjectTypes } from "app/models/service/tasks/message-related-object-types.enum";

export interface IUpdateTaskObjectOverviewModel {
    id: number;
    projectAdapterId: string;
    name: string;
    type: MessageRelatedObjectTypes;
}
