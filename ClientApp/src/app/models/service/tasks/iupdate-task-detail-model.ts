import { IUpdateTaskOverviewModel } from './iupdate-task-overview-model';
import { IUpdateTaskElementOverviewModel } from './iupdate-task-element-overview-model';
export interface IUpdateTaskDetailModel {
    overview: IUpdateTaskOverviewModel;
    elements: IUpdateTaskElementOverviewModel[];
}
