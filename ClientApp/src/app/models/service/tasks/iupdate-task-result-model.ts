export interface IUpdateTaskResultModel {
    warning: string;
    error: string;
    updatedIds: number[];
    addedIds: number[];
    deletedIds: number[];
}
