import { UpdateTaskElementStates } from './update-task-element-states.enum';
import { IUpdateTaskResultModel } from './iupdate-task-result-model';
import { IUpdateTaskObjectOverviewModel } from './iupdate-task-object-overview-model';
export interface IUpdateTaskElementOverviewModel {
    id: number;
    started?: Date;
    ended?: Date;
    state: UpdateTaskElementStates;
    result: IUpdateTaskResultModel;
    relatedObject: IUpdateTaskObjectOverviewModel;
}
