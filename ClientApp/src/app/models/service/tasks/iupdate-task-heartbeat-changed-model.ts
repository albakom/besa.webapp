export interface IUpdateTaskHeartbeatChangedModel {
    taskId: number;
    value: Date;
}
