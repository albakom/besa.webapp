import { IExplainedBooleanModel } from './../iexplained-boolean-model';
export interface ICustomerCreateModel {
    onlyHouseConnection: boolean;
    flatId: number;
    buildingId: number;
    customerPersonId: number;
    propertyOwnerPersonId?: number;
    architectPersonId?: number;
    workmanPersonId?: number;

    descriptionForHouseConnection?: string;
    civilWorkIsDoneByCustomer: boolean;
    connectioOfOtherMediaRequired: boolean;

    subscriberEndpointNearConnectionPoint?: IExplainedBooleanModel;
    ductAmount?: number;
    subscriberEndpointLength: number;
    activePointNearSubscriberEndpoint: IExplainedBooleanModel;
    powerForActiveEquipment: IExplainedBooleanModel;
    connectionAppointment: string;

    informSalesAfterFinish: boolean;
    activationAsSoonAsPossible: boolean;
}
