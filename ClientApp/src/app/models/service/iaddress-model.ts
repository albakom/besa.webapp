export interface IAddressModel {
    street: string;
    streetNumber: string;
    city: string;
    postalCode: string;
}
