import { IUniqueElement } from "../iunique-element";

export interface IArticleOverviewModel extends IUniqueElement {
    name: string;
    productSoldByMeter: boolean;
    isInUse: boolean;
}
