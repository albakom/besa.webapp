import { IUniqueElement } from "../iunique-element";

export interface IArticleEditModel extends IUniqueElement {
    name: string;
    productSoldByMeter: boolean;
}
