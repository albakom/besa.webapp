import { IUniqueElement } from "../iunique-element";

export interface IArticleDetailModel extends IUniqueElement {
    name: string;
    productSoldByMeter: boolean;
}
