export interface IArticleCreateModel {
    name: string;
    productSoldByMeter: boolean;
}
