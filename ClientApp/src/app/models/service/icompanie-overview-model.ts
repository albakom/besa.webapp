import { IUniqueElement } from './iunique-element';

export interface ICompanieOverviewModel extends IUniqueElement {
    name: string;
    employeeCount: number;
}
