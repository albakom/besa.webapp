import { IGPSCoordinate } from "../other/igpscoordinate";
import { IBuildingOverviewModel } from "./ibuilding-overview-model";
import { IStreetOverviewModel } from "./istreet-overview-model";
import { IEmptySpaceOverviewModel } from "./iempty-space-overview-model";

export interface IBuildingAreaDetailModel {
    id: number;
    coordinates: IGPSCoordinate[];
    areaName: string;
    buildings: IBuildingOverviewModel[];
    streets: IStreetOverviewModel[];
    emptySpaces: IEmptySpaceOverviewModel[];
}
