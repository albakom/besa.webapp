import { IGPSCoordinate } from "../other/igpscoordinate";

export interface IEmptySpaceOverviewModel {
    id: number;
    coordinates: IGPSCoordinate[];
}
