import { IGPSCoordinate } from "../other/igpscoordinate";

export interface IBuildingDetailModel {
    id: number;
    coordinates: IGPSCoordinate[];
    streetName: string;
    streetNumber: string;
    householdUnits: number;
    commercialUnits: number; 
    fileIds: number[];
    comment: string;
}
