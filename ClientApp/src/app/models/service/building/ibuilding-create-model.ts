import { IGPSCoordinate } from "../other/igpscoordinate";

export interface IBuildingCreateModel {
    coordinates: IGPSCoordinate[];
    streetName: string;
    streetNumber: string;
    householdUnits: number;
    commercialUnits: number; 
    fileIds: number[];
    comment: string;
}
