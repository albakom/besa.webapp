import { IGPSCoordinate } from "../other/igpscoordinate";

export interface IEmptySpaceDetailModel {
    id: number;
    coordinates: IGPSCoordinate[];
    streetName: string;
    fileIds: number[];
    comment: string;
}
