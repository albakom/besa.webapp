import { LatLngLiteral } from '@agm/core';
export interface IBuildingAreaPolygon {
    coordinates: LatLngLiteral[];
    name: string;
    id: number;
    color: string;
}
