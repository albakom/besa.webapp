import { LatLngLiteral } from "@agm/core";
import { AreaItemType } from "./area-item-type.enum";

export interface IBuildingPolygon {
    coordinates: LatLngLiteral[];
    id: number;
    color: string;
    type: AreaItemType;
}
