export interface IStreetEditModel {
    id: number;
    name: string;
}
