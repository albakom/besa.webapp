export enum AreaItemType {
    building = 1,
    street = 2,
    emptySpace = 3,
}
