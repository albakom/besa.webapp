import { LatLngLiteral } from "@agm/core";

export interface ICoordsMouseEvent extends MouseEvent {
    coords: LatLngLiteral;
}
