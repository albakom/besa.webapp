import { IGPSCoordinate } from "../other/igpscoordinate";

export interface IBuildingAreaCreateModel {
    coordinates: IGPSCoordinate[];
    areaName: string;
}
