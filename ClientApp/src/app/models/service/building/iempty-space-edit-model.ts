import { IGPSCoordinate } from "../other/igpscoordinate";

export interface IEmptySpaceEditModel {
    id: number;
    coordinates: IGPSCoordinate[];
    streetName: string;
    fileIds: number[];
    comment: string;
}
