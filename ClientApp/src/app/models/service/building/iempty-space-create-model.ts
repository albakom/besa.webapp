import { IGPSCoordinate } from '../other/igpscoordinate';
export interface IEmptySpaceCreateModel {
    coordinates: IGPSCoordinate[];
    streetName: string;
    fileIds: number[];
    comment: string;
}
