export interface IStreetOverviewModel {
    id: number;
    streetName: string;
}
