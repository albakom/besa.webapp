import { AreaItemType } from "./area-item-type.enum";
import { IBuildingCreateModel } from "./ibuilding-create-model";
import { IStreetCreateModel } from "./istreet-create-model";
import { IEmptySpaceCreateModel } from "./iempty-space-create-model";

export interface ICreateAreaItemModel {
    type: AreaItemType;
    building?: IBuildingCreateModel;
    street?: IStreetCreateModel;
    emptySpace?: IEmptySpaceCreateModel;
}
