import { IGPSCoordinate } from '../other/igpscoordinate';
export interface IBuildingOverviewModel {
    id: number;
    coordinates: IGPSCoordinate[];
}
