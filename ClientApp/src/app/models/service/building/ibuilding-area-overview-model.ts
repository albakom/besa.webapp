import { IGPSCoordinate } from './../other/igpscoordinate';
export interface IBuildingAreaOverviewModel {
    id: number;
    coordinates: IGPSCoordinate[];
    areaName: string;
}
