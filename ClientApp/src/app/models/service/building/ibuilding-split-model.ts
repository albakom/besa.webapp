import { IGPSCoordinate } from '../other/igpscoordinate';
export interface IBuildingSplitModel {
    id: number;
    splitCoordinates: IGPSCoordinate[];
}
