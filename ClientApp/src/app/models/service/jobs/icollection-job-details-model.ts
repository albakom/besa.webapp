import { IUniqueElement } from "../iunique-element";
import { ISimpleJobOverview } from "./isimple-job-overview";
import { JobStates } from "./job-states.enum";

export interface ICollectionJobDetailsModel extends IUniqueElement {
    name: string;
    end: string;
    doingPercentage: number;
    state: JobStates;
    jobs: ISimpleJobOverview[];
}
