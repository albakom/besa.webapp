import { IFinishJobModel } from "../ifinish-job-model";

export interface IFinishSpliceBranchableJobModel extends IFinishJobModel {
    cableMetric: number;
}
