import { IJobDetailModel } from './../job-detail-model';
import { IFinishPrepareBranchableForSpliceJobDetailModel } from './ifinish-prepare-branchable-for-splice-job-detail-model';
import { ISimpleBranchableWithGPSModel } from '../../construction-stage/isimple-branchable-with-gps-model';
import { ISpliceCableInfoModel } from './isplice-cable-info-model';
import { IFiberBundleModel } from '../../construction-stage/ifiber-bundle-model';
import { ISpliceEntryModel } from './isplice-entry-model';
export interface IPrepareBranchableForSpliceJobDetailModel extends IJobDetailModel<IFinishPrepareBranchableForSpliceJobDetailModel> {
    branchable: ISimpleBranchableWithGPSModel;
    mainCable?: ISpliceCableInfoModel;
    untouchedBundles: IFiberBundleModel[];
    bundlesForCustomer: IFiberBundleModel[];
    bundleForLoop?: IFiberBundleModel;
    splicesForLoop?: ISpliceEntryModel[];
    splices:  ISpliceEntryModel[]
    spliceOverviewTableUrl: string;
}
