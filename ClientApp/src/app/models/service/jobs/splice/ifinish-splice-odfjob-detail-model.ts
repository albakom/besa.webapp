import { IFinishJobDetailModel } from "../ifinish-job-detail-model";

export interface IFinishSpliceOdfjobDetailModel extends IFinishJobDetailModel {
    cableMetric: number;
}
