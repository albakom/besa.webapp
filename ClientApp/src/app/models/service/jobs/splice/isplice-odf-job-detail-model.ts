import { IJobDetailModel } from './../job-detail-model';
import { IFinishSpliceOdfjobDetailModel } from './ifinish-splice-odfjob-detail-model';
import { IGPSCoordinate } from '../../other/igpscoordinate';
import { ISpliceCableInfoModel } from './isplice-cable-info-model';
import { ISpliceEntryModel } from './isplice-entry-model';
import { ISimplePopOverviewWithGPSModel } from '../isimple-pop-overview-with-gpsmodel';
export interface ISpliceODFJobDetailModel extends IJobDetailModel<IFinishSpliceOdfjobDetailModel> {
    pop: ISimplePopOverviewWithGPSModel;
    mainCable: ISpliceCableInfoModel;
    modulCaption: string;
    trayNumberForFiberBuffer: number;

    // entries: ISpliceEntryModel[];
}
