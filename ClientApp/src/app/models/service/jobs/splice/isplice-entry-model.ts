import { IFiberOverviewModel } from "../../construction-stage/ifiber-overview-model";

export interface ISpliceEntryModel {
    firstFiber: IFiberOverviewModel;
    secondFiber: IFiberOverviewModel;
    tray: number;
    number: number;
    type: number;
}
