import { IFinishJobModel } from '../ifinish-job-model';
export interface ISpliceBranchableJobFinishModel extends IFinishJobModel {
    cableMetric: number;
}
