import { ISimpleBranchableWithGPSModel } from '../../construction-stage/isimple-branchable-with-gps-model';
import { IJobOverviewModel } from '../ijobs-overview-model';

export interface ISimplePrepareBranchableForSpliceJobOverviewModel extends IJobOverviewModel {
      branchable: ISimpleBranchableWithGPSModel;
}
