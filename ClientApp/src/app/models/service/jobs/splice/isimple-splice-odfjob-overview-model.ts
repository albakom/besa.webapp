import { IJobOverviewModel } from "../ijobs-overview-model";

export interface ISimpleSpliceODFJobOverviewModel extends IJobOverviewModel {
    cableName: string;
}