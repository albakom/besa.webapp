export interface ISpliceCableInfoModel {
    name: string;
    fiberAmount: number;
}
