import { IFinishJobDetailModel } from '../ifinish-job-detail-model';
export interface IFinishSpliceBranchableJobDetailModel extends IFinishJobDetailModel {
    cableMetric: number;
}
