import { IFinishJobModel } from "../ifinish-job-model";

export interface ISpliceODFJobFinishModel extends IFinishJobModel {
    cableMetrics: number;
}
