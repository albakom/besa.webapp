import { IJobOverviewModel } from './../ijobs-overview-model';
import { ISimpleBranchableWithGPSModel } from '../../construction-stage/isimple-branchable-with-gps-model';
import { ISimpleBuildingOverviewWithGPSModel } from '../../construction-stage/isimple-building-overview-with-gps-model';
export interface IPrepareBranchableForSpliceJobOverviewModel extends IJobOverviewModel {
    branchable: ISimpleBranchableWithGPSModel;
    buildingInfo: ISimpleBuildingOverviewWithGPSModel;
}
