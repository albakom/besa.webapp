import { ISimpleBranchableWithGPSModel } from './../../construction-stage/isimple-branchable-with-gps-model';
import { IJobOverviewModel } from './../ijobs-overview-model';
import { ISimpleBuildingOverviewModel } from '../../construction-stage/isimple-building-overview-model';
import { IContactInfo } from '../../icontact-info';
export interface ISpliceBranchableJobOverviewModel extends IJobOverviewModel {
    building: ISimpleBuildingOverviewModel;
    branchable: ISimpleBranchableWithGPSModel;
    customer: IContactInfo;
}
