import { IFinishJobModel } from './../ifinish-job-model';
export interface IPrepareBranchableForSpliceJobFinishModel extends IFinishJobModel {
    branchableId: number;
}
