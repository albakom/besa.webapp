import { IGPSCoordinate } from './../../other/igpscoordinate';
import { IJobOverviewModel } from './../ijobs-overview-model';
import { ISpliceCableInfoModel } from './isplice-cable-info-model';
export interface ISpliceODFJobOverviewModel extends IJobOverviewModel {
    popName: string;
    location: IGPSCoordinate;
    mainCable: ISpliceCableInfoModel;
}
