import { IFinishJobModel } from "../ifinish-job-model";

export interface IFinishSpliceODFJobModel  extends IFinishJobModel {
    cableMetric: number;
}
