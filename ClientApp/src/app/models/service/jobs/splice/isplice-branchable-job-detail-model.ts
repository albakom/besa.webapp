import { IColorModel } from './../../other/icolor-model';
import { IContactInfo } from './../../icontact-info';
import { ISimpleBuildingOverviewWithGPSModel } from './../../construction-stage/isimple-building-overview-with-gps-model';
import { BranchableTypes } from './../../construction-stage/branchable-types.enum';
import { IFinishSpliceBranchableJobDetailModel } from './ifinish-splice-branchable-job-detail-model';
import { IJobDetailModel } from './../job-detail-model';
import { ISimpleBranchableWithGPSModel } from '../../construction-stage/isimple-branchable-with-gps-model';
import { ISpliceEntryModel } from './isplice-entry-model';
import { ISpliceCableInfoModel } from './isplice-cable-info-model';
export interface ISpliceBranchableJobDetailModel extends IJobDetailModel<IFinishSpliceBranchableJobDetailModel> {
    branchableType: BranchableTypes;
    buildingCable: ISpliceCableInfoModel;
    mainCable: ISpliceCableInfoModel;
    trayNumberForFiberBuffer: number;
    caption: string;

    customer: IContactInfo;
    ductColor: IColorModel;
    branchable: ISimpleBranchableWithGPSModel;
    buildingInfo: ISimpleBuildingOverviewWithGPSModel;

    entries: ISpliceEntryModel[];
}
