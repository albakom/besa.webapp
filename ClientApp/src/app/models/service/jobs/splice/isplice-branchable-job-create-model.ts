export interface ISpliceBranchableJobCreateModel {
    flatId: number;
    customerContactId: number;
}
