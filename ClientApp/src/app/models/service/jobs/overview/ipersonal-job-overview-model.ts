import { JobTypes } from "../job-types.enum";

export interface IPersonalJobOverviewModel {
      jobType : JobTypes;
      name: string;
      endDate: string;
      taskAmount: number;
      jobId?: number;
      collectionJobId?: number;
}
