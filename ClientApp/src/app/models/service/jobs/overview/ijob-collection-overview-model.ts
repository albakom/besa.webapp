import { ISimpleJobOverview } from './../isimple-job-overview';
import { ISimpleCompanyOverviewModel } from './../../companies/isimple-company-overview-model';
export interface IJobCollectionOverviewModel {
    collectionJobId: number;
    boundedTo: ISimpleCompanyOverviewModel;
    finishedTill: Date;
    jobs: ISimpleJobOverview[];
    percentage: number;
    name: string;
}
