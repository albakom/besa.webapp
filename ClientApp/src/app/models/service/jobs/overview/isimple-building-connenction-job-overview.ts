import { BuildingConnenctionTypes } from "../building-connenction-types.enum";
import { JobStates } from "../job-states.enum";
import { IJobOverviewModel } from "../ijobs-overview-model";

export interface ISimpleBuildingConnenctionJobOverview extends IJobOverviewModel {
    buildingId: number;
    streetName: string;
    connectionJobType: BuildingConnenctionTypes;
}
