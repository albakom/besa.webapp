export interface IFinishJobAdministrativeModel {
    jobId: number;
    finishedAt?: string;
    createNextJobInPipeLine?: boolean;
}
