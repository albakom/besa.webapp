import { IUniqueElement } from '../iunique-element';
import { JobStates } from './job-states.enum';
import { IFinishJobDetailModel } from './ifinish-job-detail-model';
import { IFileOverviewModel } from '../files/ifile-overview-model';

export interface IJobDetailModel<TTFinishModel extends IFinishJobDetailModel> extends IUniqueElement {
    state: JobStates;
    finishInfo: TTFinishModel;
    files: IFileOverviewModel[];
}