export enum JobStates {
    Open = 1,
    Finished = 2,
    Acknowledged = 3,
}