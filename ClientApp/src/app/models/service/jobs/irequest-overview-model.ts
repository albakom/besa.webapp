import { IUniqueElement } from '../iunique-element';
import { IContactInfo } from '../icontact-info';
import { ISimpleBuildingOverviewModel } from '../construction-stage/isimple-building-overview-model';
export interface IRequestOverviewModel extends IUniqueElement {
    onlyHouseConnection: boolean;
    primaryPerson: IContactInfo
    building: ISimpleBuildingOverviewModel;
}

