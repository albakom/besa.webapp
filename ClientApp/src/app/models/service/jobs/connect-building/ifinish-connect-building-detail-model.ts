import { IExplainedBooleanModel } from './../../iexplained-boolean-model';
import { IFinishJobDetailModel } from '../ifinish-job-detail-model';

export interface IFinishConnectBuildingDetailModel extends IFinishJobDetailModel {
    ductChanged: IExplainedBooleanModel
}