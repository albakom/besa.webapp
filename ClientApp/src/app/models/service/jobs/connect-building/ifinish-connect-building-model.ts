import { IFinishJobModel } from "../ifinish-job-model";
import { IExplainedBooleanModel } from '../../iexplained-boolean-model';

export interface IFinishConnectBuildingModel extends IFinishJobModel {
    ductChanged: IExplainedBooleanModel;
}