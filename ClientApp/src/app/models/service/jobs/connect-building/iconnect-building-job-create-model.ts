
export interface IConnectBuildingJobCreateModel {
    buildingId: number;
    onlyHouseConnection: boolean;
    customerContactId?: number;
    ownerContactId: number;
    workmanContactId?: number;
    architectContactId?: number;
    description: string;
    civilWorkIsDoneByCustomer: boolean;
    connectionOfOtherMediaRequired: boolean;
    connectionAppointment?: string;
}