import { IJobDetailModel } from "../job-detail-model";
import { IFinishConnectBuildingDetailModel } from "./ifinish-connect-building-detail-model";
import { IBuildingInfoForBranchOffModel } from '../ibuilding-info-for-branch-off-model';
import { IContactInfo } from '../../icontact-info';

export interface IConnectBuildingJobDetailsModel extends IJobDetailModel<IFinishConnectBuildingDetailModel> {
    buildingInfo: IBuildingInfoForBranchOffModel;
    onlyHouseConnection: boolean;
    customer?: IContactInfo;
    owner: IContactInfo;
    workman?: IContactInfo;
    architect?: IContactInfo;
    description: string;
    civilWorkIsDoneByCustomer: boolean;
    connectionOfOtherMediaRequired: boolean;
    connectionAppointment: string;
}