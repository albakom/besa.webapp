import { IExplainedBooleanModel } from '../../iexplained-boolean-model';
import { IUserOverviewModel } from '../../user/iuser-overview-model';
import { IFileOverviewModel } from '../../files/ifile-overview-model';
export interface IFinishBranchOffDetailModel {
    jobId: number;
    ductChanged: IExplainedBooleanModel;
    problemHappend: IExplainedBooleanModel;
    comment: string;
    finishedBy: IUserOverviewModel;
    finishedAt: string;
    acceptedBy?: IUserOverviewModel; 
    acceptedAt?: string;
    files: IFileOverviewModel[];
}
