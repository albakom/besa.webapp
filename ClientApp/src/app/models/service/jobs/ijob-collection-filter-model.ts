import { ISortDirections } from './isort-directions';
import { IJobCollectionSortProperties } from './ijob-collection-sort-properties';
export interface IJobCollectionFilterModel {
    sortDirection: ISortDirections,
    sortProperty: IJobCollectionSortProperties,
    start: number;
    amount: number;
    query?: string;
}
