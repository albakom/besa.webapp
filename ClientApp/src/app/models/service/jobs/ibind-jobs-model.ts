export interface IBindJobsModel {
    jobIds: number[];
    companyId: number;
    endDate: string;
    name: string;
}
