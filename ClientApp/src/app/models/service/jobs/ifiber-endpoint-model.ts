export interface IFiberEndpointModel {
    id: number;
    moduleName: string;
    rowNumber: string;
    columnNumber: string;
    caption: string;
}
