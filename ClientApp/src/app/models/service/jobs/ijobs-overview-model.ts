import { JobStates } from "./job-states.enum";

export interface IJobOverviewModel  {
    jobId: number;
    state: JobStates;
    boundedCompanyName: string;
}