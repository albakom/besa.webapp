import { IUniqueElement } from '../iunique-element';
import { IExplainedBooleanModel } from '../iexplained-boolean-model';

export interface IFinishJobModel extends IUniqueElement {
    jobId: number;
    comment?: string;
    fileIds?: number[];
    problemHappend: IExplainedBooleanModel;
}