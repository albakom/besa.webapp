export enum IJobCollectionSortProperties {
    FinishedTill = 1,
    Name = 2,
    CompanyName = 3,
    DoingPercentage = 4
}
