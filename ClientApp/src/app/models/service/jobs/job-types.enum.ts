export enum JobTypes {
    BranchOff = 10,
    HouseConnenction = 20,
    Inject = 30,
    SpliceInBranchable = 40,
    SpliceInPoP = 44,
    PrepareBranchableForSplice = 48,
    ActivationJob = 60,
    Inventory = 70,
}
