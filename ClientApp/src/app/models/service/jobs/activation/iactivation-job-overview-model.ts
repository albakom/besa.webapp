import { IJobOverviewModel } from "../ijobs-overview-model";
import { IContactInfo } from '../../icontact-info';
import { ISimpleBuildingOverviewModel } from "../../construction-stage/isimple-building-overview-model";

export interface IActivationJobOverviewModel extends IJobOverviewModel {
    building: ISimpleBuildingOverviewModel;
    customer: IContactInfo;
    popName: string;
}
