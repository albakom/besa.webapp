export enum ActivationJobIpAddressRequirements {
    None = 1,
    Slash29 = 2,
    Slash28 = 3,
    Slash27 = 4,
}
