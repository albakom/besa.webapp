import { IJobDetailModel } from '../job-detail-model';
import { IFinishActivationJobDetailModel } from './ifinish-activation-job-detail-model';
import { IContactInfo } from '../../icontact-info';
import { ISimpleBuildingOverviewWithGPSModel } from '../../construction-stage/isimple-building-overview-with-gps-model';
import { ISimpleFlatOverviewModel } from '../../flats/isimple-flat-overview-model';
import { IFiberEndpointModel } from '../ifiber-endpoint-model';
import { ISimplePopOverviewWithGPSModel } from '../isimple-pop-overview-with-gpsmodel';

export interface IActivationJobDetailModel extends IJobDetailModel<IFinishActivationJobDetailModel> {
    customer: IContactInfo;
    building: ISimpleBuildingOverviewWithGPSModel;
    flat: ISimpleFlatOverviewModel;
    endpoints: IFiberEndpointModel[];
    pop: ISimplePopOverviewWithGPSModel;
} 