import { ISimpleFlatOverviewModel } from './../../flats/isimple-flat-overview-model';
import { IContactInfo } from '../../icontact-info';
import { ISimpleBuildingOverviewModel } from '../../construction-stage/isimple-building-overview-model';
import { IJobOverviewModel } from '../ijobs-overview-model';
export interface ISimpleActivationJobOverviewModel extends IJobOverviewModel {
    jobId: number;
    customer: IContactInfo;
    flat: ISimpleFlatOverviewModel;
    building: ISimpleBuildingOverviewModel;
}
