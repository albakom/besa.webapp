import { ActivationJobIpAddressRequirements } from "./activation-job-ip-address-requirements.enum";

export interface IActivationJobCreateModel {
    flatId: number;
    customerContactId: number;
    deviceId?: number;
    unchagedIpAdress: boolean;
    addressRequirement: ActivationJobIpAddressRequirements;
}
