export enum ISortDirections {
    Ascending = 1,
    Descending = 2
}
