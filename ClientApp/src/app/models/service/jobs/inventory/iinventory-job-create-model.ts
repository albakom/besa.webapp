export class IInventoryJobCreateModel {
    public articles: {[article: number]: number};
    public issuanceToId: number;
    public pickupAt: Date;
}
