export class IInventoryJobPositionModel {
    articleId: number;
    articleName: string;
    amount: number;
}
