import { IInventoryJobPositionModel } from "./iinventory-job-position-model";
import { IContactInfo } from "../../icontact-info";

export interface IInventoryJobDetailModel {
    id: number;
    issuer: IContactInfo;
    positions: IInventoryJobPositionModel[];
}
