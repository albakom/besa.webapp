export interface IArticleFormModel {
    id: number;
    amount: number;
    name: string;
}
