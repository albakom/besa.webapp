import { IInventoryJobPositionModel } from './iinventory-job-position-model';
import { IContactInfo } from '../../icontact-info';

export class IInventoryJobOverviewModel {
    id: number;
    issuer: IContactInfo;
    positions: IInventoryJobPositionModel[];
}
