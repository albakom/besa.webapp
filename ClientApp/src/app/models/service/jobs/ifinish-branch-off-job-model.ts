import { IExplainedBooleanModel } from '../iexplained-boolean-model';
export interface IFinishBranchOffJobModel {
    ductChanged: IExplainedBooleanModel;
    problemHappend: IExplainedBooleanModel;
    comment: string;
    jobId: number;
    fileIds: number[];
}
