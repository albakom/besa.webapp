import { IGPSCoordinate } from './../other/igpscoordinate';
export interface ISimplePopOverviewWithGPSModel {
    id: number;
    name: string;
    location: IGPSCoordinate;
}
