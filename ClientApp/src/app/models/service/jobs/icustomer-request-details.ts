import { IExplainedBooleanModel } from './../iexplained-boolean-model';
import { ISimpleFlatOverviewModel } from './../flats/isimple-flat-overview-model';
import { IUniqueElement } from "../iunique-element";
import { ISimpleBuildingOverviewModel } from '../construction-stage/isimple-building-overview-model';
import { IContactInfo } from '../icontact-info';
import { ISimpleJobOverview } from './isimple-job-overview';

export interface ICustomerRequestDetails extends IUniqueElement {
    onlyHouseConnection: boolean;
    building: ISimpleBuildingOverviewModel;
    
    flat: ISimpleFlatOverviewModel;
    customer?: IContactInfo;
    owner?: IContactInfo;
    architect?: IContactInfo;
    workman?: IContactInfo;
    
    descriptionForHouseConnection: string;
    civilWorkIsDoneByCustomer: boolean;
    connectionOfOtherMediaRequired: boolean;
    subscriberEndpointNearConnectionPoint: IExplainedBooleanModel;
    ductAmount?: number;
    subscriberEndpointLength?: number;
    activePointNearSubscriberEndpoint: IExplainedBooleanModel;
    powerForActiveEquipment: IExplainedBooleanModel;
    connectionAppointment?: string;

    isInProgress:boolean;
    mostRecentJob?: ISimpleJobOverview;
}
