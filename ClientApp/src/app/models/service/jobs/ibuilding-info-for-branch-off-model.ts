import { IUniqueElement } from '../iunique-element';
import { IGPSCoordinate } from '../other/igpscoordinate';
import { IColorModel } from '../other/icolor-model';

export interface IBuildingInfoForBranchOffModel extends IUniqueElement {
    streetName: string;
    coordinate: IGPSCoordinate
    mainColor: IColorModel;
    branchColor: IColorModel;
}
