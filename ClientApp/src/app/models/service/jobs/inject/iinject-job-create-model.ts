export interface IInjectJobCreateModel {
    buildingId: number;
    customerContactId: number;
}