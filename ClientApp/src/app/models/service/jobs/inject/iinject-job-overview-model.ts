import { IJobOverviewModel } from "../ijobs-overview-model";
import { ISimpleBuildingOverviewModel } from '../../construction-stage/isimple-building-overview-model';
import { IContactInfo } from "../../icontact-info";

export interface IInejectJobOverviewModel extends IJobOverviewModel {
    building: ISimpleBuildingOverviewModel,
    customer: IContactInfo,
}