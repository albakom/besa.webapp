import { IJobDetailModel } from '../job-detail-model';
import { IFinishInjectJobDetails } from './ifinish-inject-job-detail-model';
import { IContactInfo } from '../../icontact-info';
import { IColorModel } from '../../other/icolor-model';
import { IProjectAdapterCableType } from '../../construction-stage/iproject-adapter-cable-type';
import { ISimpleBranchableWithGPSModel } from '../../construction-stage/isimple-branchable-with-gps-model';
import { ISimpleBuildingOverviewWithGPSModel } from '../../construction-stage/isimple-building-overview-with-gps-model';
export interface IInjectJobDetailModel extends IJobDetailModel<IFinishInjectJobDetails> {
    customer: IContactInfo;
    expectedLength: number;
    injectDuctColor: IColorModel;
    cable: IProjectAdapterCableType;
    branchable: ISimpleBranchableWithGPSModel;
    buildingInfo: ISimpleBuildingOverviewWithGPSModel;
}