import { IFinishJobModel } from "../ifinish-job-model";

export interface IFinishInjectJobModel extends IFinishJobModel {
    length: number;
}