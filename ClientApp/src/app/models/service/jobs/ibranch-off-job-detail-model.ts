import { IJobDetailModel } from "./job-detail-model";
import { IFinishBranchOffDetailModel } from "./branch-off/ifinish-branch-off-detail-model";
import { IBuildingInfoForBranchOffModel } from "./ibuilding-info-for-branch-off-model";


export interface IBranchOffJobDetailModel extends IJobDetailModel<IFinishBranchOffDetailModel> {
    buildingInfo: IBuildingInfoForBranchOffModel;
}
