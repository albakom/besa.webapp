import { IExplainedBooleanModel } from '../iexplained-boolean-model';
import { IUserOverviewModel } from '../user/iuser-overview-model';
import { IFileOverviewModel } from '../files/ifile-overview-model';

export interface IFinishJobDetailModel {
    jobId: number;
    comment: string;
    problemHappend: IExplainedBooleanModel;
    finishedBy: IUserOverviewModel;
    finishedAt: string;
    files: IFileOverviewModel[];
    acceptedBy?: IUserOverviewModel;
    acceptedAt?: string;
}