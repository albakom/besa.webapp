import { JobTypes } from './job-types.enum';
import { IUniqueElement } from '../iunique-element';
import { JobStates } from './job-states.enum';
export interface ISimpleJobOverview extends IUniqueElement {
    jobType: JobTypes;
    name: string;
    state: JobStates;
}
