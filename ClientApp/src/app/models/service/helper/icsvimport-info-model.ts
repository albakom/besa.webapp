import { EncodingTypes } from "./encoding-types.enum";

export interface ICSVImportInfoModel {
    seperationChar: string;
    lines: string[][];
    mappings: { [property: number]: number };
}
