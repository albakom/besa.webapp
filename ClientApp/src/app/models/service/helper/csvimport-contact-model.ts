import { ICSVImportInfoModel } from "./icsvimport-info-model";

export interface ICSVImportContactModel extends ICSVImportInfoModel {
    defaultPostalCode: string;
    defaultCity: string;
}
