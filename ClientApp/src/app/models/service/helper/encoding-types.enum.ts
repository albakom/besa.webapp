export enum EncodingTypes {
    UTF8 = 1,
    UTF7 = 2,
    UTF16 = 3,
    ASCII = 4,
    UTF32 = 5,
}
