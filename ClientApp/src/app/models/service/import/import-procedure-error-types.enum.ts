export enum ImportProcedureErrorTypes {
    ContactNotReadable = 0,
    ContactNotFound = 1,
    StreetNotReadable = 2,
    StreetNumberNotReadable = 3,
    BuildingNotFound = 4,
    AppointmentNotReadable = 5,
}
