import { ICSVProcedureImportError } from "./icsvprocedure-import-error";

export interface ICSVImportProcedureResultModel {
    errors: ICSVProcedureImportError[];
    jobIds: number[];
    procedureIds: number[];
}
