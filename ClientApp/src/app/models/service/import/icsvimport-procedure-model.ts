import {ICSVImportInfoModel} from '../helper/icsvimport-info-model';

export interface ICSVImportProcedureModel extends ICSVImportInfoModel {
   rolebackIfErrorHappend: boolean;
   skipError: boolean;
}
