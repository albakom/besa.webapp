import { ImportProcedureErrorTypes } from "./import-procedure-error-types.enum";

export interface ICSVProcedureImportError {
    itemIndex: number;
    type: ImportProcedureErrorTypes;
    description?: string;
}
