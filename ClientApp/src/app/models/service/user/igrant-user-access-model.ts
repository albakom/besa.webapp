import { BesaRoles } from '../besa-roles.enum';

export interface IGrantUserAccessModel {
    authRequestId: number;
    surname: string;
    lastname: string;
    phone: string;
    eMailAddress: string;
    companyId: number;
    role: BesaRoles
}
