import { ISimpleCompanyOverviewModel } from '../companies/isimple-company-overview-model';
export interface IUserOverviewModel {
    id: number;
    surname: string;
    lastname: string;
    phone: string;
    eMailAddress: string;
    isMember: string;
    companyInfo: ISimpleCompanyOverviewModel;
}
