import { BesaRoles } from '../besa-roles.enum';
export interface IUserEditModel {
    id: number;
    surname: string;
    lastname: string;
    phone: string;
    eMailAddress: string;
    companyId?: number;
    role: BesaRoles;
    isManagement: boolean;
}
