export interface IUserAccessRequestCreateModel {
    surname: string;
    lastname: string;
    phone: string;
    emailAddress: string;
    companyId: number;
    authServiceId: string;
}
