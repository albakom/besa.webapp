import { IUniqueElement } from '../iunique-element';
import { ISimpleCompanyOverviewModel } from '../companies/isimple-company-overview-model';

export interface IUserAccessRequestOverviewModel extends IUniqueElement {
    surname: string;
    lastname: string;
    phone: string;
    eMailAddress: string;
    generatedAt: string;
    companyInfo: ISimpleCompanyOverviewModel;
}
