import { BesaRoles } from './../besa-roles.enum';
import { ISimpleCompanyOverviewModel } from '../companies/isimple-company-overview-model';
import { MessageModel } from '../messages/message-model';
export class IUserInfoModel {
    isManagement: boolean;
    roles: BesaRoles;
    surname: string;
    lastname: string;
    company: ISimpleCompanyOverviewModel;
    unreadMessage: MessageModel[];
}
