import { IUniqueElement } from '../iunique-element';
import { ProcedureStates } from './procedure-states.enum';
import { IContactInfo } from '../icontact-info';
import { ProcedureTypes } from './procedure-types.enum';
import { ISimpleBuildingOverviewModel } from '../construction-stage/isimple-building-overview-model';
export interface IProcedureOverviewModel extends IUniqueElement {
   start: ProcedureStates;
   end: ProcedureStates;
   current: ProcedureStates;
   name: string;
   isOpen: boolean;
   contact: IContactInfo;
   building: ISimpleBuildingOverviewModel;
   type: ProcedureTypes;
}
