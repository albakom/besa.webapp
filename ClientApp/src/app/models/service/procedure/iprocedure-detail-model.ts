import { IUniqueElement } from './../iunique-element';
import { IContactInfo } from '../icontact-info';
import { ProcedureStates } from './procedure-states.enum';
import { IProcedureTimelineElementDetailModel } from './iprocedure-timeline-element-detail-model';
export interface IProcedureDetailModel extends IUniqueElement {
        contact: IContactInfo;
        isClosed: boolean;
        start: ProcedureStates;
        expectedEnd: ProcedureStates;
        current: ProcedureStates;
        name: string;
        timeline: IProcedureTimelineElementDetailModel;
}
