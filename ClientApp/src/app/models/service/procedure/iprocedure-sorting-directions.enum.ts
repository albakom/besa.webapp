export enum IProcedureSortingDirections {
    Ascending = 1,
    Descending = 2,
}
