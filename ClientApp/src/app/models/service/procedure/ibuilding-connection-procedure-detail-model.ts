import { IProcedureDetailModel } from "./iprocedure-detail-model";
import { ISimpleBuildingOverviewWithGPSModel } from "../construction-stage/isimple-building-overview-with-gps-model";

export interface IBuildingConnectionProcedureDetailModel extends IProcedureDetailModel {
        building: ISimpleBuildingOverviewWithGPSModel;
        appointment?: string;

        informSalesAfterFinish: boolean;
        declarationOfAggrementInStock: boolean;
}
