import { ProcedureStates } from './procedure-states.enum';
import { IProcedureFilterOperations } from './iprocedure-filter-operations.enum';
import { ProcedureTypes } from './procedure-types.enum';
export interface IProcedureFilterModel {
    start: number;
    amount: number;
    openState?: boolean;
    query?: string;
    
    state?: ProcedureStates;
    stateOperator?: IProcedureFilterOperations;

    type?: ProcedureTypes;
    buildingUnitAmount?: number;
    buildingUnitAmountOperation?: IProcedureFilterOperations;

    startDate?: Date;
    endDate?: Date;

    asSoonAsPossibleState?: boolean;
    informSalesAfterFinishedState?: boolean;

}
