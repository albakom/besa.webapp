export enum IProcedureSortProperties {
    BuildingName = 1,
    State = 2,
    Appointment = 3,
}
