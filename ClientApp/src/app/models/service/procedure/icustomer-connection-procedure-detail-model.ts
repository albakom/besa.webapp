import { ISimpleFlatOverviewModel } from './../flats/isimple-flat-overview-model';
import { ISimpleBuildingOverviewWithGPSModel } from '../construction-stage/isimple-building-overview-with-gps-model';
import { IProcedureDetailModel } from './iprocedure-detail-model';
export interface ICustomerConnectionProcedureDetailModel extends IProcedureDetailModel {
        flat: ISimpleFlatOverviewModel;
        building: ISimpleBuildingOverviewWithGPSModel;
        appointment?: string;
        declarationOfAggrementInStock: boolean;
        contractInStock: boolean;
        activationAsSoonAsPossible: boolean;
}
