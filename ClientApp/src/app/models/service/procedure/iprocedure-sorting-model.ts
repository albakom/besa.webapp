import { IProcedureSortProperties } from './iprocedure-sort-properties.enum';
import { IProcedureSortingDirections } from './iprocedure-sorting-directions.enum';
export interface IProcedureSortingModel {

    direction: IProcedureSortingDirections;
    property: IProcedureSortProperties;

}
