import { JobTypes } from './../jobs/job-types.enum';
import { IUniqueElement } from './../iunique-element';
import { ProcedureStates } from './procedure-states.enum';
export interface IProcedureTimelineElementDetailModel extends IUniqueElement {
        state: ProcedureStates;
        timestamp: string;
        relatedJobId?: number;
        relatedJobType?: JobTypes;
        relatedRequestId?: number;
        comment?: string;
}
