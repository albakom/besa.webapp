export enum IProcedureFilterOperations {
    Equal = 1,
    Unqual = 2,
    Greater = 3,
    Smaller = 4
}
