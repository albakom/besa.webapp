export interface IExplainedBooleanModel {
    value: boolean;
    description: string;
}
