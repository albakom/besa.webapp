export enum BesaRoles {
    Unknown = 0,
    Admin = 1,
    ProjectManager = 2,
    CivilWorker = 4,
    AirInjector = 8,
    Sales = 16,
    Inventory = 32,
    Splice = 64,
    ActiveNetwork = 128,
}
