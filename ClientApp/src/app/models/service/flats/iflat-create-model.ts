
export interface IFlatCreateModel  {
    buildingId: number;
    number: string;
    description: string;
    floor: string;
}
