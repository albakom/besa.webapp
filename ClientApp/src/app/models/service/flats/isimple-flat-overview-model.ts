import { IUniqueElement } from '../iunique-element';

export interface ISimpleFlatOverviewModel extends IUniqueElement {
    buildingId: number;
    number: string;
    description: string;
    floor: string;
}
