export enum MemberStatus {
    NoMember = 0,
    RequestedOutstanding = 1,
    IsMember = 2
}
