import { IUniqueElement } from './iunique-element';
import { PersonTypes } from './person-types.enum';
import { IAddressModel } from './iaddress-model';

export interface IContactInfo extends IUniqueElement {
        type: PersonTypes;
        surname: string;
        companyName: string;
        lastname: string;
        address: IAddressModel;
        emailAddress: string;
        phone: string;
}
