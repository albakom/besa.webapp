export enum UploadStatus {
    notStarted = 0,
    queue = 1,
    upload = 2,
    finished = 4,
    canceled = 8,
}
