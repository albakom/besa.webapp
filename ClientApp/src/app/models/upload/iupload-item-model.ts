import { UploadStatus } from './upload-status.enum';
export interface IUploadItemModel {
    file: File;
    percentage: number;
    status: UploadStatus;
}
