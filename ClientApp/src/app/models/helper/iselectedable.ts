export interface ISelectedable<T> {
    value: T;
    isSelected: boolean;
}
