export class FormErrorMessage {
    constructor(
        public forControl: string = null,
        public forValidator: string = null,
        public errorMessage: string = null
    ) {

    }
}
