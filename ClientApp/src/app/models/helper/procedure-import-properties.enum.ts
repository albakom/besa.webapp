export enum ProcedureImportProperties {
    ContactSurname = 1,
    ContactLastname = 2,
    ContactPhone = 3,
    ContactId = 4,
    Street = 5,
    StreetNumber = 6,
    Aggrement = 7,
    Contract = 8,
    Appointment = 9,
    ConnectBuildingFinished = 10,
    InjectFinished = 11,
    SpliceFinished = 12,
    ActivationFinished = 13,
    ActiveCustomer = 14,
    InformSalesAfterFinished = 15,
}
