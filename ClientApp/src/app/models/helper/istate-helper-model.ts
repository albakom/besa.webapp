import { ProgressStates } from "./progress-states.enum";

export interface IStateHelperModel<TItem,TResult> {
    item: TItem;
    state: ProgressStates;
    result?: TResult,
}
