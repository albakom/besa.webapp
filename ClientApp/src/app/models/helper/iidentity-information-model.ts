export interface IIdentityInformationModel {
    email: string;
    email_verified: boolean;
    sub: string;
}
