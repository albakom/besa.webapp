export class CSVLineModel {

    //#region Properties

    //#endregion

    constructor(public parts: string[]) {

    }

    //#region Methods
    public getValue(index: number): string {
        if (index >= this.parts.length) { return ''; }
        else { return this.parts[index]; }
    }
}