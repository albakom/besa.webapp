export interface IErrorModel {
    code: number;
    title: string;
    description?: string;
}
