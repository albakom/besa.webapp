import { IUpdateTaskOverviewModel } from './../service/tasks/iupdate-task-overview-model';
import { UpdateTasks } from './../service/tasks/update-tasks.enum';
export interface IUpdateTaskOverviewTabModel {
    tab: UpdateTasks;
    elements: IUpdateTaskOverviewModel[];
}
