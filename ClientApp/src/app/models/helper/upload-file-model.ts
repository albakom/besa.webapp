export interface UploadFileModel {
    file: File;
    type: 'image' | 'csv' | 'file';
    name: string;
    previewLoading?: boolean;
    imgAsString?: string;
    content?: string;
}
