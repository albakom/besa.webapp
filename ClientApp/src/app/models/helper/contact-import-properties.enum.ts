export enum ContactImportProperties {
    Surname = 1,
    Lastname = 2,
    CompanyName = 3,
    Street = 4,
    StreetNumer = 5,
    Email = 6,
    Phone = 7,
    Type = 8,
    City = 9,
    ZipCode = 10,
}
