export enum ProgressStates {
    Untouched = 1,
    InProgress = 2,
    Success = 3,
    Error = 4,
}
