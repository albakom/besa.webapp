export interface ICreateCustomerHouseConnectiontForm {
    description: string;
    civilWorkIsDoneByCustomer: boolean;
    connectioOfOtherMediaRequired: boolean;
    informSalesAfterFinish: boolean;
}
