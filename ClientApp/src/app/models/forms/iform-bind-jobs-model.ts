export interface IFormBindJobsModel {
    companyId: number;
    finishTill: string;
    name: string;
}
