export interface ICreateCustomerBuildingAndFlatForm {
    buildingId?: number;
    flatId?: number;
    isActiveConnectionRequired?: boolean;
    ownerContactId?: number;
    architectContactId?: number;
    workmanContactId?: number;
}