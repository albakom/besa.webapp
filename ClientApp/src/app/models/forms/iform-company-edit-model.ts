export interface IFormCompanyEditModel {
    name: string;
    phone: string;
    street:string;
    streetNumber: string;
    city: string;
    postalCode: string;
}
