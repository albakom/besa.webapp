import { IExplainedBooleanModel } from "../service/iexplained-boolean-model";

export interface ICreateCustomerActiveConnectiontForm {
    customerContactId?: number
    ductAmount?: number;
    subscriberEndpointLength?: number;

    subscriberEndpointNearConnectionPoint: IExplainedBooleanModel;
    activePointNearSubscriberEndpoint: IExplainedBooleanModel;
    powerForActiveEquipment: IExplainedBooleanModel;

    connectionAppointment: string;
    activationAsSoonAsPossible: boolean;
}
