import { BesaRoles } from './../service/besa-roles.enum';
export interface IFormUserEditModel {
    surname: string;
    lastname: string;
    phone: string;
    eMailAddress: string;
    role: BesaRoles;
    isManagement: boolean;
}
