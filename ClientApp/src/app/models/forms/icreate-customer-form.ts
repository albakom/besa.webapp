import { ICreateCustomerBuildingAndFlatForm } from "./icreate-customer-building-and-flat-form";
import { ICreateCustomerHouseConnectiontForm } from "./icreate-customer-house-connectiont-form";
import { ICreateCustomerActiveConnectiontForm } from "./icreate-customer-active-connectiont-form";

export interface ICreateCustomerForm {
    buildingTermValue?: string;
    searchCustomermValue?: string;
    searchOwnerValue?: string;
    searchArchitectValue?: string;
    searchWorkmanValue?: string;
    buildingAndFlat: ICreateCustomerBuildingAndFlatForm;
    houseConnection: ICreateCustomerHouseConnectiontForm,
    activeConnenction: ICreateCustomerActiveConnectiontForm,
}
