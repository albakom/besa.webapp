export interface IFormCompanyCreateModel {
    name: string;
    phone: string;
    street:string;
    streetNumber: string;
    city: string;
    postalCode: string;
}
