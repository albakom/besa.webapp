import { Subscription } from 'rxjs';
export class SubscriptionAwareComponent {

    //#region Fields

    private _subcriptions: Subscription[];

    //#endregion

    //#region Constructor

    constructor() {
        this._subcriptions = new Array();
    }

    //#endregion

    //#region Methods

    public addSubscription(...subscription: Subscription[]): void {
        this._subcriptions.push(...subscription);
    }

    public unsubcripeAll(): void {
        for (let i = 0; i < this._subcriptions.length; i++) {
            this._subcriptions[i].unsubscribe();
        }
    }

    //#endregion
}

