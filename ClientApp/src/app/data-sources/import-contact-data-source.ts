import { MatTableDataSource } from "@angular/material";

export class ImportContactDataSource<T> extends MatTableDataSource<T> {

    constructor(data: T[]) {
        super(data);
    }
}
