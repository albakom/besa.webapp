import { FuseNavigation } from '@fuse/types';

export const civilWorkItems: FuseNavigation[] =
    [{
        id: 'civil-work-menu',
        title: 'Tiefbau',
        translate: 'NAV.SAMPLE.TITLE',
        type: 'group',
        icon: 'account',
        children:
            [
                {
                    id: 'civil-work-overview',
                    title: 'Übersicht',
                    translate: 'NAV.SAMPLE.TITLE',
                    type: 'item',
                    icon: 'account',
                    url: '/member/civil-work-overview',
                }
            ],
    }
    ];

export const adminNavItems: FuseNavigation[] = [
    {
        id: 'user',
        title: 'Administration',
        translate: 'NAV.SAMPLE.TITLE',
        type: 'group',
        icon: 'account',
        children:
            [
                {
                    id: 'user-request-overview',
                    title: 'Anfragen',
                    translate: 'NAV.SAMPLE.TITLE',
                    type: 'item',
                    icon: 'account',
                    url: '/member/user-request-overview',
                },
                {
                    id: 'company-overview',
                    title: 'Firmen',
                    translate: 'NAV.SAMPLE.TITLE',
                    type: 'item',
                    icon: 'account',
                    url: '/member/company-overview',
                },
                {
                    id: 'users-overview',
                    title: 'Alle Benutzer',
                    translate: 'NAV.SAMPLE.TITLE',
                    type: 'item',
                    icon: 'account',
                    url: '/member/users-overview',
                },
                {
                    id: 'article-overview',
                    title: 'Waren',
                    translate: 'NAV.SAMPLE.TITLE',
                    type: 'item',
                    icon: 'account',
                    url: '/member/article-overview',
                },
            ],
    },
];

export const projectManagerNavItems: FuseNavigation[] = [
    {
        id: 'project-management',
        title: 'Projektmanagement',
        translate: 'NAV.SAMPLE.TITLE',
        type: 'group',
        icon: 'account',
        children: [
            {
                id: 'create_new_task',
                title: 'Aufgaben erstellen',
                translate: 'NAV.SAMPLE.TITLE',
                type: 'item',
                icon: 'account',
                url: '/member/create-new-task',
            },
            {
                id: 'task_overview',
                title: 'Aufgabenübersicht',
                translate: 'NAV.SAMPLE.TITLE',
                type: 'item',
                icon: 'account',
                url: '/member/task-overview',
            },
            {
                id: 'acknowledge_task',
                title: 'Aufgaben bestätigen',
                translate: 'NAV.SAMPLE.TITLE',
                type: 'item',
                icon: 'account',
                url: '/member/jobs-to-acknowledge',
            },
            {
                id: 'customer_request_overview',
                title: 'Kundenanfragen',
                translate: 'NAV.SAMPLE.TITLE',
                type: 'item',
                icon: 'account',
                url: '/member/customer-request-overview',
            },
            {
                id: 'construction_stage_overview',
                title: 'Bauabschnitte',
                translate: 'NAV.SAMPLE.TITLE',
                type: 'item',
                icon: 'account',
                url: '/member/construction-stage-overview',
            },
            {
                id: 'convert-jobs',
                title: 'Aufträge konvertieren',
                translate: 'NAV.SAMPLE.TITLE',
                type: 'item',
                icon: 'account',
                url: '/member/convert-jobs',
            },
            {
                id: 'files',
                title: 'Dateien',
                translate: 'NAV.SAMPLE.TITLE',
                type: 'item',
                icon: 'account',
                url: '/member/files',
            },
            {
                id: 'update-planning-data',
                title: 'Planungsdaten aktualisieren',
                translate: 'NAV.SAMPLE.TITLE',
                type: 'item',
                icon: 'account',
                url: '/member/update-planning-data',
            },
            {
                id: 'protocol',
                title: 'Protokoll',
                translate: 'NAV.SAMPLE.TITLE',
                type: 'item',
                icon: 'account',
                url: '/member/protocol',
            },
            {
                id: 'area-planning',
                title: 'Gebietsplanung',
                translate: 'NAV.SAMPLE.TITLE',
                type: 'item',
                icon: 'account',
                url: '/member/area-planning',
            }
        ]
    }
];

export const airInjectorNavItems: FuseNavigation[] = [
    {
        id: 'inject',
        title: 'Einblasen',
        translate: 'NAV.SAMPLE.TITLE',
        type: 'group',
        icon: 'account',
        children: [
            {
                id: 'construction-stage-overview-inject-jobs',
                title: 'Übersicht',
                translate: 'NAV.SAMPLE.TITLE',
                type: 'item',
                icon: 'task',
                url: '/member/inject-job-overview',
            }],
    }];

export const salesItems: FuseNavigation[] =
    [
        {
            id: 'sales',
            title: 'Vertrieb',
            translate: 'NAV.SAMPLE.TITLE',
            type: 'group',
            icon: 'sales',
            children: [
                {
                    id: ' procedure-overview',
                    title: 'Vorgänge',
                    translate: 'NAV.SAMPLE.TITLE',
                    type: 'item',
                    icon: 'task',
                    url: '/member/procedure-overview',
                },
                {
                    id: 'new-customer',
                    title: 'Neuer Kunde',
                    translate: 'NAV.SAMPLE.TITLE',
                    type: 'item',
                    icon: 'task',
                    url: '/member/create-customer',
                },
                {
                    id: 'contacts',
                    title: 'Kontakte',
                    translate: 'NAV.SAMPLE.TITLE',
                    type: 'item',
                    icon: 'account',
                    url: '/member/contact-overview',
                },
                {
                    id: 'import-contacts',
                    title: 'Kontakte importieren',
                    translate: 'NAV.SAMPLE.TITLE',
                    type: 'item',
                    icon: 'account',
                    url: '/member/contact-import',
                },
                {
                    id: 'procedure-imports',
                    title: 'Vorgänge importieren',
                    translate: 'NAV.SAMPLE.TITLE',
                    type: 'item',
                    icon: 'account',
                    url: '/member/procedure-import',
                },
                {
                    id: 'contact-after-sales-job',
                    title: 'Kunden...',
                    type: 'item',
                    icon: 'account',
                    url: '/member/contact-after-sales-job-overview',
                }
            ]
        }
    ];

export const inventoryItems: FuseNavigation[] =
    [
        {
            id: 'inventory',
            title: 'Lager',
            translate: 'NAV.SAMPLE.TITLE',
            type: 'group',
            icon: 'inventory',
            children: [
                {
                    id: ' inventory-overview',
                    title: 'Übersicht',
                    translate: 'NAV.SAMPLE.TITLE',
                    type: 'item',
                    icon: 'task',
                    url: '/member/inventory-overview',
                }
            ]
        }
    ];


export const spliceItems: FuseNavigation[] =
    [
        {
            id: 'splice',
            title: 'Spleiße',
            translate: 'NAV.SAMPLE.TITLE',
            type: 'group',
            icon: 'inventory',
            children: [
                {
                    id: 'splice-customer-overview',
                    title: 'Übersicht Kunden',
                    translate: 'NAV.SAMPLE.TITLE',
                    type: 'item',
                    icon: 'task',
                    url: '/member/splice-customer-overview',
                },
                {
                    id: 'splice-customer-overview',
                    title: 'Übersicht KVZ',
                    translate: 'NAV.SAMPLE.TITLE',
                    type: 'item',
                    icon: 'task',
                    url: '/member/prepare-branchable-overview',
                },
                {
                    id: 'splice-odf-overview',
                    title: 'Übersicht PoP',
                    translate: 'NAV.SAMPLE.TITLE',
                    type: 'item',
                    icon: 'task',
                    url: '/member/splice-odf-overview',
                }
            ]
        }
    ];

    export const activeNetworkItems: FuseNavigation[] =
    [
        {
            id: 'active-network',
            title: 'Anschlüsse',
            translate: 'NAV.SAMPLE.TITLE',
            type: 'group',
            icon: 'inventory',
            children: [
                {
                    id: 'activation-job-overview',
                    title: 'Übersicht',
                    translate: 'NAV.SAMPLE.TITLE',
                    type: 'item',
                    icon: 'task',
                    url: '/member/activation-job-overview',
                },
            ]
        }
    ];


    export const generellMenuItems: FuseNavigation[] =
    [
        {
            id: 'applications',
            title: 'Allgemein',
            translate: 'NAV.APPLICATIONS',
            type: 'group',
            children: [
                {
                    id: 'dashboard',
                    title: 'Meine Aufgaben',
                    translate: 'NAV.SAMPLE.TITLE',
                    type: 'item',
                    icon: 'account',
                    url: '/member/dashboard',
                },
                {
                    id: 'messages',
                    title: 'Nachrichten',
                    translate:'NAV.SAMPLE.TITLE',
                    type: 'item',
                    icon: 'account',
                    url: '/member/messages',
                }
            ]
        }
    ];
