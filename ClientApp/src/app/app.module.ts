import { MailDetailsComponent } from './pages/messages-page/mail-details/mail-details.component';
import { UpdateTaskService } from './services/update-task/update-task.service';
import { ActivationJobOverviewPageComponent } from './pages/activation-job-overview-page/activation-job-overview-page.component';
import { AgmCoreModule } from '@agm/core';
import { FilesService } from './services/files/files.service';
import { ArticleOverviewPageComponent } from './pages/article-overview-page/article-overview-page.component';
import { UserOverviewPageComponent } from './pages/user-overview-page/user-overview-page.component';
import { JobService } from './services/job.service';
import { HubService } from './services/hubs/hub.service';
import { CanNavigateToAdminAreaGuard } from './routing-guards/can-navigate-to-admin-area.guard';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LocalStorageModule } from 'angular-2-local-storage';
import { MatMomentDateModule } from '@angular/material-moment-adapter';

import { AppComponent } from './app.component';
import { OAuthModule } from 'angular-oauth2-oidc';
import { FuseModule } from '@fuse/fuse.module';
import { FuseSharedModule } from '@fuse/shared.module';
import { FuseSidebarModule, FuseThemeOptionsModule } from '@fuse/components';

import { fuseConfig } from './fuse-config';


import { HomePageComponent } from './pages/home-page/home-page.component';
import { DashboardPageComponent } from './pages/dashboard-page/dashboard-page.component';
import { AppRoutingModule } from './app-routing.module';
import { LoginPageComponent } from './pages/login-page/login-page.component';
import { ErrorPageComponent } from './pages/error-page/error-page.component';
import { ErrorService } from './services/error.service';
import { LoginService } from './services/login.service';
import { UriService } from './services/uri.service';
import { NotFoundComponent } from './pages/not-found/not-found.component';
import { CanNavigateToMemberAreaGuard } from './routing-guards/can-navigate-to-member-area.guard';
import { AnotherPageComponent } from './pages/another-page/another-page.component';
import { WebServiceClientService } from './services/web-service-client.service';
import { DashboardService } from './services/dashboard.service';
import { RefreshManagerService } from './services/refresh-manager.service';
import { LogoutPageComponent } from './pages/logout-page/logout-page.component';
import { UserPageComponent } from './pages/user-page/user-page.component';
import { AccessDeniedComponent } from './pages/access-denied/access-denied.component';
import { RequestAcessPageComponent } from './pages/request-acess-page/request-acess-page.component';
import { RequestAcessOutstandingPageComponent } from './pages/request-acess-outstanding-page/request-acess-outstanding-page.component';
import { RequestAccessService } from './services/request-access.service';
import { UserAccessRequestOverviewPageComponent } from './pages/user-access-request-overview-page/user-access-request-overview-page.component';
import { GrantUserAccessPageComponent } from './pages/grant-user-access-page/grant-user-access-page.component';
import { BesaRoleToNamePipe } from './pipes/besa-role-to-name.pipe';
import { RequestAccessHubService } from './services/hubs/request-access-hub.service';
import { CompanyOverviewPageComponent } from './pages/company-overview-page/company-overview-page.component';
import { ConstructionStageOverviewPageComponent } from './pages/construction-stage-overview-page/construction-stage-overview-page.component';
import { ConstructionStageService } from './services/construction-stage.service';
import { CanNavigateToProjectManangerAreaGuard } from './routing-guards/can-navigate-to-project-mananger-area.guard';
import { CreateConstructionStageComponent } from './pages/create-construction-stage/create-construction-stage.component';
import { CompanyService } from './services/companies/company.service';
import { SimpleDialogComponent } from './dialogs/simple-dialog/simple-dialog.component';
import { CompanyEditPageComponent } from './pages/company-edit-page/company-edit-page.component';
import { CompanyCreatePageComponent } from './pages/company-create-page/company-create-page.component';
import { CreateNewTaskPageComponent } from './pages/create-new-task-page/create-new-task-page.component';
import { CilvilWorkOverviewPageComponent } from './pages/cilvil-work-overview-page/cilvil-work-overview-page.component';
import { CilvilWorkDetailPageComponent } from './pages/cilvil-work-detail-page/cilvil-work-detail-page.component';
import { BindTaskOverviewPageComponent } from './pages/bind-task-overview-page/bind-task-overview-page.component';
import { JobTypeToNamePipe } from './pipes/job-type-to-name.pipe';
import { DaysLeftPipe } from './pipes/days-left.pipe';
import { BranchOffJobDetailsPageComponent } from './pages/branch-off-job-details-page/branch-off-job-details-page.component';
import { ToColorPipe } from './pipes/to-color.pipe';

import { CollectionJobDetailsPageComponent } from './pages/collection-job-details-page/collection-job-details-page.component';
import { UserService } from './services/user/user.service';
import { UserEditPageComponent } from './pages/user-edit-page/user-edit-page.component';
import { ArticleEditPageComponent } from './pages/article-edit-page/article-edit-page.component';
import { ArticleService } from './services/article/article.service';
import { ArticleCreatePageComponent } from './pages/article-create-page/article-create-page.component';
import { DaysLeftToColorPipe } from './pipes/days-left-to-color.pipe';
import { CreateCustomerPageComponent } from './pages/create-customer-page/create-customer-page.component';
import { CanNavigateToSalesAreaGuard } from './routing-guards/can-navigate-to-sales-area.guard';
import { CreateFlatDialogComponent } from './dialogs/create-flat-dialog/create-flat-dialog.component';
import { CreateContactDialogComponent } from './dialogs/create-contact-dialog/create-contact-dialog.component';
import { CustomerService } from './services/customer.service';
import { PersonTypeToNamePipe } from './pipes/person-type-to-name.pipe';
import { UploadComponent } from './pages/upload/upload.component';
import { UploadService } from './services/upload/upload.service';
import { FileSizeToUnitPipe } from './pipes/file-size-to-unit.pipe';
import { FinishBranchOffJobPageComponent } from './pages/finish-branch-off-job-page/finish-branch-off-job-page.component';
import { TakePicturesDialogComponent } from './dialogs/take-pictures-dialog/take-pictures-dialog.component';
import { InputFileComponent } from './components/input-file/input-file.component';
import { ShowPictureDialogComponent } from './dialogs/show-picture-dialog/show-picture-dialog.component';
import { FilesComponent } from './pages/files/files.component';
import { FileTypeToIconPipe } from './pipes/file-type-to-icon.pipe';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { FileDetailComponent } from './pages/file-detail/file-detail.component';
import { JobStateToNamePipe } from './pipes/job-state-to-name.pipe';
import { JobsToAcknowledgePageComponent } from './pages/jobs-to-acknowledge-page/jobs-to-acknowledge-page.component';
import { FileTypeIdtoStringPipe } from './pipes/file-type-idto-string.pipe';
import { CustomerRequestOverviewPageComponent } from './pages/customer-request-overview-page/customer-request-overview-page.component';
import { CustomerRequestDetailsPageComponent } from './pages/customer-request-details-page/customer-request-details-page.component';
import { ConnectBuildingJobDetailsPageComponent } from './pages/connect-building-job-details-page/connect-building-job-details-page.component';
import { ContactOverviewPageComponent } from './pages/contact-overview-page/contact-overview-page.component';
import { PersonInfoShortViewComponent } from './components/person-info-short-view/person-info-short-view.component';
import { ChangeLogPageComponent } from './pages/change-log-page/change-log-page.component';
import { DocumentationPageComponent } from './pages/documentation-page/documentation-page.component';
import { FinishConnectBuildingJobPageComponent } from './pages/finish-connect-building-job-page/finish-connect-building-job-page.component';
import { OAuthResourceServerErrorHandler } from 'angular-oauth2-oidc';
import { RedirectToLoginPageResourceServerErrorHandler } from './services/redirect-to-login-page-resource-server-error-handler';
import { InjectJobOverviewPageComponent } from './pages/inject-job-overview-page/inject-job-overview-page.component';
import { InjectConstructionStageDetailPageComponent } from './pages/inject-construction-stage-detail-page/inject-construction-stage-detail-page.component';
import { InjectJobDetailsPageComponent } from './pages/inject-job-details-page/inject-job-details-page.component';
import { ContactEditPageComponent } from './pages/contact-edit-page/contact-edit-page.component';
import { GenderTypeIdToStringPipe } from './pipes/gender-type-id-to-string.pipe';
import { ContactDetailPageComponent } from './pages/contact-detail-page/contact-detail-page.component';
import { FinishInjectJobPageComponent } from './pages/finish-inject-job-page/finish-inject-job-page.component';
import { CreateConnectBuildingTaskComponent } from './components/create-connect-building-task/create-connect-building-task.component';
import { CreateInjectTaskComponent } from './components/create-inject-task/create-inject-task.component';
import { BindInjectJobsComponent } from './components/bind-inject-jobs/bind-inject-jobs.component';
import { BindConnectionJobsComponent } from './components/bind-connection-jobs/bind-connection-jobs.component';
import { UploadFileDialogComponent } from './dialogs/upload-file-dialog/upload-file-dialog.component';
import { FileAccessObjectTypeToNamePipe } from './pipes/file-access-object-type-to-name.pipe';
import { ConnenctionJobTypeToNamePipe } from './pipes/connenction-job-type-to-name.pipe';
import { ImportContactsPageComponent } from './pages/import-contacts-page/import-contacts-page.component';
import { AmountToArrayPipe } from './pipes/amount-to-array.pipe';
import { PersonPropertyToNamePipe } from './pipes/person-property-to-name.pipe';
import { CompanyDetailPageComponent } from './pages/company-detail-page/company-detail-page.component';
import { UserDetailPageComponent } from './pages/user-detail-page/user-detail-page.component';
import { SpliceBranchableJobOverviewPageComponent } from './pages/splice-branchable-job-overview-page/splice-branchable-job-overview-page.component';
import { SpliceBranchableJobDetailPageComponent } from './pages/splice-branchable-job-detail-page/splice-branchable-job-detail-page.component';
import { SpliceOdfjobDetailPageComponent } from './pages/splice-odfjob-detail-page/splice-odfjob-detail-page.component';
import { SpliceOdfjobOverviewPageComponent } from './pages/splice-odfjob-overview-page/splice-odfjob-overview-page.component';
import { PrepareBranchableForSpliceJobDetailPageComponent } from './pages/prepare-branchable-for-splice-job-detail-page/prepare-branchable-for-splice-job-detail-page.component';
import { PrepareBranchableForSpliceJobOverviewPageComponent } from './pages/prepare-branchable-for-splice-job-overview-page/prepare-branchable-for-splice-job-overview-page.component';
import { CreatePrepareBranchableForSpliceTaskComponent } from './components/create-prepare-branchable-for-splice-task/create-prepare-branchable-for-splice-task.component';
import { CreateSpliceCustomerTaskComponent } from './components/create-splice-customer-task/create-splice-customer-task.component';
import { ImportProceduresPageComponent } from './pages/import-procedures-page/import-procedures-page.component';
import { ImportService } from './services/import.service';
import { ProcedurePropertToNamePipe } from './pipes/procedure-propert-to-name.pipe';
import { ProcedureOverviewPageComponent } from './pages/procedure-overview-page/procedure-overview-page.component';
import { ProcedureService } from './services/procedure.service';
import { ConnectBuildingProcedureDetailPageComponent } from './pages/connect-building-procedure-detail-page/connect-building-procedure-detail-page.component';
import { ConnectCustomerProcedureDetailPageComponent } from './pages/connect-customer-procedure-detail-page/connect-customer-procedure-detail-page.component';
import { CreateInventoryTaskComponent } from './components/create-inventory-task/create-inventory-task.component';
import { ProcedureStateToColorPipe } from './pipes/procedure-state-to-color.pipe';
import { ProcedureStateToNamePipe } from './pipes/procedure-state-to-name.pipe';
import { ProcedureTypeToNamePipe } from './pipes/procedure-type-to-name.pipe';
import { CreateSpliceODFTaskComponent } from './components/create-splice-odf-task/create-splice-odf-task.component';
import { CreateActivationTaskComponent } from './components/create-activation-task/create-activation-task.component';
import { BindSpliceCustomerJobsComponent } from './components/bind-splice-customer-jobs/bind-splice-customer-jobs.component';
import { BindPrepareBranchableJobsComponent } from './components/bind-prepare-branchable-jobs/bind-prepare-branchable-jobs.component';
import { BindSpliceODFJobsComponent } from './components/bind-splice-odf-jobs/bind-splice-odf-jobs.component';
import { BindActivationJobsComponent } from './components/bind-activation-jobs/bind-activation-jobs.component';
import { ActivationJobIpAddressRequirementsToNamePipe } from './pipes/activation-job-ip-address-requirements-to-name.pipe';
import { InventoryOverviewPageComponent } from './pages/inventory-overview-page/inventory-overview-page.component';
import { CanNavigateToInventoryAreaGuard } from './routing-guards/can-navigate-to-inventory-area.guard';
import { ProcedureFilterOperatorToNamePipe } from './pipes/procedure-filter-operator-to-name.pipe';
import { ProcedureSortingDirectionsToNamePipe } from './pipes/procedure-sorting-directions-to-name.pipe';
import { IprocedureSortPropertiesToNamePipe } from './pipes/iprocedure-sort-properties-to-name.pipe';
import { InventoryDetailPageComponent } from './pages/inventory-detail-page/inventory-detail-page.component';
import { FinishInventoryJobDialogComponent } from './dialogs/finish-inventory-job-dialog/finish-inventory-job-dialog.component';
import { CanNavigateToSpliceAreaGuardGuard } from './routing-guards/can-navigate-to-splice-area-guard.guard';
import { SpliceBranchableConstructionStageDetailPageComponent } from './pages/splice-branchable-construction-stage-detail-page/splice-branchable-construction-stage-detail-page.component';
import { PrepareBranchableConstructionStageDetailPageComponent } from './pages/prepare-branchable-construction-stage-detail-page/prepare-branchable-construction-stage-detail-page.component';
import { CanNavigateToActiveNetworkAreaGuard } from './routing-guards/can-navigate-to-active-network-area-guard.guard';
import { ActivationJobDetailsPageComponent } from './pages/activation-job-details-page/activation-job-details-page.component';
import { FinishActivationJobPageComponent } from './pages/finish-activation-job-page/finish-activation-job-page.component';
import { FinishSpliceBranchableJobPageComponent } from './pages/finish-splice-branchable-job-page/finish-splice-branchable-job-page.component';
import { FinishPrepareBranchableJobPageComponent } from './pages/finish-prepare-branchable-job-page/finish-prepare-branchable-job-page.component';
import { FinishSpliceOdfjobPageComponent } from './pages/finish-splice-odfjob-page/finish-splice-odfjob-page.component';
import { UpdatePlanningDataComponent } from './pages/update-planning-data/update-planning-data.component';
import { NumberToBranchableTypePipe } from './pipes/number-to-branchable-type.pipe';
import { FinishJobAdministrativeDialogComponent } from './dialogs/finish-job-administrative-dialog/finish-job-administrative-dialog.component';
import { ConvertJobPageComponent } from './pages/convert-job-page/convert-job-page.component';
import { ConvertBranchOffJobToConnectBuildingJobComponent } from './components/convert-branch-off-job-to-connect-building-job/convert-branch-off-job-to-connect-building-job.component';
import { ConstructionStageUpdatePageComponent } from './pages/construction-stage-update-page/construction-stage-update-page.component';
import { TaskOverviewPageComponent } from './pages/task-overview-page/task-overview-page.component';
import { JobCollectionSortPropertiesToNamePipe } from './pipes/job-collection-sort-properties-to-name.pipe';
import { ProtocolPageComponent } from './pages/protocol-page/protocol-page.component';
import { ProtocolActionToStringPipe } from './pipes/protocol-action-to-string.pipe';
import { MessageActionToStringPipe } from './pipes/message-action-to-string.pipe';

import { LayoutModule } from './layout/layout.module';
import { MaterialModule } from '@fuse/material.module';

import { InMemoryWebApiModule } from 'angular-in-memory-web-api';
import { FakeDbService } from './fake-db/fake-db.service';
import { ActionTypeToStringPipe } from './pipes/action-type-to-string.pipe';
import { SpliceBranchableOverviewComponent } from './components/splice-branchable-overview/splice-branchable-overview.component';
import { MarkedAsReadPipe } from './pipes/marked-as-read.pipe';
import { WriteCSVFileDialogComponent } from './dialogs/write-csvfile-dialog/write-csvfile-dialog.component';
import { ContactAfterSalesJobOverviewPageComponent } from './pages/contact-after-sales-job-overview-page/contact-after-sales-job-overview-page.component';
import { ContactAfterSalesJobDetailPageComponent } from './pages/contact-after-sales-job-detail-page/contact-after-sales-job-detail-page.component';
import { CustomerInterestedDialogComponent } from './dialogs/customer-interested-dialog/customer-interested-dialog.component';
import { CustomerNotInterestedDialogComponent } from './dialogs/customer-not-interested-dialog/customer-not-interested-dialog.component';
import { UpdateTasksToStringPipe } from './pipes/update-tasks-to-string.pipe';
import { UpdatePlanningDataDetailsPageComponent } from './pages/update-planning-data-details-page/update-planning-data-details-page.component';
import { UpdateTaskElementStateToStringPipe } from './pipes/update-task-element-state-to-string.pipe';
import { SelectUpdateTaskObjectsDialogComponent } from './dialogs/select-update-task-objects-dialog/select-update-task-objects-dialog.component';
import { AreaPlanningPageComponent } from './pages/area-planning-page/area-planning-page.component';
import { AreaPlanningService } from './services/area-planning/area-planning.service';
import { CreateAreaItemDialogComponent } from './dialogs/create-area-item-dialog/create-area-item-dialog.component';
import { AreaItemToStringPipe } from './pipes/area-item-to-string.pipe';
import { CommentDialogComponent } from './dialogs/comment-dialog/comment-dialog.component';
import { AreaPlanningBuildingDetailPageComponent } from './pages/area-planning-building-detail-page/area-planning-building-detail-page.component';
import { AreaPlanningEmptySpaceDetailPageComponent } from './pages/area-planning-empty-space-detail-page/area-planning-empty-space-detail-page.component';
import { EditAreaItemDialogComponent } from './dialogs/edit-area-item-dialog/edit-area-item-dialog.component';
import { CreateAreaDialogComponent } from './dialogs/create-area-dialog/create-area-dialog.component';
import { AreaIdToStringPipe } from './pipes/area-id-to-string.pipe';
import { MessageRelatedObjectTypeToNamePipe } from './pipes/message-related-object-type-to-name.pipe';
import { MailComponent } from './pages/messages-page/mail.component';
import { MailListComponent } from './pages/messages-page/mail-list/mail-list.component';
import { MailListItemComponent } from './pages/messages-page/mail-list/mail-list-item/mail-list-item.component';
import { MailMainSidebarComponent } from './pages/messages-page/sidebars/main/main-sidebar.component';
import { MessageActionCollectionJobDetailsComponent } from './pages/messages-page/mail-details/message-action-collection-job-details/message-action-collection-job-details.component';
import { MessageActionEmployesBoundDetailsComponent } from './pages/messages-page/mail-details/message-action-employes-bound-details/message-action-employes-bound-details.component';
import { MessageActionJobAcknowledgedDetailsComponent } from './pages/messages-page/mail-details/message-action-job-acknowledged-details/message-action-job-acknowledged-details.component';
import { MessageActionJobBoundDetailsComponent } from './pages/messages-page/mail-details/message-action-job-bound-details/message-action-job-bound-details.component';
import { MessageActionJobDiscardDetailsComponent } from './pages/messages-page/mail-details/message-action-job-discard-details/message-action-job-discard-details.component';
import { MessageActionJobFinishedDetailsComponent } from './pages/messages-page/mail-details/message-action-job-finished-details/message-action-job-finished-details.component';
import { MessageActionJobFinishJobAdministrativeDetailComponent } from './pages/messages-page/mail-details/message-action-job-finish-job-administrative-detail/message-action-job-finish-job-administrative-detail.component';
import { MessageActionProcedureFinishedDetailsComponent } from './pages/messages-page/mail-details/message-action-procedure-finished-details/message-action-procedure-finished-details.component';
import { MessageActionRemoveMemberDetailsComponent } from './pages/messages-page/mail-details/message-action-remove-member-details/message-action-remove-member-details.component';
import { MessageActionUpdateFromAdapterDetailsComponent } from './pages/messages-page/mail-details/message-action-update-from-adapter-details/message-action-update-from-adapter-details.component';
import { MessageActionDetailsComponent } from './pages/messages-page/mail-details/message-action-details/message-action-details.component';
import { SimpleInputDialogComponent } from './dialogs/simple-input-dialog/simple-input-dialog.component';


@NgModule({
  entryComponents: [
    SimpleDialogComponent,
    CreateFlatDialogComponent,
    CreateContactDialogComponent,
    TakePicturesDialogComponent,
    ShowPictureDialogComponent,
    UploadFileDialogComponent,
    FinishInventoryJobDialogComponent,
    FinishJobAdministrativeDialogComponent,
    WriteCSVFileDialogComponent,
    CustomerInterestedDialogComponent,
    CustomerNotInterestedDialogComponent,
    SelectUpdateTaskObjectsDialogComponent,
    CreateAreaItemDialogComponent,
    CommentDialogComponent,
    EditAreaItemDialogComponent,
    CreateAreaDialogComponent,
    SimpleInputDialogComponent
  ],
  declarations: [
    AppComponent,
    HomePageComponent,
    DashboardPageComponent,
    LoginPageComponent,
    ErrorPageComponent,
    NotFoundComponent,
    AnotherPageComponent,
    LogoutPageComponent,
    UserPageComponent,
    AccessDeniedComponent,
    RequestAcessPageComponent,
    RequestAcessOutstandingPageComponent,
    UserAccessRequestOverviewPageComponent,
    GrantUserAccessPageComponent,
    BesaRoleToNamePipe,
    CompanyOverviewPageComponent,
    ConstructionStageOverviewPageComponent,
    CreateConstructionStageComponent,
    SimpleDialogComponent,
    CompanyEditPageComponent,
    CompanyCreatePageComponent,
    CreateNewTaskPageComponent,
    CilvilWorkOverviewPageComponent,
    CilvilWorkDetailPageComponent,
    BindTaskOverviewPageComponent,
    JobTypeToNamePipe,
    DaysLeftPipe,
    BranchOffJobDetailsPageComponent,
    ToColorPipe,
    CollectionJobDetailsPageComponent,
    UserOverviewPageComponent,
    UserEditPageComponent,
    ArticleOverviewPageComponent,
    ArticleEditPageComponent,
    ArticleCreatePageComponent,
    DaysLeftToColorPipe,
    CreateCustomerPageComponent,
    CreateFlatDialogComponent,
    CreateContactDialogComponent,
    PersonTypeToNamePipe,
    UploadComponent,
    FileSizeToUnitPipe,
    FinishBranchOffJobPageComponent,
    TakePicturesDialogComponent,
    InputFileComponent,
    ShowPictureDialogComponent,
    FilesComponent,
    FileTypeToIconPipe,
    FileDetailComponent,
    JobStateToNamePipe,
    JobsToAcknowledgePageComponent,
    FileTypeIdtoStringPipe,
    CustomerRequestOverviewPageComponent,
    CustomerRequestDetailsPageComponent,
    ConnectBuildingJobDetailsPageComponent,
    ContactOverviewPageComponent,
    PersonInfoShortViewComponent,
    ChangeLogPageComponent,
    DocumentationPageComponent,
    FinishConnectBuildingJobPageComponent,
    InjectJobOverviewPageComponent,
    InjectConstructionStageDetailPageComponent,
    InjectJobDetailsPageComponent,
    ContactEditPageComponent,
    GenderTypeIdToStringPipe,
    ContactDetailPageComponent,
    FinishInjectJobPageComponent,
    CreateConnectBuildingTaskComponent,
    CreateInjectTaskComponent,
    BindInjectJobsComponent,
    BindConnectionJobsComponent,
    UploadFileDialogComponent,
    FileAccessObjectTypeToNamePipe,
    ConnenctionJobTypeToNamePipe,
    ImportContactsPageComponent,
    AmountToArrayPipe,
    PersonPropertyToNamePipe,
    CompanyDetailPageComponent,
    UserDetailPageComponent,
    SpliceBranchableJobOverviewPageComponent,
    SpliceBranchableJobDetailPageComponent,
    SpliceOdfjobDetailPageComponent,
    SpliceOdfjobOverviewPageComponent,
    PrepareBranchableForSpliceJobDetailPageComponent,
    PrepareBranchableForSpliceJobOverviewPageComponent,
    CreatePrepareBranchableForSpliceTaskComponent,
    CreateSpliceCustomerTaskComponent,
    ImportProceduresPageComponent,
    ProcedurePropertToNamePipe,
    ProcedureOverviewPageComponent,
    ConnectBuildingProcedureDetailPageComponent,
    ConnectCustomerProcedureDetailPageComponent,
    CreateInventoryTaskComponent,
    ProcedureStateToColorPipe,
    ProcedureStateToNamePipe,
    ProcedureTypeToNamePipe,
    CreateSpliceODFTaskComponent,
    CreateActivationTaskComponent,
    BindSpliceCustomerJobsComponent,
    BindPrepareBranchableJobsComponent,
    BindSpliceODFJobsComponent,
    BindActivationJobsComponent,
    ActivationJobIpAddressRequirementsToNamePipe,
    InventoryOverviewPageComponent,
    ActivationJobOverviewPageComponent,
    ProcedureFilterOperatorToNamePipe,
    ProcedureSortingDirectionsToNamePipe,
    IprocedureSortPropertiesToNamePipe,
    InventoryDetailPageComponent,
    FinishInventoryJobDialogComponent,
    SpliceBranchableConstructionStageDetailPageComponent,
    PrepareBranchableConstructionStageDetailPageComponent,
    ActivationJobDetailsPageComponent,
    FinishActivationJobPageComponent,
    FinishSpliceBranchableJobPageComponent,
    FinishPrepareBranchableJobPageComponent,
    FinishSpliceOdfjobPageComponent,
    UpdatePlanningDataComponent,
    NumberToBranchableTypePipe,
    FinishJobAdministrativeDialogComponent,
    ConvertJobPageComponent,
    ConvertBranchOffJobToConnectBuildingJobComponent,
    ConstructionStageUpdatePageComponent,
    TaskOverviewPageComponent,
    JobCollectionSortPropertiesToNamePipe,
    ProtocolPageComponent,
    ProtocolActionToStringPipe,
    MessageActionToStringPipe,
    SpliceBranchableOverviewComponent,
    WriteCSVFileDialogComponent,
    ContactAfterSalesJobOverviewPageComponent,
    ContactAfterSalesJobDetailPageComponent,
    CustomerInterestedDialogComponent,
    CustomerNotInterestedDialogComponent,
    UpdateTasksToStringPipe,
    UpdatePlanningDataDetailsPageComponent,
    UpdateTaskElementStateToStringPipe,
    SelectUpdateTaskObjectsDialogComponent,
    AreaPlanningPageComponent,
    CreateAreaItemDialogComponent,
    AreaItemToStringPipe,
    CommentDialogComponent,
    AreaPlanningBuildingDetailPageComponent,
    AreaPlanningEmptySpaceDetailPageComponent,
    EditAreaItemDialogComponent,
    CreateAreaDialogComponent,
    AreaIdToStringPipe,
    MessageRelatedObjectTypeToNamePipe,
    MailComponent,
    MailListComponent,
    MailListItemComponent,
    MailDetailsComponent,
    MailMainSidebarComponent,
    MarkedAsReadPipe,
    MessageActionCollectionJobDetailsComponent,
    MessageActionEmployesBoundDetailsComponent,
    MessageActionJobAcknowledgedDetailsComponent,
    MessageActionJobBoundDetailsComponent,
    MessageActionJobDiscardDetailsComponent,
    MessageActionJobFinishedDetailsComponent,
    MessageActionJobFinishJobAdministrativeDetailComponent,
    MessageActionProcedureFinishedDetailsComponent,
    MessageActionRemoveMemberDetailsComponent,
    MessageActionUpdateFromAdapterDetailsComponent,
    MessageActionDetailsComponent,
    SimpleInputDialogComponent,
  ],
  imports: [
    AppRoutingModule,
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpClientModule,
    FormsModule,
    BrowserAnimationsModule,
    // Fuse modules
    FuseModule.forRoot(fuseConfig),
    FuseSharedModule,
    FuseSidebarModule,
    FuseThemeOptionsModule,

    // Material Design
    MaterialModule,

    //Only Temp
    InMemoryWebApiModule.forRoot(FakeDbService, {
      delay: 0,
      passThruUnknownUrl: true
    }),

    // App modules
    LayoutModule,
    MatMomentDateModule,
    InfiniteScrollModule,
    OAuthModule.forRoot({
      resourceServer: {
        allowedUrls: [],
        sendAccessToken: true,
      }
    }),
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyCeC-LfVUDsNm9bvkq4_mOA0Wicx2mNcgE'
    }),
    LocalStorageModule.withConfig(
      {
        prefix: 'besa-web-app',
        storageType: 'localStorage',
      }),
  ],
  providers: [
    RequestAccessService,
    ErrorService,
    LoginService,
    UriService,
    WebServiceClientService,
    DashboardService,
    RefreshManagerService,
    CanNavigateToAdminAreaGuard,
    CanNavigateToMemberAreaGuard,
    CanNavigateToInventoryAreaGuard,
    CanNavigateToProjectManangerAreaGuard,
    CanNavigateToSalesAreaGuard,
    CanNavigateToSpliceAreaGuardGuard,
    CanNavigateToActiveNetworkAreaGuard,
    HubService,
    RequestAccessHubService,
    ConstructionStageService,
    CompanyService,
    JobService,
    UserService,
    ArticleService,
    CustomerService,
    UploadService,
    FilesService,
    ImportService,
    UpdateTaskService,
    AreaPlanningService,
    ProcedureService,
    { provide: OAuthResourceServerErrorHandler, useClass: RedirectToLoginPageResourceServerErrorHandler },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
